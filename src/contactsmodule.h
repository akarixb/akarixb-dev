/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef CONTACTSMODULE_H
#define CONTACTSMODULE_H

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QListWidget>
#include <QInputDialog>
#include <QMessageBox>
#include <QGroupBox>

#include <QXmppClient.h>
#include <QXmppRosterManager.h>

#include "globalobject.h"
#include "helpers.h"


class ContactsModule : public QWidget
{
    Q_OBJECT
public:
    explicit ContactsModule(GlobalObject *globalObject,
                            QWidget *parent = nullptr);
    ~ContactsModule();

    bool isUserOnline(QString bareJid);
    QListWidgetItem *createContactItem(QString bareJid);
    void setContactItemText(QListWidgetItem *item, QString name, QString jid);

    void toggleButtons(bool state);


signals:


public slots:
    void onRosterReceived();
    void onServerDisconnected();
    void onPresenceChanged(QString bareJid, QString resource);
    void onSubscriptionReceived(QString bareJid);
    void onSubscriptionAccepted();
    void onSubscriptionRefused();

    void onContactSelected(int row);
    void onContactDoubleclicked(QListWidgetItem *item);

    void onStartChat();
    void onAddContact();
    void onRenameContact();
    void onRemoveContact();

    void onRosterItemAdded(QString bareJid);
    void onRosterItemChanged(QString bareJid);
    void onRosterItemRemoved(QString bareJid);


private:
    QVBoxLayout *m_mainLayout;
    QHBoxLayout *m_topLayout;
    QVBoxLayout *m_buttonsLayout;

    QListWidget *m_rosterListWidget;

    QPushButton *m_startChatButton;
    QPushButton *m_addContactButton;
    QPushButton *m_renameContactButton;
    QPushButton *m_removeContactButton;


    QGroupBox *m_requestsGroupBox;
    QHBoxLayout *m_requestsLayout;
    QListWidget *m_requestsListWidget;
    QPushButton *m_acceptRequestButton;
    QPushButton *m_refuseRequestButton;


    QXmppClient *m_xmppClient;
    QXmppRosterManager *m_rosterManager;

    GlobalObject *m_globalObject;
};

#endif // CONTACTSMODULE_H
