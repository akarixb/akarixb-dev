/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */


#include "identitymodule.h"


IdentityModule::IdentityModule(GlobalObject *globalObject,
                               QWidget *parent) : QWidget(parent)
{
    m_globalObject = globalObject;

    m_dataFile = new DataFile(m_globalObject->getDataDirectory()
                              + QStringLiteral("/identity.axb"),
                              this);


    m_nickLineEdit = new QLineEdit(this); // Multinicks? -- FIXME

    // Add formal name field? Last name fields?

    m_dobDateEdit = new QDateEdit(this);
    m_dobDateEdit->setCalendarPopup(true);
    m_dobDateEdit->calendarWidget()->setGridVisible(true);
    m_dobDateEdit->setDisplayFormat("dd MMMM yyyy");


    m_saveButton = new QPushButton(QIcon::fromTheme("document-save",
                                                    QIcon(":/images/button-save.png")),
                                   tr("&Save"),
                                   this);
    connect(m_saveButton, &QAbstractButton::clicked,
            this, &IdentityModule::saveIdentity);


    m_formLayout = new QFormLayout();
    m_formLayout->addRow(tr("Nickname"),       m_nickLineEdit);
    m_formLayout->addRow(tr("Date of Birth"),  m_dobDateEdit);

    m_mainLayout = new QVBoxLayout();
    m_mainLayout->addLayout(m_formLayout);
    m_mainLayout->addStretch(1);
    m_mainLayout->addWidget(m_saveButton, 0, Qt::AlignRight);
    this->setLayout(m_mainLayout);


    loadIdentity();

    qDebug() << "IdentityModule created";
}

IdentityModule::~IdentityModule()
{
    qDebug() << "IdentityModule destroyed";
}



void IdentityModule::loadIdentity()
{
    const QVariantList identityDataList = m_dataFile->loadData();

    QVariantMap identityMap;
    if (!identityDataList.isEmpty())
    {
        identityMap = identityDataList.first().toMap();
    }

    QString nickname = identityMap.value("nickname").toString();
    if (nickname.isEmpty())
    {
        nickname = QStringLiteral("Akari"); // Default
    }
    m_nickLineEdit->setText(nickname);
    m_globalObject->setOwnNickname(nickname);

    const QDate dateOfBirth = identityMap.value("dateOfBirth").toDate();
    if (dateOfBirth.isValid())
    {
        m_dobDateEdit->setDate(dateOfBirth);
    }
    else
    {
        m_dobDateEdit->setDate(QDate::currentDate());
    }


    // Load other values, likes list, dislikes list, etc


    if (!identityDataList.isEmpty())
    {
        m_globalObject->addToLog(tr("Loaded identity data for %1.",
                                    "%1 is a nickname")
                                 .arg("<b>" + nickname + "</b>"));
    }
    else
    {
        m_globalObject->addToLog(tr("Identity data has not been defined yet."),
                                 true,
                                 GlobalObject::LogBad);
    }
}


//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// SLOTS //////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


void IdentityModule::saveIdentity()
{
    QString nickname = m_nickLineEdit->text().trimmed();
    if (nickname.isEmpty())
    {
        nickname = QStringLiteral("Akari");  // Default
        m_nickLineEdit->setText(nickname);
    }

    m_globalObject->setOwnNickname(nickname);


    QVariantMap identityMap;
    identityMap.insert("nickname",    nickname);
    identityMap.insert("dateOfBirth", m_dobDateEdit->date());
    // TODO: Insert other key:value pairs


    QVariantList identityDataList;
    identityDataList.append(identityMap);
    m_dataFile->saveData(identityDataList);

    m_globalObject->addToLog(tr("Saved identity data for %1.",
                                "%1 is a nickname")
                             .arg("<b>" + nickname + "</b>"),
                             true,
                             GlobalObject::LogGood);
}

