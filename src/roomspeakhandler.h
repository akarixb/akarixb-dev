/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef ROOMSPEAKHANDLER_H
#define ROOMSPEAKHANDLER_H


#include <QObject>
#include <QTimer>
#include <QRegularExpression>

#include <QDebug>

#include <QXmppClient.h>
#include <QXmppMucManager.h>
#include <QXmppMessage.h>

#include "globalobject.h"
#include "variableparser.h"
#include "speechdatastore.h"
#include "helpers.h"


class RoomSpeakHandler : public QObject
{
    Q_OBJECT

public:
    explicit RoomSpeakHandler(GlobalObject *globalObject,
                              QXmppMucRoom *muc,
                              QObject *parent = nullptr);
    ~RoomSpeakHandler();

    QString getRoomJid();

    void scheduleDelayedMessage();


signals:


public slots:
    void onMucJoined();

    void onParticipantJoined(QString jid);
    void onParticipantLeft(QString jid);

    void sendDelayedMessage();
    void forgetOneWhoLeft();

    void sendCasualMessage();

    void onMucMessageReceived(QXmppMessage message);


private:
    QXmppMucRoom *m_muc;


    QTimer *m_delayedMessageTimer;
    QString m_delayedMessage;

    QStringList m_greetedJids;
    QTimer *m_forgetGreetedTimer;
    QStringList m_greetedWhoLeft;

    QTimer *m_casualSpeakTimer;

    VariableParser *m_variableParser;
    SpeechDataStore *m_speechDataStore;
    GlobalObject *m_globalObject;

    QString m_mucJid;
};

#endif // ROOMSPEAKHANDLER_H
