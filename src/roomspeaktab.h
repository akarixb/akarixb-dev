/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef ROOMSPEAKTAB_H
#define ROOMSPEAKTAB_H

#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>

#include <QDebug>

#include "globalobject.h"
#include "stringlistwidget.h"
#include "datafile.h"
#include "speechdatastore.h"


class RoomSpeakTab : public QWidget
{
    Q_OBJECT

public:
    enum SentenceType
    {
        SentenceGreeting,
        SentenceCasual,
        SentenceReaction,
        SentenceConversation
    };

    explicit RoomSpeakTab(SentenceType tabType,
                          QString tabTitle,
                          QString explanation,
                          QString dataFileName,
                          GlobalObject *globalObject,
                          QWidget *parent = nullptr);
    ~RoomSpeakTab();

    StringListWidget *getStringListWidget();

    void loadSentences();
    void syncToSpeechDataStore(QVariantList sentencesList);


signals:


public slots:
    void saveSentences();


private:
    QVBoxLayout *m_mainLayout;
    QLabel *m_explanationLabel;
    StringListWidget *m_stringListWidget;
    QPushButton *m_saveButton;


    SentenceType m_tabType;
    QString m_tabTitle;
    DataFile *m_dataFile;
    SpeechDataStore *m_speechDataStore;

    GlobalObject *m_globalObject;
};

#endif // ROOMSPEAKTAB_H
