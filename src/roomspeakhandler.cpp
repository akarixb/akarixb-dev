/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "roomspeakhandler.h"


RoomSpeakHandler::RoomSpeakHandler(GlobalObject *globalObject,
                                   QXmppMucRoom *muc,
                                   QObject *parent) : QObject(parent)
{
    m_globalObject = globalObject;
    m_variableParser = globalObject->getVariableParser();
    m_speechDataStore = globalObject->getSpeechDataStore();

    m_muc = muc;
    connect(m_muc, &QXmppMucRoom::participantAdded,
            this, &RoomSpeakHandler::onParticipantJoined);
    connect(m_muc, &QXmppMucRoom::participantRemoved,
            this, &RoomSpeakHandler::onParticipantLeft);
    connect(m_muc, &QXmppMucRoom::joined,
            this, &RoomSpeakHandler::onMucJoined);
    connect(m_muc, &QXmppMucRoom::messageReceived,
            this, &RoomSpeakHandler::onMucMessageReceived);

    // Save JID as string so we can use it at any time
    m_mucJid = muc->jid();


    // Timer to send delayed greetings and reactions
    m_delayedMessageTimer = new QTimer(this);
    m_delayedMessageTimer->setSingleShot(true);
    connect(m_delayedMessageTimer, &QTimer::timeout,
            this, &RoomSpeakHandler::sendDelayedMessage);

    // Timer to forget people who were greeted, some time after they leave
    m_forgetGreetedTimer = new QTimer(this);
    m_forgetGreetedTimer->setSingleShot(true);
    m_forgetGreetedTimer->setInterval(600000); // HARDCODED FIXME, after 10 min
    connect(m_forgetGreetedTimer, &QTimer::timeout,
             this, &RoomSpeakHandler::forgetOneWhoLeft);


    // Timer to speak casually, from time to time
    m_casualSpeakTimer = new QTimer(this);
    m_casualSpeakTimer->setSingleShot(true);
    connect(m_casualSpeakTimer, &QTimer::timeout,
            this, &RoomSpeakHandler::sendCasualMessage);


    qDebug() << "RoomSpeakHandler created for" << m_mucJid;
}

RoomSpeakHandler::~RoomSpeakHandler()
{
    qDebug() << "RoomSpeakHandler destroyed for" << m_mucJid;
}



QString RoomSpeakHandler::getRoomJid()
{
    return m_mucJid;
}


void RoomSpeakHandler::scheduleDelayedMessage()
{
    if (!m_delayedMessage.isEmpty() && m_delayedMessage != QStringLiteral("*"))
    {
        const int timeToWaitMs = (m_globalObject->getReactionTime() * 1000)
                               + (m_delayedMessage.length() * 200); // 200ms per keystroke
        m_delayedMessageTimer->start(timeToWaitMs);
    }
}


//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// SLOTS //////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


/*
 * Upon joining, add all existing participants as greeted,
 * and start the timer for casual speak
 *
 */
void RoomSpeakHandler::onMucJoined()
{
    m_greetedJids.append(m_muc->participants());

    // FIXME: only start this timer if the room is already allowing casual speak
    m_casualSpeakTimer->start(Helpers::randomBetween(300000, 1200000)); // HARDCODED FIXME
}


void RoomSpeakHandler::onParticipantJoined(QString jid)
{
    QString nick = Helpers::nickFromRoomJid(jid);
    nick = Helpers::safeForHtml(nick);

    // Capability to auto-greet under certain conditions
    if (m_globalObject->isRoomFullyJoined(m_mucJid)
        && m_muc->property("allowGreetings").toBool()
        && Helpers::probability(m_globalObject->getAvailability())
        && Helpers::probability(m_muc->property("greetingProb").toInt())
        && !m_delayedMessageTimer->isActive()
        && !m_greetedJids.contains(jid))
    {
        const QStringList allGreetings = m_speechDataStore->getAllGreetings();
        m_delayedMessage = Helpers::randomString(allGreetings);
        m_delayedMessage = m_variableParser->getParsed(m_delayedMessage);
        m_delayedMessage.replace(QStringLiteral("%user%"), nick);

        this->scheduleDelayedMessage();

        // Remember them, to avoid greeting more than once if they leave and enter again
        m_greetedJids.append(jid);
    }

    // If this user was scheduled to be forgotten, don't, since they're back
    if (m_greetedWhoLeft.contains(jid))
    {
        m_greetedWhoLeft.removeAll(jid);
    }
}


void RoomSpeakHandler::onParticipantLeft(QString jid)
{
    if (!m_greetedWhoLeft.contains(jid))
    {
        m_greetedWhoLeft.append(jid);
        if (!m_forgetGreetedTimer->isActive())
        {
            m_forgetGreetedTimer->start();
        }
    }
}

/*
 * Send the actual message to the room when the timer times out
 *
 */
void RoomSpeakHandler::sendDelayedMessage()
{
    m_muc->sendMessage(m_delayedMessage); // Message has been verified to not be empty or *

    qDebug() << QTime::currentTime().toString()
             << "RoomSpeakHandler, sending delayed message "
                "(greeting/reaction) to" << m_mucJid
             << ":" << m_delayedMessage
             << "after" << m_delayedMessageTimer->interval();
}


void RoomSpeakHandler::forgetOneWhoLeft()
{
    if (!m_greetedWhoLeft.isEmpty())
    {
        const QString firstJid = m_greetedWhoLeft.first();
        m_greetedJids.removeAll(firstJid);
        m_greetedWhoLeft.removeFirst();
        if (!m_greetedWhoLeft.isEmpty()) // If still not empty
        {
            m_forgetGreetedTimer->start();
        }
    }
}

/*
 * Send casual, mostly random-themed, messages to the room, if allowed,
 * at random intervals
 *
 */
void RoomSpeakHandler::sendCasualMessage()
{
    if (m_muc->property("allowCasualSpeak").toBool()
        && Helpers::probability(m_globalObject->getAvailability())
        && Helpers::probability(m_muc->property("casualSpeakProb").toInt())
        && !m_delayedMessageTimer->isActive())
    {
        const QStringList allCasual = m_speechDataStore->getAllCasual();
        QString casualMessage = Helpers::randomString(allCasual);
        casualMessage = m_variableParser->getParsed(casualMessage);

        if (!casualMessage.isEmpty())
        {
            m_muc->sendMessage(casualMessage);
        }

        // Schedule to speak again after a random delay -- HARDCODED FIXME 7~25m
        m_casualSpeakTimer->start(Helpers::randomBetween(420000, 1500000));
    }
}


/*
 * Read messages from the room, and react to some of them, if allowed, etc
 *
 */
void RoomSpeakHandler::onMucMessageReceived(QXmppMessage message)
{
    const QString userNick = Helpers::nickFromRoomJid(message.from());

    if (m_globalObject->isRoomFullyJoined(m_mucJid)
        && m_muc->property("allowCasualSpeak").toBool()
        && userNick != m_muc->nickName()
        && Helpers::probability(m_globalObject->getAvailability())
        && Helpers::probability(m_muc->property("casualSpeakProb").toInt()) // FIXME, own value?
        && !m_delayedMessageTimer->isActive())
    {
        const QString body = message.body();
        const QVariantList allRegExps = m_speechDataStore->getAllReactions();

        for (QVariant oneRegExp : allRegExps)
        {
            const QVariantMap reMap = oneRegExp.toMap();
            if (reMap.keys().isEmpty())
            {
                return;
            }

            const QString reString = reMap.keys().first();
            QRegularExpression re(reString, // Defaulting to case insensitive for now - FIXME
                                  QRegularExpression::CaseInsensitiveOption);

            QRegularExpressionMatch reMatch = re.match(body);
            if (reMatch.hasMatch())
            {
                const QStringList reactions = reMap.value(reString).toStringList();
                m_delayedMessage = Helpers::randomString(reactions);
                m_delayedMessage = m_variableParser->getParsed(m_delayedMessage);
                m_delayedMessage.replace(QStringLiteral("%user%"), userNick);

                // Replace %cg#% with corresponding captured groups contents
                for (int capCount = 1; capCount <= reMatch.lastCapturedIndex(); ++capCount)
                {
                    m_delayedMessage.replace(QString("%cg%1%").arg(capCount), // FIXME
                                             reMatch.captured(capCount));
                }

                this->scheduleDelayedMessage();

                return;
            }
        }
    }
}
