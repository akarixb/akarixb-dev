/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "conversationmodule.h"


ConversationModule::ConversationModule(GlobalObject *globalObject,
                                       QWidget *parent) : QWidget(parent)
{
    m_globalObject = globalObject;

    m_conversationTab = new RoomSpeakTab(RoomSpeakTab::SentenceConversation,
                                         tr("Conversation"),
                                         tr("Here you can add regular expressions "
                                            "that will be matched against "
                                            "messages coming from private "
                                            "conversations, or from chatrooms "
                                            "in specific cases, and the possible "
                                            "messages to send as a reply.")
                                         + "<br />"
                                         + tr("Example:")
                                         + QStringLiteral(" <b>\\b(he+llo+|hi+)\\b</b> "
                                                          "&nbsp; &bull; &nbsp; ")
                                         + "<a href=\"https://perldoc.perl.org/perlre.html\">"
                                         + tr("Documentation for Perl regular "
                                              "expressions")
                                         + " (perl.org)</a>",
                                         QStringLiteral("/conversation.axb"),
                                         m_globalObject,
                                         this);
    // Syntax checking enabled for sub-list, not for main list, which holds regexps
    m_conversationTab->getStringListWidget()
                     ->setSublistSyntaxEnabled(m_globalObject->getVariableParser(),
                                               QStringList{"%user%",
                                                           "%cg1%","%cg2%","%cg3%",
                                                           "%cg4%","%cg5%","%cg6%",
                                                           "%cg7%","%cg8%","%cg9%"});

    m_mainLayout = new QVBoxLayout();
    m_mainLayout->addWidget(m_conversationTab);
    this->setLayout(m_mainLayout);

    qDebug() << "ConversationModule created";
}


ConversationModule::~ConversationModule()
{
    qDebug() << "ConversationModule destroyed";
}



//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// SLOTS //////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


