/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef IDENTITYMODULE_H
#define IDENTITYMODULE_H

#include <QWidget>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QDateEdit>
#include <QCalendarWidget>

#include <QDebug>

#include "globalobject.h"
#include "stringlistwidget.h"
#include "datafile.h"


class IdentityModule : public QWidget
{
    Q_OBJECT

public:
    explicit IdentityModule(GlobalObject *globalObject,
                            QWidget *parent = nullptr);
    ~IdentityModule();

    void loadIdentity();


signals:


public slots:
    void saveIdentity();


private:
    QVBoxLayout *m_mainLayout;
    QFormLayout *m_formLayout;

    QLineEdit *m_nickLineEdit;
    QDateEdit *m_dobDateEdit;

    QPushButton *m_saveButton;

    DataFile *m_dataFile;

    GlobalObject *m_globalObject;
};

#endif // IDENTITYMODULE_H
