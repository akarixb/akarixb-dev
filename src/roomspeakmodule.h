/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef ROOMSPEAKMODULE_H
#define ROOMSPEAKMODULE_H

#include <QTabWidget>
#include <QDebug>

#include "globalobject.h"
#include "roomspeaktab.h"
#include "stringlistwidget.h"


class RoomSpeakModule : public QTabWidget
{
    Q_OBJECT

public:
    explicit RoomSpeakModule(GlobalObject *globalObject,
                             QWidget *parent = nullptr);
    ~RoomSpeakModule();


signals:


public slots:


private:
    RoomSpeakTab *m_greetingsTab;
    RoomSpeakTab *m_casualSpeakTab;
    RoomSpeakTab *m_reactionsTab;

    GlobalObject *m_globalObject;
};

#endif // ROOMSPEAKMODULE_H
