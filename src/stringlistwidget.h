/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef STRINGLISTWIDGET_H
#define STRINGLISTWIDGET_H

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QTableView>
#include <QHeaderView>
#include <QStandardItemModel>
#include <QSortFilterProxyModel>
#include <QLineEdit>
#include <QToolButton>
#include <QInputDialog>
#include <QTextEdit>
#include <QPushButton>
#include <QLabel>
#include <QMessageBox>
#include <QRegularExpression>
#include <QAction>
#include <QResizeEvent>
#include <QShowEvent>
#include <QTimer>

#include <QDebug>

#include "variableparser.h"


class StringListWidget : public QWidget
{
    Q_OBJECT

public:
    enum ListType
    {
        BasicList,
        StringsWithTags,
        KeysWithValues,
        RegExpsWithTagsAndReplies
    };

    explicit StringListWidget(ListType listType, QWidget *parent = nullptr);
    ~StringListWidget();

    void setLowercaseItems(bool lowercaseItems);
    void setDuplicatesAllowed(bool duplicatesAllowed);
    void setCheckSyntaxEnabled(VariableParser *variableParser,
                               QStringList extraSubstitutions=QStringList());
    void setSublistSyntaxEnabled(VariableParser *variableParser,
                                 QStringList extraSubstitutions=QStringList());

    void adjustTable();
    void setHeader();

    void clearList();
    void addStringsToList(const QStringList stringList);
    QStringList getStringsFromList();

    void addItemsToList(QVariantMap stringMap);
    QVariantMap getItemsFromList();

    QString askForValidString(const QString title,
                              const QString message,
                              const QString previousValue=QString());

    bool stringExists(QString text);
    QStandardItem *getCurrentTableItem();

    void enableRelevantButtons();
    void toggleSearchField();
    void setFocusToMainList();

    void updateItemCounter();

    void notifySyntaxError();
    void notifyRegExpError(QRegularExpression re);


signals:
    void itemsChanged();


public slots:
    void addToList();
    void editItem();
    void removeFromList();

    void moveItemUp();
    void moveItemDown();
    void toggleUpDownButtons();

    void selectFirstRow();
    void selectLastRow();

    void onSearchTextChanged(const QString searchText);
    void focusSearchField();
    void cancelSearch();

    void onKeyValueRowChanged(QModelIndex index);
    void onRegExpRowChanged(QModelIndex index);
    void onSubItemsChanged();

    void updateItemData();


protected:
    virtual void showEvent(QShowEvent *event);
    virtual void resizeEvent(QResizeEvent *event);


private:
    QHBoxLayout *m_mainLayout;
    QVBoxLayout *m_listLayout;
    QTableView *m_tableView;
    QStandardItemModel *m_tableModel;
    QSortFilterProxyModel *m_filterModel;

    QHBoxLayout *m_searchLayout;
    QLabel *m_searchIconLabel;
    QLineEdit *m_searchLineEdit;
    QLabel *m_matchCountLabel;
    QPushButton *m_showAllButton;


    QVBoxLayout *m_buttonsLayout;
    QToolButton *m_addButton;
    QToolButton *m_editButton;
    QToolButton *m_removeButton;
    QToolButton *m_moveUpButton;
    QToolButton *m_moveDownButton;
    QLabel *m_itemCountLabel;

    QTextEdit *m_itemDataTextEdit;
    QPushButton *m_updateButton;

    StringListWidget *m_repliesListWidget;


    QAction *m_addAction;
    QAction *m_editAction;
    QAction *m_removeAction;
    QAction *m_goFirstAction;
    QAction *m_goLastAction;
    QAction *m_searchAction;
    QAction *m_cancelSearchAction;

    QTimer *m_resizeTimer;

    ListType m_listType;
    bool m_forKeyValues;
    bool m_lowercaseItems;
    bool m_duplicatesAllowed;
    int m_columns;
    QStringList m_headerLabels;
    bool m_syntaxCheckEnabled;
    bool m_regexpCheckEnabled;
    QStringList m_extraSubstitutions;
    VariableParser *m_variableParser;
};

#endif // STRINGLISTWIDGET_H
