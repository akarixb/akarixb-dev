/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "activitymodule.h"


ActivityModule::ActivityModule(ActivityHandler *activityHandler,
                               GlobalObject *globalObject,
                               QWidget *parent) : QWidget(parent)
{
    m_globalObject = globalObject;
    m_activityHandler = activityHandler;

    m_variableParser = m_globalObject->getVariableParser();


    m_dataFile = new DataFile(m_globalObject->getDataDirectory()
                              + QStringLiteral("/activities.axb"),
                              this);


    // Left side
    m_syncActivitiesButton = new QPushButton(QIcon::fromTheme("document-save",
                                                              QIcon(":/images/button-save.png")),
                                             tr("&Sync Activities"),
                                             this);
    connect(m_syncActivitiesButton, &QAbstractButton::clicked,
            this, &ActivityModule::syncActivities);



    m_activityListWidget = new QListWidget(this);
    m_activityListWidget->setDragDropMode(QAbstractItemView::InternalMove); // Movable items
    connect(m_activityListWidget, &QListWidget::currentRowChanged,
            this, &ActivityModule::onActivitySelected);

    m_addActivityButton = new QPushButton(QIcon::fromTheme("list-add",
                                                           QIcon(":/images/list-add.png")),
                                          tr("&Add Activity"),
                                          this);
    connect(m_addActivityButton, &QAbstractButton::clicked,
            this, &ActivityModule::addActivity);


    m_removeActivityButton = new QPushButton(QIcon::fromTheme("list-remove",
                                                              QIcon(":/images/list-remove.png")),
                                             tr("&Remove Activity"),
                                             this);
    connect(m_removeActivityButton, &QAbstractButton::clicked,
            this, &ActivityModule::removeActivity);




    // Right side
    m_nameLineEdit = new QLineEdit(this);
    connect(m_nameLineEdit, &QLineEdit::textChanged,
            this, &ActivityModule::onNameChanged);


    m_timesPerDaySpinbox = new QSpinBox(this);
    m_timesPerDaySpinbox->setRange(1, 100);

    m_probabilitySpinbox = new QSpinBox(this);
    m_probabilitySpinbox->setRange(0, 100);
    m_probabilitySpinbox->setValue(50);
    m_probabilitySpinbox->setSuffix("%");

    m_minTimeEdit = new QTimeEdit(QTime::currentTime(), this);
    m_maxTimeEdit = new QTimeEdit(QTime::currentTime().addSecs(60), this);

    m_minDateEdit = new QDateEdit(QDate::currentDate(), this);
    m_minDateEdit->setCalendarPopup(true);
    m_minDateEdit->calendarWidget()->setGridVisible(true);
    m_minDateEdit->setDisplayFormat("dd MMMM");
    m_maxDateEdit = new QDateEdit(QDate::currentDate().addDays(1), this);
    m_maxDateEdit->setCalendarPopup(true);
    m_maxDateEdit->calendarWidget()->setGridVisible(true);
    m_maxDateEdit->setDisplayFormat("dd MMMM");

    m_wdayMonCheckbox = new QCheckBox(tr("Mon", "Monday, shortened if possible"),
                                      this);
    m_wdayTueCheckbox = new QCheckBox(tr("Tue", "Tuesday, shortened if possible"),
                                      this);
    m_wdayWedCheckbox = new QCheckBox(tr("Wed", "Wednesday, shortened if possible"),
                                      this);
    m_wdayThuCheckbox = new QCheckBox(tr("Thu", "Thursday, shortened if possible"),
                                      this);
    m_wdayFriCheckbox = new QCheckBox(tr("Fri", "Friday, shortened if possible"),
                                      this);
    m_wdaySatCheckbox = new QCheckBox(tr("Sat", "Saturday, shortened if possible"),
                                      this);
    m_wdaySunCheckbox = new QCheckBox(tr("Sun", "Sunday, shortened if possible"),
                                      this);

    m_minDurationSpinbox = new QSpinBox(this);
    m_minDurationSpinbox->setRange(1, 1440); // Up to 1440 min, 24h
    m_maxDurationSpinbox = new QSpinBox(this);
    m_maxDurationSpinbox->setRange(1, 1440);
    m_maxDurationSpinbox->setSuffix(" " + tr("minutes"));

    m_statusTypeCombobox = new QComboBox(this);
    m_statusTypeCombobox->addItem(tr("Online"));
    m_statusTypeCombobox->addItem(tr("Free for Chat"));
    m_statusTypeCombobox->addItem(tr("Away"));
    m_statusTypeCombobox->addItem(tr("Extended Away"));
    m_statusTypeCombobox->addItem(tr("Do not Disturb"));

    m_statusMessagesListWidget = new StringListWidget(StringListWidget::BasicList,
                                                      this);
    m_statusMessagesListWidget->setCheckSyntaxEnabled(m_variableParser);


    m_availabilitySpinbox = new QSpinBox(this);
    m_availabilitySpinbox->setRange(0, 100);
    m_availabilitySpinbox->setValue(50);
    m_availabilitySpinbox->setSuffix("%");


    m_minReactionSpinbox = new QSpinBox(this);
    m_minReactionSpinbox->setRange(0, 300);
    m_maxReactionSpinbox = new QSpinBox(this);
    m_maxReactionSpinbox->setRange(1, 300);
    m_maxReactionSpinbox->setSuffix(" " + tr("seconds"));


    m_descriptionListWidget = new StringListWidget(StringListWidget::StringsWithTags,
                                                   this);
    m_descriptionListWidget->setCheckSyntaxEnabled(m_variableParser);

    m_msgBeforeListWidget = new StringListWidget(StringListWidget::StringsWithTags,
                                                 this);
    m_msgBeforeListWidget->setCheckSyntaxEnabled(m_variableParser);

    m_msgAfterListWidget = new StringListWidget(StringListWidget::StringsWithTags,
                                                this);
    m_msgAfterListWidget->setCheckSyntaxEnabled(m_variableParser);


    // Send messsages to:
    //   All rooms/some rooms/few rooms + active private conversations + specific jids
    m_msgToRoomsCombobox = new QComboBox(this);
    m_msgToRoomsCombobox->addItem(tr("All rooms"));
    m_msgToRoomsCombobox->addItem(tr("Several rooms"));
    m_msgToRoomsCombobox->addItem(tr("A few rooms"));
    m_msgToRoomsCombobox->addItem(tr("None of the rooms"));


    m_msgToActiveChatsCheckbox = new QCheckBox(tr("Also to active "
                                                  "private chats"),
                                               this);
    m_msgToActiveChatsCheckbox->setDisabled(true); // TMP FIXME: Until the setting is implemented


    m_specificJidsText = new QLabel(tr("Additional specific JIDs:"), this);

    // FIXME? Probably better to use a StringListWidget, and put it below
    m_specificJidsLineEdit = new QLineEdit(this);
    m_specificJidsLineEdit->setToolTip("<b></b>"
                                       + tr("List of room or user addresses, "
                                            "separated by space"));



    m_saveActivityButton = new QPushButton(QIcon::fromTheme("document-save",
                                                            QIcon(":/images/button-save.png")),
                                           tr("Sa&ve Activity"),
                                           this);
    connect(m_saveActivityButton, &QAbstractButton::clicked,
            this, &ActivityModule::saveActivity);


    // Layout
    m_leftLayout = new QVBoxLayout();
    m_leftLayout->addWidget(m_syncActivitiesButton);
    m_leftLayout->addWidget(m_activityListWidget);
    m_leftLayout->addWidget(m_addActivityButton);
    m_leftLayout->addWidget(m_removeActivityButton);


    m_timeLayout = new QHBoxLayout();
    m_timeLayout->addWidget(m_minTimeEdit);
    m_timeLayout->addSpacing(8);
    m_timeLayout->addWidget(m_maxTimeEdit);

    m_dateLayout = new QHBoxLayout();
    m_dateLayout->addWidget(m_minDateEdit);
    m_dateLayout->addSpacing(8);
    m_dateLayout->addWidget(m_maxDateEdit);

    m_weekdaysLayout = new QHBoxLayout();
    m_weekdaysLayout->addWidget(m_wdayMonCheckbox);
    m_weekdaysLayout->addWidget(m_wdayTueCheckbox);
    m_weekdaysLayout->addWidget(m_wdayWedCheckbox);
    m_weekdaysLayout->addWidget(m_wdayThuCheckbox);
    m_weekdaysLayout->addWidget(m_wdayFriCheckbox);
    m_weekdaysLayout->addWidget(m_wdaySatCheckbox);
    m_weekdaysLayout->addWidget(m_wdaySunCheckbox);

    m_durationLayout = new QHBoxLayout();
    m_durationLayout->addWidget(m_minDurationSpinbox);
    m_durationLayout->addSpacing(8);
    m_durationLayout->addWidget(m_maxDurationSpinbox);

    m_reactionLayout = new QHBoxLayout();
    m_reactionLayout->addWidget(m_minReactionSpinbox);
    m_reactionLayout->addSpacing(8);
    m_reactionLayout->addWidget(m_maxReactionSpinbox);


    m_recipientsTopLayout = new QHBoxLayout();
    m_recipientsTopLayout->addWidget(m_msgToRoomsCombobox);
    m_recipientsTopLayout->addWidget(m_msgToActiveChatsCheckbox);

    m_recipientsBottomLayout = new QHBoxLayout();
    m_recipientsBottomLayout->addWidget(m_specificJidsText);
    m_recipientsBottomLayout->addWidget(m_specificJidsLineEdit);

    m_msgRecipientsLayout = new QVBoxLayout();
    m_msgRecipientsLayout->addLayout(m_recipientsTopLayout);
    m_msgRecipientsLayout->addLayout(m_recipientsBottomLayout);


    m_formLayout = new QFormLayout();
    m_formLayout->addRow(tr("Name"),                  m_nameLineEdit);
    m_formLayout->addRow(tr("Maximum Times Per Day"), m_timesPerDaySpinbox);
    m_formLayout->addRow(tr("Probability"),           m_probabilitySpinbox);
    m_formLayout->addRow(tr("Time Range"),            m_timeLayout);
    m_formLayout->addRow(tr("Date Range"),            m_dateLayout);
    m_formLayout->addRow(tr("Days Of The Week"),      m_weekdaysLayout);
    m_formLayout->addRow(tr("Duration"),              m_durationLayout);
    m_formLayout->addRow(tr("Set Status Type"),       m_statusTypeCombobox);
    m_formLayout->addRow(tr("Status Messages"),       m_statusMessagesListWidget);
    m_formLayout->addRow(tr("Availability"),          m_availabilitySpinbox);
    m_formLayout->addRow(tr("Reaction Time"),         m_reactionLayout);
    m_formLayout->addRow(tr("Descriptions"),          m_descriptionListWidget);
    m_formLayout->addRow(tr("Messages Before"),       m_msgBeforeListWidget);
    m_formLayout->addRow(tr("Messages Afterwards"),   m_msgAfterListWidget);
    m_formLayout->addRow(tr("Send Messages To"),      m_msgRecipientsLayout);


    m_rightWidget = new QWidget(this);
    m_rightWidget->setLayout(m_formLayout);

    m_rightScrollArea = new QScrollArea(this);
    m_rightScrollArea->setContentsMargins(1, 1, 1, 1);
    m_rightScrollArea->setFrameStyle(QFrame::Box | QFrame::Raised);
    m_rightScrollArea->setWidgetResizable(true);
    m_rightScrollArea->setWidget(m_rightWidget);

    m_rightLayout = new QVBoxLayout();
    m_rightLayout->addWidget(m_rightScrollArea);
    m_rightLayout->addWidget(m_saveActivityButton, 0, Qt::AlignRight);

    m_mainLayout = new QHBoxLayout();
    m_mainLayout->setContentsMargins(0, 0, 0, 0);
    m_mainLayout->addLayout(m_leftLayout,  1);
    m_mainLayout->addLayout(m_rightLayout, 4);
    this->setLayout(m_mainLayout);


    m_removeActivityButton->setDisabled(true);
    m_rightWidget->setDisabled(true);
    m_saveActivityButton->setDisabled(true);

    this->loadActivities();


    qDebug() << "ActivityModule created";
}


ActivityModule::~ActivityModule()
{
    qDebug() << "ActivityModule destroyed";
}



void ActivityModule::loadActivities()
{
    const QVariantList activityDataList = m_dataFile->loadData();

    for (const QVariant listItem : activityDataList)
    {
        const QVariantMap map = listItem.toMap();
        QListWidgetItem *item = new QListWidgetItem();

        item->setText(map.value("name").toString());
        item->setData(Qt::UserRole,      map.value("name"));
        item->setData(Qt::UserRole + 1,  map.value("timesPerDay"));
        item->setData(Qt::UserRole + 2,  map.value("probability"));
        item->setData(Qt::UserRole + 3,  map.value("minTime"));
        item->setData(Qt::UserRole + 4,  map.value("maxTime"));
        item->setData(Qt::UserRole + 5,  map.value("minDate"));
        item->setData(Qt::UserRole + 6,  map.value("maxDate"));
        item->setData(Qt::UserRole + 7,  map.value("daysOfWeek", 127));
        item->setData(Qt::UserRole + 8,  map.value("minDuration"));
        item->setData(Qt::UserRole + 9,  map.value("maxDuration"));
        item->setData(Qt::UserRole + 10, map.value("statusType"));
        item->setData(Qt::UserRole + 11, map.value("statusMessages"));
        item->setData(Qt::UserRole + 12, map.value("availability"));
        item->setData(Qt::UserRole + 13, map.value("minReaction"));
        item->setData(Qt::UserRole + 14, map.value("maxReaction"));
        item->setData(Qt::UserRole + 15, map.value("descriptions"));
        item->setData(Qt::UserRole + 16, map.value("msgBefore"));
        item->setData(Qt::UserRole + 17, map.value("msgAfter"));
        item->setData(Qt::UserRole + 18, map.value("msgToRooms"));
        item->setData(Qt::UserRole + 19, map.value("msgToChats"));
        item->setData(Qt::UserRole + 20, map.value("specificJids"));

        m_activityListWidget->addItem(item);
    }

    m_activityHandler->setActivityData(activityDataList);

    m_globalObject->addToLog(tr("Loaded %Ln activities.",
                                "",
                                activityDataList.length()));
}


bool ActivityModule::validateActivity()
{
    return true; /////// tmp FIXME
}



//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// SLOTS //////////////////////////////////
//////////////////////////////////////////////////////////////////////////////



void ActivityModule::onNameChanged(QString newName)
{
    m_saveActivityButton->setDisabled(newName.trimmed().isEmpty());
}



void ActivityModule::addActivity()
{
    QListWidgetItem *item = new QListWidgetItem("-- "
                                                + tr("New Activity")
                                                + " --");
    ////// Defaults
    item->setData(Qt::UserRole,     "");

    // Times per day
    item->setData(Qt::UserRole + 1, 1);

    // Probability %
    item->setData(Qt::UserRole + 2, 50);

    // Time and date ranges
    item->setData(Qt::UserRole + 3, QTime(0,  0));
    item->setData(Qt::UserRole + 4, QTime(23, 59));
    item->setData(Qt::UserRole + 5, QDate(2000,  1,  1));
    item->setData(Qt::UserRole + 6, QDate(2000, 12, 31));

    // Days of the week
    item->setData(Qt::UserRole + 7, 127); // All selected (7 lower bits on)

    // Duration range
    item->setData(Qt::UserRole + 8, 1);
    item->setData(Qt::UserRole + 9, 10);

    // Status type
    item->setData(Qt::UserRole + 10, 0);

    // Nothing to set here for the status messages list (UR + 11)

    // Availability %
    item->setData(Qt::UserRole + 12, 50);

    // Reaction time range
    item->setData(Qt::UserRole + 13, 1);
    item->setData(Qt::UserRole + 14, 10);

    // Nothing to set here for the lists (UR + 15, 16, 17)

    // Send messages to all/several/few room
    item->setData(Qt::UserRole + 18, 0);

    // Messages also to active private chats
    item->setData(Qt::UserRole + 19, true);

    // Specific recipient JIDs
    item->setData(Qt::UserRole + 20, QVariant());

    m_activityListWidget->addItem(item);

    const int lastRow = m_activityListWidget->count() - 1;
    m_activityListWidget->setCurrentRow(lastRow);
    m_activityListWidget->setCurrentItem(item);
    item->setSelected(true);

    m_nameLineEdit->setFocus();
}


void ActivityModule::removeActivity()
{
    const int row = m_activityListWidget->currentRow();
    if (row != -1)
    {
        QListWidgetItem *item = m_activityListWidget->takeItem(row);
        delete item;
    }

    if (m_activityListWidget->count() == 0) // No activities left
    {
        m_removeActivityButton->setDisabled(true);
        m_rightWidget->setDisabled(true);
        m_saveActivityButton->setDisabled(true);
    }
}


void ActivityModule::onActivitySelected(int row)
{
    if (row < 0) // Protection
    {
        return;
    }

    QListWidgetItem *item = m_activityListWidget->item(row);

    m_nameLineEdit->setText(item->data(Qt::UserRole).toString());
    m_timesPerDaySpinbox->setValue(item->data(Qt::UserRole + 1).toInt());
    m_probabilitySpinbox->setValue(item->data(Qt::UserRole + 2).toInt());

    m_minTimeEdit->setTime(item->data(Qt::UserRole + 3).toTime());
    m_maxTimeEdit->setTime(item->data(Qt::UserRole + 4).toTime());
    m_minDateEdit->setDate(item->data(Qt::UserRole + 5).toDate());
    m_maxDateEdit->setDate(item->data(Qt::UserRole + 6).toDate());

    const int weekdays = item->data(Qt::UserRole + 7).toInt();
    m_wdayMonCheckbox->setChecked(weekdays & 1);
    m_wdayTueCheckbox->setChecked(weekdays & 2);
    m_wdayWedCheckbox->setChecked(weekdays & 4);
    m_wdayThuCheckbox->setChecked(weekdays & 8);
    m_wdayFriCheckbox->setChecked(weekdays & 16);
    m_wdaySatCheckbox->setChecked(weekdays & 32);
    m_wdaySunCheckbox->setChecked(weekdays & 64);

    m_minDurationSpinbox->setValue(item->data(Qt::UserRole + 8).toInt());
    m_maxDurationSpinbox->setValue(item->data(Qt::UserRole + 9).toInt());

    m_statusTypeCombobox->setCurrentIndex(item->data(Qt::UserRole + 10).toInt());
    m_statusMessagesListWidget->clearList();
    m_statusMessagesListWidget->addStringsToList(item->data(Qt::UserRole + 11)
                                                      .toStringList());

    m_availabilitySpinbox->setValue(item->data(Qt::UserRole + 12).toInt());

    m_minReactionSpinbox->setValue(item->data(Qt::UserRole + 13).toInt());
    m_maxReactionSpinbox->setValue(item->data(Qt::UserRole + 14).toInt());

    m_descriptionListWidget->clearList();
    m_descriptionListWidget->addStringsToList(item->data(Qt::UserRole + 15)
                                                   .toStringList());

    m_msgBeforeListWidget->clearList();
    m_msgBeforeListWidget->addStringsToList(item->data(Qt::UserRole + 16)
                                                 .toStringList());

    m_msgAfterListWidget->clearList();
    m_msgAfterListWidget->addStringsToList(item->data(Qt::UserRole + 17)
                                                .toStringList());

    m_msgToRoomsCombobox->setCurrentIndex(item->data(Qt::UserRole + 18).toInt());
    m_msgToActiveChatsCheckbox->setChecked(item->data(Qt::UserRole + 19).toBool());
    m_specificJidsLineEdit->setText(item->data(Qt::UserRole + 20).toString());


    m_removeActivityButton->setEnabled(true);
    m_rightWidget->setEnabled(true);
}


void ActivityModule::saveActivity()
{
    const QString newName = m_nameLineEdit->text().trimmed();

    // Check for duplicates
    for (int counter = 0; counter < m_activityListWidget->count(); ++counter)
    {
        QListWidgetItem *activityItem = m_activityListWidget->item(counter);
        if (activityItem->data(Qt::UserRole).toString() == newName)
        {
            if (activityItem != m_activityListWidget->currentItem())
            {
                QMessageBox::warning(this,
                                     tr("Activity exists"),
                                     tr("Error: There is already an activity "
                                        "in the list with this name."));
                m_nameLineEdit->setFocus();

                return;
            }
        }
    }

    // Check minimum required fields are filled
    if (!validateActivity())
    {
        return;
    }

    QListWidgetItem *item = m_activityListWidget->currentItem();

    m_nameLineEdit->setText(newName); // Show it as it will be stored
    item->setText(newName);
    item->setData(Qt::UserRole,     newName);
    item->setData(Qt::UserRole + 1, m_timesPerDaySpinbox->value());
    item->setData(Qt::UserRole + 2, m_probabilitySpinbox->value());

    item->setData(Qt::UserRole + 3, m_minTimeEdit->time());
    item->setData(Qt::UserRole + 4, m_maxTimeEdit->time());
    item->setData(Qt::UserRole + 5, m_minDateEdit->date());
    item->setData(Qt::UserRole + 6, m_maxDateEdit->date());

    int weekdays = 0;
    weekdays += m_wdayMonCheckbox->isChecked() ? 1  : 0;
    weekdays += m_wdayTueCheckbox->isChecked() ? 2  : 0;
    weekdays += m_wdayWedCheckbox->isChecked() ? 4  : 0;
    weekdays += m_wdayThuCheckbox->isChecked() ? 8  : 0;
    weekdays += m_wdayFriCheckbox->isChecked() ? 16 : 0;
    weekdays += m_wdaySatCheckbox->isChecked() ? 32 : 0;
    weekdays += m_wdaySunCheckbox->isChecked() ? 64 : 0;
    item->setData(Qt::UserRole + 7, weekdays);

    item->setData(Qt::UserRole + 8, m_minDurationSpinbox->value());
    item->setData(Qt::UserRole + 9, m_maxDurationSpinbox->value());

    item->setData(Qt::UserRole + 10, m_statusTypeCombobox->currentIndex());
    item->setData(Qt::UserRole + 11, m_statusMessagesListWidget->getStringsFromList());

    item->setData(Qt::UserRole + 12, m_availabilitySpinbox->value());

    item->setData(Qt::UserRole + 13, m_minReactionSpinbox->value());
    item->setData(Qt::UserRole + 14, m_maxReactionSpinbox->value());

    item->setData(Qt::UserRole + 15, m_descriptionListWidget->getStringsFromList());
    item->setData(Qt::UserRole + 16, m_msgBeforeListWidget->getStringsFromList());
    item->setData(Qt::UserRole + 17, m_msgAfterListWidget->getStringsFromList());

    item->setData(Qt::UserRole + 18, m_msgToRoomsCombobox->currentIndex());
    item->setData(Qt::UserRole + 19, m_msgToActiveChatsCheckbox->isChecked());
    item->setData(Qt::UserRole + 20, m_specificJidsLineEdit->text());


    m_activityListWidget->setFocus();
}


void ActivityModule::syncActivities()
{
    QVariantList activityDataList;

    for (int counter = 0; counter < m_activityListWidget->count(); ++counter)
    {
        QListWidgetItem *item = m_activityListWidget->item(counter);

        const QString activityName = item->data(Qt::UserRole).toString()
                                                             .trimmed();

        QVariantMap activityMap;
        activityMap.insert("name",           activityName);
        activityMap.insert("timesPerDay",    item->data(Qt::UserRole + 1));
        activityMap.insert("probability",    item->data(Qt::UserRole + 2));
        activityMap.insert("minTime",        item->data(Qt::UserRole + 3));
        activityMap.insert("maxTime",        item->data(Qt::UserRole + 4));
        activityMap.insert("minDate",        item->data(Qt::UserRole + 5));
        activityMap.insert("maxDate",        item->data(Qt::UserRole + 6));
        activityMap.insert("daysOfWeek",     item->data(Qt::UserRole + 7));
        activityMap.insert("minDuration",    item->data(Qt::UserRole + 8));
        activityMap.insert("maxDuration",    item->data(Qt::UserRole + 9));
        activityMap.insert("statusType",     item->data(Qt::UserRole + 10));
        activityMap.insert("statusMessages", item->data(Qt::UserRole + 11));
        activityMap.insert("availability",   item->data(Qt::UserRole + 12));
        activityMap.insert("minReaction",    item->data(Qt::UserRole + 13));
        activityMap.insert("maxReaction",    item->data(Qt::UserRole + 14));
        activityMap.insert("descriptions",   item->data(Qt::UserRole + 15));
        activityMap.insert("msgBefore",      item->data(Qt::UserRole + 16));
        activityMap.insert("msgAfter",       item->data(Qt::UserRole + 17));
        activityMap.insert("msgToRooms",     item->data(Qt::UserRole + 18));
        activityMap.insert("msgToChats",     item->data(Qt::UserRole + 19));
        activityMap.insert("specificJids",   item->data(Qt::UserRole + 20));


        // Don't save activities without a name
        if (!activityName.isEmpty())
        {
            activityDataList.append(activityMap);
        }
    }

    m_activityHandler->setActivityData(activityDataList);
    m_dataFile->saveData(activityDataList);

    m_globalObject->addToLog(tr("Saved %Ln activities.",
                                "",
                                activityDataList.length()),
                             true,
                             GlobalObject::LogGood);
}

