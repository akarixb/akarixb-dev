/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "helpwidget.h"


HelpWidget::HelpWidget(QWidget *parent) : QWidget(parent)
{
    this->setWindowTitle(tr("General Help") + " - AkariXB");
    this->setWindowIcon(QIcon::fromTheme("system-help",
                                         QIcon(":/icon/64x64/akarixb.png")));
    this->setWindowFlags(Qt::Window);
    this->setMinimumSize(320, 240);

    QSettings settings;
    this->resize(settings.value("HelpWidget/helpWindowSize",
                                QSize(640, 700)).toSize());

    /////////////////////////////////////////////////////////// Help text start
    QString helpText;

    // Table of contents
    const QString sectionStarting    = tr("Getting started");
    const QString sectionSettings    = tr("Settings");
    const QString sectionCommands    = tr("Commands");
    const QString sectionActivities  = tr("Activities");
    const QString sectionSyntax      = tr("Substitution syntax");
    const QString sectionCommandLine = tr("Command line options");

    helpText.append("<h1>" + tr("Contents") + "</h1>"
                    "<ul>"
                    "<li><a href=#starting>"    + sectionStarting
                    + "</a></li>"
                    "<li><a href=#settings>"    + sectionSettings
                    + "</a></li>"
                    "<li><a href=#commands>"    + sectionCommands
                    + "</a></li>"
                    "<li><a href=#activities>"  + sectionActivities
                    + "</a></li>"
                    "<li><a href=#syntax>"      + sectionSyntax
                    + "</a></li>"
                    "<li><a href=#commandline>" + sectionCommandLine
                    + "</a></li>"
                    "</ul>"
                    "<hr />");

    // Actual contents
    helpText.append("<a id=starting>"
                    "<h1><img src=:/icon/32x32/akarixb.png/> "
                    + sectionStarting
                    + "</h1></a>");

    helpText.append(tr("The first time you start AkariXB you will see the "
                       "Connection Settings window, where you need to enter "
                       "the XMPP address the bot will use, and its password.")
                    + "<br /><br />"
                    + tr("Afterwards, you can configure chatrooms to be "
                         "automatically joined, manage the contact list, "
                         "define commands for which the bot will provide "
                         "answers, etc."));


    helpText.append("<br /><hr / >"); // ----------------------


    helpText.append("<a id=settings>"
                    "<h1><img src=:/images/button-configure.png/> "
                    + sectionSettings
                    + "</h1></a>");

    helpText.append(tr("You can configure several things to your liking in "
                       "the settings, like which symbol will act as prefix "
                       "for commands, which XMPP addresses will be able to "
                       "use special administration commands with the bot, "
                       "whether you want popup notifications for certain "
                       "events, etc."));


    helpText.append("<br /><hr / >"); // ----------------------


    helpText.append("<a id=commands>" // Need to specify width to be like the others
                    "<h1><img width=32 src=:/images/section-commands.png/> "
                    + sectionCommands
                    + "</h1></a>");

    helpText.append(tr("You can define several kinds of custom commands, "
                       "like static message, random message from a list, "
                       "keyword-based message, and program execution-based "
                       "commands.")
                    + "<br />"
                    + tr("Each type of command has an explanation of how it "
                         "works, visible while you're adding the new command.")
                    + "<br /><br />"
                    + tr("If the access level for the command is not set for "
                         "everyone, only users listed in the administrators "
                         "list in the Program Configuration window will be "
                         "able to use that command."));


    helpText.append("<br /><hr / >"); // ----------------------


    helpText.append("<a id=activities>"
                    "<h1><img src=:/images/button-away.png/> "
                    + sectionActivities
                    + "</h1></a>");

    helpText.append(tr("Activities can be defined to make the bot change "
                       "its status, send messages, adapt its reaction time "
                       "and so on, at certain times and dates.")
                    + "<br /><br />"
                    + tr("If you have defined messages to be sent before or "
                         "after the activity, you can decrease the chances of "
                         "them being sent by adding some strings with a single "
                         "asterisk (*) in them to the list of messages."));


    helpText.append("<br /><hr / >"); // ----------------------


    helpText.append("<a id=syntax>"
                    "<h1><img src=:/images/button-edit.png/> "
                    + sectionSyntax
                    + "</h1></a>");

    helpText.append(tr("There are several special words that can be entered "
                       "inside custom strings like replies to commands, "
                       "conversation texts and status messages, which will be "
                       "replaced by the appropriate variable text at the time "
                       "the string is used. These are:")
                    + "<ul>"
                      "<li><b>%user%</b></li>"
                      + tr("Replaced by the name of the user to whom this "
                           "message is directed. Nickname in the case of "
                           "chatrooms, and account name in the case of chats "
                           "with contacts.")
                    + "<li><b>%ownnick%</b></li>"
                    + tr("The nickname of the bot, as specified in the "
                         "Identity module.")
                    + "<li><b>%params%</b></li>"
                      + tr("In replies to commands, this is replaced with "
                           "whatever follows the command name.")
                    + "<li><b>%output%</b></li>"
                      + tr("Used in a custom reply pattern for a command, this "
                           "will be replaced by the original reply to that "
                           "command.")
                    + "<li><b>%line%</b></li>"
                      + tr("This is replaced by a newline character.")
                    + "<li><b>%smiley%</b></li>"
                      + tr("This is replaced by a random old-style smiley "
                           "face.")
                    + "<li><b>%happyemoji%, %sademoji%</b></li>"
                      + tr("These are replaced by a random, happy or sad, "
                           "Unicode-based emoji face.")
                    + " "
                    + tr("Keep in mind that some users might not be able to "
                         "see these symbols.")
                    + "<li><b>%cg1%, %cg2%, %cg3%, %cg4%, %cg5%, %cg6%, %cg7%, "
                      "%cg8%, %cg9%</b></li>"
                      + tr("These are replaced by the content of captured groups "
                           "(the parts inside parenthesis) from the regular "
                           "expression that's being answered.")
                    + " "
                    + tr("For example, a regular expression like '(my name is|call me) "
                         "(.+)' could have a reply like 'Hello %cg2%, I'm %ownnick%!'.")
                    + "<br />" // Extra newline before ending the list,
                      "</ul>"  // to fix a spacing issue in last item
                      "<br />"
                    + tr("Example: 'Hello %user%' could be converted to 'Hello "
                         "Mary' if a user called Mary does something that "
                         "triggers that reply.")
                    + "<br /><br />"
                    + tr("In addition, syntax like %1 can be used to insert "
                         "one out of several choices at random, at any "
                         "desired point in the string.")
                    .arg(QStringLiteral("<b>{AAA|BBB|CCC}</b>"))
                    + "<br />"
                    + tr("%1, %2 and %3 can't be used inside these choices.")
                      .arg("<b>%smiley%</b>")
                      .arg("<b>%happyemoji%</b>")
                      .arg("<b>%sademoji%</b>"));


    helpText.append("<br /><hr / >"); // ----------------------


    helpText.append("<a id=commandline>"
                    "<h1><img src=:/images/button-configure.png/> "
                    + sectionCommandLine
                    + "</h1></a>");

    helpText.append(tr("You can use the --config parameter (or -c) to run the "
                       "program with a different configuration. This way, you "
                       "can run two instances of AkariXB at the same time, "
                       "with different accounts, different rooms configured, "
                       "using a different command prefix, etc.")
                    + "<br /><br />");
    helpText.append(tr("Use the --debug parameter (or -d) to have extra "
                       "information in your terminal window, about what the "
                       "program is doing.")
                    + "<br /><br />");
    helpText.append(tr("Use the --xmppdebug parameter (or -x) to have "
                       "extensive XMPP traffic information (XML stanzas) shown "
                       "in your terminal window."));


    helpText.append("<br /><br />");
    ///////////////////////////////////////////////////////////// Help text end


    m_helpTextBrowser = new QTextBrowser(this);
    m_helpTextBrowser->setReadOnly(true);
    m_helpTextBrowser->setOpenExternalLinks(true);
    m_helpTextBrowser->setText(helpText);

    m_closeButton = new QPushButton(QIcon::fromTheme("window-close",
                                                     QIcon(":/images/button-close.png")),
                                    tr("&Close"),
                                    this);
    connect(m_closeButton, &QAbstractButton::clicked,
            this, &QWidget::hide);

    QList<QKeySequence> closeShortcuts;
    closeShortcuts << QKeySequence(Qt::Key_Escape);
    closeShortcuts << QKeySequence(Qt::Key_F1);
    m_closeAction = new QAction(this);
    m_closeAction->setShortcuts(closeShortcuts);
    connect(m_closeAction, &QAction::triggered,
            this, &QWidget::hide);
    this->addAction(m_closeAction);

    m_layout = new QVBoxLayout();
    m_layout->setContentsMargins(2, 2, 2, 2);
    m_layout->addWidget(m_helpTextBrowser);
    m_layout->addWidget(m_closeButton, 0, Qt::AlignRight);
    this->setLayout(m_layout);

    qDebug() << "HelpWidget created";
}

HelpWidget::~HelpWidget()
{
    qDebug() << "HelpWidget destroyed";
}


//////////////////////////////////////////////////////////////////////////////
/////////////////////////////// PROTECTED ////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


void HelpWidget::closeEvent(QCloseEvent *event)
{
    this->hide();
    event->ignore();
}

void HelpWidget::hideEvent(QHideEvent *event)
{
    QSettings settings;
    if (settings.isWritable())
    {
        settings.setValue("HelpWidget/helpWindowSize", this->size());
    }

    event->accept();
}

