/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QApplication>
#include <QMenuBar>
#include <QMenu>
#include <QSplitter>
#include <QStatusBar>
#include <QSystemTrayIcon>

#include <QListWidget>
#include <QStackedWidget>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QTextBrowser>
#include <QSettings>
#include <QMessageBox>
#include <QResizeEvent>
#include <QDesktopServices>
#include <QStandardPaths> // Qt6 needs explicit include
#include <QDir>
#include <QTimer>

#include <QDebug>

#include <QXmppClient.h>
#include <QXmppMessage.h>
#include <QXmppMucManager.h>
#include <QXmppVersionManager.h>
#include <QXmppVersionIq.h>
#include <QXmppMessageReceiptManager.h>
#include <QXmppDiscoveryManager.h>

#include <iostream>


#include "globalobject.h"
#include "connectiondialog.h"
#include "configdialog.h"
#include "helpwidget.h"

#include "generalmodule.h"
#include "roommodule.h"
#include "chatmodule.h"
#include "contactsmodule.h"
#include "commandmodule.h"
#include "roomspeakmodule.h"
#include "conversationmodule.h"
#include "activitymodule.h"
#include "identitymodule.h"
#include "logmodule.h"

#include "messagehandler.h"
#include "commandhandler.h"
#include "activityhandler.h"
#include "variableparser.h"
#include "speechdatastore.h"
#include "helpers.h"


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void createMenus();
    void createTrayIcon();
    void createStatusBarWidgets();
    void createDataDir();

    void setupXmppClient();
    void enableXmppDebug();

    void loadSettings();
    void saveSettings();

    void setStatusIconOnline(QXmppPresence::AvailableStatusType statusType,
                             bool isOnline);

    void increaseReconnectionTimeout();


public slots:
    void connectToServer(QString jid, QString password,
                         bool autoconnect, QString resource,
                         int priority, bool ignoreSslErrors);
    void autoConnectToServer();
    void onConnected();
    void disconnectFromServer();
    void abortConnectionAttempts();
    void onDisconnected();
    void onConnectionTimerAbort();
    void onStatusChanged(QXmppPresence presence);

    void processMessage(QXmppMessage message);
    void processPresence(QXmppPresence presence);
    void processIq(QXmppIq iq);
    void processVersionIq(QXmppVersionIq versionIq);

    void onNetworkError(QXmppClient::Error error);

    void setChatsTitle(int count);
    void focusChatsModule();

    void visitWebsite();
    void visitBugTracker();
    void visitXmppOrg();
    void aboutAkariXB();

    void setTitleAndTrayInfo();
    void setStatusBarMessage(QString message);
    void showNotification(QString title, QString message);
    void trayControl(QSystemTrayIcon::ActivationReason reason);
    void toggleMainWindow(bool onStartup=false);
    void quitProgram();


protected:
    virtual void resizeEvent(QResizeEvent *event);
    virtual void closeEvent(QCloseEvent *event);


private:
    GlobalObject *m_globalObject;

    // Menus
    QMenu *m_sessionMenu;
    QAction *m_sessionConnectAction;
    QAction *m_sessionDisconnectAction;
    QAction *m_sessionAbortAction;
    QAction *m_sessionQuitAction;

    QMenu *m_settingsMenu;
    QAction *m_settingsConfigAction;

    QMenu *m_helpMenu;
    QAction *m_helpGeneralHelpAction;
    QAction *m_helpWebsiteAction;
    QAction *m_helpBugTrackerAction;
    QAction *m_helpAboutXmppAction;
    QAction *m_helpAboutAction;


    QSystemTrayIcon *m_systrayIcon;
    bool m_trayIconAvailable;
    QMenu *m_systrayMenu;
    QAction *m_trayTitleAction;
    QAction *m_trayShowWindowAction;


    // GUI
    QSplitter *m_mainSplitter;
    QListWidget *m_categoriesList;
    QStackedWidget *m_stackedWidget;

    QLabel *m_statusBarMessageLabel;
    QLabel *m_statusBarIconLabel; // Will hold an icon indicating away, DND...


    // Dialogs
    ConnectionDialog *m_connectionDialog;
    ConfigDialog     *m_configDialog;
    HelpWidget       *m_helpWidget;

    // Embedded modules
    GeneralModule      *m_generalModule;
    RoomModule         *m_roomModule;
    ChatModule         *m_chatModule;
    ContactsModule     *m_contactsModule;
    CommandModule      *m_commandModule;
    RoomSpeakModule    *m_roomSpeakModule;
    ConversationModule *m_conversationModule;
    ActivityModule     *m_activityModule;
    IdentityModule     *m_identityModule;
    LogModule          *m_logModule;


    VariableParser *m_variableParser;
    SpeechDataStore *m_speechDataStore;

    MessageHandler  *m_messageHandler;
    CommandHandler  *m_commandHandler;
    ActivityHandler *m_activityHandler;


    QTimer *m_initialConnectionTimer;
    QTimer *m_reconnectionTimer;
    int m_reconnectionDelaySecs;


    // XMPP
    QXmppClient *m_xmppClient;
    QXmppMucManager *m_mucManager;
    QXmppMessageReceiptManager *m_receiptManager;
    QXmppDiscoveryManager *m_discoveryManager;
    QXmppVersionManager *m_versionManager;


    QString m_userJid;
    QString m_userPassword;
    bool m_userAutoconnect;
    QString m_userResource;
    int m_userPriority;
    QString m_userServer;
    bool m_ignoreSslErrors;

    bool m_disconnectedManually;
};

#endif // MAINWINDOW_H
