/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "commandhandler.h"


CommandHandler::CommandHandler(GlobalObject *globalObject,
                               QObject *parent) : QObject(parent)
{
    m_globalObject = globalObject;

    m_xmppClient = m_globalObject->getXmppClient();
    m_mucManager = m_globalObject->getMucManager();
    m_variableParser = m_globalObject->getVariableParser();

    qDebug() << "CommandHandler created";
}


CommandHandler::~CommandHandler()
{
    qDebug() << "CommandHandler destroyed";
}


void CommandHandler::processCommand(QXmppMessage message)
{
    QStringList from = message.from().split("/");
    const QString bareFrom = from.takeFirst();

    QVariantMap commandReply;


    int userAccessLevel = 0;
    if (m_globalObject->getAdminJids().contains(bareFrom))
    {
        userAccessLevel = 100; // Admins get level 100
    }

    if (message.type() == QXmppMessage::GroupChat)
    {
        // Find the matching room object
        QXmppMucRoom *room = m_globalObject->roomFromJid(bareFrom);
        if (room != nullptr)
        {
            const QString fromNick = from.join("/");
            if (fromNick == room->nickName()) // Command from ourselves!
            {
                return;
            }

            // Ignore commands if the room is configured to not allow them
            if (!room->property("allowCommands").toBool())
            {
                qDebug() << "Room" << bareFrom << "doesn't allow commands:"
                         << message.body();
                return;
            }

            commandReply = this->parseCommand(message.body(),
                                              userAccessLevel,
                                              fromNick);
            const QString reply = commandReply.value("replyString").toString();
            const bool privateReply = commandReply.value("privateReply")
                                                  .toBool();
            const bool hasReplyPattern = commandReply.value("haveReplyPattern")
                                                     .toBool();
            const QString deniedCommand = commandReply.value("deniedCommand")
                                                      .toString();

            if (!deniedCommand.isEmpty())
            {
                m_globalObject->addToLog(tr("Command %1 requested by %2 in "
                                            "room %3 without the right access "
                                            "level.")
                                         .arg("'<b>" + deniedCommand + "</b>'")
                                         .arg("'<b>" + fromNick.toHtmlEscaped()
                                              + "</b>'")
                                         .arg("<b>" + bareFrom.toHtmlEscaped()
                                              + "</b>"),
                                         true, // In status bar, too
                                         GlobalObject::LogBad);
            }
            else if (!reply.isEmpty())
            {
                m_globalObject->addToLog(tr("Command from %1 in room %2: %3")
                                         .arg("'<b>" + fromNick.toHtmlEscaped()
                                              + "</b>'")
                                         .arg("<b>" + bareFrom.toHtmlEscaped()
                                              + "</b>")
                                         .arg(message.body().toHtmlEscaped()));

                if (privateReply)
                {
                    m_globalObject->sendPrivateMessage(message.from(), reply);
                }
                else
                {
                    if (hasReplyPattern)
                    {
                        room->sendMessage(reply);
                    }
                    else // No custom reply pattern, so we use "Nick: message"
                    {
                        room->sendMessage(QString("%1: %2").arg(fromNick)
                                                           .arg(reply));
                    }
                }
            }
        }
    }
    else // Any other type; Error type is handled before entering this method
    {
        // Get the username part of the JID as nickname
        const QString niceNick = bareFrom.split("@").takeFirst();

        // TODO: handle per-user flood

        commandReply = this->parseCommand(message.body(),
                                          userAccessLevel,
                                          niceNick);
        const QString reply = commandReply.value("replyString").toString();

        const QString deniedCommand = commandReply.value("deniedCommand")
                                                  .toString();

        if (!deniedCommand.isEmpty())
        {
            m_globalObject->addToLog(tr("Command %1 requested by %2 without "
                                        "the right access level.")
                                     .arg("'<b>" + deniedCommand + "</b>'")
                                     .arg("'<b>" + message.from().toHtmlEscaped()
                                          + "</b>'"),
                                     true, // In status bar, too
                                     GlobalObject::LogBad);
        }
        else if (!reply.isEmpty())
        {
            m_globalObject->addToLog(tr("Command from %1: %2")
                                     .arg("<b>" + message.from().toHtmlEscaped()
                                          + "</b>")
                                     .arg(message.body().toHtmlEscaped()));

            m_globalObject->sendPrivateMessage(message.from(), reply);
        }
    }
}


/*
 * Look for the requested command and return the matching reply string,
 * and other relevant data
 *
 */
QVariantMap CommandHandler::parseCommand(QString commandString, int userAccess,
                                         QString callingUser)
{
    // Remove prefix
    commandString.remove(0, m_globalObject->getCommandPrefix().length());

    QStringList commandStringList = commandString.split(" ");

    const QString command = commandStringList.takeFirst(); // FIXME: to lowercase?
    QString parameters = commandStringList.join(" ").trimmed();


    const QString commandFailedString = tr("The command failed.");
    QString commandReply;
    QString outputPattern;
    bool privateReply = false; // Initialize for the hardcoded commands
    QString deniedCommand;


    if (command == QStringLiteral("help"))
    {
        if (parameters.isEmpty())
        {
            // FIXME: Make translatable, but ensure "commands" and "help" aren't
            commandReply = tr("Use %1 to list the available commands.")
                           .arg(m_globalObject->getCommandPrefix()
                                + QStringLiteral("commands"))
                           + "\n\n"
                           + tr("Use %1 [command] to get help "
                                "about a specific command.")
                             .arg(m_globalObject->getCommandPrefix()
                                  + QStringLiteral("help"));
        }
        else
        {
            const QString query = parameters.trimmed();
            QString helpMessage;
            foreach (const QVariant mapVariant, m_commandDataList)
            {
                const QVariantMap map = mapVariant.toMap();
                if (map.value("trigger").toString() == query)
                {
                    // Show help only if user is allowed to use this command
                    if (map.value("accessLevel").toInt() <= userAccess)
                    {
                        helpMessage = map.value("helpText").toString().trimmed();
                    }

                    break;
                }
            }

            if (helpMessage.isEmpty())
            {
                commandReply = tr("I don't have help about: %1").arg(query);
            }
            else
            {
                commandReply = tr("Help for %1:").arg("[ " + query + " ]")
                               + "\n-- " + helpMessage;
            }
        }
    }
    else if (command == QStringLiteral("commands"))
    {
        const QStringList availableCommands = this->getCommandList(userAccess);

        commandReply = tr("Available commands")
                       + ":\n>    ";
        foreach (const QString trigger, availableCommands)
        {
            commandReply.append(QString("%1,   ").arg(trigger));
        }

        commandReply.remove(-4, 4); // Remove last comma and space
    }
    else if (command == QStringLiteral("version"))
    {
        commandReply = QString("AkariXB v%1").arg(qApp->applicationVersion());
    }
    else if (command == QStringLiteral("date"))
    {
        commandReply = QLocale::system().toString(QDateTime::currentDateTime()
                                                  .toLocalTime(),
                                                  QLocale::LongFormat);

        const qint64 uptime = m_globalObject->getConnectionStartTime()
                                             .secsTo(QDateTime::currentDateTimeUtc());

        commandReply.append("\nUp since: " /// TMP?
                            + QLocale::system().toString(m_globalObject->getConnectionStartTime()
                                                         .toLocalTime(),
                                                         QLocale::LongFormat)
                            + " ~ "
                            + Helpers::durationString(uptime));
    }
    else //////////////////////////////////////////////// User-defined commands
    {
        foreach (const QVariant mapVariant, m_commandDataList)
        {
            const QVariantMap map = mapVariant.toMap();
            if (map.value("trigger").toString() == command)
            {
                if (userAccess < map.value("accessLevel").toInt())
                {
                    deniedCommand = command;

                    qDebug() << "Command" << command
                             << "requested without privileges!";
                    break;
                }

                outputPattern = map.value("outputPattern").toString();
                privateReply = map.value("privateReply").toBool();


                const int commandType = map.value("type").toInt();
                if (commandType == 0)
                {
                    commandReply = map.value("staticReply").toString();
                }
                else if (commandType == 1)
                {
                    const QStringList replies = map.value("randomSentence")
                                                   .toStringList();
                    commandReply = Helpers::randomString(replies);
                }
                else if (commandType == 2)
                {
                    /*
                     * TODO: Optimize. Load at startup and notify clearly
                     *       if there's a problem loading the file.
                     */
                    QFile textFile(map.value("filename").toString());
                    if (textFile.open(QIODevice::ReadOnly))
                    {
                        QString fileLines = QString::fromUtf8(textFile.readAll())
                                                    .trimmed();
                        QStringList linesList = fileLines.split("\n");

                        int lineNum = QRandomGenerator::global()->bounded(linesList.length());
                        if (lineNum < linesList.length())
                        {
                            commandReply = linesList.at(lineNum).trimmed();
                        }
                    }
                    else
                    {
                        m_globalObject->addToLog(tr("Error loading file %1 "
                                                    "for command: %2")
                                                 .arg("<b>"
                                                      + map.value("filename")
                                                           .toString()
                                                      + "</b>")
                                                 .arg(command),
                                                 true,
                                                 GlobalObject::LogBad);
                        commandReply = commandFailedString;
                    }
                }
                else if (commandType == 3)
                {
                    if (parameters.trimmed().isEmpty())
                    {
                        commandReply = tr("I need a parameter for the query:");

                        const QStringList keywords = map.value("keywords")
                                                        .toMap().keys();
                        commandReply.append("\n>    ");
                        foreach (const QString keyword, keywords)
                        {
                            commandReply.append(QString("%1,   ").arg(keyword));
                        }
                        commandReply.remove(-4, 4); // Remove last comma
                    }
                    else
                    {
                        const QVariantMap replies = map.value("keywords")
                                                       .toMap();

                        // Keywords are lowercase
                        parameters = parameters.toLower().trimmed();

                        commandReply = replies.value(parameters)
                                              .toString().trimmed();

                        if (commandReply.isEmpty())
                        {
                            commandReply = tr("I don't have a match for %1")
                                           .arg("'" + parameters + "'");

                            commandReply.append("\n>  ");
                            const QStringList keywords = replies.keys();
                            foreach (const QString keyword, keywords)
                            {
                                commandReply.append(QString("%1,  ").arg(keyword));
                            }
                            commandReply.remove(-3, 3); // Remove last comma
                        }
                    }
                }
                else if (commandType == 4)
                {
                    const QVariantMap programToRun = map.value("programToRun")
                                                        .toMap();

                    QString commandLine = programToRun.value("commandLine")
                                                      .toString().trimmed();

                    QProcess programProcess;
                    if (programToRun.value("detached").toInt() == 1) // Detached
                    {
                        if (programProcess.startDetached(commandLine))
                        {
                            commandReply = tr("Started detached program "
                                              "successfully.");
                            qDebug() << "Started detached process:"
                                     << commandLine;
                        }
                        else
                        {
                            commandReply = commandFailedString;
                            qDebug() << "Could NOT START detached process:"
                                     << commandLine;
                        }
                    }
                    else // = 0, wait for it to finish
                    {
                        programProcess.start(commandLine);
                        if (programProcess.waitForStarted())
                        {
                            qDebug() << "Started process:"
                                     << programProcess.program()
                                     << programProcess.arguments();

                            if (programProcess.waitForFinished(10000)) // Up to 10 secs; FIXME
                            {
                                commandReply = QString::fromUtf8(programProcess
                                                                 .readAllStandardOutput()
                                                                 .trimmed());

                                // Nothing on stdout, read stderr
                                if (commandReply.isEmpty())
                                {
                                    commandReply = QString::fromUtf8(programProcess
                                                                     .readAllStandardError()
                                                                     .trimmed());
                                }

                                // Still empty...
                                if (commandReply.isEmpty())
                                {
                                    commandReply = tr("Command gave no output text.");
                                }
                            }
                            else
                            {
                                commandReply = commandFailedString;
                                qDebug() << "Took more than allowed time to complete!";
                            }
                        }
                        else
                        {
                            commandReply = commandFailedString;
                            qDebug() << "Could NOT START process:"
                                     << programProcess.program()
                                     << programProcess.errorString();
                        }
                    }
                }
                else if (commandType == 5)
                {
                    const QString aliasFor = map.value("aliasFor").toString();

                    const QString aliasCommand = m_globalObject->getCommandPrefix()
                                                 + aliasFor
                                                 + " "
                                                 + parameters;
                    qDebug() << "This is an alias for:" << aliasFor;

                    // Protection against infinite recursion
                    if (aliasFor.trimmed() != command)
                    {
                        // Recursive!
                        commandReply = this->parseCommand(aliasCommand,
                                                          userAccess,
                                                          callingUser)
                                             .value("replyString").toString();
                    }
                    else
                    {
                        // This shouldn't happen, unless a user edits the data files manually
                        commandReply = tr("This command is an alias to itself!");
                    }
                }

                break;
            }
        }
    }

    // Optional reply pattern; it can contain the actual reply, in %output%
    if (!outputPattern.isEmpty())
    {
        outputPattern.replace(QStringLiteral("%output%"), commandReply);
        commandReply = outputPattern;
    }

    // Parse and replace things like %line%, random choices in {a|b|c}, etc
    commandReply = m_variableParser->getParsed(commandReply);

    // Original parameters can be used in reply via %params%
    commandReply.replace(QStringLiteral("%params%"), parameters);

    // A suitable name for the calling user will replace %user%
    commandReply.replace(QStringLiteral("%user%"), callingUser);


    QVariantMap replyMap;
    replyMap.insert("replyString",      commandReply);
    replyMap.insert("privateReply",     privateReply);
    replyMap.insert("haveReplyPattern", !outputPattern.isEmpty());
    replyMap.insert("deniedCommand",    deniedCommand);

    return replyMap;
}


void CommandHandler::setCommandData(QVariantList dataList)
{
    //qDebug() << "Sync'ing command data:\n\n" << dataList;
    m_commandDataList = dataList;
}


QStringList CommandHandler::getCommandList(int accessLevel)
{
    QStringList list;
    foreach (const QVariant mapVariant, m_commandDataList)
    {
        const QVariantMap commandMap = mapVariant.toMap();
        if (commandMap.value("accessLevel").toInt() <= accessLevel)
        {
            list.append(commandMap.value("trigger").toString());
        }
    }

    return list;
}

