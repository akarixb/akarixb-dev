/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "contactsmodule.h"


ContactsModule::ContactsModule(GlobalObject *globalObject,
                               QWidget *parent) : QWidget(parent)
{
    m_globalObject = globalObject;
    m_xmppClient = m_globalObject->getXmppClient();
    connect(m_xmppClient, &QXmppClient::disconnected,
            this, &ContactsModule::onServerDisconnected);

    m_rosterManager = m_xmppClient->findExtension<QXmppRosterManager>();
    connect(m_rosterManager, &QXmppRosterManager::rosterReceived,
            this, &ContactsModule::onRosterReceived);
    connect(m_rosterManager, &QXmppRosterManager::presenceChanged,
            this, &ContactsModule::onPresenceChanged);
    connect(m_rosterManager, &QXmppRosterManager::subscriptionReceived,
            this, &ContactsModule::onSubscriptionReceived);
    connect(m_rosterManager, &QXmppRosterManager::itemAdded,
            this, &ContactsModule::onRosterItemAdded);
    connect(m_rosterManager, &QXmppRosterManager::itemChanged,
            this, &ContactsModule::onRosterItemChanged);
    connect(m_rosterManager, &QXmppRosterManager::itemRemoved,
            this, &ContactsModule::onRosterItemRemoved);


    // Roster and buttons
    m_rosterListWidget = new QListWidget(this);
    connect(m_rosterListWidget, &QListWidget::currentRowChanged,
            this, &ContactsModule::onContactSelected);
    connect(m_rosterListWidget, &QListWidget::itemDoubleClicked,
            this, &ContactsModule::onContactDoubleclicked);


    m_startChatButton = new QPushButton(QIcon::fromTheme("view-conversation-balloon",
                                                         QIcon(":/images/section-conv.png")),
                                        tr("&Start chat"),
                                        this);
    m_startChatButton->setDisabled(true);
    connect(m_startChatButton, &QPushButton::clicked,
            this, &ContactsModule::onStartChat);


    m_addContactButton = new QPushButton(QIcon::fromTheme("list-add",
                                                          QIcon(":/images/list-add.png")),
                                         tr("&Add Contact"),
                                         this);
    m_addContactButton->setDisabled(true);
    connect(m_addContactButton, &QPushButton::clicked,
            this, &ContactsModule::onAddContact);

    m_renameContactButton = new QPushButton(QIcon::fromTheme("document-edit",
                                                             QIcon(":/images/button-edit.png")),
                                            tr("Rename Contact"),
                                            this);
    m_renameContactButton->setDisabled(true); // Initially disabled
    connect(m_renameContactButton, &QPushButton::clicked,
            this, &ContactsModule::onRenameContact);

    m_removeContactButton = new QPushButton(QIcon::fromTheme("list-remove",
                                                             QIcon(":/images/list-remove.png")),
                                            tr("&Remove Contact"),
                                            this);
    m_removeContactButton->setDisabled(true); // Initially disabled
    connect(m_removeContactButton, &QPushButton::clicked,
            this, &ContactsModule::onRemoveContact);

    m_buttonsLayout = new QVBoxLayout();
    m_buttonsLayout->setAlignment(Qt::AlignBottom);
    m_buttonsLayout->addWidget(m_startChatButton);
    m_buttonsLayout->addStretch(1);
    m_buttonsLayout->addWidget(m_addContactButton);
    m_buttonsLayout->addWidget(m_renameContactButton);
    m_buttonsLayout->addWidget(m_removeContactButton);

    m_topLayout = new QHBoxLayout();
    m_topLayout->addWidget(m_rosterListWidget);
    m_topLayout->addLayout(m_buttonsLayout);


    // Pending subscription requests list, and buttons to accept/refuse
    m_requestsListWidget = new QListWidget(this);
    m_requestsListWidget->setSizePolicy(QSizePolicy::MinimumExpanding,
                                        QSizePolicy::Ignored);

    m_acceptRequestButton = new QPushButton(QIcon::fromTheme("dialog-ok",
                                                             QIcon(":/images/button-apply.png")),
                                            tr("Accept Request"),
                                            this);
    connect(m_acceptRequestButton, &QPushButton::clicked,
            this, &ContactsModule::onSubscriptionAccepted);

    m_refuseRequestButton = new QPushButton(QIcon::fromTheme("dialog-cancel",
                                                             QIcon(":/images/button-cancel.png")),
                                            tr("Refuse Request"),
                                            this);
    connect(m_refuseRequestButton, &QPushButton::clicked,
            this, &ContactsModule::onSubscriptionRefused);

    m_requestsLayout = new QHBoxLayout();
    m_requestsLayout->addWidget(m_requestsListWidget);
    m_requestsLayout->addWidget(m_acceptRequestButton);
    m_requestsLayout->addWidget(m_refuseRequestButton);

    m_requestsGroupBox = new QGroupBox(tr("Pending Subscription Requests"),
                                       this);
    m_requestsGroupBox->setLayout(m_requestsLayout);


    m_mainLayout = new QVBoxLayout();
    m_mainLayout->setContentsMargins(0, 0, 0, 0);
    m_mainLayout->addLayout(m_topLayout,        5);
    m_mainLayout->addWidget(m_requestsGroupBox, 1);
    this->setLayout(m_mainLayout);


    m_requestsGroupBox->hide(); // Until some subscription request is received

    qDebug() << "ContactsModule created";
}

ContactsModule::~ContactsModule()
{
    qDebug() << "ContactsModule destroyed";
}

/*
 * Check if a contact has any online resource
 *
 */
bool ContactsModule::isUserOnline(QString bareJid)
{
    QMap<QString, QXmppPresence> presences = m_rosterManager
                                             ->getAllPresencesForBareJid(bareJid);

    foreach (const QString resource, presences.keys())
    {
        if (m_rosterManager->getPresence(bareJid, resource).type() == QXmppPresence::Available)
        {
            return true;
        }
    }

    return false;
}


QListWidgetItem *ContactsModule::createContactItem(QString bareJid)
{
    QListWidgetItem *contactItem = new QListWidgetItem();
    QXmppRosterIq::Item rosterEntry = m_rosterManager->getRosterEntry(bareJid);

    QString subscriptionType;
    switch (rosterEntry.subscriptionType())
    {
        case QXmppRosterIq::Item::From:
            subscriptionType = "From";
            break;

        case QXmppRosterIq::Item::To:
            subscriptionType = "To";
            break;

        case QXmppRosterIq::Item::Both:
            subscriptionType = "Both";
            break;

        default:
            subscriptionType = "None";
    }

    // Offline icon until presence is received
    contactItem->setIcon(QIcon::fromTheme("user-offline",
                                          QIcon(":/images/button-offline.png")));

    this->setContactItemText(contactItem, rosterEntry.name(), bareJid);

    contactItem->setToolTip(tr("Subscription status: %1") // FIXME, needs updating
                            .arg(subscriptionType));      // after any change

    contactItem->setData(Qt::UserRole, bareJid);

    return contactItem;
}


void ContactsModule::setContactItemText(QListWidgetItem *item,
                                        QString name, QString jid)
{
    item->setText(name + " <" + jid + ">");
}


void ContactsModule::toggleButtons(bool state)
{
    m_startChatButton->setEnabled(state);
    m_addContactButton->setEnabled(state);
    m_renameContactButton->setEnabled(state);
    m_removeContactButton->setEnabled(state);
}


//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// SLOTS //////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


void ContactsModule::onRosterReceived()
{
    m_rosterListWidget->clear();

    const QStringList rosterJids = m_rosterManager->getRosterBareJids();

    foreach (const QString jid, rosterJids)
    {
        m_rosterListWidget->addItem(this->createContactItem(jid));
    }

    if (!rosterJids.isEmpty())
    {
        m_rosterListWidget->sortItems(); // Because QXMPP's roster manager sorts by JID
        m_rosterListWidget->setCurrentRow(0); // Auto-select first contact
    }

    // Add Contact button should be available as soon as we're online
    m_addContactButton->setEnabled(true);

    // The subscription-handling part might need re-enabling (after disconnection)
    m_requestsGroupBox->setEnabled(true);
}


void ContactsModule::onServerDisconnected()
{
    this->toggleButtons(false);
    m_requestsGroupBox->setEnabled(false);
}


void ContactsModule::onPresenceChanged(QString bareJid, QString resource)
{
    Q_UNUSED(resource)

    for (int row = 0; row < m_rosterListWidget->count(); ++row)
    {
        QListWidgetItem *rosterItem = m_rosterListWidget->item(row);
        if (rosterItem->data(Qt::UserRole).toString() == bareJid)
        {
            if (this->isUserOnline(bareJid)) // FIXME, this is a little overkill
            {
                rosterItem->setIcon(QIcon::fromTheme("user-online",
                                                      QIcon(":/images/button-online.png")));
            }
            else
            {
                rosterItem->setIcon(QIcon::fromTheme("user-offline",
                                                      QIcon(":/images/button-offline.png")));
            }

            // TODO: tooltip with each resource, etc, maybe

            break;
        }
    }
}

/*
 * React to a subscription REQUEST by another user
 *
 */
void ContactsModule::onSubscriptionReceived(QString bareJid)
{
    // First, check if already listed
    for (int row = 0; row < m_requestsListWidget->count(); ++row)
    {
        if (m_requestsListWidget->item(row)->text() == bareJid)
        {
            qDebug() << "Subscription request already listed:" << bareJid;
            return;
        }
    }

    m_requestsListWidget->addItem(bareJid);
    // Move to this last item; no need to enable/disable the accept/refuse buttons
    m_requestsListWidget->setCurrentRow(m_requestsListWidget->count() - 1);
    m_requestsGroupBox->show();

    // TODO: setting to auto-accept subscription requests
}


void ContactsModule::onSubscriptionAccepted()
{
    if (m_requestsListWidget->currentRow() >= 0)
    {
        QListWidgetItem *item = m_requestsListWidget->currentItem();
        const QString jid = item->text();
        m_rosterManager->acceptSubscription(jid);
        // A reason could be sent along, but not all clients show that
        delete item;

        if (m_requestsListWidget->count() < 1)
        {
            m_requestsGroupBox->hide();
        }

        m_globalObject->addToLog(tr("Accepted subscription request from %1.",
                                    "%1 is an XMPP address")
                                 .arg("<b>" + jid.toHtmlEscaped() + "</b>"),
                                 true,
                                 GlobalObject::LogGood);
    }

    // TODO: check if subscribing to this contact is needed, and if so, subscribe (optional)
}


void ContactsModule::onSubscriptionRefused()
{
    if (m_requestsListWidget->currentRow() >= 0)
    {
        QListWidgetItem *item = m_requestsListWidget->currentItem();
        const QString jid = item->text();
        m_rosterManager->refuseSubscription(jid);
        delete item;

        if (m_requestsListWidget->count() < 1)
        {
            m_requestsGroupBox->hide();
        }

        m_globalObject->addToLog(tr("Refused subscription request from %1.",
                                    "%1 is an XMPP address")
                                 .arg("<b>" + jid.toHtmlEscaped() + "</b>"),
                                 true,
                                 GlobalObject::LogBad);
    }
}


/*
 * Handle which buttons should be available
 *
 */
void ContactsModule::onContactSelected(int row)
{
    bool shouldEnable = row >= 0 && m_globalObject->connectedToServer();

    m_startChatButton->setEnabled(shouldEnable);
    m_renameContactButton->setEnabled(shouldEnable);
    m_removeContactButton->setEnabled(shouldEnable);
}


void ContactsModule::onContactDoubleclicked(QListWidgetItem *item)
{
    m_globalObject->startPrivateConversation(item->data(Qt::UserRole).toString());
}

void ContactsModule::onStartChat()
{
    if (m_rosterListWidget->currentRow() >= 0)
    {
        m_globalObject->startPrivateConversation(m_rosterListWidget
                                                 ->currentItem()
                                                 ->data(Qt::UserRole).toString());
    }
}


void ContactsModule::onAddContact()
{
    QString jidToAdd = QInputDialog::getText(this,
                                             tr("Add a contact") + " - AkariXB",
                                             tr("Enter the full XMPP address, "
                                                "with user@server format")
                                             + " &nbsp; &nbsp; &nbsp;"
                                               " &nbsp; &nbsp; &nbsp;"
                                               " &nbsp; &nbsp; &nbsp;" // Ensure extra width
                                               "<br />",
                                            QLineEdit::Normal);
    jidToAdd = jidToAdd.trimmed();

    if (Helpers::isValidJid(jidToAdd))
    {
        m_rosterManager->addItem(jidToAdd);
        m_rosterManager->subscribe(jidToAdd);
        qDebug() << "Adding to roster:" << jidToAdd;
    }
}

void ContactsModule::onRenameContact()
{
    if (m_rosterListWidget->currentRow() >= 0)
    {
        const QString jid = m_rosterListWidget->currentItem()
                                              ->data(Qt::UserRole).toString();

        const QString oldName = m_rosterManager->getRosterEntry(jid).name();

        const QString oldInfo = "<b>" + oldName.toHtmlEscaped()
                                + " &lt;" + jid.toHtmlEscaped() + "&gt;</b>";

        QString newName = QInputDialog::getText(this,
                                                tr("Rename a contact") + " - AkariXB",
                                                tr("Enter the new name for %1",
                                                   "%1 is contact's name + address")
                                                .arg(oldInfo)
                                                + " &nbsp; &nbsp; &nbsp;"
                                                  " &nbsp; &nbsp; &nbsp;" // Ensure extra width
                                                  "<br />",
                                                QLineEdit::Normal,
                                                oldName);
        newName = newName.trimmed();

        if (!newName.isEmpty() && newName != oldName)
        {
            m_rosterManager->renameItem(jid, newName);
            qDebug() << "Renaming contact:" << jid << oldName << newName;
        }
    }
}

void ContactsModule::onRemoveContact()
{
    if (m_rosterListWidget->currentRow() >= 0)
    {
        const QString jid = m_rosterListWidget->currentItem()
                                              ->data(Qt::UserRole).toString();

        const QString name = m_rosterManager->getRosterEntry(jid).name();

        const QString contactInfo = "<b>" + name.toHtmlEscaped()
                                    + " &lt;" + jid.toHtmlEscaped()
                                    + "&gt;</b>";

        const int confirm = QMessageBox::question(this,
                             tr("Remove contact?") + " - AkariXB",
                             tr("Do you want to remove %1 from your "
                                "contact list?",
                                "%1 is contact's name + address")
                             .arg(contactInfo)
                             + "<br />",
                             tr("Yes, remove",
                                "Regarding a contact"), tr("No"),
                             QString(), 1, 1);
        if (confirm == 0)
        {
            m_rosterManager->unsubscribe(jid);
            m_rosterManager->removeItem(jid);
            qDebug() << "Removing from roster:" << jid << name;
        }
    }
}


/*
 * React to roster push events, from the server
 *
 */
void ContactsModule::onRosterItemAdded(QString bareJid)
{
    m_rosterListWidget->addItem(this->createContactItem(bareJid));
    m_rosterListWidget->sortItems();

    m_globalObject->addToLog(tr("Added %1 to the contact list.",
                                "%1 is an XMPP address")
                             .arg("<b>" + bareJid.toHtmlEscaped() + "</b>"),
                             true,
                             GlobalObject::LogGood);
}

void ContactsModule::onRosterItemChanged(QString bareJid)
{
    for (int row = 0; row < m_rosterListWidget->count(); ++row)
    {
        QListWidgetItem *rosterItem = m_rosterListWidget->item(row);
        if (rosterItem->data(Qt::UserRole).toString() == bareJid)
        {
            this->setContactItemText(rosterItem,
                                     m_rosterManager->getRosterEntry(bareJid).name(),
                                     bareJid);
            break;
        }
    }

    m_rosterListWidget->sortItems();
}

void ContactsModule::onRosterItemRemoved(QString bareJid)
{
    for (int row = 0; row < m_rosterListWidget->count(); ++row)
    {
        QListWidgetItem *rosterItem = m_rosterListWidget->item(row);
        if (rosterItem->data(Qt::UserRole).toString() == bareJid)
        {
            delete rosterItem;

            m_globalObject->addToLog(tr("Removed %1 from the contact list.",
                                        "%1 is an XMPP address")
                                     .arg("<b>" + bareJid.toHtmlEscaped() + "</b>"),
                                     true,
                                     GlobalObject::LogBad);
            break;
        }
    }
}

