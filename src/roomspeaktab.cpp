/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "roomspeaktab.h"


RoomSpeakTab::RoomSpeakTab(SentenceType tabType, QString tabTitle,
                           QString explanation,
                           QString dataFileName,
                           GlobalObject *globalObject,
                           QWidget *parent) : QWidget(parent)
{
    m_globalObject = globalObject;
    m_tabType = tabType;
    m_tabTitle = tabTitle;
    m_speechDataStore = m_globalObject->getSpeechDataStore();


    m_explanationLabel = new QLabel(explanation, this);
    m_explanationLabel->setAlignment(Qt::AlignLeft | Qt::AlignTop);
    m_explanationLabel->setWordWrap(true);
    m_explanationLabel->setOpenExternalLinks(true);


    if (tabType == SentenceGreeting || tabType == SentenceCasual)
    {
        m_stringListWidget = new StringListWidget(StringListWidget::StringsWithTags,
                                                  this);
    }
    else // RoomSpeakTab::SentenceReaction || RoomSpeakTab::SentenceConversation
    {
        m_stringListWidget = new StringListWidget(StringListWidget::RegExpsWithTagsAndReplies,
                                                  this);
    }


    m_saveButton = new QPushButton(QIcon::fromTheme("document-save",
                                                    QIcon(":/images/button-save.png")),
                                   tr("&Save"),
                                   this);
    connect(m_saveButton, &QPushButton::clicked,
            this, &RoomSpeakTab::saveSentences);



    m_dataFile = new DataFile(m_globalObject->getDataDirectory()
                              + dataFileName,
                              this);


    m_mainLayout = new QVBoxLayout();
    m_mainLayout->addSpacing(2);
    m_mainLayout->addWidget(m_explanationLabel);
    m_mainLayout->addSpacing(4);
    m_mainLayout->addWidget(m_stringListWidget);
    m_mainLayout->addWidget(m_saveButton, 0, Qt::AlignRight);
    this->setLayout(m_mainLayout);


    this->loadSentences();

    qDebug() << "RoomSpeakTab created for:" << dataFileName << m_tabTitle;
}


RoomSpeakTab::~RoomSpeakTab()
{
    qDebug() << "RoomSpeakTab destroyed";
}

/*
 * Allow access to the StringListWidget from the outside,
 * to set custom syntax checking as needed
 *
 */
StringListWidget *RoomSpeakTab::getStringListWidget()
{
    return m_stringListWidget;
}


/*
 * Sentences will be stored as a list of single maps with just one key.
 * The map will have pairs of string:tags or regexp:replies
 *
 */
void RoomSpeakTab::loadSentences()
{
    const QVariantList sentencesList = m_dataFile->loadData();

    for (const QVariant sentencesVariant : sentencesList)
    {
        // It's a QVariantMap of just one key, like "string":"tags"
        m_stringListWidget->addItemsToList(sentencesVariant.toMap());
    }


    this->syncToSpeechDataStore(sentencesList);

    m_globalObject->addToLog(tr("Loaded %Ln sentence(s) of type %1.",
                                "",
                                sentencesList.size())
                             .arg("<b>'" + m_tabTitle + "'</b>"));
}


void RoomSpeakTab::syncToSpeechDataStore(QVariantList sentencesList)
{
    if (m_tabType == RoomSpeakTab::SentenceGreeting)
    {
        m_speechDataStore->setGreetingSentences(sentencesList);
    }
    else if (m_tabType == RoomSpeakTab::SentenceCasual)
    {
        m_speechDataStore->setCasualSentences(sentencesList);
    }
    else if (m_tabType == RoomSpeakTab::SentenceReaction)
    {
        m_speechDataStore->setReactionSentences(sentencesList);
    }
    else if (m_tabType == RoomSpeakTab::SentenceConversation)
    {
        m_speechDataStore->setConversationSentences(sentencesList);
    }
}


//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// SLOTS //////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


void RoomSpeakTab::saveSentences()
{
    QVariantList sentencesList;
    const QVariantMap sentencesMap = m_stringListWidget->getItemsFromList();

    // This is the same as sentencesMap.keys(), but in user order, which is desired
    const QStringList sentencesKeys = m_stringListWidget->getStringsFromList();


    for (const QString key : sentencesKeys)
    {
        QVariantMap oneKeyMap;
        QVariant sentenceValues = sentencesMap.value(key);
        if (sentenceValues.isNull()) // Safeguard to avoid saving broken entries
        {
            oneKeyMap.insert(key, "...");
        }
        else
        {
            oneKeyMap.insert(key, sentenceValues);
        }

        sentencesList.append(oneKeyMap);
    }


    m_dataFile->saveData(sentencesList);
    this->syncToSpeechDataStore(sentencesList);

    m_globalObject->addToLog(tr("Saved %Ln sentence(s) of type %1.",
                                "",
                                sentencesList.size())
                             .arg("<b>'" + m_tabTitle + "'</b>"),
                             true,
                             GlobalObject::LogGood);
}
