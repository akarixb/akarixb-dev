<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ca_ES">
<context>
    <name>ActivityHandler</name>
    <message>
        <source>Starting activity %1, which will end at %2.</source>
        <translation>Començant activitat %1, que acabarà a les %2.</translation>
    </message>
    <message>
        <source>Activity ended: %1.</source>
        <translation>Activitat finalitzada: %1.</translation>
    </message>
    <message>
        <source>None</source>
        <comment>Regarding an activity</comment>
        <translation>Cap</translation>
    </message>
    <message numerus="yes">
        <source>%Ln hour(s)</source>
        <translation>
            <numerusform>1 hora</numerusform>
            <numerusform>%Ln hores</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%Ln minute(s)</source>
        <translation>
            <numerusform>1 minut</numerusform>
            <numerusform>%Ln minuts</numerusform>
        </translation>
    </message>
    <message>
        <source>Not sending message to room %1 because I am not currently in it.</source>
        <translation>No puc enviar el missatge a la sala %1 perquè no em trobo actualment allà.</translation>
    </message>
</context>
<context>
    <name>ActivityModule</name>
    <message>
        <source>&amp;Add Activity</source>
        <translation>&amp;Afegir activitat</translation>
    </message>
    <message>
        <source>minutes</source>
        <translation>minuts</translation>
    </message>
    <message>
        <source>Online</source>
        <translation>Connectat</translation>
    </message>
    <message>
        <source>Free for Chat</source>
        <translation type="unfinished">Disponible per xerrar</translation>
    </message>
    <message>
        <source>Away</source>
        <translation>Ausent</translation>
    </message>
    <message>
        <source>Extended Away</source>
        <translation>No disponible</translation>
    </message>
    <message>
        <source>Do not Disturb</source>
        <translation>No molestar</translation>
    </message>
    <message>
        <source>seconds</source>
        <translation>segons</translation>
    </message>
    <message>
        <source>All rooms</source>
        <translation>Totes les sales</translation>
    </message>
    <message>
        <source>Several rooms</source>
        <translation>Algunes sales</translation>
    </message>
    <message>
        <source>A few rooms</source>
        <translation>Unes poques sales</translation>
    </message>
    <message>
        <source>Also to active private chats</source>
        <translation>També a converses privades actives</translation>
    </message>
    <message>
        <source>Sa&amp;ve Activity</source>
        <translation>&amp;Guardar activitat</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Maximum Times Per Day</source>
        <translation>Màxim de vegades per dia</translation>
    </message>
    <message>
        <source>Probability</source>
        <translation>Probabilitat</translation>
    </message>
    <message>
        <source>Time Range</source>
        <translation>Rang d&apos;hores</translation>
    </message>
    <message>
        <source>Date Range</source>
        <translation>Rang de dates</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation>Duració</translation>
    </message>
    <message>
        <source>Set Status Type</source>
        <translation>Establir el tipus d&apos;estat</translation>
    </message>
    <message>
        <source>Availability</source>
        <translation>Disponibilitat</translation>
    </message>
    <message>
        <source>Reaction Time</source>
        <translation>Temps de reacció</translation>
    </message>
    <message>
        <source>Descriptions</source>
        <translation>Descripcions</translation>
    </message>
    <message>
        <source>Messages Before</source>
        <translation>Missatges abans</translation>
    </message>
    <message>
        <source>Send Messages To</source>
        <translation>Enviar missatges a</translation>
    </message>
    <message>
        <source>&amp;Sync Activities</source>
        <translation>&amp;Sincronitzar activitats</translation>
    </message>
    <message>
        <source>&amp;Remove Activity</source>
        <translation>&amp;Treure activitat</translation>
    </message>
    <message>
        <source>Messages Afterwards</source>
        <translation>Missatges després</translation>
    </message>
    <message>
        <source>New Activity</source>
        <translation>Nova activitat</translation>
    </message>
    <message>
        <source>Activity exists</source>
        <translation>L&apos;activitat existeix</translation>
    </message>
    <message>
        <source>Error: There is already an activity in the list with this name.</source>
        <translation>Error: Ja hi ha una activitat a la llista amb aquest nom.</translation>
    </message>
    <message>
        <source>List of room or user addresses, separated by space</source>
        <translation>Llista d&apos;adreces de sales o d&apos;usuaris, separades per espais</translation>
    </message>
    <message>
        <source>Status Messages</source>
        <translation>Missatges d&apos;estat</translation>
    </message>
    <message numerus="yes">
        <source>Saved %Ln activities.</source>
        <translation>
            <numerusform>S&apos;ha guardat 1 activitat.</numerusform>
            <numerusform>S&apos;han guardat %Ln activitats.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>Loaded %Ln activities.</source>
        <translation>
            <numerusform>S&apos;ha carregat 1 activitat.</numerusform>
            <numerusform>S&apos;han carregat %Ln activitats.</numerusform>
        </translation>
    </message>
    <message>
        <source>None of the rooms</source>
        <translation>Cap de les sales</translation>
    </message>
    <message>
        <source>Additional specific JIDs:</source>
        <translation>JIDs específics adicionals:</translation>
    </message>
    <message>
        <source>Mon</source>
        <comment>Monday, shortened if possible</comment>
        <translation>Dl</translation>
    </message>
    <message>
        <source>Tue</source>
        <comment>Tuesday, shortened if possible</comment>
        <translation type="unfinished">Dt</translation>
    </message>
    <message>
        <source>Wed</source>
        <comment>Wednesday, shortened if possible</comment>
        <translation type="unfinished">Dc</translation>
    </message>
    <message>
        <source>Thu</source>
        <comment>Thursday, shortened if possible</comment>
        <translation>Dj</translation>
    </message>
    <message>
        <source>Fri</source>
        <comment>Friday, shortened if possible</comment>
        <translation>Dv</translation>
    </message>
    <message>
        <source>Sat</source>
        <comment>Saturday, shortened if possible</comment>
        <translation>Ds</translation>
    </message>
    <message>
        <source>Sun</source>
        <comment>Sunday, shortened if possible</comment>
        <translation>Dg</translation>
    </message>
    <message>
        <source>Days Of The Week</source>
        <translation>Dies de la setmana</translation>
    </message>
</context>
<context>
    <name>ChatModule</name>
    <message>
        <source>Chat started by %1.</source>
        <comment>%1 = A nickname</comment>
        <translation>Conversa iniciada per %1.</translation>
    </message>
    <message>
        <source>New chat</source>
        <translation>Nova conversa</translation>
    </message>
    <message>
        <source>A new chat was started by %1.</source>
        <translation>%1 ha començat una nova conversa.</translation>
    </message>
    <message>
        <source>There are no active chats</source>
        <translation>No hi ha converses actives</translation>
    </message>
</context>
<context>
    <name>ChatWidget</name>
    <message>
        <source>Reason:</source>
        <translation>Motiu:</translation>
    </message>
    <message>
        <source>Kicked from the room!</source>
        <translation>Expulsat de la sala!</translation>
    </message>
    <message>
        <source>Joined room %1, as %2.</source>
        <translatorcomment>No m&apos;agrada, la VO probablement canvii a la v0.8</translatorcomment>
        <translation>S&apos;ha entrat a la sala %1, com %2.</translation>
    </message>
    <message>
        <source>Left room %1.</source>
        <translation>S&apos;ha sortit de la sala %1.</translation>
    </message>
    <message>
        <source>Room name is: %1</source>
        <translation>El nom de la sala és: %1</translation>
    </message>
</context>
<context>
    <name>CommandHandler</name>
    <message>
        <source>Command from %1 in room %2: %3</source>
        <translation>Ordre de %1 a la sala %2: %3</translation>
    </message>
    <message>
        <source>Command from %1: %2</source>
        <translation>Ordre de %1: %2</translation>
    </message>
    <message>
        <source>The command failed.</source>
        <translation>Ha fallat la ordre.</translation>
    </message>
    <message>
        <source>Error loading file %1 for command: %2</source>
        <translation>Error al carregar l&apos;arxiu %1 per a la ordre: %2</translation>
    </message>
    <message>
        <source>Command gave no output text.</source>
        <translation>La ordre no ha donat cap text de sortida.</translation>
    </message>
    <message>
        <source>I don&apos;t have help about: %1</source>
        <translation>No tinc ajuda sobre: %1</translation>
    </message>
    <message>
        <source>Help for %1:</source>
        <translation>Ajuda de %1:</translation>
    </message>
    <message>
        <source>Available commands</source>
        <translation>Ordres disponibles</translation>
    </message>
    <message>
        <source>I don&apos;t have a match for %1</source>
        <translation>No tinc cap coincidència per a %1</translation>
    </message>
    <message>
        <source>This command is an alias to itself!</source>
        <translation>Aquesta ordre és un àlies per ella mateixa!</translation>
    </message>
    <message>
        <source>Use %1 to list the available commands.</source>
        <translation>Utilitza %1 per a llistar les ordres disponibles.</translation>
    </message>
    <message>
        <source>Use %1 [command] to get help about a specific command.</source>
        <translation>Utilitza %1 [ordre] per rebre ajuda sobre una ordre específica.</translation>
    </message>
    <message>
        <source>Started detached program successfully.</source>
        <translation>S&apos;ha iniciat correctament el programa, de forma independent.</translation>
    </message>
    <message>
        <source>I need a parameter for the query:</source>
        <translation>Necessito un paràmetre per la consulta:</translation>
    </message>
    <message>
        <source>Command %1 requested by %2 in room %3 without the right access level.</source>
        <translation>%2 ha demanat la ordre %1 a la sala %3 sense el nivell d&apos;accés adequat.</translation>
    </message>
    <message>
        <source>Command %1 requested by %2 without the right access level.</source>
        <translation>%2 ha demanat la ordre %1 sense el nivell d&apos;accés adequat.</translation>
    </message>
</context>
<context>
    <name>CommandModule</name>
    <message>
        <source>&amp;Add Command</source>
        <translation>&amp;Afegir ordre</translation>
    </message>
    <message>
        <source>Static Message</source>
        <translation>Missatge estàtic</translation>
    </message>
    <message>
        <source>Random Sentence</source>
        <translation>Frase aleatòria</translation>
    </message>
    <message>
        <source>Save &amp;Command</source>
        <translation>Guardar &amp;ordre</translation>
    </message>
    <message>
        <source>Command</source>
        <translation>Ordre</translation>
    </message>
    <message>
        <source>Help Text</source>
        <translation>Text d&apos;ajuda</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Tipus</translation>
    </message>
    <message>
        <source>Access Level</source>
        <translation>Nivell d&apos;accés</translation>
    </message>
    <message>
        <source>&amp;Sync Commands</source>
        <translation>&amp;Sincronitzar ordres</translation>
    </message>
    <message>
        <source>&amp;Remove Command</source>
        <translation>&amp;Treure ordre</translation>
    </message>
    <message>
        <source>Anyone can use it</source>
        <comment>&apos;it&apos; refers to a command</comment>
        <translation>Qualsevol ho pot utilitzar</translation>
    </message>
    <message>
        <source>Random Line from Text File</source>
        <translation>Línia aleatòria d&apos;un arxiu de text</translation>
    </message>
    <message>
        <source>Run Program</source>
        <translation>Executar un programa</translation>
    </message>
    <message>
        <source>Alias for another command</source>
        <translation>Àlies per a una altra ordre</translation>
    </message>
    <message>
        <source>Select File...</source>
        <translation>Seleccionar arxiu...</translation>
    </message>
    <message>
        <source>Static reply is empty.</source>
        <translation>La resposta estàtica està buida.</translation>
    </message>
    <message>
        <source>There are no sentences in the list.</source>
        <translation>No hi ha frases a la llista.</translation>
    </message>
    <message>
        <source>There is no file selected.</source>
        <translation>No s&apos;ha seleccionat cap arxiu.</translation>
    </message>
    <message>
        <source>There is no program specified.</source>
        <translation>No s&apos;ha especificat cap programa.</translation>
    </message>
    <message>
        <source>You need to enter the command this alias will execute.</source>
        <translation>Has d&apos;introduir la ordre que aquest àlies executarà.</translation>
    </message>
    <message>
        <source>Error validating fields</source>
        <translation>Error al validar els camps</translation>
    </message>
    <message>
        <source>Select a text file</source>
        <translation>Selecciona un arxiu de text</translation>
    </message>
    <message>
        <source>Selected file does not exist.</source>
        <translation>L&apos;arxiu seleccionat no existeix.</translation>
    </message>
    <message>
        <source>New Command</source>
        <translation>Nova ordre</translation>
    </message>
    <message>
        <source>No file selected</source>
        <translation>No hi ha cap arxiu seleccionat</translation>
    </message>
    <message>
        <source>Command exists</source>
        <translation>La ordre ja existeix</translation>
    </message>
    <message>
        <source>Just enter a single sentence.</source>
        <translation>Introdueix una sola frase.</translation>
    </message>
    <message>
        <source>Add as many sentences as you want.
One of them will be selected at random as a reply.</source>
        <translation>Afegeix totes les frases que vulguis.
Una d&apos;elles serà seleccionada a l&apos;atzar com a resposta.</translation>
    </message>
    <message>
        <source>A random line from the chosen file will be used as a reply.
File encoding should be UTF-8.</source>
        <translation>S&apos;utilitzarà com a resposta una línia a l&apos;atzar de l&apos;arxiu seleccionat.
La codificació de l&apos;arxiu hauria de ser UTF-8.</translation>
    </message>
    <message>
        <source>Keyword-based Message</source>
        <translation>Missatge basat en paraules clau</translation>
    </message>
    <message>
        <source>Add keywords to the list, then enter the corresponding definition for each keyword on the other side.</source>
        <translation>Afegeix paraules clau a la llista, i llavors introdueix la definició corresponent per a cada paraula clau a l&apos;altre costat.</translation>
    </message>
    <message>
        <source>Enter the original command that should be run when this alias is used.</source>
        <translation>Introdueix la ordre original que s&apos;ha d&apos;executar quan s&apos;utilitzi aquest àlies.</translation>
    </message>
    <message>
        <source>Private Reply</source>
        <translation>Resposta en privat</translation>
    </message>
    <message>
        <source>There are no keywords in the list.</source>
        <translation>No hi ha paraules clau a la llista.</translation>
    </message>
    <message>
        <source>A command can&apos;t be an alias of itself.</source>
        <translation>Una ordre no pot ser un àlies d&apos;ella mateixa.</translation>
    </message>
    <message numerus="yes">
        <source>Loaded %Ln command(s).</source>
        <translation>
            <numerusform>S&apos;ha carregat 1 ordre.</numerusform>
            <numerusform>S&apos;han carregat %Ln ordres.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>Saved %Ln command(s).</source>
        <translation>
            <numerusform>S&apos;ha guardat 1 ordre.</numerusform>
            <numerusform>S&apos;han guardat %Ln ordres.</numerusform>
        </translation>
    </message>
    <message>
        <source>If the program you launch will take a long time to finish (like a GUI), you need to select the option to launch it independently, but then it will not be possible to use the program output as the reply for the command.</source>
        <translation>Si el programa que executes trigarà molt de temps en acabar (com un GUI), has de seleccionar la opció d&apos;executar-ho independentment, pero llavors no serà possible utilitzar la sortida del programa com a resposta per la ordre.</translation>
    </message>
    <message>
        <source>Wait for the program to finish</source>
        <translation>Esperar a que el programa acabi</translation>
    </message>
    <message>
        <source>Launch the program independently</source>
        <translation>Executar el programa independentment</translation>
    </message>
    <message>
        <source>Error: There is already a command in the list with this name.</source>
        <translation>Error: Ja hi ha una ordre a la llista amb aquest nom.</translation>
    </message>
    <message>
        <source>Enter a system command line to be run, with any needed parameters.
The reply will be the console output of the program, unless you specify a reply pattern, and don&apos;t include the %output% variable somewhere in it.</source>
        <comment>Don&apos;t translate %output%!</comment>
        <translation>Introdueix una línia d&apos;ordres de sistema per executar, amb els paràmetres que pugui necessitar.
La resposta serà la sortida per consola del programa, llevat que especifiquis un patró de resposta, i no hi incloguis la variable %output% en algun punt.</translation>
    </message>
    <message>
        <source>Reply Pattern</source>
        <translation>Patró de resposta</translation>
    </message>
    <message>
        <source>Set an optional reply pattern</source>
        <translation>Estableix, opcionalment, un patró de resposta</translation>
    </message>
    <message>
        <source>With a reply pattern you can make any possible reply to a command fit into a predefined structure, like having a specific symbol at the start or the end of the reply, or always start the reply with a fixed word.</source>
        <translation>Amb un patró de resposta pots fer que qualsevol resposta possible a una ordre encaixi dins d&apos;una estructura predefinida, com tenir un símbol específic al principi o final de la resposta, o començar sempre la resposta amb una paraula fixa.</translation>
    </message>
    <message>
        <source>You can insert variables like %user%, %params%, %output%, %line% and %smiley%.</source>
        <comment>Do not translate the words between % symbols</comment>
        <translation>Pots inserir variables com %user%, %params%, %output%, %line% i %smiley%.</translation>
    </message>
    <message>
        <source>Usually, you will want to have the %output% variable somewhere in the pattern.</source>
        <comment>Do not translate %output%</comment>
        <translation>Normalment, t&apos;interessarà tenir la variable %output% en algun punt del patró.</translation>
    </message>
    <message>
        <source>If you don&apos;t set a reply pattern, replies in chatrooms will use the default pattern, %user%: %output%.</source>
        <comment>Do not translate %user% or %output%</comment>
        <translation>Si no estableixes un patró de resposta, les respostes a les sales de xat utilitzaran el patró per defecte, %user%: %output%.</translation>
    </message>
    <message>
        <source>Example: [RESULT] &gt; %output%</source>
        <comment>Do not translate %output%</comment>
        <translation>Exemple: [RESULTAT] &gt; %output%</translation>
    </message>
    <message>
        <source>Explanation shown to users with the %1 command</source>
        <translation>Explicació que es mostrarà als usuaris amb la ordre %1</translation>
    </message>
    <message>
        <source>The command name can&apos;t contain spaces.</source>
        <translation>El nom de la ordre no pot tenir espais.</translation>
    </message>
    <message>
        <source>The name, without the prefix or any spaces</source>
        <comment>Referring to the command&apos;s name/trigger</comment>
        <translation>El nom, sense el prefix o espais</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <source>Program Configuration</source>
        <translation>Configuració del programa</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Guardar</translation>
    </message>
    <message>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <source>Administrator JIDs</source>
        <translation>JIDs administradors</translation>
    </message>
    <message>
        <source>Command prefix</source>
        <translation>Prefix d&apos;ordres</translation>
    </message>
    <message>
        <source>Notify new chats</source>
        <translation>Notificar de noves converses</translation>
    </message>
    <message>
        <source>Hide window on startup</source>
        <translation>Amagar la finestra a l&apos;inici</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Notificacions</translation>
    </message>
    <message>
        <source>System Tray</source>
        <translation>Safata del sistema</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancel·lar</translation>
    </message>
    <message>
        <source>seconds</source>
        <comment>Suffix next to a number</comment>
        <translation>segons</translation>
    </message>
    <message>
        <source>Connection timeout</source>
        <translation>Temps màxim d&apos;espera de la connexió</translation>
    </message>
    <message>
        <source>Notify disconnection</source>
        <translation>Notificar desconnexió</translation>
    </message>
    <message>
        <source>Connection ping interval</source>
        <translation>Interval de ping de la connexió</translation>
    </message>
    <message>
        <source>Program configuration saved.</source>
        <translation>S&apos;ha guardat la configuració del programa.</translation>
    </message>
    <message>
        <source>Select...</source>
        <translation>Seleccionar...</translation>
    </message>
    <message>
        <source>Use a custom icon</source>
        <translation>Utilitzar una icona personalitzada</translation>
    </message>
    <message>
        <source>Invalid icon</source>
        <translation>Icona no vàlida</translation>
    </message>
    <message>
        <source>The selected image is not valid.</source>
        <translation>La imatge seleccionada no és vàlida.</translation>
    </message>
    <message>
        <source>Icon file %1 does not exist</source>
        <translation>L&apos;arxiu de icona %1 no existeix</translation>
    </message>
    <message>
        <source>Select custom icon</source>
        <translation>Selecciona una icona personalitzada</translation>
    </message>
    <message>
        <source>Image files</source>
        <translation>Arxius d&apos;imatge</translation>
    </message>
    <message>
        <source>All files</source>
        <translation>Tots els arxius</translation>
    </message>
</context>
<context>
    <name>ConnectionDialog</name>
    <message>
        <source>Connection Settings</source>
        <translation>Configuració de la connexió</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancel·lar</translation>
    </message>
    <message>
        <source>XMPP Address (Jabber ID)</source>
        <translation>Adreça XMPP (ID Jabber)</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Contrasenya</translation>
    </message>
    <message>
        <source>Connect Automatically</source>
        <translation>Connectar automàticament</translation>
    </message>
    <message>
        <source>Resource</source>
        <translation>Recurs</translation>
    </message>
    <message>
        <source>Priority</source>
        <translation>Prioritat</translation>
    </message>
    <message>
        <source>Like user@server</source>
        <translation>Com usuari@servidor</translation>
    </message>
    <message>
        <source>&amp;Connect</source>
        <translation>&amp;Connectar</translation>
    </message>
    <message>
        <source>Use carefully.</source>
        <translation type="unfinished">Utilitzar amb compte.</translation>
    </message>
    <message>
        <source>This allows connecting to servers with bad security certificates.</source>
        <translation>Això permet connectar a servidors amb certificats de seguretat incorrectes.</translation>
    </message>
    <message>
        <source>Ignore SSL errors</source>
        <translation>Ignorar errors SSL</translation>
    </message>
</context>
<context>
    <name>ContactsModule</name>
    <message>
        <source>&amp;Start chat</source>
        <translation>&amp;Iniciar conversa</translation>
    </message>
    <message>
        <source>&amp;Add Contact</source>
        <translation>&amp;Afegir contacte</translation>
    </message>
    <message>
        <source>Rename Contact</source>
        <translation>Canviar nom del contacte</translation>
    </message>
    <message>
        <source>&amp;Remove Contact</source>
        <translation>&amp;Treure contacte</translation>
    </message>
    <message>
        <source>Enter the new name for %1</source>
        <comment>%1 is contact&apos;s name + address</comment>
        <translation>Introdueix el nou nom per a %1</translation>
    </message>
    <message>
        <source>Remove contact?</source>
        <translation>Treure el contacte?</translation>
    </message>
    <message>
        <source>Do you want to remove %1 from your contact list?</source>
        <comment>%1 is contact&apos;s name + address</comment>
        <translation>Vols treure a %1 de la teva llista de contactes?</translation>
    </message>
    <message>
        <source>Yes, remove</source>
        <comment>Regarding a contact</comment>
        <translation>Sí, treure&apos;l</translation>
    </message>
    <message>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <source>Added %1 to the contact list.</source>
        <comment>%1 is an XMPP address</comment>
        <translation>S&apos;ha afegit a %1 a la llista de contactes.</translation>
    </message>
    <message>
        <source>Removed %1 from the contact list.</source>
        <comment>%1 is an XMPP address</comment>
        <translation>S&apos;ha tret a %1 de la llista de contactes.</translation>
    </message>
    <message>
        <source>Add a contact</source>
        <translation>Afegir un contacte</translation>
    </message>
    <message>
        <source>Accept Request</source>
        <translation>Acceptar sol·licitud</translation>
    </message>
    <message>
        <source>Refuse Request</source>
        <translation>Rebutjar sol·licitud</translation>
    </message>
    <message>
        <source>Pending Subscription Requests</source>
        <translation>Sol·licituds de subscripció pendents</translation>
    </message>
    <message>
        <source>Accepted subscription request from %1.</source>
        <comment>%1 is an XMPP address</comment>
        <translation>S&apos;ha acceptat la sol·licitud de subscripció de %1.</translation>
    </message>
    <message>
        <source>Refused subscription request from %1.</source>
        <comment>%1 is an XMPP address</comment>
        <translation>S&apos;ha rebutjat la sol·licitud de subscripció de %1.</translation>
    </message>
    <message>
        <source>Subscription status: %1</source>
        <translation>Estat de la subscripció: %1</translation>
    </message>
    <message>
        <source>Rename a contact</source>
        <translation>Canviar el nom del contacte</translation>
    </message>
    <message>
        <source>Enter the full XMPP address, with user@server format</source>
        <translation>Introdueix l&apos;adreça XMPP completa, amb format usuari@servidor</translation>
    </message>
</context>
<context>
    <name>GeneralModule</name>
    <message>
        <source>Offline</source>
        <translation>Desconnectat</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Connectar</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation>Desconnectar</translation>
    </message>
    <message>
        <source>Current Activity:</source>
        <translation>Activitat actual:</translation>
    </message>
    <message>
        <source>Status Message:</source>
        <translation>Missatge d&apos;estat:</translation>
    </message>
    <message>
        <source>Uptime:</source>
        <translatorcomment>Temps connectat? Temps d&apos;activitat?</translatorcomment>
        <translation type="unfinished">Temps de connexió:</translation>
    </message>
    <message>
        <source>Rooms:</source>
        <translation>Sales:</translation>
    </message>
    <message>
        <source>%1 joined, out of %2 configured</source>
        <comment>Number of rooms joined and configured</comment>
        <translatorcomment>Meh...</translatorcomment>
        <translation>S&apos;ha entrat a %1, de %2 configurades</translation>
    </message>
    <message>
        <source>Server version: %1</source>
        <translation>Versió del servidor: %1</translation>
    </message>
</context>
<context>
    <name>HelpWidget</name>
    <message>
        <source>General Help</source>
        <translation>Ajuda general</translation>
    </message>
    <message>
        <source>Getting started</source>
        <translation>Començant</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Configuració</translation>
    </message>
    <message>
        <source>Commands</source>
        <translation>Ordres</translation>
    </message>
    <message>
        <source>Activities</source>
        <translation>Activitats</translation>
    </message>
    <message>
        <source>Substitution syntax</source>
        <translation>Sintaxi de substitucions</translation>
    </message>
    <message>
        <source>Command line options</source>
        <translation>Opcions de línia d&apos;ordres</translation>
    </message>
    <message>
        <source>Contents</source>
        <translation>Taula de continguts</translation>
    </message>
    <message>
        <source>The first time you start AkariXB you will see the Connection Settings window, where you need to enter the XMPP address the bot will use, and its password.</source>
        <translation>La primera vegada que posis en marxa AkariXB veuràs la finestra Configuració de la connexió, on has d&apos;introduir l&apos;adreça XMPP que el bot utilitzarà, i la seva contrasenya.</translation>
    </message>
    <message>
        <source>Each type of command has an explanation of how it works, visible while you&apos;re adding the new command.</source>
        <translation>Cada tipus de ordre té una explicació de com funciona, visible mentre estàs afegint la nova ordre.</translation>
    </message>
    <message>
        <source>There are several special words that can be entered inside custom strings like replies to commands, conversation texts and status messages, which will be replaced by the appropriate variable text at the time the string is used. These are:</source>
        <translation>Hi ha diverses paraules especials que es poden introduir a dins de cadenes de text personalitzades, com respostes a ordres, textos de conversa i missatges d&apos;estat, que seran reemplaçades pel text variable apropiat en el moment en que s&apos;utilitzi la cadena de text. Aquestes són:</translation>
    </message>
    <message>
        <source>Replaced by the name of the user to whom this message is directed. Nickname in the case of chatrooms, and account name in the case of chats with contacts.</source>
        <translation>Reemplaçada pel nom de l&apos;usuari a qui va dirigit aquest missatge. El sobrenom en el cas de sales de xat, i el nom del compte en el cas de converses amb contactes.</translation>
    </message>
    <message>
        <source>Used in a custom reply pattern for a command, this will be replaced by the original reply to that command.</source>
        <translation>Utilitzada en un patró de resposta personalitzat per a una ordre, aquesta es reemplaçarà per la resposta original per aquella ordre.</translation>
    </message>
    <message>
        <source>This is replaced by a newline character.</source>
        <translation>Aquesta es reemplaça per un caràcter de salt de línia.</translation>
    </message>
    <message>
        <source>This is replaced by a random old-style smiley face.</source>
        <translation>Aquesta es reemplaça per una emoticona clàssica aleatòria.</translation>
    </message>
    <message>
        <source>Example: &apos;Hello %user%&apos; could be converted to &apos;Hello Mary&apos; if a user called Mary does something that triggers that reply.</source>
        <translation>Exemple: &apos;Hola %user%&apos; es podría convertir en &apos;Hola Maria&apos; si una usuària anomenada Maria fa alguna cosa que activi aquesta resposta.</translation>
    </message>
    <message>
        <source>In addition, syntax like %1 can be used to insert one out of several choices at random, at any desired point in the string.</source>
        <translation>A més a més, es pot utilitzar sintaxi com %1 per inserir una de entre diverses possibilitats a l&apos;atzar, a qualsevol punt desitjat a la cadena de text.</translation>
    </message>
    <message>
        <source>You can use the --config parameter (or -c) to run the program with a different configuration. This way, you can run two instances of AkariXB at the same time, with different accounts, different rooms configured, using a different command prefix, etc.</source>
        <translation>Pots fer servir el paràmetre --config (o -c) per executar el programa amb una configuració diferent. D&apos;aquesta manera, pots executar dos instàncies d&apos;AkariXB a la vegada, amb comptes diferents, sales configurades diferents, utilitzant un prefix d&apos;ordres diferent, etc.</translation>
    </message>
    <message>
        <source>Use the --debug parameter (or -d) to have extra information in your terminal window, about what the program is doing.</source>
        <translation>Fes servir el paràmetre --debug (o -d) per tenir informació addicional a la finestra de la terminal, sobre el que està fent el programa.</translation>
    </message>
    <message>
        <source>Use the --xmppdebug parameter (or -x) to have extensive XMPP traffic information (XML stanzas) shown in your terminal window.</source>
        <translation>Fes servir el paràmetre --xmppdebug (o -x) per tenir informació extensa del trànsit XMPP (stanzas XML) mostrada a la teva finestra de terminal.</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Tancar</translation>
    </message>
    <message>
        <source>These are replaced by a random, happy or sad, Unicode-based emoji face.</source>
        <translation>Aquestes es reemplacen amb una cara emoji (Unicode) feliç o trista, a l&apos;atzar.</translation>
    </message>
    <message>
        <source>Keep in mind that some users might not be able to see these symbols.</source>
        <translation>Recorda que potser alguns usuaris no podran veure aquests símbols.</translation>
    </message>
    <message>
        <source>%1, %2 and %3 can&apos;t be used inside these choices.</source>
        <translation>%1, %2 i %3 no es poden fer servir a dins d&apos;aquestes possibilitats.</translation>
    </message>
    <message>
        <source>Afterwards, you can configure chatrooms to be automatically joined, manage the contact list, define commands for which the bot will provide answers, etc.</source>
        <translation>A continuació, pots configurar sales de xat a les que s&apos;entrarà automàticament, gestionar la llista de contactes, definir ordres per les que el bot donarà respostes, etc.</translation>
    </message>
    <message>
        <source>Activities can be defined to make the bot change its status, send messages, adapt its reaction time and so on, at certain times and dates.</source>
        <translation>Es poden definir activitats per fer que el bot canviï el seu estat, enviï missatges, adapti el seu temps de reacció, etc, a determinades hores i dates.</translation>
    </message>
    <message>
        <source>If you have defined messages to be sent before or after the activity, you can decrease the chances of them being sent by adding some strings with a single asterisk (*) in them to the list of messages.</source>
        <translation>Si has definit missatges per enviar abans o després de l&apos;activitat, pots reduir les possibilitats de que s&apos;enviïn, afegint algunes cadenes de text amb un sol asterisc (*) en elles a la llista de missatges.</translation>
    </message>
    <message>
        <source>In replies to commands, this is replaced with whatever follows the command name.</source>
        <translation>En respostes a ordres, aquesta es reemplaça amb qualsevol cosa que vingui a continuació del nom de la ordre.</translation>
    </message>
    <message>
        <source>You can define several kinds of custom commands, like static message, random message from a list, keyword-based message, and program execution-based commands.</source>
        <translation>Pots definir diversos tipus d&apos;ordres personalitzades, com missatge estàtic, missatge aleatori d&apos;una llista, missatge basat en paraules clau, i ordres basades en l&apos;execució de programes.</translation>
    </message>
    <message>
        <source>You can configure several things to your liking in the settings, like which symbol will act as prefix for commands, which XMPP addresses will be able to use special administration commands with the bot, whether you want popup notifications for certain events, etc.</source>
        <translation>Pots configurar diverses coses al teu gust a la configuració, com quin símbol s&apos;utilitzarà com a prefix per les ordres, quines adreces XMPP podran fer servir ordres especials d&apos;administració amb el bot, si vols notificacions emergents per determinats events, etc.</translation>
    </message>
    <message>
        <source>If the access level for the command is not set for everyone, only users listed in the administrators list in the Program Configuration window will be able to use that command.</source>
        <translation>Si el nivell d&apos;accés per la ordre no està establert per tothom, només els usuaris que es trobin a la llista d&apos;administradors a la finestra de Configuració del programa podran utilitzar aquesta ordre.</translation>
    </message>
    <message>
        <source>The nickname of the bot, as specified in the Identity module.</source>
        <translation>El sobrenom del bot, tal com s&apos;especifica al mòdul Identitat.</translation>
    </message>
</context>
<context>
    <name>IdentityModule</name>
    <message>
        <source>&amp;Update</source>
        <translation>&amp;Actualitzar</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Sobrenom</translation>
    </message>
    <message>
        <source>Loaded identity data for %1.</source>
        <comment>%1 is a nickname</comment>
        <translation>S&apos;han carregat les dades d&apos;identitat per a %1.</translation>
    </message>
    <message>
        <source>Saved identity data for %1.</source>
        <comment>%1 is a nickname</comment>
        <translation>S&apos;han guardat les dades d&apos;identitat per a %1.</translation>
    </message>
    <message>
        <source>Identity data has not been defined yet.</source>
        <translation>Les dades d&apos;identitat encara no s&apos;han definit.</translation>
    </message>
    <message>
        <source>Date of Birth</source>
        <translation>Data de naixement</translation>
    </message>
</context>
<context>
    <name>LogModule</name>
    <message>
        <source>AkariXB v%1 started.</source>
        <comment>%1 = Version number</comment>
        <translation>AkariXB v%1 iniciat.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Connect</source>
        <translation>Connectar</translation>
    </message>
    <message>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <source>Rooms</source>
        <translation>Sales</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Sortir</translation>
    </message>
    <message>
        <source>About AkariXB</source>
        <translation>Sobre AkariXB</translation>
    </message>
    <message>
        <source>Connecting...</source>
        <translation>Connectant...</translation>
    </message>
    <message>
        <source>AkariXB is a Jabber/XMPP bot.</source>
        <translation>AkariXB és un bot per Jabber/XMPP.</translation>
    </message>
    <message>
        <source>You can talk to it, make it join chatrooms, etc.</source>
        <translation>Pots parlar-li, fer que entri a sales de xat, etc.</translation>
    </message>
    <message>
        <source>English translation by JanKusanagi.</source>
        <comment>TRANSLATORS: Change this with your language and name. If there was another translator before you, add your name after theirs ;)</comment>
        <translation>Traducció al català per JanKusanagi.</translation>
    </message>
    <message>
        <source>Thanks to all the testers, translators and packagers, who help make AkariXB better!</source>
        <translation>Gràcies a tots els &apos;testers&apos;, traductors i empaquetadors, que ajuden a fer AkariXB millor!</translation>
    </message>
    <message>
        <source>&amp;Hide Window</source>
        <translation>&amp;Amagar finestra</translation>
    </message>
    <message>
        <source>&amp;Show Window</source>
        <translation>&amp;Mostrar finestra</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>Desconnectat</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation>Desconnectar</translation>
    </message>
    <message>
        <source>Log</source>
        <translation>Registre</translation>
    </message>
    <message>
        <source>Configure AkariXB</source>
        <translation>Configurar AkariXB</translation>
    </message>
    <message>
        <source>Visit Website</source>
        <translation>Visitar lloc web</translation>
    </message>
    <message>
        <source>Report a Bug</source>
        <translation>Informar d&apos;un error</translation>
    </message>
    <message>
        <source>%1 connected.</source>
        <translation>%1 connectat.</translation>
    </message>
    <message>
        <source>Disconnected.</source>
        <translation>Desconnectat.</translation>
    </message>
    <message>
        <source>Network error: %1</source>
        <translation>Error de xarxa: %1</translation>
    </message>
    <message>
        <source>Chats</source>
        <translation type="unfinished">Converses</translation>
    </message>
    <message>
        <source>Commands</source>
        <translation>Ordres</translation>
    </message>
    <message>
        <source>Conversation</source>
        <translatorcomment>FIXME</translatorcomment>
        <translation type="unfinished">Conversa</translation>
    </message>
    <message>
        <source>Activities</source>
        <translation>Activitats</translation>
    </message>
    <message>
        <source>AkariXB is Free Software, licensed under the GNU GPL license, and uses some Oxygen icons under LGPL license.</source>
        <translation>AkariXB es software lliure, llicenciat sota la GNU GPL, i utilitza algunes icones Oxygen amb llicència LGPL.</translation>
    </message>
    <message>
        <source>Account is not configured</source>
        <translation>El compte no està configurat</translation>
    </message>
    <message>
        <source>Data directory created: %1</source>
        <translation>S&apos;ha creat el directori de dades: %1</translation>
    </message>
    <message>
        <source>Error creating data directory: %1</source>
        <translation>Error durant la creació del directori de dades: %1</translation>
    </message>
    <message>
        <source>Data directory in use: %1</source>
        <translation>Directori de dades en ús: %1</translation>
    </message>
    <message>
        <source>Identity</source>
        <translation>Identitat</translation>
    </message>
    <message>
        <source>Ready.</source>
        <translation>Preparat.</translation>
    </message>
    <message>
        <source>&amp;Session</source>
        <translation>&amp;Sessió</translation>
    </message>
    <message>
        <source>S&amp;ettings</source>
        <translation>&amp;Configuració</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>Aj&amp;uda</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation>Desconnectat</translation>
    </message>
    <message>
        <source>Connection to the server was lost.</source>
        <translation>S&apos;ha perdut la connexió amb el servidor.</translation>
    </message>
    <message>
        <source>Socket Error</source>
        <translatorcomment>Socket?</translatorcomment>
        <translation>Error de sòcol</translation>
    </message>
    <message>
        <source>KeepAlive Error</source>
        <translation>Error de KeepAlive</translation>
    </message>
    <message>
        <source>XMPP Stream Error</source>
        <translatorcomment>flux?</translatorcomment>
        <translation type="unfinished">Error de stream XMPP</translation>
    </message>
    <message>
        <source>About XMPP</source>
        <translation>Sobre XMPP</translation>
    </message>
    <message>
        <source>Quit?</source>
        <translation>Sortir?</translation>
    </message>
    <message>
        <source>Are you sure?</source>
        <translation>Segur?</translation>
    </message>
    <message>
        <source>Yes, quit AkariXB</source>
        <translation>Sí, sortir d&apos;AkariXB</translation>
    </message>
    <message>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <source>Connection seems stuck. Trying again...</source>
        <translation>La connexió sembla encallada. Intentant-ho de nou...</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation>Contactes</translation>
    </message>
    <message>
        <source>Stop connection attempts</source>
        <translation>Aturar intents de connexió</translation>
    </message>
    <message>
        <source>General &amp;Help</source>
        <translation>&amp;Ajuda general</translation>
    </message>
    <message>
        <source>Aborted connection attempt.</source>
        <translation>S&apos;ha interromput l&apos;intent de connexió.</translation>
    </message>
    <message>
        <source>Presence error %1 from %2: %3</source>
        <translation>Error de presència %1 de %2: %3</translation>
    </message>
    <message>
        <source>%1 wants to subscribe to your presence.</source>
        <comment>%1 is another user&apos;s address</comment>
        <translation>%1 es vol subscriure a la teva presència.</translation>
    </message>
    <message>
        <source>%1 unsubscribed from your presence.</source>
        <comment>%1 is another user&apos;s address</comment>
        <translation>%1 ha cancel·lat la seva subscripció a la teva presència.</translation>
    </message>
    <message>
        <source>%1 removed your authorization to see their presence.</source>
        <comment>%1 is another user&apos;s address</comment>
        <translation>%1 ha suprimit la teva autorització per veure la seva presència.</translation>
    </message>
    <message>
        <source>%1 accepted your subscription request.</source>
        <comment>%1 is another user&apos;s address</comment>
        <translation>%1 ha acceptat la teva sol·licitud de subscripció.</translation>
    </message>
    <message>
        <source>Settings file in use: %1</source>
        <translation>Arxiu de configuració en ús: %1</translation>
    </message>
    <message>
        <source>Room Speech</source>
        <translatorcomment>Meh...</translatorcomment>
        <translation type="unfinished">Conversa en sales</translation>
    </message>
    <message>
        <source>Received version information from %1:</source>
        <translation>S&apos;ha rebut la informació de versió de %1:</translation>
    </message>
    <message>
        <source>Warning! Ignoring errors in SSL connection to server.</source>
        <translation type="unfinished">¡Avís! Ignorant errors a la connexió SSL al servidor.</translation>
    </message>
</context>
<context>
    <name>MessageHandler</name>
    <message>
        <source>Error %1 from %2: %3</source>
        <translation>Error %1 de %2: %3</translation>
    </message>
    <message>
        <source>Headline from %1: %2</source>
        <comment>%1 is a user&apos;s address,%2 is the title of a message</comment>
        <translatorcomment>Com Psi, Gajim no te aquest concepte</translatorcomment>
        <translation>Titular de %1: %2</translation>
    </message>
</context>
<context>
    <name>RoomModule</name>
    <message>
        <source>Add &amp;New Room</source>
        <translation>Afegir &amp;nova sala</translation>
    </message>
    <message>
        <source>&amp;Remove Room</source>
        <translation>&amp;Treure sala</translation>
    </message>
    <message>
        <source>Room address</source>
        <translation>Adreça de la sala</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Sobrenom</translation>
    </message>
    <message>
        <source>Autojoin</source>
        <translatorcomment>Meh...</translatorcomment>
        <translation>Auto-entrar</translation>
    </message>
    <message>
        <source>Join</source>
        <translation>Entrar</translation>
    </message>
    <message>
        <source>Leave</source>
        <translation>Sortir</translation>
    </message>
    <message>
        <source>Select a room from the list above</source>
        <translation>Selecciona una sala de la llista de la part superior</translation>
    </message>
    <message>
        <source>Room Details</source>
        <translation>Detalls de la sala</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <source>Room address is not valid.</source>
        <translation>L&apos;adreça de la sala no és vàlida.</translation>
    </message>
    <message>
        <source>Nickname is not valid.</source>
        <translation>El sobrenom no és vàlid.</translation>
    </message>
    <message>
        <source>New Room</source>
        <translation>Sala nova</translation>
    </message>
    <message>
        <source>Leaving room %1...</source>
        <translation>Sortint de la sala %1...</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation>&amp;Editar</translation>
    </message>
    <message>
        <source>&amp;Update</source>
        <translation>&amp;Actualitzar</translation>
    </message>
    <message>
        <source>Joining room %1...</source>
        <translation>Entrant a al sala %1...</translation>
    </message>
    <message>
        <source>Password, if needed</source>
        <translation>Contrasenya, si cal</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Contrasenya</translation>
    </message>
    <message numerus="yes">
        <source>Loaded %Ln room(s).</source>
        <translation>
            <numerusform>S&apos;ha carregat 1 sala.</numerusform>
            <numerusform>S&apos;han carregat %Ln sales.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>Saved %Ln room(s).</source>
        <translation>
            <numerusform>S&apos;ha guardat 1 sala.</numerusform>
            <numerusform>S&apos;han guardat %Ln sales.</numerusform>
        </translation>
    </message>
    <message>
        <source>Remove room?</source>
        <translation>Treure la sala?</translation>
    </message>
    <message>
        <source>Yes, remove it</source>
        <comment>It = A chat room</comment>
        <translation>Sí, treure-la</translation>
    </message>
    <message>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <source>Do you want to remove room %1 from your list?</source>
        <translation>Vols treure la sala %1 de la teva llista?</translation>
    </message>
    <message>
        <source>Allowed</source>
        <comment>Column header for features allowed</comment>
        <translation>Permès</translation>
    </message>
    <message>
        <source>Allowed to:</source>
        <translation>Es permet:</translation>
    </message>
    <message>
        <source>Reply to commands</source>
        <translation>Respondre a ordres</translation>
    </message>
    <message>
        <source>Greet people</source>
        <translation>Saludar a gent</translation>
    </message>
    <message>
        <source>Converse</source>
        <translation>Conversar</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancel·lar</translation>
    </message>
    <message numerus="yes">
        <source>%Ln room(s)</source>
        <translation>
            <numerusform>1 sala</numerusform>
            <numerusform>%Ln sales</numerusform>
        </translation>
    </message>
    <message>
        <source>There is already a room in the list with this address, in row %1.</source>
        <translation>Ja hi ha una sala a la llista amb aquesta adreça, a la fila %1.</translation>
    </message>
    <message>
        <source>Speak casually</source>
        <comment>Casually as in, from time to time</comment>
        <translation>Parlar de forma casual</translation>
    </message>
</context>
<context>
    <name>RoomSpeakModule</name>
    <message>
        <source>Greetings</source>
        <translation>Salutacions</translation>
    </message>
    <message>
        <source>Casual speak</source>
        <translation type="unfinished">Parlar casualment</translation>
    </message>
    <message>
        <source>Reactions to words</source>
        <translation>Reaccions a paraules</translation>
    </message>
    <message>
        <source>Sentences used to greet people who enter a chatroom.</source>
        <translation>Frases utilitzades per saludar a les persones que entren a les sales de xat.</translation>
    </message>
    <message>
        <source>The %1 variable can be used to insert their names in the sentences.</source>
        <translation>La variable %1 es pot utilitzar per inserir els seus noms a les frases.</translation>
    </message>
    <message>
        <source>Example:</source>
        <translation>Exemple:</translation>
    </message>
    <message>
        <source>Documentation for Perl regular expressions</source>
        <translation>Documentació d&apos;expressions regulars de Perl</translation>
    </message>
    <message>
        <source>Sentences to be sent to chatrooms at random intervals.</source>
        <translation>Frases per a enviar a les sales de xat a intervals aleatoris.</translation>
    </message>
    <message>
        <source>Regular expressions to be matched against messages coming from the chatrooms, and possible messages to send in response.</source>
        <translation>Expressions regulars amb les que comparar els missatges provinents de les sales de xat, i possibles missatges per enviar en resposta.</translation>
    </message>
</context>
<context>
    <name>RoomSpeakTab</name>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Guardar</translation>
    </message>
    <message numerus="yes">
        <source>Loaded %Ln sentence(s) of type %1.</source>
        <translation>
            <numerusform>S&apos;ha carregat 1 frase de tipus %1.</numerusform>
            <numerusform>S&apos;han carregat %Ln frases de tipus %1.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>Saved %Ln sentence(s) of type %1.</source>
        <translation>
            <numerusform>S&apos;ha guardat 1 frase de tipus %1.</numerusform>
            <numerusform>S&apos;han guardat %Ln frases de tipus %1.</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>StringListWidget</name>
    <message>
        <source>Enter text</source>
        <translation>Introdueix el text</translation>
    </message>
    <message>
        <source>Enter a new line of text for the list</source>
        <translation>Introdueix una nova línia de text per la llista</translation>
    </message>
    <message>
        <source>Update the text for this item</source>
        <translation>Actualitza el text d&apos;aquest element</translation>
    </message>
    <message>
        <source>Update the definition of the item</source>
        <translation>Actualitzar la definició de l&apos;element</translation>
    </message>
    <message>
        <source>The item already exists</source>
        <translation>L&apos;element ja existeix</translation>
    </message>
    <message>
        <source>Error: There is already an item in the list with the text %1, and this list does not allow duplicates.</source>
        <translation>Error: Ja hi ha un element a la llista amb el text %1, i aquesta llista no permet duplicats.</translation>
    </message>
    <message>
        <source>Edit the selected item (you can also double-click it or press F2/Enter)</source>
        <translation>Editar l&apos;element seleccionat (també li pots fer doble clic o prémer F2/Intro)</translation>
    </message>
    <message>
        <source>Item text is not valid</source>
        <translation>El text de l&apos;element no es vàlid</translation>
    </message>
    <message>
        <source>Modify text</source>
        <translation>Modifica el text</translation>
    </message>
    <message>
        <source>Add an item to the list (you can also press the Insert key)</source>
        <translation>Afegir un element a la llista (també pots prémer la tecla Insert)</translation>
    </message>
    <message>
        <source>String</source>
        <translation>Cadena</translation>
    </message>
    <message>
        <source>Regular Expression</source>
        <translation>Expressió regular</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Etiquetes</translation>
    </message>
    <message>
        <source>Search...</source>
        <translation>Cercar...</translation>
    </message>
    <message>
        <source>Show All</source>
        <translation>Mostrar-ho tot</translation>
    </message>
    <message>
        <source>Remove the selected item (you can also press Shift+Delete)</source>
        <translation>Treure l&apos;element seleccionat (també pots prémer Majúscules+Supr)</translation>
    </message>
    <message>
        <source>Regular expression is not valid</source>
        <translation>L&apos;expressió regular no és vàlida</translation>
    </message>
    <message>
        <source>Syntax error at position %1:</source>
        <translation>Error de sintaxi a la posició %1:</translation>
    </message>
</context>
<context>
    <name>VariableParser</name>
    <message>
        <source>Unbalanced braces: %1 opening and %2 closing</source>
        <translation>Claus descompensades: %1 que obren i %2 que tanquen</translation>
    </message>
    <message>
        <source>This substitution is unknown, or not valid in this context: %1</source>
        <translation>Aquesta substitució és desconeguda, o no és vàlida en aquest context: %1</translation>
    </message>
    <message>
        <source>Syntax error:</source>
        <translation>Error de sintaxi:</translation>
    </message>
</context>
</TS>
