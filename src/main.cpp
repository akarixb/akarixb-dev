/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include <QApplication>
#include <QTranslator>
#include <QLibraryInfo>

#include <iostream>

#include "mainwindow.h"


void customMessageHandler(QtMsgType type, const QMessageLogContext &context,
                          const QString &msg)
{
    Q_UNUSED(type)
    Q_UNUSED(context)
    Q_UNUSED(msg)

    // Do nothing

    return;
}



int main(int argc, char *argv[])
{
    QApplication akariApp(argc, argv);
    akariApp.setApplicationName(QStringLiteral("AkariXB"));
    akariApp.setApplicationVersion(QStringLiteral("1.0-dev+12"));
    akariApp.setOrganizationName(QStringLiteral("JanCoding"));
    akariApp.setOrganizationDomain(QStringLiteral("jancoding.wordpress.com"));
    // This is mainly needed for window managers under Wayland
    akariApp.setDesktopFileName(QStringLiteral("org.nongnu.akarixb.desktop"));

    std::cout << QString("AkariXB v%1 - JanKusanagi 2015-2024\n"
                         "https://jancoding.wordpress.com/akarixb\n\n")
                 .arg(akariApp.applicationVersion())
                 .toStdString();

    std::cout << QString("- Built with Qt v%1").arg(QT_VERSION_STR)
                                               .toStdString();
    /*
     * REPRODUCIBLEBUILD is defined via .pro file when SOURCE_DATE_EPOCH is
     * defined in the build environment. This is used to avoid hardcoding
     * timestamps and this way make the builds reproducible.
     *
     */
#ifndef REPRODUCIBLEBUILD
    std::cout << QString(" on %1, %2")
                 .arg(__DATE__)
                 .arg(__TIME__).toStdString();
#endif
    std::cout << "\n";

    std::cout << QString("- Running with Qt v%1\n").arg(qVersion())
                                                   .toStdString();
    std::cout << QString("- QXMPP v%1\n").arg(QXmppVersion())
                                         .toStdString();
    std::cout << QString("- Qt Platform plugin in use: %1\n\n")
                 .arg(qApp->platformName()).toStdString();
    std::cout.flush();



    //// Parse command line parameters
    const QStringList cmdLine = qApp->arguments();

    bool debugMode = false;
    bool xmppDebugging = false;
    bool nextParameterIsConfig = false;

    if (!cmdLine.isEmpty())
    {
        for (int param = 1; param < cmdLine.length(); ++param) // Skipping first param,
        {                                                      // program executable/path
            const QString argument = cmdLine.at(param);

            // Help
            if (argument.startsWith("--help", Qt::CaseInsensitive)
                || argument.startsWith("-h", Qt::CaseInsensitive))
            {
                std::cout << "\nHelp:\n";
                std::cout << "    " << argv[0] << " [options]\n\n";
                std::cout << "Options:\n";
                std::cout << "    -c  --config [name]       Use a different "
                             "configuration file and data folder\n";
                std::cout << "    -d  --debug               Show debug messages "
                             "in the terminal\n";
                std::cout << "    -x  --xmppdebug           Show detailed XMPP "
                             "debugging messages (XML stanzas)\n";
                std::cout << "    -v  --version             Show version "
                             "information and exit\n";
                std::cout << "    -h  --help                Show this help and "
                             "exit\n";
                std::cout << "\n";

                return 0; // Exit to shell
            }


            // Version info
            if (argument.startsWith("--version", Qt::CaseInsensitive)
                || argument.startsWith("-v", Qt::CaseInsensitive))
            {
                // Version information already shown
                std::cout << "\n\n"
                             "You can get the latest stable version from:\n"
                             "http://akarixb.nongnu.org\n\n"
                             "And the latest development version from:\n"
                             "https://gitlab.com/akarixb/akarixb-dev"
                             "\n\n";

                return 0;
            }


            // Use different config file, by setting a different applicationName
            if (argument.startsWith("--config", Qt::CaseInsensitive)
                || argument.startsWith("-c", Qt::CaseInsensitive))
            {
                nextParameterIsConfig = true;
            }
            else if (nextParameterIsConfig)
            {
                if (!argument.startsWith("-"))
                {
                    const QString configName = "AkariXB_" + argument.toLower();
                    akariApp.setApplicationName(configName);
                    nextParameterIsConfig = false;

                    std::cout << "Using alternate config file: "
                              << configName.toStdString() << "\n";
                }
                else
                {
                    std::cout << "Error: Alternate config name can't start "
                                 "with a dash: "
                              << argument.toStdString()
                              << "\n\n";

                    return 0;
                }

            }

            // Debug mode
            if (argument.startsWith("--debug", Qt::CaseInsensitive)
                || argument.startsWith("-d", Qt::CaseInsensitive))
            {
                debugMode = true;

                std::cout << "Debug messages enabled\n";
            }

            // XML debug mode
            if (argument.startsWith("--xmppdebug", Qt::CaseInsensitive)
                || argument.startsWith("-x", Qt::CaseInsensitive))
            {
                xmppDebugging = true;
                std::cout << "XMPP debugging enabled\n";
            }

        } // End ranged for
    }


    // Register custom message handler, to hide debug messages unless specified
    if (!debugMode)
    {
        std::cout << "To see debug messages while running, use --debug\n";

#ifdef Q_OS_WIN
        FreeConsole();
#endif

        qInstallMessageHandler(customMessageHandler);
    }




    // Load translation files
    // Get language from LANG environment variable or system's locale
    QString languageString = qgetenv("LANG");
    if (languageString.isEmpty())
    {
        languageString = QLocale::system().name();
    }

    QString languageFile;
    bool languageLoaded;


    QTranslator translatorQt;
    languageFile = QString("qt_%1").arg(languageString);
    std::cout << "\n"
              << "Using Qt translation "
              << QLibraryInfo::location(QLibraryInfo::TranslationsPath).toStdString()
              << "/" << languageFile.toStdString() << "... ";
    languageLoaded = translatorQt.load(languageFile,
                                       QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    if (languageLoaded)
    {
        std::cout << "OK";
        akariApp.installTranslator(&translatorQt);
    }
    else
    {
        std::cout << "Unavailable";
    }


    QTranslator translatorAkariXB;
    languageFile = QString(":/translations/akarixb_%1").arg(languageString);
    std::cout << "\n"
              << "Using program translation "
              << languageFile.toStdString() << "... ";

    languageLoaded = translatorAkariXB.load(languageFile);
    if (languageLoaded)
    {
        std::cout << "OK";
        akariApp.installTranslator(&translatorAkariXB);
    }
    else
    {
        std::cout << "Unavailable";
    }


    std::cout << "\n\n";
    std::cout.flush();


    MainWindow akariWindow;
    akariWindow.toggleMainWindow(true); // onStartup=true

    if (xmppDebugging)
    {
        akariWindow.enableXmppDebug();
    }

    return akariApp.exec();
}
