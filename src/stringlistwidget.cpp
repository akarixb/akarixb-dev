/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "stringlistwidget.h"


StringListWidget::StringListWidget(ListType listType,
                                   QWidget *parent) : QWidget(parent)
{
    m_listType = listType;
    m_forKeyValues = false;
    m_duplicatesAllowed = true;
    if (listType == KeysWithValues)
    {
        m_forKeyValues = true;
        // By default, don't allow duplicates when it's a key-value kind of widget
        m_duplicatesAllowed = false;
    }

    m_lowercaseItems = false;
    m_syntaxCheckEnabled = false; // Enable with setCheckSyntaxEnabled() if relevant
    m_regexpCheckEnabled = false;

    m_columns = 1;
    if (listType == StringsWithTags || listType == RegExpsWithTagsAndReplies)
    {
        m_columns = 2;

        if (listType == StringsWithTags)
        {
            m_headerLabels << tr("String");
        }
        else
        {
            m_headerLabels << tr("Regular Expression");
            m_regexpCheckEnabled = true;
        }

        m_headerLabels << tr("Tags");
    }


    m_tableModel = new QStandardItemModel(this);
    this->setHeader();

    m_filterModel = new QSortFilterProxyModel(this);
    m_filterModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
    m_filterModel->setSourceModel(m_tableModel);

    // The table itself
    m_tableView = new QTableView(this);
    m_tableView->setDragDropMode(QTableView::NoDragDrop);
    m_tableView->setEditTriggers(QTableView::NoEditTriggers);
    m_tableView->setTabKeyNavigation(false);
    m_tableView->setShowGrid(false);
    m_tableView->setSelectionMode(QTableView::SingleSelection);
    m_tableView->setSelectionBehavior(QTableView::SelectRows);
    // ... More adjustments -- TODO
    if (m_columns == 1)
    {
        m_tableView->horizontalHeader()->hide();
    }
    else if (m_columns == 2)
    {
        m_tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);// TMP FIXME
        m_tableView->horizontalHeader()->setSectionsMovable(false);
        m_tableView->horizontalHeader()->setSectionsClickable(false);
    }
    m_tableView->verticalHeader()->hide();
    m_tableView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_tableView->setWordWrap(false);
    m_tableView->setModel(m_filterModel);
    connect(m_tableView, &QTableView::doubleClicked,
            this, &StringListWidget::editItem);
    connect(m_tableView->selectionModel(), &QItemSelectionModel::currentRowChanged,
            this, &StringListWidget::toggleUpDownButtons);


    // Search / filter bar
    m_searchAction = new QAction(this);
    m_searchAction->setShortcut(QKeySequence("Ctrl+F"));
    m_searchAction->setShortcutContext(Qt::WidgetShortcut);
    connect(m_searchAction, &QAction::triggered,
            this, &StringListWidget::focusSearchField);
    m_tableView->addAction(m_searchAction); // Added to the table only, to prevent
                                            // conflicts with sub-StringListWidget

    m_searchLineEdit = new QLineEdit(this);
    m_searchLineEdit->setPlaceholderText(tr("Search...")
                                         + " ("
                                         + m_searchAction->shortcut()
                                                          .toString(QKeySequence::NativeText)
                                         + ")");
    m_searchLineEdit->setClearButtonEnabled(true);
    connect(m_searchLineEdit, &QLineEdit::textChanged,
            this, &StringListWidget::onSearchTextChanged);

    m_cancelSearchAction = new QAction(this);
    m_cancelSearchAction->setShortcut(QKeySequence(Qt::Key_Escape));
    m_cancelSearchAction->setShortcutContext(Qt::WidgetShortcut);
    connect(m_cancelSearchAction, &QAction::triggered,
            this, &StringListWidget::cancelSearch);
    m_searchLineEdit->addAction(m_cancelSearchAction);


    const int iconSize = m_searchLineEdit->sizeHint().height();
    m_searchIconLabel = new QLabel(this);
    m_searchIconLabel->setAlignment(Qt::AlignCenter);
    m_searchIconLabel->setPixmap(QIcon::fromTheme("edit-find",
                                                QIcon(":/images/list-find.png"))
                                       .pixmap(iconSize, iconSize));

    m_matchCountLabel = new QLabel(this);

    m_showAllButton = new QPushButton(QIcon::fromTheme("view-list-icons",
                                                       QIcon(":/images/button-close.png")),
                                      tr("Show All"),
                                      this);
    connect(m_showAllButton, &QPushButton::clicked,
            this, &StringListWidget::cancelSearch);


    // Right side buttons
    m_addButton = new QToolButton(this);
    m_addButton->setIcon(QIcon::fromTheme("list-add",
                                          QIcon(":/images/list-add.png")));
    m_addButton->setToolTip("<b></b>" // HTMLize to get wordwrap
                            + tr("Add an item to the list (you can also press "
                                 "the Insert key)"));
    connect(m_addButton, &QAbstractButton::clicked,
            this, &StringListWidget::addToList);


    m_editButton = new QToolButton(this);
    m_editButton->setIcon(QIcon::fromTheme("document-edit",
                                           QIcon(":/images/button-edit.png")));
    m_editButton->setToolTip("<b></b>"
                             + tr("Edit the selected item (you can also "
                                  "double-click it or press F2/Enter)"));
    connect(m_editButton, &QAbstractButton::clicked,
            this, &StringListWidget::editItem);


    m_removeButton = new QToolButton(this);
    m_removeButton->setIcon(QIcon::fromTheme("list-remove",
                                             QIcon(":/images/list-remove.png")));
    m_removeButton->setToolTip("<b></b>"
                               + tr("Remove the selected item (you can also "
                                    "press Shift+Delete)"));
    connect(m_removeButton, &QAbstractButton::clicked,
            this, &StringListWidget::removeFromList);


    m_moveUpButton = new QToolButton(this);
    m_moveUpButton->setIcon(QIcon::fromTheme("arrow-up",
                                             QIcon(":/images/list-up.png")));
    m_moveUpButton->setToolTip("<b></b>"
                               + tr("Move the selected item up"));
    connect(m_moveUpButton, &QAbstractButton::clicked,
            this, &StringListWidget::moveItemUp);
    m_moveUpButton->setDisabled(true);

    m_moveDownButton = new QToolButton(this);
    m_moveDownButton->setIcon(QIcon::fromTheme("arrow-down",
                                               QIcon(":/images/list-down.png")));
    m_moveDownButton->setToolTip("<b></b>"
                                 + tr("Move the selected item down"));
    connect(m_moveDownButton, &QAbstractButton::clicked,
            this, &StringListWidget::moveItemDown);
    m_moveDownButton->setDisabled(true);


    m_itemCountLabel = new QLabel(this);
    m_itemCountLabel->setAlignment(Qt::AlignCenter);
    m_itemCountLabel->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
    m_itemCountLabel->setMargin(2);



    if (listType == KeysWithValues)
    {
        m_itemDataTextEdit = new QTextEdit(this);
        connect(m_tableView->selectionModel(), &QItemSelectionModel::currentRowChanged,
                this, &StringListWidget::onKeyValueRowChanged);  // Second param ignored

        m_updateButton = new QPushButton(QIcon::fromTheme("dialog-ok-apply",
                                                          QIcon(":/images/button-apply.png")),
                                         QString(),
                                         this);
        m_updateButton->setToolTip("<b></b>"
                                   + tr("Update the definition of the item"));
        connect(m_updateButton, &QAbstractButton::clicked,
                this, &StringListWidget::updateItemData);
    }
    else if (listType == RegExpsWithTagsAndReplies)
    {
        // Secondary basic list for the replies
        m_repliesListWidget = new StringListWidget(StringListWidget::BasicList,
                                                   this);
        // Syntax checking for this one will be enabled after initializing the
        // parent StringListWidget

        connect(m_repliesListWidget, &StringListWidget::itemsChanged,
                this, &StringListWidget::onSubItemsChanged); // Save replies as list item data

        connect(m_tableView->selectionModel(), &QItemSelectionModel::currentRowChanged,
                this, &StringListWidget::onRegExpRowChanged); // Second param ignored
    }


    // Layout
    m_searchLayout = new QHBoxLayout();
    m_searchLayout->addWidget(m_searchIconLabel);
    m_searchLayout->addWidget(m_searchLineEdit);
    m_searchLayout->addWidget(m_matchCountLabel);
    m_searchLayout->addWidget(m_showAllButton);

    m_listLayout = new QVBoxLayout();
    m_listLayout->addWidget(m_tableView);
    m_listLayout->addLayout(m_searchLayout);

    m_buttonsLayout = new QVBoxLayout();
    m_buttonsLayout->addWidget(m_addButton);
    m_buttonsLayout->addWidget(m_editButton);
    m_buttonsLayout->addSpacing(4);
    m_buttonsLayout->addWidget(m_removeButton);
    m_buttonsLayout->addStretch();
    m_buttonsLayout->addWidget(m_moveUpButton);
    m_buttonsLayout->addSpacing(4);
    m_buttonsLayout->addWidget(m_moveDownButton);
    m_buttonsLayout->addStretch();
    m_buttonsLayout->addWidget(m_itemCountLabel);

    m_mainLayout = new QHBoxLayout();
    m_mainLayout->setContentsMargins(0, 0, 0, 0);
    m_mainLayout->addLayout(m_listLayout,       1);
    m_mainLayout->addLayout(m_buttonsLayout);
    if (listType == KeysWithValues)
    {
        m_mainLayout->addWidget(m_itemDataTextEdit, 2);
        m_mainLayout->addWidget(m_updateButton);
    }
    else if (listType == RegExpsWithTagsAndReplies)
    {
        m_mainLayout->addSpacing(4);
        m_mainLayout->addWidget(m_repliesListWidget, 1);
    }

    this->setLayout(m_mainLayout);


    // Keyboard shortcuts to add new items, edit or remove currently selected item
    m_addAction = new QAction(this);
    m_addAction->setShortcut(Qt::Key_Insert);
    m_addAction->setShortcutContext(Qt::WidgetShortcut);
    connect(m_addAction, &QAction::triggered,
            this, &StringListWidget::addToList);
    m_tableView->addAction(m_addAction); // Added to the table only, to prevent
                                         // conflicts with sub-StringListWidget

    m_editAction = new QAction(this);
    QList<QKeySequence> editShortcuts;
    editShortcuts << QKeySequence(Qt::Key_F2)
                  << QKeySequence(Qt::Key_Return)
                  << QKeySequence(Qt::Key_Enter);
    m_editAction->setShortcuts(editShortcuts);
    m_editAction->setShortcutContext(Qt::WidgetShortcut);
    connect(m_editAction, &QAction::triggered,
            this, &StringListWidget::editItem);
    m_tableView->addAction(m_editAction);

    m_removeAction = new QAction(this);
    m_removeAction->setShortcut(QKeySequence("Shift+Del"));
    m_removeAction->setShortcutContext(Qt::WidgetShortcut);
    connect(m_removeAction, &QAction::triggered,
            this, &StringListWidget::removeFromList);
    m_tableView->addAction(m_removeAction);


    // Keyboard shortcuts to go to first/last items with Home/End keys, like basic lists
    m_goFirstAction = new QAction(this);
    m_goFirstAction->setShortcut(Qt::Key_Home);
    m_goFirstAction->setShortcutContext(Qt::WidgetShortcut);
    connect(m_goFirstAction, &QAction::triggered,
            this, &StringListWidget::selectFirstRow);
    m_tableView->addAction(m_goFirstAction);

    m_goLastAction = new QAction(this);
    m_goLastAction->setShortcut(Qt::Key_End);
    m_goLastAction->setShortcutContext(Qt::WidgetShortcut);
    connect(m_goLastAction, &QAction::triggered,
            this, &StringListWidget::selectLastRow);
    m_tableView->addAction(m_goLastAction);


    // Delayed column resize, to be able to account for scrollbars (dis)appearing
    m_resizeTimer = new QTimer(this);
    m_resizeTimer->setInterval(100);
    m_resizeTimer->setSingleShot(true);
    connect(m_resizeTimer, &QTimer::timeout,
            this, &StringListWidget::adjustTable);


    enableRelevantButtons();
    toggleSearchField(); // Hidden until a number of items are added
    updateItemCounter();

    qDebug() << "StringListWidget created";
}


StringListWidget::~StringListWidget()
{
    qDebug() << "StringListWidget destroyed";
}


void StringListWidget::setLowercaseItems(bool lowercaseItems)
{
    m_lowercaseItems = lowercaseItems;
}

void StringListWidget::setDuplicatesAllowed(bool duplicatesAllowed)
{
    m_duplicatesAllowed = duplicatesAllowed;
}


void StringListWidget::setCheckSyntaxEnabled(VariableParser *variableParser,
                                             QStringList extraSubstitutions)
{
    m_syntaxCheckEnabled = true;

    m_variableParser = variableParser;
    m_extraSubstitutions = extraSubstitutions;
}

void StringListWidget::setSublistSyntaxEnabled(VariableParser *variableParser,
                                               QStringList extraSubstitutions)
{
    m_repliesListWidget->setCheckSyntaxEnabled(variableParser,
                                               extraSubstitutions);
}


void StringListWidget::adjustTable()
{
    const int oneThird = m_tableView->viewport()->width() / 3;
    if (m_columns == 1)
    {
        m_tableView->setColumnWidth(0, oneThird * 3);
    }
    else if (m_columns == 2)
    {
        m_tableView->setColumnWidth(0, oneThird * 2);
        m_tableView->setColumnWidth(1, oneThird);
    }
}


void StringListWidget::setHeader()
{
    if (m_columns == 2)
    {
        m_tableModel->setHorizontalHeaderLabels(m_headerLabels);
    }
}


void StringListWidget::clearList()
{
    m_tableModel->clear(); // This removes the header too
    this->setHeader();

    if (m_listType == KeysWithValues)
    {
        m_itemDataTextEdit->clear();
    }

    enableRelevantButtons();
    toggleSearchField();
    m_searchLineEdit->clear();

    this->adjustTable();
    updateItemCounter();
}


void StringListWidget::addStringsToList(const QStringList stringList)
{
    const int rowCountBefore = m_tableModel->rowCount();

    for (const QString string : stringList)
    {
        // FIXME? Check duplicates if needed
        m_tableModel->appendRow(new QStandardItem(string));
    }

    // If there was nothing before adding, select the first row, so all buttons make sense
    if (rowCountBefore == 0)
    {
        m_tableView->selectRow(0);
    }

    enableRelevantButtons();
    updateItemCounter();
    toggleSearchField();
    m_searchLineEdit->clear();

    this->adjustTable();    // Pre-adjustment, to avoid some flickering
    m_resizeTimer->start();
}


QStringList StringListWidget::getStringsFromList()
{
    QStringList stringList;

    for (int counter = 0; counter < m_tableModel->rowCount(); ++counter)
    {
        stringList.append(m_tableModel->item(counter)->text());
    }

    return stringList;
}


void StringListWidget::addItemsToList(QVariantMap stringMap)
{
    const QStringList mapKeys = stringMap.keys();
    for (QString key : mapKeys)
    {
        key = key.trimmed();

        if (m_lowercaseItems)
        {
            key = key.toLower();
        }

        // FIXME? Check for duplicates? Data comes from a file, should be OK

        QStandardItem *item = new QStandardItem(key);
        item->setData(stringMap.value(key), Qt::UserRole + 1);

        m_tableModel->appendRow(item);
    }

    enableRelevantButtons();
    updateItemCounter();
    toggleSearchField();
    m_searchLineEdit->clear();

    this->adjustTable();    // Pre-adjustment, to avoid flickering on delayed resize
    m_resizeTimer->start();

    // Select first line
    m_tableView->selectRow(0);
}

QVariantMap StringListWidget::getItemsFromList()
{
    QVariantMap stringMap;

    for (int counter = 0; counter < m_tableModel->rowCount(); ++counter)
    {
        stringMap.insert(m_tableModel->item(counter)->text(),
                         m_tableModel->item(counter)->data(Qt::UserRole + 1));
    }

    return stringMap;
}


QString StringListWidget::askForValidString(const QString title,
                                            const QString message,
                                            const QString previousValue)
{
    QString newString = previousValue;
    bool stringIsValid;

    do
    {
        stringIsValid = true; // Until some check says otherwise
        bool dialogAccepted;

        // FIXME: this widget should have the option to be set to use
        // QInputDialog::getMultiLineText() for certain uses
        newString = QInputDialog::getText(this,
                                          title + " - AkariXB",
                                          message
                                          + "\n"
                                            "\t\t\t\t\t\t\t\t\t\t" // Ensure extra width
                                            "\n",
                                          QLineEdit::Normal,
                                          newString,
                                          &dialogAccepted);
        if (!dialogAccepted)
        {
            break;
        }


        newString = newString.trimmed();

        if (newString.isEmpty())
        {
            newString = previousValue;
            stringIsValid = false;
        }
        else
        {
            // Set to require lowercase items?
            if (m_lowercaseItems)
            {
                newString = newString.toLower();
            }

            // Duplicates allowed?
            if (!m_duplicatesAllowed
                && this->stringExists(newString) && newString != previousValue)
            {
                QMessageBox::warning(this,
                                     tr("The item already exists") + " - AkariXB",
                                     "<br />"
                                     + tr("Error: There is already an item in "
                                          "the list with the text %1, and this "
                                          "list does not allow duplicates.")
                                       .arg("<b>'" + newString + "'</b>")
                                     + "<br />"
                                       "<br />");
                stringIsValid = false;
            }

            // Syntax checking enabled? Matters here ONLY if this is a single list,
            if (m_syntaxCheckEnabled && !m_forKeyValues) // keys don't need checking
            {
                if (!m_variableParser->checkSyntax(newString, m_extraSubstitutions))
                {
                    this->notifySyntaxError();
                    stringIsValid = false;
                }
            }

            // RegExp check?
            if (m_regexpCheckEnabled)
            {
                QRegularExpression re(newString);
                if (!re.isValid())
                {
                    this->notifyRegExpError(re);
                    stringIsValid = false;
                }
            }
        }
    } while (!stringIsValid);

    return newString;
}


bool StringListWidget::stringExists(QString text)
{
    for (int counter = 0; counter < m_tableModel->rowCount(); ++counter)
    {
        if (m_tableModel->item(counter)->text() == text)
        {
            return true;
        }
    }

    return false;
}


QStandardItem *StringListWidget::getCurrentTableItem()
{
    return m_tableModel->itemFromIndex(m_filterModel
                                       ->mapToSource(m_tableView
                                                     ->currentIndex()));
}


void StringListWidget::enableRelevantButtons()
{
    const bool buttonEnabled = (m_tableModel->rowCount() > 0);

    m_editButton->setEnabled(buttonEnabled);
    m_removeButton->setEnabled(buttonEnabled);

    toggleUpDownButtons();

    if (m_listType == KeysWithValues)
    {
        m_itemDataTextEdit->setEnabled(buttonEnabled);
        m_updateButton->setEnabled(buttonEnabled);
    }
    else if (m_listType == RegExpsWithTagsAndReplies)
    {
        m_repliesListWidget->setEnabled(buttonEnabled);
    }
}


void StringListWidget::toggleSearchField()
{
    bool visible = false;
    if (m_tableModel->rowCount() > 5) // TMP HARDCODED FIXME
    {
        visible = true;
    }

    m_searchIconLabel->setVisible(visible);
    m_searchLineEdit->setVisible(visible);

    m_matchCountLabel->setVisible(false);
    m_showAllButton->setVisible(false);
}


void StringListWidget::setFocusToMainList()
{
    m_tableView->setFocus();
}


void StringListWidget::updateItemCounter()
{
    m_itemCountLabel->setText(QString::number(m_tableModel->rowCount()));
}


void StringListWidget::notifySyntaxError()
{
    QMessageBox::warning(this,
                         tr("Item text is not valid") +  " - AkariXB",
                         QStringLiteral("<br />")
                         + m_variableParser->getLastCheckError()
                         + "&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; " // Ensure some width
                           "&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; "
                           "&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; "
                           "<br />");
}


void StringListWidget::notifyRegExpError(QRegularExpression re)
{
    QMessageBox::warning(this,
                         tr("Regular expression is not valid") +  " - AkariXB",
                         QStringLiteral("<br />")
                         + tr("Syntax error at position %1:")
                           .arg(re.patternErrorOffset() + 1)
                         + " &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;" // Ensure some width
                           " &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;"
                           " &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;"
                           " &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;"
                           " &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;"
                           "<br /><br />"
                         + re.errorString()
                         + QStringLiteral("<br />"));
}


//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// SLOTS //////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


void StringListWidget::addToList()
{
    QString newString = this->askForValidString(tr("Enter text"),
                                                tr("Enter a new line of text "
                                                   "for the list"));

    if (!newString.isEmpty())
    {
        m_tableModel->appendRow(new QStandardItem(newString));

        enableRelevantButtons();
        updateItemCounter();
        toggleSearchField();
        m_searchLineEdit->clear();

        // Select this last line
        m_tableView->selectRow(m_tableModel->rowCount() - 1);

        if (m_listType == KeysWithValues)
        {
            m_itemDataTextEdit->setFocus();
        }
        else if (m_listType == RegExpsWithTagsAndReplies)
        {
            m_repliesListWidget->setFocusToMainList();
        }

        emit itemsChanged();
        m_resizeTimer->start();
    }
}


void StringListWidget::editItem()
{
    QStandardItem *item = this->getCurrentTableItem();

    if (item != 0)
    {
        const QString previousString = item->text().trimmed();

        QString columnName;
        if (m_columns == 2)
        {
            columnName = QStringLiteral(" (")
                         + m_headerLabels.at(item->column())
                         + QStringLiteral(")");
        }

        const QString newString = this->askForValidString(tr("Modify text"),
                                                          tr("Update the text "
                                                             "for this item")
                                                          + columnName,
                                                          previousString);

        if (!newString.isEmpty() && newString != previousString)
        {
            item->setText(newString);
        }

        emit itemsChanged();
    }
}


void StringListWidget::removeFromList()
{
    QStandardItem *item = this->getCurrentTableItem();

    // FIXME: ask for confirmation? Could be optional upon widget creation
    if (item != 0)
    {
        m_tableModel->removeRow(item->row()); // 'item' gets deleted
        updateItemCounter();
        toggleSearchField();

        emit itemsChanged();
        m_resizeTimer->start();
    }

    if (m_tableModel->rowCount() == 0) // No items left
    {
        enableRelevantButtons();
    }

    toggleUpDownButtons();
}


void StringListWidget::moveItemUp()
{
    QStandardItem *item = this->getCurrentTableItem();

    if (item != 0)
    {
        const int previousRow = item->row();
        qDebug() << "**moving item UP, from ROW " << previousRow;
        if (previousRow > 0)
        {
            QList<QStandardItem *> itemsToMove = m_tableModel->takeRow(previousRow);
            m_tableModel->insertRow(previousRow - 1, itemsToMove);
            m_tableView->selectRow(previousRow - 1);

            emit itemsChanged(); // FIXME: needed here?
        }
    }
}

void StringListWidget::moveItemDown()
{
    QStandardItem *item = this->getCurrentTableItem();

    if (item != 0)
    {
        const int previousRow = item->row();
        qDebug() << "**moving item DOWN, from ROW " << previousRow;
        if (previousRow < m_tableModel->rowCount() - 1)
        {
            QList<QStandardItem *> itemsToMove = m_tableModel->takeRow(previousRow);
            m_tableModel->insertRow(previousRow + 1, itemsToMove);
            m_tableView->selectRow(previousRow + 1);

            emit itemsChanged(); // FIXME: needed here?
        }
    }
}

void StringListWidget::toggleUpDownButtons()
{
    QStandardItem *item = this->getCurrentTableItem();
    if (item != 0)
    {
        m_moveUpButton->setEnabled(item->row() != 0);
        m_moveDownButton->setEnabled(item->row() < m_tableModel->rowCount() - 1);
    }
    else
    {
        m_moveUpButton->setEnabled(false);
        m_moveDownButton->setEnabled(false);
    }
}


void StringListWidget::selectFirstRow()
{
    m_tableView->selectRow(0);
}

void StringListWidget::selectLastRow()
{
    m_tableView->selectRow(m_tableModel->rowCount() - 1);
}


void StringListWidget::onSearchTextChanged(const QString searchText)
{
    m_filterModel->setFilterFixedString(searchText);

    // Show number of strings matching the filter, like updateItemCounter()
    if (searchText.isEmpty())
    {
        m_matchCountLabel->hide();
        m_matchCountLabel->setText(QString());
        m_showAllButton->hide();

        if (m_listType == RegExpsWithTagsAndReplies)
        {
            m_repliesListWidget->setEnabled(true);
        }
    }
    else
    {
        const int matchCount = m_filterModel->rowCount();
        m_matchCountLabel->show();
        m_matchCountLabel->setText(QString("(%1)").arg(matchCount));
        m_showAllButton->show();

        m_tableView->selectRow(0);
        if (m_listType == RegExpsWithTagsAndReplies)
        {
            m_repliesListWidget->setEnabled(matchCount > 0);
        }
    }
}


void StringListWidget::focusSearchField()
{
    if (m_searchLineEdit->isHidden())
    {
        m_searchIconLabel->setVisible(true);
        m_searchLineEdit->setVisible(true);
    }

    m_searchLineEdit->setFocus();
}


void StringListWidget::cancelSearch()
{
    m_searchLineEdit->clear();
    m_tableView->setFocus();

    // If cancelling with 0 results, select first item
    if (m_tableView->currentIndex().row() == -1)
    {
        m_tableView->selectRow(0);
    }
}


void StringListWidget::onKeyValueRowChanged(QModelIndex index)
{
    QString itemText;

    if (index.row() != -1)
    {
        // Get item from filtered model, in case there's a search in progress
        const QStandardItem *item = m_tableModel
                                    ->itemFromIndex(m_filterModel
                                                    ->mapToSource(index));
        itemText = item->data(Qt::UserRole + 1).toString();
    }

    m_itemDataTextEdit->setText(itemText);
    m_itemDataTextEdit->setEnabled(true);
    m_updateButton->setEnabled(true);
}


void StringListWidget::onRegExpRowChanged(QModelIndex index)
{
    if (index.row() != -1)
    {
        if (index.column() != 0) // Clicked on second column, so adapt index
        {                        // to point to first column
            index = index.sibling(index.row(), 0);
        }

        // Get from filtered model, in case there's a search in progress
        const QStandardItem *item = m_tableModel
                                    ->itemFromIndex(m_filterModel
                                                    ->mapToSource(index));

        m_repliesListWidget->clearList();
        m_repliesListWidget->addStringsToList(item->data(Qt::UserRole + 1)
                                                   .toStringList());
    }
}


void StringListWidget::onSubItemsChanged()
{
    QStandardItem *item = this->getCurrentTableItem();

    if (item != 0)
    {
        item->setData(m_repliesListWidget->getStringsFromList(),
                      Qt::UserRole + 1);
    }
}


void StringListWidget::updateItemData()
{
    QStandardItem *item = this->getCurrentTableItem();

    if (item != 0)
    {
        const QString itemText = m_itemDataTextEdit->toPlainText();

        // Syntax checking enabled?
        if (m_syntaxCheckEnabled)
        {
            if (!m_variableParser->checkSyntax(itemText, m_extraSubstitutions))
            {
                this->notifySyntaxError();

                m_itemDataTextEdit->setFocus();
                return;
            }
        }

        item->setData(itemText, Qt::UserRole + 1);
    }
}


//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// PROTECTED ///////////////////////////////
//////////////////////////////////////////////////////////////////////////////


void StringListWidget::showEvent(QShowEvent *event)
{
    m_resizeTimer->start();

    event->accept();
}

void StringListWidget::resizeEvent(QResizeEvent *event)
{
    m_resizeTimer->start();

    event->accept();
}

