/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "configdialog.h"


ConfigDialog::ConfigDialog(GlobalObject *globalObject,
                           QWidget *parent) : QWidget(parent)
{
    this->setWindowTitle(tr("Program Configuration") + " - AkariXB");
    this->setWindowIcon(QIcon::fromTheme("configure",
                                         QIcon(":/images/button-configure.png")));
    this->setWindowFlags(Qt::Dialog);
    this->setWindowModality(Qt::WindowModal);
    this->setMinimumSize(440, 320);

    m_globalObject = globalObject;


    // Tab 1: General options
    m_adminJidListWidget = new StringListWidget(StringListWidget::BasicList,
                                                this);
    m_adminJidListWidget->setDuplicatesAllowed(false);
    m_adminJidListWidget->addStringsToList(m_globalObject->getAdminJids());

    m_commandPrefixLineEdit = new QLineEdit(m_globalObject->getCommandPrefix(),
                                            this);
    m_commandPrefixLineEdit->setSizePolicy(QSizePolicy::Maximum,
                                           QSizePolicy::Maximum);


    m_keepAliveIntervalSpinbox = new QSpinBox(this);
    m_keepAliveIntervalSpinbox->setRange(30, 9999);
    m_keepAliveIntervalSpinbox->setSuffix(" " + tr("seconds",
                                                   "Suffix next to a number"));
    m_keepAliveIntervalSpinbox->setValue(m_globalObject->getKeepAliveInterval());

    m_keepAliveTimeoutSpinbox = new QSpinBox(this);
    m_keepAliveTimeoutSpinbox->setRange(10, 600);
    m_keepAliveTimeoutSpinbox->setSuffix(" " + tr("seconds",
                                                  "Suffix next to a number"));
    m_keepAliveTimeoutSpinbox->setValue(m_globalObject->getKeepAliveTimeout());

    // Sync both KeepAlive values: timeout should be lower
    connect(m_keepAliveIntervalSpinbox, SIGNAL(valueChanged(int)),
             this, SLOT(onKeepAliveChanges(int)));
    connect(m_keepAliveTimeoutSpinbox, SIGNAL(valueChanged(int)),
             this, SLOT(onKeepAliveChanges(int)));


    m_generalOptionsLayout = new QFormLayout();
    m_generalOptionsLayout->addRow(tr("Administrator JIDs"),
                                   m_adminJidListWidget);
    m_generalOptionsLayout->addRow(tr("Command prefix"),
                                   m_commandPrefixLineEdit);
    m_generalOptionsLayout->addRow(tr("Connection ping interval"),
                                   m_keepAliveIntervalSpinbox);
    m_generalOptionsLayout->addRow(tr("Connection timeout"),
                                   m_keepAliveTimeoutSpinbox);


    m_generalOptionsWidget = new QWidget(this);
    m_generalOptionsWidget->setLayout(m_generalOptionsLayout);


    // Tab 2: Notification options
    m_notifyNewChatsCheckbox = new QCheckBox(this);
    m_notifyNewChatsCheckbox->setChecked(m_globalObject->getNotifyNewChats());
    m_notifyDisconnectionCheckbox = new QCheckBox(this);
    m_notifyDisconnectionCheckbox->setChecked(m_globalObject->getNotifyDisconnection());


    m_notificationOptionsLayout = new QFormLayout();
    m_notificationOptionsLayout->addRow(tr("Notify new chats"),
                                        m_notifyNewChatsCheckbox);
    m_notificationOptionsLayout->addRow(tr("Notify disconnection"),
                                        m_notifyDisconnectionCheckbox);

    m_notificationOptionsWidget = new QWidget(this);
    m_notificationOptionsWidget->setLayout(m_notificationOptionsLayout);



    // Tab 3: System tray options
    m_hideWindowCheckbox = new QCheckBox(this);
    m_hideWindowCheckbox->setChecked(m_globalObject->getHideInTray());
    m_customIconCheckbox = new QCheckBox(this);
    m_customIconButton = new QPushButton(QIcon::fromTheme("user-identity",
                                                          QIcon(":/images/no-avatar.png")),
                                         tr("Select..."),
                                         this);
    m_customIconLastDir = QDir::homePath();
    this->setCustomIconButtonImage(m_globalObject->getCustomTrayIconFilename(),
                                   false); // Don't report errors
    m_customIconButton->setDisabled(true);
    connect(m_customIconButton, &QPushButton::clicked,
            this, &ConfigDialog::pickCustomIconFile);

    connect(m_customIconCheckbox, &QCheckBox::toggled,
            m_customIconButton, &QPushButton::setEnabled);

    m_customIconCheckbox->setChecked(m_globalObject->getUseCustomTrayIcon());


    m_customIconLayout = new QHBoxLayout();
    m_customIconLayout->addWidget(m_customIconCheckbox);
    m_customIconLayout->addWidget(m_customIconButton);

    m_systrayOptionsLayout = new QFormLayout();
    m_systrayOptionsLayout->addRow(tr("Hide window on startup"),
                                   m_hideWindowCheckbox);
    m_systrayOptionsLayout->addRow(tr("Use a custom icon"),
                                   m_customIconLayout);

    m_systrayOptionsWidget = new QWidget(this);
    m_systrayOptionsWidget->setLayout(m_systrayOptionsLayout);



    // Tab widget
    m_tabWidget = new QTabWidget(this);
    m_tabWidget->addTab(m_generalOptionsWidget,
                        QIcon::fromTheme("preferences-other",
                                         QIcon(":/images/button-configure.png")),
                        tr("General"));

    m_tabWidget->addTab(m_notificationOptionsWidget,
                        QIcon::fromTheme("preferences-desktop-notification",
                                         QIcon(":/images/button-online.png")),
                        tr("Notifications"));

    m_tabWidget->addTab(m_systrayOptionsWidget,
                        QIcon::fromTheme("configure-toolbars",
                                         QIcon(":/images/button-configure.png")),
                        tr("System Tray"));


    m_saveButton = new QPushButton(QIcon::fromTheme("document-save",
                                                    QIcon(":/images/button-save.png")),
                                   tr("&Save"),
                                   this);
    connect(m_saveButton, &QAbstractButton::clicked,
            this, &ConfigDialog::saveSettings);


    m_cancelButton = new QPushButton(QIcon::fromTheme("dialog-cancel",
                                                      QIcon(":/images/button-cancel.png")),
                                     tr("Cancel"),
                                     this);
    connect(m_cancelButton, &QAbstractButton::clicked,
            this, &QWidget::hide);



    // Layout
    m_bottomButtonBox = new QDialogButtonBox(this);
    m_bottomButtonBox->addButton(m_saveButton, QDialogButtonBox::AcceptRole);
    m_bottomButtonBox->addButton(m_cancelButton, QDialogButtonBox::RejectRole);

    m_mainLayout = new QVBoxLayout();
    m_mainLayout->addWidget(m_tabWidget);
    m_mainLayout->addWidget(m_bottomButtonBox);
    this->setLayout(m_mainLayout);

    qDebug() << "ConfigDialog created";
}


ConfigDialog::~ConfigDialog()
{
    qDebug() << "ConfigDialog created";
}


void ConfigDialog::setCustomIconButtonImage(const QString filename,
                                            bool reportErrors)
{
    if (filename.isEmpty())
    {
        return;
    }

    QPixmap iconPixmap = QPixmap(filename);
    if (!iconPixmap.isNull())
    {
        m_customIconFilename = filename;
        m_customIconButton->setIcon(QIcon(m_customIconFilename));
        m_customIconButton->setToolTip("<b></b>"
                                       + m_customIconFilename);
    }
    else
    {
        if (reportErrors)
        {
            QMessageBox::warning(this,
                                 tr("Invalid icon"),
                                 tr("The selected image is not valid."));
        }
        else
        {
            m_customIconButton->setToolTip("<b></b>"
                                           + tr("Icon file %1 does not "
                                                "exist").arg(filename));
        }

        qDebug() << "Invalid tray icon file selected" << filename;
    }
}


//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// SLOTS //////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


void ConfigDialog::onKeepAliveChanges(int newValue)
{
    Q_UNUSED(newValue)

    const int kaInterval = m_keepAliveIntervalSpinbox->value();

    if (m_keepAliveTimeoutSpinbox->value() >= kaInterval)
    {
        m_keepAliveTimeoutSpinbox->setValue(kaInterval - 1);
    }
}


void ConfigDialog::pickCustomIconFile()
{
    QString newIconFN = QFileDialog::getOpenFileName(this,
                                                     tr("Select custom icon"),
                                                     m_customIconLastDir,
                                                     tr("Image files")
                                                     + " (*.png *.jpg *.jpe "
                                                       "*.jpeg *.gif);;"
                                                     + tr("All files")
                                                     + " (*)");

    if (!newIconFN.isEmpty())
    {
        QFileInfo fileInfo(newIconFN);
        m_customIconLastDir = fileInfo.path();

        this->setCustomIconButtonImage(newIconFN);
    }
}



void ConfigDialog::saveSettings()
{
    QSettings settings;
    settings.beginGroup("Configuration");


    settings.setValue("adminJids",
                      m_adminJidListWidget->getStringsFromList());
    settings.setValue("commandPrefix",
                      m_commandPrefixLineEdit->text().trimmed());
    settings.setValue("keepAliveInterval",
                      m_keepAliveIntervalSpinbox->value());
    settings.setValue("keepAliveTimeout",
                      m_keepAliveTimeoutSpinbox->value());
    m_globalObject->syncGeneralSettings(m_adminJidListWidget->getStringsFromList(),
                                        m_commandPrefixLineEdit->text().trimmed(),
                                        m_keepAliveIntervalSpinbox->value(),
                                        m_keepAliveTimeoutSpinbox->value());

    settings.setValue("notifyNewChats",
                      m_notifyNewChatsCheckbox->isChecked());
    settings.setValue("notifyDisconnection",
                      m_notifyDisconnectionCheckbox->isChecked());
    m_globalObject->syncNotificationSettings(m_notifyNewChatsCheckbox->isChecked(),
                                             m_notifyDisconnectionCheckbox->isChecked());

    settings.setValue("hideInTray",
                      m_hideWindowCheckbox->isChecked());
    settings.setValue("useCustomTrayIcon",
                      m_customIconCheckbox->isChecked());
    settings.setValue("customTrayIconFilename",
                      m_customIconFilename);
    m_globalObject->syncSystraySettings(m_hideWindowCheckbox->isChecked(),
                                        m_customIconCheckbox->isChecked(),
                                        m_customIconFilename);


    settings.endGroup();
    settings.sync();
    m_globalObject->addToLog(tr("Program configuration saved."),
                             true,
                             GlobalObject::LogGood);

    emit settingsSaved();
    this->hide();
}


//////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// PROTECTED ////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


/*
 * Handle closing dialog via ESC.
 * Using keyPressEvent() instead of a QAction with a shortcut, to prevent
 * shortcut conflicts with StringListWidget's own ESC shortcut to clear
 * the search field.
 *
 */
void ConfigDialog::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Escape)
    {
        event->accept();
        this->hide();
    }
}
