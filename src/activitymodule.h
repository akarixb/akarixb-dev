/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef ACTIVITYMODULE_H
#define ACTIVITYMODULE_H

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QScrollArea>
#include <QListWidget>
#include <QLabel>
#include <QLineEdit>
#include <QComboBox>
#include <QSpinBox>
#include <QTimeEdit>
#include <QDateEdit>
#include <QCalendarWidget>
#include <QCheckBox>
#include <QPushButton>
#include <QMessageBox>

#include <QDebug>

#include "globalobject.h"
#include "activityhandler.h"
#include "stringlistwidget.h"
#include "variableparser.h"
#include "datafile.h"


class ActivityModule : public QWidget
{
    Q_OBJECT

public:
    explicit ActivityModule(ActivityHandler *activityHandler,
                            GlobalObject *globalObject,
                            QWidget *parent = nullptr);
    ~ActivityModule();

    void loadActivities();

    bool validateActivity();


signals:


public slots:
    void onNameChanged(QString newName);

    void addActivity();
    void removeActivity();

    void onActivitySelected(int row);

    void saveActivity();

    void syncActivities();


private:
    QHBoxLayout *m_mainLayout;

    QVBoxLayout *m_leftLayout;
    QPushButton *m_syncActivitiesButton;
    QListWidget *m_activityListWidget;
    QPushButton *m_addActivityButton;
    QPushButton *m_removeActivityButton;


    QScrollArea *m_rightScrollArea;
    QWidget *m_rightWidget;
    QVBoxLayout *m_rightLayout;
    QFormLayout *m_formLayout;

    QLineEdit *m_nameLineEdit;

    QSpinBox *m_timesPerDaySpinbox;
    QSpinBox *m_probabilitySpinbox;

    QHBoxLayout *m_timeLayout;
    QTimeEdit *m_minTimeEdit;
    QTimeEdit *m_maxTimeEdit;

    QHBoxLayout *m_dateLayout;
    QDateEdit *m_minDateEdit;
    QDateEdit *m_maxDateEdit;

    QHBoxLayout *m_weekdaysLayout;
    QCheckBox *m_wdayMonCheckbox;
    QCheckBox *m_wdayTueCheckbox;
    QCheckBox *m_wdayWedCheckbox;
    QCheckBox *m_wdayThuCheckbox;
    QCheckBox *m_wdayFriCheckbox;
    QCheckBox *m_wdaySatCheckbox;
    QCheckBox *m_wdaySunCheckbox;

    QHBoxLayout *m_durationLayout;
    QSpinBox *m_minDurationSpinbox;
    QSpinBox *m_maxDurationSpinbox;

    QComboBox *m_statusTypeCombobox;
    StringListWidget *m_statusMessagesListWidget;

    QSpinBox *m_availabilitySpinbox;

    QHBoxLayout *m_reactionLayout;
    QSpinBox *m_minReactionSpinbox;
    QSpinBox *m_maxReactionSpinbox;

    StringListWidget *m_descriptionListWidget;

    StringListWidget *m_msgBeforeListWidget;
    StringListWidget *m_msgAfterListWidget;

    QVBoxLayout *m_msgRecipientsLayout;
    QHBoxLayout *m_recipientsTopLayout;
    QComboBox *m_msgToRoomsCombobox;
    QCheckBox *m_msgToActiveChatsCheckbox;
    QHBoxLayout *m_recipientsBottomLayout;
    QLabel *m_specificJidsText;
    QLineEdit *m_specificJidsLineEdit;

    QPushButton *m_saveActivityButton;


    DataFile *m_dataFile;
    ActivityHandler *m_activityHandler;
    VariableParser *m_variableParser;
    GlobalObject *m_globalObject;
};

#endif // ACTIVITYMODULE_H
