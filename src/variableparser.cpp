/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "variableparser.h"


VariableParser::VariableParser(QObject *parent) : QObject(parent)
{
    m_ownNickname = QStringLiteral("AkariXB");

    qDebug() << "VariableParser created";
}

VariableParser::~VariableParser()
{
    qDebug() << "VariableParser destroyed";
}


void VariableParser::setOwnNickname(QString newNickname)
{
    m_ownNickname = newNickname;
}



QString VariableParser::getParsed(QString originalString)
{
    QString parsedString = originalString.trimmed();

    parsedString.replace(QStringLiteral("%smiley%"),
                         QStringLiteral("{;)|:)|=)|:D|xD}"));

    parsedString.replace(QStringLiteral("%happyemoji%"),
                         QString::fromUtf8("{"
                                           "\360\237\230\203|" // Smiling open mouth
                                           "\360\237\230\201|" // Grinning
                                           "\360\237\230\216|" // With sunglasses
                                           "\360\237\230\205|" // Smiling open mouth cold sweat
                                           "\360\237\230\202|" // Tears of joy
                                           "\360\237\230\217|" // Smirking
                                           "\360\237\230\211|" // Winking
                                           "\360\237\244\243|" // Rolling on floor
                                           "\360\237\230\206"  // Tightly-closed eyes
                                           "}"));
    parsedString.replace(QStringLiteral("%sademoji%"),
                         QString::fromUtf8("{"
                                           "\360\237\230\225|" // Confused
                                           "\360\237\230\237|" // Worried
                                           "\360\237\230\236|" // Disappointed
                                           "\360\237\231\204|" // Rolling eyes
                                           "\360\237\230\247|" // Anguished
                                           "\360\237\230\260|" // Open mouth cold sweat
                                           "\360\237\230\261|" // Screaming in fear
                                           "\360\237\230\242|" // Crying
                                           "\360\237\230\255"  // Loudly crying
                                           "}"));


    /*
     * Replace {a|b|c|d} with one of the options, selected randomly
     *
     * FIXME: Handle nested cases like { a | { b | c } | d }
     *
     */
    QRegularExpression choiceRE(QStringLiteral("\\{.+?\\}")); // "?" for non-greedy match
    QRegularExpressionMatch choiceMatch;

    int pos = 0;
    while ((choiceMatch = choiceRE.match(parsedString, pos)).hasMatch())
    {
        pos = choiceMatch.capturedStart();
        QString matchedString = choiceMatch.captured();
        qDebug() << "getParsed(); matched string:" << matchedString;
        QString chosenString;
        if (matchedString.contains(QStringLiteral("|")))
        {
            matchedString.remove(0,  1); // Remove {
            matchedString.remove(-1, 1); // Remove }
            // Split on pipe, choose one
            const QStringList choices = matchedString.split(QStringLiteral("|"));
            const int choiceNum = QRandomGenerator::global()->bounded(choices.length());
            chosenString = choices.at(choiceNum);

            // Replace at specific position to avoid identical choices having the same result
            parsedString.replace(pos,
                                 choiceMatch.captured().length(),
                                 chosenString);
        }
        else
        {
            pos += matchedString.length(); // No pipe, so advance the {something} characters
        }

        if (!chosenString.isEmpty()) // There was a pipe, and something not empty was chosen
        {
            pos += chosenString.length();
        }
    }


    // Replace a few other variables or special characters
    parsedString.replace(QStringLiteral("%line%"),    QStringLiteral("\n"));
    parsedString.replace(QStringLiteral("%ownnick%"), m_ownNickname);

    return parsedString;
}


bool VariableParser::checkSyntax(QString stringToTest,
                                 QStringList extraSubstitutions)
{
    m_lastCheckError.clear();

    // Very basic check for {a|b|c} constructs
    const int braceOpenerCount = stringToTest.count(QStringLiteral("{"));
    const int braceCloserCount = stringToTest.count(QStringLiteral("}"));
    if (braceOpenerCount != braceCloserCount)
    {
        // FIXME: this only matters if there are one or more pipes between them
        m_lastCheckError = tr("Unbalanced braces: %1 opening and %2 closing")
                           .arg(braceOpenerCount)
                           .arg(braceCloserCount);
        return false;
    }


    const QStringList validWords = QStringList{ "%smiley%",
                                                "%happyemoji%",
                                                "%sademoji%",
                                                "%line%",
                                                "%ownnick%"
                                              }
                                   + extraSubstitutions; // Add context-enabled words,
                                                         // like %user%, from optional param



    const QRegularExpression substitutionsRE(QStringLiteral("\%.+?\%")); // "?" for non-greedy match
    QRegularExpressionMatch substitutionMatch;

    int pos = 0;
    while ((substitutionMatch = substitutionsRE.match(stringToTest, pos)).hasMatch())
    {
        pos = substitutionMatch.capturedStart();
        const QString matchedString = substitutionMatch.captured();
        qDebug() << "checkSyntax(); matched string:" << matchedString;

        if (!validWords.contains(matchedString))
        {
            m_lastCheckError = tr("This substitution is unknown, "
                                  "or not valid in this context: %1")
                               .arg("<big><b>" + matchedString + "</b></big>");
            return false;
        }

        pos += matchedString.length(); // Advance the %something% characters
    }


    return true;
}


QString VariableParser::getLastCheckError()
{
    if (!m_lastCheckError.isEmpty())
    {
        return tr("Syntax error:")
               + QStringLiteral("<br /><br />")
               + m_lastCheckError;
    }

    return m_lastCheckError;
}

