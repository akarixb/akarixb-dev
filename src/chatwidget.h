/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef CHATWIDGET_H
#define CHATWIDGET_H

#include <QWidget>
#include <QVBoxLayout>
#include <QTextBrowser>
#include <QLineEdit>
#include <QShowEvent>
#include <QScrollBar>

#include <QDebug>

#include <QXmppClient.h>
#include <QXmppMucManager.h>
#include <QXmppMessage.h>


#include "globalobject.h"
#include "helpers.h"


class ChatWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ChatWidget(GlobalObject *globalObject, QWidget *parent = nullptr);
    ~ChatWidget();

    void setRoom(QXmppMucRoom *room);
    QString getRoomJid();

    void setContact(QString jid);
    QString getContactJid();
    QString getContactBareJid();
    void setContactNick(QString nickname);

    void setChatTitle(QString title);
    QString getChatTitle();

    int getUnreadCount();

    void focusLineEdit();


signals:
    void roomJoined(QString jid);
    void roomLeft(QString jid);

    void messagesRead(QString chatTitle, int count);


public slots:
    void appendMessage(QXmppMessage message);
    void appendOutgoingMessage(QString messageText);
    void sendMessage();

    // MUC related
    void onParticipantJoined(QString jid);
    void onParticipantLeft(QString jid);

    void onJoined();
    void onLeft();
    void onKicked(QString jid, QString reason);

    void onRoomNameChanged(QString newName);


protected:
    virtual void showEvent(QShowEvent *event);


private:
    QVBoxLayout *m_mainLayout;

    QTextBrowser *m_chatBrowser;
    QLineEdit *m_messageLineEdit;

    bool m_isMuc;
    QXmppMucRoom *m_muc;
    QString m_mucJid;
    QString m_kickedFromMucMessage;

    QString m_chatTitle;

    QString m_contactJid;
    QString m_contactNick;

    int m_unreadCount;
    QDate m_lastTimestamp;

    QXmppClient *m_xmppClient;
    GlobalObject *m_globalObject;
};

#endif // CHATWIDGET_H
