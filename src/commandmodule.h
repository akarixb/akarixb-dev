/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef COMMANDMODULE_H
#define COMMANDMODULE_H

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QScrollArea>
#include <QListWidget>
#include <QLineEdit>
#include <QComboBox>
#include <QSpinBox>
#include <QCheckBox>
#include <QPushButton>
#include <QMessageBox>
#include <QLabel>
#include <QFileDialog>

#include <QDebug>


#include "globalobject.h"
#include "commandhandler.h"
#include "stringlistwidget.h"
#include "datafile.h"
#include "variableparser.h"


class CommandModule : public QWidget
{
    Q_OBJECT

public:
    explicit CommandModule(CommandHandler *commandHandler,
                           GlobalObject *globalObject,
                           QWidget *parent = nullptr);
    ~CommandModule();

    void loadCommands();

    bool validateCommand(int commandType);


signals:


public slots:
    void onTriggerChanged(QString newTrigger);

    void findFile();


    void addCommand();
    void removeCommand();

    void onCommandSelected(int row);

    void onTypeChanged(int commandType);

    void saveCommand();

    void syncCommands();


private:
    QHBoxLayout *m_mainLayout;

    QVBoxLayout *m_leftLayout;
    QPushButton *m_syncCommandsButton;
    QListWidget *m_commandListWidget;
    QPushButton *m_addCommandButton;
    QPushButton *m_removeCommandButton;


    QScrollArea *m_rightScrollArea;
    QWidget *m_rightWidget;
    QVBoxLayout *m_rightLayout;
    QFormLayout *m_formLayout;
    QLineEdit *m_triggerLineEdit;
    QLineEdit *m_helpLineEdit;
    QSpinBox  *m_accessLevelSpinbox;
    QComboBox *m_typeCombobox;
    QLabel *m_typeExplanationLabel;


    QVBoxLayout *m_typeLayout;
    QLineEdit *m_staticLineEdit;
    StringListWidget *m_randomListWidget;

    QHBoxLayout *m_filenameLayout;
    QLabel *m_filenameLabel;
    QString m_textFilename;
    QPushButton *m_filenameButton;

    StringListWidget *m_keywordListWidget;

    QHBoxLayout *m_programLayout;
    QLineEdit *m_programLineEdit;
    QComboBox *m_runDetachedCombobox;

    QLineEdit *m_aliasLineEdit;


    QLineEdit *m_outputLineEdit;
    QLabel *m_outputPatternHelpLabel;
    QCheckBox *m_privateReplyCheckbox;


    QPushButton *m_saveCommandButton;


    DataFile *m_dataFile;
    CommandHandler *m_commandHandler;
    VariableParser *m_variableParser;
    GlobalObject *m_globalObject;
};

#endif // COMMANDMODULE_H
