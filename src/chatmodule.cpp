/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "chatmodule.h"

ChatModule::ChatModule(GlobalObject *globalObject,
                       QWidget *parent) : QWidget(parent)
{
    m_globalObject = globalObject;
    connect(m_globalObject, &GlobalObject::privateMessageSent,
            this, &ChatModule::addOutgoingMessage);
    connect(m_globalObject, &GlobalObject::conversationTabRequested,
            this, &ChatModule::startConversation);

    m_xmppClient = m_globalObject->getXmppClient();


    m_unreadCount = 0;


    m_tabWidget = new QTabWidget(this);
    m_tabWidget->setTabsClosable(true);
    // Tabs not movable for now; requires adjustments to avoid crashes
    // m_tabWidget->setMovable(true);
    connect(m_tabWidget, &QTabWidget::tabCloseRequested,
            this, &ChatModule::closeTab);

    m_noChatsLabel = new QLabel("<br /><br /><br />"
                                "<h2>" + tr("There are no active chats")
                                + "</h2>"
                                "<br /><br /><br />",
                                this);
    m_noChatsLabel->setAlignment(Qt::AlignCenter);


    m_closeTabAction = new QAction(this);
    m_closeTabAction->setShortcut(QKeySequence("Ctrl+W"));
    connect(m_closeTabAction, &QAction::triggered,
            this, &ChatModule::closeCurrentTab);
    this->addAction(m_closeTabAction);


    m_mainLayout = new QVBoxLayout();
    m_mainLayout->setContentsMargins(0, 0, 0, 0);
    m_mainLayout->addWidget(m_noChatsLabel);
    m_mainLayout->addWidget(m_tabWidget);
    this->setLayout(m_mainLayout);

    qDebug() << "ChatModule created";
}


ChatModule::~ChatModule()
{
    qDebug() << "ChatModule destroyed";
}


void ChatModule::setTabTitle(int tabNumber, QString name, int unread)
{
    if (unread > 0)
    {
        name.append(QString("  (%1)").arg(unread));
    }

    m_tabWidget->setTabText(tabNumber, name);
}

/*
 * Find or create a new tab with a ChatWidget for a given JID
 *
 */
int ChatModule::tabForJid(QString jid, int messageDirection)
{
    const QString bareJid = jid.split("/").first();

    // If this is a private message via MUC room
    bool fromMuc = false;
    if (m_globalObject->getJoinedRooms().contains(bareJid))
    {
        // Will use full JID as title, and resource as nickname
        fromMuc = true;
    }

    int tabCount = 0;
    bool tabFound = false;
    for (; tabCount < m_openedChats.count(); ++tabCount)
    {
        if (fromMuc)
        {
            if (m_openedChats.at(tabCount)->getContactJid() == jid)
            {
                tabFound = true;
                break;
            }
        }
        else
        {
            if (m_openedChats.at(tabCount)->getContactBareJid() == bareJid)
            {
                tabFound = true;
                break;
            }
        }
    }


    ChatWidget *chatWidget;
    if (tabFound)
    {
        chatWidget = m_openedChats.at(tabCount);
    }
    else
    {
        chatWidget = new ChatWidget(m_globalObject, this);
        m_openedChats.append(chatWidget);

        m_tabWidget->addTab(chatWidget,
                            QIcon::fromTheme("user-identity",
                                             QIcon(":/images/no-avatar.png")),
                            bareJid);

        chatWidget->setContact(jid);

        QString tabTitle;
        if (fromMuc)
        {
            tabTitle = jid; // Full JID, room@server/nickname
            QStringList contactNick = jid.split("/");
            contactNick.removeFirst(); // Can't just use .last(), since nickname could contain "/"
            chatWidget->setContactNick(contactNick.join("/"));
        }
        else
        {
            tabTitle = bareJid; // Username part of JID
        }

        chatWidget->setChatTitle(tabTitle);

        // It's important to connect() *after* the tab has been added
        connect(chatWidget, &ChatWidget::messagesRead,
                this, &ChatModule::resetTabTitle);


        // Log and notify only if started by contacts, not by us
        if (messageDirection == ChatModule::MessageIncoming)
        {
            m_globalObject->addToLog(tr("Chat started by %1.",
                                        "%1 = A nickname")
                                     .arg("'<b>" + jid + "</b>'"));

            if (m_globalObject->getNotifyNewChats())
            {
                m_globalObject->showNotification(tr("New chat"),
                                                 tr("A new chat was started "
                                                    "by %1.")
                                                 .arg(jid.toHtmlEscaped()));
            }
        }

        // TODO: optional email/XMPP notification for this event

        // Hide message about no active chats
        m_noChatsLabel->hide();
    }


    return tabCount;
}



//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// SLOTS //////////////////////////////////
//////////////////////////////////////////////////////////////////////////////



void ChatModule::addChatTab(QXmppMessage message)
{
    const int tabNumber = this->tabForJid(message.from(),
                                          ChatModule::MessageIncoming);

    ChatWidget *currentChatWidget = m_openedChats.at(tabNumber);
    currentChatWidget->appendMessage(message);


    // Update title of that tab, not necessarily current tab
    this->setTabTitle(tabNumber,
                      currentChatWidget->getChatTitle(),
                      currentChatWidget->getUnreadCount());

    if (!currentChatWidget->isVisible())
    {
        ++m_unreadCount;
        emit unreadCountChanged(m_unreadCount);
    }
}


void ChatModule::resetTabTitle(QString chatTitle, int count)
{
    m_unreadCount -= count;
    emit unreadCountChanged(m_unreadCount); // Update total chats unread count

    // Find tab by raw title, and reset title
    for (int currentTab = 0;  currentTab < m_tabWidget->count(); ++currentTab)
    {
        if (m_openedChats.at(currentTab)->getChatTitle() == chatTitle)
        {
            this->setTabTitle(currentTab,
                              chatTitle,
                              0); // Clearly, no more unread remain in that tab
            break;
        }
    }
}


void ChatModule::closeTab(int tabIndex)
{
    // Update unread counts: tab can be closed without activating it previously
    m_unreadCount -= m_openedChats.at(tabIndex)->getUnreadCount();
    emit unreadCountChanged(m_unreadCount);

    // It's important to remove the tab before deleting the widget on it
    m_tabWidget->removeTab(tabIndex);

    m_openedChats.at(tabIndex)->deleteLater();
    m_openedChats.removeAt(tabIndex);

    if (m_openedChats.count() < 1)
    {
        m_noChatsLabel->show();
    }
}

void ChatModule::closeCurrentTab()
{
    const int currentTab = m_tabWidget->currentIndex();
    if (currentTab != -1)
    {
        this->closeTab(currentTab);
    }
}


void ChatModule::addOutgoingMessage(QString toJid, QString message)
{
    qDebug() << "Sending private message to" << toJid << ": " << message;

    const int tabNumber = this->tabForJid(toJid, ChatModule::MessageOutgoing);

    ChatWidget *currentChatWidget = m_openedChats.at(tabNumber);
    currentChatWidget->appendOutgoingMessage(message);


    // Update title of that tab, not necessarily current tab
    this->setTabTitle(tabNumber,
                      currentChatWidget->getChatTitle(),
                      currentChatWidget->getUnreadCount());
}


void ChatModule::startConversation(QString toJid)
{
    qDebug() << "Starting conversation with" << toJid;

    const int tabNumber = this->tabForJid(toJid, ChatModule::MessageOutgoing);
    m_tabWidget->setCurrentIndex(tabNumber);
    m_openedChats.at(tabNumber)->focusLineEdit();
}
