/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef COMMANDHANDLER_H
#define COMMANDHANDLER_H

#include <QObject>
#include <QApplication>
#include <QVariantList>
#include <QFile>
#include <QProcess>
#include <QRandomGenerator>

#include <QDebug>

#include <QXmppClient.h>
#include <QXmppMucManager.h>
#include <QXmppMessage.h>

#include "globalobject.h"
#include "variableparser.h"
#include "helpers.h"


class CommandHandler : public QObject
{
    Q_OBJECT

public:
    explicit CommandHandler(GlobalObject *globalObject,
                            QObject *parent = nullptr);
    ~CommandHandler();


    void processCommand(QXmppMessage message);
    QVariantMap parseCommand(QString commandString, int userAccess,
                             QString callingUser);

    void setCommandData(QVariantList dataList);

    QStringList getCommandList(int accessLevel);


signals:


public slots:


private:
    QXmppClient *m_xmppClient;
    QXmppMucManager *m_mucManager;

    QVariantList m_commandDataList;

    GlobalObject *m_globalObject;
    VariableParser *m_variableParser;
};

#endif // COMMANDHANDLER_H
