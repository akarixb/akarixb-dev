/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "logmodule.h"


LogModule::LogModule(QWidget *parent) : QTextBrowser(parent)
{
    this->setReadOnly(true);

    // See if we can use the nice "cross mark" symbol, or a more common fallback
    QFontMetrics fontMetrics(this->font());
    if (fontMetrics.inFont(QChar(0x274C)))
    {
        // CROSS MARK
        m_crossSymbolString = QString::fromUtf8("\342\235\214 ");
    }
    else
    {
        // HEAVY BALLOT X as fallback (or regular BALLOT X)
        m_crossSymbolString = QString::fromUtf8("\342\234\230 ");
    }

    this->addToLog(tr("AkariXB v%1 started.",
                      "%1 = Version number").arg(qApp->applicationVersion()),
                   0); // 0=LogNormal, FIXME

    qDebug() << "LogModule created";
}

LogModule::~LogModule()
{
    qDebug() << "LogModule destroyed";
}



//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// SLOTS //////////////////////////////////
//////////////////////////////////////////////////////////////////////////////



void LogModule::addToLog(QString message, int messageType)
{
    QString logLine = "<b>["
                      + QLocale::system().toString(QDate::currentDate(),
                                                   QLocale::ShortFormat)
                      + " &nbsp;" // 2 space separation
                      + QTime::currentTime().toString() // Default HH:MM:SS
                      + "]</b> ";

    if (messageType == 1)       // GlobalObject::LogGood
    {
        logLine.append(QString::fromUtf8("\342\234\224 ")); // Heavy check mark
    }
    else if (messageType == 2)  // GlobalObject::LogBad
    {
        logLine.append(m_crossSymbolString);                // Cross mark
    }

    logLine.append(message);

    this->append(logLine);
}
