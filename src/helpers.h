/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef HELPERS_H
#define HELPERS_H

#include <QObject>
#include <QStringList>
#include <QRegularExpression>
#include <QRandomGenerator>

#include <QDebug>


class Helpers : public QObject
{
    Q_OBJECT

public:
    explicit Helpers(QObject *parent = nullptr);
    ~Helpers();

    static bool isValidJid(QString jid);
    static QString nickFromRoomJid(QString jid);
    static QString safeForHtml(QString text);
    static QString newlinesRemoved(QString text);

    static QString randomString(QStringList stringList);
    static QStringList someStrings(QStringList stringList, int percent);
    static int randomBetween(int lowNum, int highNum);
    static bool probability(int chancePercent);

    static QString durationString(qint64 seconds);


signals:

public slots:

};

#endif // HELPERS_H
