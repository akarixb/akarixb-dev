/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef SPEECHDATASTORE_H
#define SPEECHDATASTORE_H

#include <QObject>
#include <QVariantList>
#include <QVariantMap>
#include <QStringList>

#include <QDebug>


class SpeechDataStore : public QObject
{
    Q_OBJECT

public:
    explicit SpeechDataStore(QObject *parent = nullptr);
    ~SpeechDataStore();

    void setGreetingSentences(QVariantList greetingsList);
    QStringList getAllGreetings();

    void setCasualSentences(QVariantList casualSentencesList);
    QStringList getAllCasual();

    void setReactionSentences(QVariantList reactionSentencesList);
    QVariantList getAllReactions();

    void setConversationSentences(QVariantList conversationSentencesList);
    QVariantList getAllConversationSentences();


signals:


public slots:



private:
    QVariantList m_greetingSentences;
    QVariantList m_casualSentences;
    QVariantList m_reactionSentences;
    QVariantList m_conversationSentences;
};

#endif // SPEECHDATASTORE_H
