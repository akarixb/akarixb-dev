<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>ActivityHandler</name>
    <message numerus="yes">
        <location filename="../src/activityhandler.cpp" line="230"/>
        <source>%Ln hour(s)</source>
        <translation>
            <numerusform>%Ln heure</numerusform>
            <numerusform>%Ln heures</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/activityhandler.cpp" line="241"/>
        <source>%Ln minute(s)</source>
        <translation>
            <numerusform>%Ln minute</numerusform>
            <numerusform>%Ln minutes</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/activityhandler.cpp" line="254"/>
        <source>Starting activity %1, which will end at %2.</source>
        <translation>Lancement de l&apos;activité %1, qui prendra fin à %2.</translation>
    </message>
    <message>
        <location filename="../src/activityhandler.cpp" line="358"/>
        <source>Not sending message to room %1 because I am not currently in it.</source>
        <translation>Pas d&apos;envoi du message au salon %1 car je n&apos;y suis pas.</translation>
    </message>
    <message>
        <location filename="../src/activityhandler.cpp" line="403"/>
        <source>Activity ended: %1.</source>
        <translation>Activité terminée : %1.</translation>
    </message>
    <message>
        <location filename="../src/activityhandler.cpp" line="410"/>
        <source>None</source>
        <comment>Regarding an activity</comment>
        <translation>Aucune</translation>
    </message>
</context>
<context>
    <name>ActivityModule</name>
    <message>
        <location filename="../src/activitymodule.cpp" line="42"/>
        <source>&amp;Sync Activities</source>
        <translation>&amp;Synchroniser les activités</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="56"/>
        <source>&amp;Add Activity</source>
        <translation>&amp;Ajouter une activité</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="64"/>
        <source>&amp;Remove Activity</source>
        <translation>Supp&amp;rimer une activité</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="98"/>
        <source>Mon</source>
        <comment>Monday, shortened if possible</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="100"/>
        <source>Tue</source>
        <comment>Tuesday, shortened if possible</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="102"/>
        <source>Wed</source>
        <comment>Wednesday, shortened if possible</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="104"/>
        <source>Thu</source>
        <comment>Thursday, shortened if possible</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="106"/>
        <source>Fri</source>
        <comment>Friday, shortened if possible</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="108"/>
        <source>Sat</source>
        <comment>Saturday, shortened if possible</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="110"/>
        <source>Sun</source>
        <comment>Sunday, shortened if possible</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="117"/>
        <source>minutes</source>
        <translation>minutes</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="120"/>
        <source>Online</source>
        <translation>En ligne</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="121"/>
        <source>Free for Chat</source>
        <translation>Disponible</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="122"/>
        <source>Away</source>
        <translation>Absent</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="123"/>
        <source>Extended Away</source>
        <translation>Absence prolongée</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="124"/>
        <source>Do not Disturb</source>
        <translation>Ne pas déranger</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="141"/>
        <source>seconds</source>
        <translation>secondes</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="160"/>
        <source>All rooms</source>
        <translation>Tous les salons</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="161"/>
        <source>Several rooms</source>
        <translation>Plusieurs salons</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="162"/>
        <source>A few rooms</source>
        <translation>Quelques salons</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="177"/>
        <source>List of room or user addresses, separated by space</source>
        <translation>Liste des adresses de salons ou utilisateurs, séparés par un espace</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="247"/>
        <source>Days Of The Week</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="166"/>
        <source>Also to active private chats</source>
        <translation>Aussi pour les discussions privées actives</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="163"/>
        <source>None of the rooms</source>
        <translation>Aucun des salons</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="172"/>
        <source>Additional specific JIDs:</source>
        <translation>JID spécifiques supplémentaires :</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="184"/>
        <source>Sa&amp;ve Activity</source>
        <translation>&amp;Enregister l&apos;activité</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="242"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="243"/>
        <source>Maximum Times Per Day</source>
        <translation>Temps maximum par jour</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="244"/>
        <source>Probability</source>
        <translation>Probabilité</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="245"/>
        <source>Time Range</source>
        <translation>Intervalle de temps</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="246"/>
        <source>Date Range</source>
        <translation>Intervalle de dates</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="248"/>
        <source>Duration</source>
        <translation>Durée</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="249"/>
        <source>Set Status Type</source>
        <translation>Définir le type de statut</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="250"/>
        <source>Status Messages</source>
        <translation>Messages d&apos;état</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="251"/>
        <source>Availability</source>
        <translation>Disponibilité</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="252"/>
        <source>Reaction Time</source>
        <translation>Temps de réaction</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="253"/>
        <source>Descriptions</source>
        <translation>Descriptions</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="254"/>
        <source>Messages Before</source>
        <translation>Messages avant</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="255"/>
        <source>Messages Afterwards</source>
        <translation>Messages après</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="256"/>
        <source>Send Messages To</source>
        <translation>Envoyer les messages à</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/activitymodule.cpp" line="334"/>
        <source>Loaded %Ln activities.</source>
        <translation>
            <numerusform>Chargé %Ln activité.</numerusform>
            <numerusform>Chargé %Ln activités.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="363"/>
        <source>New Activity</source>
        <translation>Nouvelle activité</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="514"/>
        <source>Activity exists</source>
        <translation>L&apos;activité existe</translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="515"/>
        <source>Error: There is already an activity in the list with this name.</source>
        <translation>Erreur : Il existe déjà une activité dans la liste avec ce nom.</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/activitymodule.cpp" line="622"/>
        <source>Saved %Ln activities.</source>
        <translation>
            <numerusform>Enregistré %Ln activité.</numerusform>
            <numerusform>Enregistré %Ln activités.</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>ChatModule</name>
    <message>
        <location filename="../src/chatmodule.cpp" line="46"/>
        <source>There are no active chats</source>
        <translation>Il n&apos;y a pas de discussions actives</translation>
    </message>
    <message>
        <location filename="../src/chatmodule.cpp" line="165"/>
        <source>Chat started by %1.</source>
        <comment>%1 = A nickname</comment>
        <translation>Discussion lancée par %1.</translation>
    </message>
    <message>
        <location filename="../src/chatmodule.cpp" line="171"/>
        <source>New chat</source>
        <translation>Nouvelle discussion</translation>
    </message>
    <message>
        <location filename="../src/chatmodule.cpp" line="172"/>
        <source>A new chat was started by %1.</source>
        <translation>Une nouvelle discussion a été lancée par %1.</translation>
    </message>
</context>
<context>
    <name>ChatWidget</name>
    <message>
        <location filename="../src/chatwidget.cpp" line="414"/>
        <source>Reason:</source>
        <translation>Raison :</translation>
    </message>
    <message>
        <location filename="../src/chatwidget.cpp" line="423"/>
        <source>Kicked from the room!</source>
        <translation>Viré du salon !</translation>
    </message>
    <message>
        <location filename="../src/chatwidget.cpp" line="442"/>
        <source>Room name is: %1</source>
        <translation>Le nom du salon est : %1</translation>
    </message>
    <message>
        <location filename="../src/chatwidget.cpp" line="366"/>
        <source>Joined room %1, as %2.</source>
        <translation>Entre dans le salon %1, en tant que %2.</translation>
    </message>
    <message>
        <location filename="../src/chatwidget.cpp" line="395"/>
        <source>Left room %1.</source>
        <translation>Parti du salon %1.</translation>
    </message>
</context>
<context>
    <name>CommandHandler</name>
    <message>
        <location filename="../src/commandhandler.cpp" line="103"/>
        <source>Command %1 requested by %2 in room %3 without the right access level.</source>
        <translation>Commande %1 exécutée par l&apos;utilisateur %2 du salon %3 sans les droits requis.</translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="116"/>
        <source>Command from %1 in room %2: %3</source>
        <translation>Commande de %1 dans le salon %2 : %3</translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="159"/>
        <source>Command %1 requested by %2 without the right access level.</source>
        <translation>Commande %1 exécutée par l&apos;utilisateur %2 sans les droits requis.</translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="169"/>
        <source>Command from %1: %2</source>
        <translation>Commande de %1 : %2</translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="197"/>
        <source>The command failed.</source>
        <translation>La commande a échouée.</translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="209"/>
        <source>Use %1 to list the available commands.</source>
        <translation>Utilisez %1 pour lister les commandes disponibles.</translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="213"/>
        <source>Use %1 [command] to get help about a specific command.</source>
        <translation>Utilisez %1 [commande] pour obtenir de l&apos;aide sur une commande spécifique.</translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="239"/>
        <source>I don&apos;t have help about: %1</source>
        <translation>Je n&apos;ai pas d&apos;aide à propos de : %1</translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="243"/>
        <source>Help for %1:</source>
        <translation>Aide pour %1 :</translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="252"/>
        <source>Available commands</source>
        <translation>Commandes disponibles</translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="333"/>
        <source>Error loading file %1 for command: %2</source>
        <translation>Erreur du chargement du fichier %1 de la commande : %2</translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="349"/>
        <source>I need a parameter for the query:</source>
        <translation>J&apos;ai besoin d&apos;un paramètre pour la requête :</translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="373"/>
        <source>I don&apos;t have a match for %1</source>
        <translation>Je n&apos;ai pas de résultats pour %1</translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="399"/>
        <source>Started detached program successfully.</source>
        <translation>Le programme détaché est lancé.</translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="437"/>
        <source>Command gave no output text.</source>
        <translation>La commande n&apos;a donnée aucun texte de sortie.</translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="477"/>
        <source>This command is an alias to itself!</source>
        <translation>Cette commande est un alias d&apos;elle-même !</translation>
    </message>
</context>
<context>
    <name>CommandModule</name>
    <message>
        <location filename="../src/commandmodule.cpp" line="41"/>
        <source>&amp;Sync Commands</source>
        <translation>&amp;Synchroniser les commandes</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="56"/>
        <source>&amp;Add Command</source>
        <translation>&amp;Ajouter une commande</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="63"/>
        <source>&amp;Remove Command</source>
        <translation>Supp&amp;rimer une commande</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="72"/>
        <source>The name, without the prefix or any spaces</source>
        <comment>Referring to the command&apos;s name/trigger</comment>
        <translation>Le nom, sans préfixe ni espace</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="85"/>
        <source>Anyone can use it</source>
        <comment>&apos;it&apos; refers to a command</comment>
        <translation>Tout le monde peut l&apos;utiliser</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="89"/>
        <source>Static Message</source>
        <translation>Message statique</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="90"/>
        <source>Just enter a single sentence.</source>
        <translation>Il suffit d&apos;entrer une seule phrase.</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="91"/>
        <source>Random Sentence</source>
        <translation>Phrase aléatoire</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="92"/>
        <source>Add as many sentences as you want.
One of them will be selected at random as a reply.</source>
        <translation>Ajoutez autant de phrases que vous voulez.
L&apos;une d&apos;elles sera sélectionnée au hasard comme une réponse.</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="95"/>
        <source>Random Line from Text File</source>
        <translation>Ligne aléatoire à partir du fichier texte</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="96"/>
        <source>A random line from the chosen file will be used as a reply.
File encoding should be UTF-8.</source>
        <translation>Une ligne aléatoire du fichier choisi sera utilisé pour répondre.
L&apos;encodage du fichier doit être UTF-8.</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="99"/>
        <source>Keyword-based Message</source>
        <translation>Message basé sur des mots clés</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="100"/>
        <source>Add keywords to the list, then enter the corresponding definition for each keyword on the other side.</source>
        <translation>Ajouter des mots clés à la liste, puis entrer la définition correspondante de chaque mot clé de l&apos;autre côté.</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="103"/>
        <source>Run Program</source>
        <translation>Exécuter le programme</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="112"/>
        <source>If the program you launch will take a long time to finish (like a GUI), you need to select the option to launch it independently, but then it will not be possible to use the program output as the reply for the command.</source>
        <translation>Si le programme que vous lancez est long (comme une interface graphique), vous avez besoin de le lancer indépendamment, mais sa sortie ne pourra pas être utilisée comme réponse de la commande.</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="117"/>
        <source>Alias for another command</source>
        <translation>Alias pour une autre commande</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="118"/>
        <source>Enter the original command that should be run when this alias is used.</source>
        <translation>Entrez la commande d&apos;origine qui doit être exécutée lorsque cet alias est utilisé.</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="148"/>
        <source>Select File...</source>
        <translation>Sélectionner un fichier...</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="104"/>
        <source>Enter a system command line to be run, with any needed parameters.
The reply will be the console output of the program, unless you specify a reply pattern, and don&apos;t include the %output% variable somewhere in it.</source>
        <comment>Don&apos;t translate %output%!</comment>
        <translation>Ligne de commande à lancer, avec les paramètres nécessaires.
La réponse proviendra de la sortie de la commande sur la console, sauf si vous spécifiez un modèle de réponse sans y inclure la variable %output%.</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="166"/>
        <source>Wait for the program to finish</source>
        <translation>Attendez la fin du programme</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="167"/>
        <source>Launch the program independently</source>
        <translation>Lancer le programme indépendamment</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="181"/>
        <source>With a reply pattern you can make any possible reply to a command fit into a predefined structure, like having a specific symbol at the start or the end of the reply, or always start the reply with a fixed word.</source>
        <translation>Avec un modèle de réponse, vous pouvez intégrer n&apos;importe quelle réponse possible à une commande dans une structure prédéfinie, comme avec un symbole spécifique au début ou à la fin de la réponse, ou avec un mot fixe toujours au début de la réponse.</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="186"/>
        <source>You can insert variables like %user%, %params%, %output%, %line% and %smiley%.</source>
        <comment>Do not translate the words between % symbols</comment>
        <translation>Vous pouvez insérer des variables comme %user%, %params%, %output%, %line% et %smiley%.</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="190"/>
        <source>Usually, you will want to have the %output% variable somewhere in the pattern.</source>
        <comment>Do not translate %output%</comment>
        <translation>Habituellement, vous voudrez avoir la variable %output% à un endroit du modèle.</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="194"/>
        <source>If you don&apos;t set a reply pattern, replies in chatrooms will use the default pattern, %user%: %output%.</source>
        <comment>Do not translate %user% or %output%</comment>
        <translation>Si vous ne définissez pas de modèle de réponse, les réponses dans les salons de discussion utiliseront le modèle par défaut, %user% : %output%.</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="198"/>
        <source>Example: [RESULT] &gt; %output%</source>
        <comment>Do not translate %output%</comment>
        <translation>Exemple : [RESULTAT] &gt; %output%</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="213"/>
        <source>Save &amp;Command</source>
        <translation>Enregistrer la &amp;commande</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="239"/>
        <source>Command</source>
        <translation>Commande</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="240"/>
        <source>Help Text</source>
        <translation>Texte explicatif</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="241"/>
        <source>Access Level</source>
        <translation>Niveau d&apos;accès</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="243"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="178"/>
        <source>Set an optional reply pattern</source>
        <translation>Définir un modèle de réponse optionnel</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="79"/>
        <source>Explanation shown to users with the %1 command</source>
        <translation>Explication fournie aux utilisateurs avec la commande %1</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="247"/>
        <source>Reply Pattern</source>
        <translation>Modèle de réponse</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="249"/>
        <source>Private Reply</source>
        <translation>Réponse privée</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/commandmodule.cpp" line="352"/>
        <source>Loaded %Ln command(s).</source>
        <translation>
            <numerusform>Chargé %Ln commande.</numerusform>
            <numerusform>Chargé %Ln commandes.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="368"/>
        <source>The command name can&apos;t contain spaces.</source>
        <translation>Le nom de la commande ne peut pas contenir d&apos;espaces.</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="375"/>
        <source>Static reply is empty.</source>
        <translation>La réponse statique est vide.</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="390"/>
        <source>There are no sentences in the list.</source>
        <translation>Il n&apos;y a pas de phrases dans la liste.</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="398"/>
        <source>There is no file selected.</source>
        <translation>Il n&apos;y a pas de fichier sélectionné.</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="406"/>
        <source>There are no keywords in the list.</source>
        <translation>Il n&apos;y a pas de mots-clés dans la liste.</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="416"/>
        <source>There is no program specified.</source>
        <translation>Il n&apos;y a pas de programme spécifié.</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="424"/>
        <source>You need to enter the command this alias will execute.</source>
        <translation>Vous devez entrer la commande que cet alias exécutera.</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="430"/>
        <source>A command can&apos;t be an alias of itself.</source>
        <translation>Une commande ne peut pas être un alias d&apos;elle-même.</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="452"/>
        <source>Error validating fields</source>
        <translation>Erreur champs de validation</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="480"/>
        <source>Select a text file</source>
        <translation>Sélectionner un fichier texte</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="486"/>
        <source>Selected file does not exist.</source>
        <translation>Le fichier sélectionné n&apos;existe pas.</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="501"/>
        <source>New Command</source>
        <translation>Nouvelle commande</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="549"/>
        <source>No file selected</source>
        <translation>Aucun fichier sélectionné</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="660"/>
        <source>Command exists</source>
        <translation>La commande existe</translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="661"/>
        <source>Error: There is already a command in the list with this name.</source>
        <translation>Erreur : Il y a déjà une commande dans la liste avec ce nom.</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/commandmodule.cpp" line="790"/>
        <source>Saved %Ln command(s).</source>
        <translation>
            <numerusform>Enregistré %Ln commande.</numerusform>
            <numerusform>Enregistré %Ln commandes.</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../src/configdialog.cpp" line="27"/>
        <source>Program Configuration</source>
        <translation>Configuration du programme</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="51"/>
        <location filename="../src/configdialog.cpp" line="57"/>
        <source>seconds</source>
        <comment>Suffix next to a number</comment>
        <translation>secondes</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="69"/>
        <source>Administrator JIDs</source>
        <translation>JIDs Administrateurs</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="71"/>
        <source>Command prefix</source>
        <translation>Préfixe de commande</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="75"/>
        <source>Connection timeout</source>
        <translation>Délai d&apos;attente de la connexion</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="73"/>
        <source>Connection ping interval</source>
        <translation>Intervalle d&apos;écho de connection</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="91"/>
        <source>Notify new chats</source>
        <translation>Notifier les nouvelles discussions</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="93"/>
        <source>Notify disconnection</source>
        <translation>Notifier une déconnexion</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="107"/>
        <source>Select...</source>
        <translation>Sélectionner...</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="127"/>
        <source>Hide window on startup</source>
        <translation>Cacher la fenêtre au démarrage</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="129"/>
        <source>Use a custom icon</source>
        <translation>Utiliser une icône personnalisée</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="142"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="147"/>
        <source>Notifications</source>
        <translation>Notifications</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="152"/>
        <source>System Tray</source>
        <translation>Zone de notification</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="157"/>
        <source>&amp;Save</source>
        <translation>&amp;Enregistrer</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="165"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="213"/>
        <source>Invalid icon</source>
        <translation>Icône invalide</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="214"/>
        <source>The selected image is not valid.</source>
        <translation>L&apos;image sélectionnée n&apos;est pas valide.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="219"/>
        <source>Icon file %1 does not exist</source>
        <translation>Le fichier de l&apos;icône %1 n&apos;existe pas</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="249"/>
        <source>Select custom icon</source>
        <translation>Sélectionner une icône personnalisée</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="251"/>
        <source>Image files</source>
        <translation>Fichiers Image</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="254"/>
        <source>All files</source>
        <translation>Tous les fichiers</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="307"/>
        <source>Program configuration saved.</source>
        <translation>Configuration du programme enregistrée.</translation>
    </message>
</context>
<context>
    <name>ConnectionDialog</name>
    <message>
        <location filename="../src/connectiondialog.cpp" line="29"/>
        <source>Connection Settings</source>
        <translation>Paramètres de connexion</translation>
    </message>
    <message>
        <location filename="../src/connectiondialog.cpp" line="37"/>
        <source>Like user@server</source>
        <translation>Comme utilisateur@service</translation>
    </message>
    <message>
        <location filename="../src/connectiondialog.cpp" line="62"/>
        <source>Use carefully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/connectiondialog.cpp" line="63"/>
        <source>This allows connecting to servers with bad security certificates.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/connectiondialog.cpp" line="72"/>
        <source>&amp;Connect</source>
        <translation>&amp;Connecter</translation>
    </message>
    <message>
        <location filename="../src/connectiondialog.cpp" line="82"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../src/connectiondialog.cpp" line="96"/>
        <source>XMPP Address (Jabber ID)</source>
        <translation>Adresse XMPP (Jabber ID)</translation>
    </message>
    <message>
        <location filename="../src/connectiondialog.cpp" line="97"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location filename="../src/connectiondialog.cpp" line="98"/>
        <source>Connect Automatically</source>
        <translation>Connecter automatiquement</translation>
    </message>
    <message>
        <location filename="../src/connectiondialog.cpp" line="99"/>
        <source>Resource</source>
        <translation>Ressource</translation>
    </message>
    <message>
        <location filename="../src/connectiondialog.cpp" line="100"/>
        <source>Priority</source>
        <translation>Priorité</translation>
    </message>
    <message>
        <location filename="../src/connectiondialog.cpp" line="101"/>
        <source>Ignore SSL errors</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ContactsModule</name>
    <message>
        <location filename="../src/contactsmodule.cpp" line="57"/>
        <source>&amp;Start chat</source>
        <translation>Commencer une discu&amp;ssion</translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="66"/>
        <source>&amp;Add Contact</source>
        <translation>&amp;Ajouter un contact</translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="74"/>
        <source>Rename Contact</source>
        <translation>Renommer un contact</translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="82"/>
        <source>&amp;Remove Contact</source>
        <translation>Supp&amp;rimer un contact</translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="108"/>
        <source>Accept Request</source>
        <translation>Accepter la demande</translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="115"/>
        <source>Refuse Request</source>
        <translation>Refuser la demande</translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="125"/>
        <source>Pending Subscription Requests</source>
        <translation>Requêtes de souscription en cours</translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="198"/>
        <source>Subscription status: %1</source>
        <translation>Statut de la souscription : %1</translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="327"/>
        <source>Accepted subscription request from %1.</source>
        <comment>%1 is an XMPP address</comment>
        <translation>La requête de souscription a été acceptée par %1.</translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="352"/>
        <source>Refused subscription request from %1.</source>
        <comment>%1 is an XMPP address</comment>
        <translation>La requête de souscription a été refusée par %1.</translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="394"/>
        <source>Add a contact</source>
        <translation>Ajouter un contact</translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="395"/>
        <source>Enter the full XMPP address, with user@server format</source>
        <translation>Entrez l&apos;adresse XMPP complète, au format utilisateur@serveur</translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="425"/>
        <source>Rename a contact</source>
        <translation>Renommer un contact</translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="426"/>
        <source>Enter the new name for %1</source>
        <comment>%1 is contact&apos;s name + address</comment>
        <translation>Entrer le nouveau nom pour %1</translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="458"/>
        <source>Remove contact?</source>
        <translation>Supprimer le contact ?</translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="459"/>
        <source>Do you want to remove %1 from your contact list?</source>
        <comment>%1 is contact&apos;s name + address</comment>
        <translation>Voulez-vous supprimer %1 de votre liste de contacts ?</translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="464"/>
        <source>Yes, remove</source>
        <comment>Regarding a contact</comment>
        <translation>Oui, supprimer</translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="465"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="486"/>
        <source>Added %1 to the contact list.</source>
        <comment>%1 is an XMPP address</comment>
        <translation>Ajout de %1 à la liste de contacts.</translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="519"/>
        <source>Removed %1 from the contact list.</source>
        <comment>%1 is an XMPP address</comment>
        <translation>Suppression de %1 de la liste de contacts.</translation>
    </message>
</context>
<context>
    <name>GeneralModule</name>
    <message>
        <location filename="../src/generalmodule.cpp" line="40"/>
        <source>Offline</source>
        <translation>Hors ligne</translation>
    </message>
    <message>
        <location filename="../src/generalmodule.cpp" line="99"/>
        <source>Connect</source>
        <translation>Connecter</translation>
    </message>
    <message>
        <location filename="../src/generalmodule.cpp" line="107"/>
        <source>Disconnect</source>
        <translation>Déconnecter</translation>
    </message>
    <message>
        <location filename="../src/generalmodule.cpp" line="122"/>
        <source>Uptime:</source>
        <translation>Durée de fonctionnement :</translation>
    </message>
    <message>
        <location filename="../src/generalmodule.cpp" line="123"/>
        <source>Current Activity:</source>
        <translation>Activité actuelle :</translation>
    </message>
    <message>
        <location filename="../src/generalmodule.cpp" line="124"/>
        <source>Status Message:</source>
        <translation>Message d&apos;état :</translation>
    </message>
    <message>
        <location filename="../src/generalmodule.cpp" line="125"/>
        <source>Rooms:</source>
        <translation>Salons :</translation>
    </message>
    <message>
        <location filename="../src/generalmodule.cpp" line="157"/>
        <source>Server version: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/generalmodule.cpp" line="295"/>
        <source>%1 joined, out of %2 configured</source>
        <comment>Number of rooms joined and configured</comment>
        <translation>%1 rejoints, sur %2 configurés</translation>
    </message>
</context>
<context>
    <name>HelpWidget</name>
    <message>
        <location filename="../src/helpwidget.cpp" line="26"/>
        <source>General Help</source>
        <translation>Aide générale</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="40"/>
        <source>Getting started</source>
        <translation>Pour commencer</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="41"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="42"/>
        <source>Commands</source>
        <translation>Commandes</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="43"/>
        <source>Activities</source>
        <translation>Activités</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="44"/>
        <source>Substitution syntax</source>
        <translation>Syntaxe de substitution</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="45"/>
        <source>Command line options</source>
        <translation>Options de ligne de commande</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="47"/>
        <source>Contents</source>
        <translation>Contenu</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="70"/>
        <source>The first time you start AkariXB you will see the Connection Settings window, where you need to enter the XMPP address the bot will use, and its password.</source>
        <translation>Au premier démarrage de AkariXB, vous verrez une fenêtre de paramétrage où vous devrez renseigner l&apos;adresse XMPP que vous utilisez et le mot de passe associé.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="74"/>
        <source>Afterwards, you can configure chatrooms to be automatically joined, manage the contact list, define commands for which the bot will provide answers, etc.</source>
        <translation>Ensuite, vous pouvez configurer des salons de discussion à joindre automatiquement, gérer la liste de contacts, définir les commandes pour lesquels le robot fournira les réponses, etc.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="88"/>
        <source>You can configure several things to your liking in the settings, like which symbol will act as prefix for commands, which XMPP addresses will be able to use special administration commands with the bot, whether you want popup notifications for certain events, etc.</source>
        <translation>Vous pouvez configurer plusieurs préférences dans les paramètres, tels que le symbole utilisé comme préfixe des commandes, quel adresse XMPP sera autorisée à fournir des commandes d&apos;administration au robot, pour quels évènements vous voulez des notifications, etc.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="104"/>
        <source>You can define several kinds of custom commands, like static message, random message from a list, keyword-based message, and program execution-based commands.</source>
        <translation>Vous pouvez définir plusieurs sortes de commandes personnalisées, telles que des messages statiques, des messages aléatoires issus d&apos;une liste, des messages à mots-clés, et des commandes basées sur l&apos;exécution des programme.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="109"/>
        <source>Each type of command has an explanation of how it works, visible while you&apos;re adding the new command.</source>
        <translation>Chaque de commande a une explication sur son fonctionnement, visible lors de l&apos;introduction de la nouvelle commande.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="112"/>
        <source>If the access level for the command is not set for everyone, only users listed in the administrators list in the Program Configuration window will be able to use that command.</source>
        <translation>Si le niveau d&apos;accès à la commande n&apos;est pas universel, seuls les utilisateurs de la liste des administrateurs de la fenâtre Confguration du Programme seront autorisés.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="126"/>
        <source>Activities can be defined to make the bot change its status, send messages, adapt its reaction time and so on, at certain times and dates.</source>
        <translation>Les activités peuvent être définies de telle sorte que le robot change son statut, envoie des messages, adapte son temps de réaction entre autre, à des moments prédéfinis.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="130"/>
        <source>If you have defined messages to be sent before or after the activity, you can decrease the chances of them being sent by adding some strings with a single asterisk (*) in them to the list of messages.</source>
        <translation>SI vous avez défini des messsages à envoyer avant ou après une activité, vous pouvez diminuer les chances qu&apos;ils soient envoyés en ajoutant des chaînes contenant un unique astérisque (*)  dans la liste des messages.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="144"/>
        <source>There are several special words that can be entered inside custom strings like replies to commands, conversation texts and status messages, which will be replaced by the appropriate variable text at the time the string is used. These are:</source>
        <translation>Des mots spéciaux à inclure dans les phrases, telles que des commandes, des textes de discussion et des messages de statut, seront remplacés par du texte des variables appropriées lors de l&apos;utilisation des phrases. Ce sont :</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="151"/>
        <source>Replaced by the name of the user to whom this message is directed. Nickname in the case of chatrooms, and account name in the case of chats with contacts.</source>
        <translation>Remplacé par le nom du destinataire du message. Le pseudo dans le cas des salons de discussion, le nom du compte dans le cas de discussions avec les contacts.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="156"/>
        <source>The nickname of the bot, as specified in the Identity module.</source>
        <translation>Le pseudo du robot, tel qu&apos;indiqué dans le module Identité.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="159"/>
        <source>In replies to commands, this is replaced with whatever follows the command name.</source>
        <translation>En réponse aux commandes, ceci est remplacé par tout ce qui suit le nom de la commande.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="162"/>
        <source>Used in a custom reply pattern for a command, this will be replaced by the original reply to that command.</source>
        <translation>Lors d&apos;une utilisation dans un modèle de réponse personnalisé pour une commande, ceci sera remplacé par la réponse originale à cette commande.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="166"/>
        <source>This is replaced by a newline character.</source>
        <translation>Ceci est remplacé par un caractère de retour à la ligne.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="168"/>
        <source>This is replaced by a random old-style smiley face.</source>
        <translation>Ceci est remplacé par un émoticone ancien style aléatoire.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="171"/>
        <source>These are replaced by a random, happy or sad, Unicode-based emoji face.</source>
        <translation>Ceci est remplacé alétoirement par un émoticone joyeux ou triste de type Unicode.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="174"/>
        <source>Keep in mind that some users might not be able to see these symbols.</source>
        <translation>Gardez à l&apos;esprit que certains utilisateurs pourraient ne pas visualiser ces symboles.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="179"/>
        <source>Example: &apos;Hello %user%&apos; could be converted to &apos;Hello Mary&apos; if a user called Mary does something that triggers that reply.</source>
        <translation>Exemple : &apos;Bonjour %user%&apos; peut être converti en &apos;Bonjour Marie&apos; si un utilisateur nomé Marie fait quelque chose qui déclenche cette réponse.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="183"/>
        <source>In addition, syntax like %1 can be used to insert one out of several choices at random, at any desired point in the string.</source>
        <translation>En outre, une syntaxe telles que %1 peut être utilisée pour insérer aléatoirement l&apos;un des choix multiples à un point déterminé de la phrase.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="188"/>
        <source>%1, %2 and %3 can&apos;t be used inside these choices.</source>
        <translation>%1, %2 et %3 ne peuvent pas être utilisés à l&apos;intérieur de ces choix.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="202"/>
        <source>You can use the --config parameter (or -c) to run the program with a different configuration. This way, you can run two instances of AkariXB at the same time, with different accounts, different rooms configured, using a different command prefix, etc.</source>
        <translation>Vous pouvez utiliser le paramètre --config (ou -c) pour exécuter le programme avec une configuration différente. Ainsi, vous pouvez avoir deux instances d&apos;AkariXB simultanées, avec des comptes distincts, différents salons, en utilisant des préfixes de commande différents, etc.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="208"/>
        <source>Use the --debug parameter (or -d) to have extra information in your terminal window, about what the program is doing.</source>
        <translation>Utilisez le paramètre --debug (ou -d) pour avoir plus d&apos;information dans la console sur ce que fait le programme.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="212"/>
        <source>Use the --xmppdebug parameter (or -x) to have extensive XMPP traffic information (XML stanzas) shown in your terminal window.</source>
        <translation>Utilisez le paramètre --xmppdebug (ou -x) pour avoir une information abondante sur le trafic XMPP (instance XML) dans la console.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="228"/>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
</context>
<context>
    <name>IdentityModule</name>
    <message>
        <location filename="../src/identitymodule.cpp" line="47"/>
        <source>&amp;Update</source>
        <translation>&amp;Mise à jour</translation>
    </message>
    <message>
        <location filename="../src/identitymodule.cpp" line="54"/>
        <source>Nickname</source>
        <translation>Pseudo</translation>
    </message>
    <message>
        <location filename="../src/identitymodule.cpp" line="55"/>
        <source>Date of Birth</source>
        <translation>Date de naissance</translation>
    </message>
    <message>
        <location filename="../src/identitymodule.cpp" line="109"/>
        <source>Loaded identity data for %1.</source>
        <comment>%1 is a nickname</comment>
        <translation>Chargement des données d&apos;identité de %1.</translation>
    </message>
    <message>
        <location filename="../src/identitymodule.cpp" line="115"/>
        <source>Identity data has not been defined yet.</source>
        <translation>Les données d&apos;identité n&apos;ont pas encore été définies.</translation>
    </message>
    <message>
        <location filename="../src/identitymodule.cpp" line="149"/>
        <source>Saved identity data for %1.</source>
        <comment>%1 is a nickname</comment>
        <translation>Sauvegarde les données d&apos;identité de %1.</translation>
    </message>
</context>
<context>
    <name>LogModule</name>
    <message>
        <location filename="../src/logmodule.cpp" line="41"/>
        <source>AkariXB v%1 started.</source>
        <comment>%1 = Version number</comment>
        <translation>AkariXB v%1 démarré.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="70"/>
        <source>Settings file in use: %1</source>
        <translation>Fichier de paramètres en cours d&apos;utilisation : %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="132"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="135"/>
        <source>Rooms</source>
        <translation>Salons</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="142"/>
        <source>Contacts</source>
        <translation>Contacts</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="145"/>
        <source>Commands</source>
        <translation>Commandes</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="148"/>
        <source>Room Speech</source>
        <translation type="unfinished">Phrases pour les salons</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="151"/>
        <source>Conversation</source>
        <translation>Conversation</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="154"/>
        <source>Activities</source>
        <translation>Activités</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="157"/>
        <source>Identity</source>
        <translation>Identité</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="160"/>
        <source>Log</source>
        <translation>Journal</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="237"/>
        <source>Ready.</source>
        <translation>Prêt.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="269"/>
        <source>Connect</source>
        <translation>Connecter</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="277"/>
        <source>Disconnect</source>
        <translation>Déconnecter</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="286"/>
        <source>Stop connection attempts</source>
        <translation>Arrêter les tentatives de connexion</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="294"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="301"/>
        <source>&amp;Session</source>
        <translation>&amp;Session</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="313"/>
        <source>Configure AkariXB</source>
        <translation>Configurer AkariXB</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="319"/>
        <source>S&amp;ettings</source>
        <translation>Pré&amp;férences</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="329"/>
        <source>General &amp;Help</source>
        <translation>&amp;Aide générale</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="337"/>
        <source>Visit Website</source>
        <translation>Visiter le site Web</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="344"/>
        <source>Report a Bug</source>
        <translation>Signaler un bogue</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="351"/>
        <source>About XMPP</source>
        <translation>À propos de XMPP</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="357"/>
        <location filename="../src/mainwindow.cpp" line="1098"/>
        <source>About AkariXB</source>
        <translation>À propos de AkariXB</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="362"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="471"/>
        <source>Data directory created: %1</source>
        <translation>Dossier de données créé : %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="477"/>
        <source>Error creating data directory: %1</source>
        <translation>Erreur lors de la création du dossier de données : %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="486"/>
        <source>Data directory in use: %1</source>
        <translation>Dossier de données en cours d&apos;utilisation : %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="677"/>
        <source>Connecting...</source>
        <translation>Connexion...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="695"/>
        <source>Warning! Ignoring errors in SSL connection to server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="722"/>
        <source>%1 connected.</source>
        <translation>%1 connecté.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="780"/>
        <source>Aborted connection attempt.</source>
        <translation>Tentative de connexion abandonnée.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="794"/>
        <source>Disconnected.</source>
        <translation>Déconnecté.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="815"/>
        <source>Disconnected</source>
        <translation>Déconnecté</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="816"/>
        <source>Connection to the server was lost.</source>
        <translation>La connexion au serveur a été perdue.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="832"/>
        <source>Connection seems stuck. Trying again...</source>
        <translation>La connexion semble bloquée. Nouvel essai...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="909"/>
        <source>Presence error %1 from %2: %3</source>
        <translation>Présence d&apos;erreur %1 de %2 : %3</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="926"/>
        <source>%1 wants to subscribe to your presence.</source>
        <comment>%1 is another user&apos;s address</comment>
        <translation>%1 souhaite installer un indicateur à votre présence.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="934"/>
        <source>%1 accepted your subscription request.</source>
        <comment>%1 is another user&apos;s address</comment>
        <translation>%1 a accepté votre requête de souscription.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="942"/>
        <source>%1 unsubscribed from your presence.</source>
        <comment>%1 is another user&apos;s address</comment>
        <translation>%1 a supprimé l&apos;indicateur de votre présence.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="950"/>
        <source>%1 removed your authorization to see their presence.</source>
        <comment>%1 is another user&apos;s address</comment>
        <translation>%1 vous a supprimé l&apos;autorisation d&apos;indication de sa présence.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="990"/>
        <source>Received version information from %1:</source>
        <translation>Version obtenue de %1 :</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1013"/>
        <source>Socket Error</source>
        <translation>Erreur Socket</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1018"/>
        <source>KeepAlive Error</source>
        <translation>Erreur KeepAlive</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1022"/>
        <source>XMPP Stream Error</source>
        <translation>Erreur Stream XMPP</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1031"/>
        <source>Network error: %1</source>
        <translation>Erreur réseau : %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1043"/>
        <source>Chats</source>
        <translation>Discussions</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1106"/>
        <source>AkariXB is a Jabber/XMPP bot.</source>
        <translation>AkariXB est un robot Jabber/XMPP.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1109"/>
        <source>You can talk to it, make it join chatrooms, etc.</source>
        <translation>Vous pouvez lui parler, lui faire rejoindre des salons de discussion, etc.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1113"/>
        <source>English translation by JanKusanagi.</source>
        <comment>TRANSLATORS: Change this with your language and name. If there was another translator before you, add your name after theirs ;)</comment>
        <translation>Traduction française par Adrien DAUGABEL, David GEIGER, Papoteur, Sébastien Morin et Jybz.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1120"/>
        <source>Thanks to all the testers, translators and packagers, who help make AkariXB better!</source>
        <translation>Merci à tous les testeurs, traducteurs et packageurs, qui contribuent à l&apos;amélioration d&apos;AkariXB !</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1125"/>
        <source>AkariXB is Free Software, licensed under the GNU GPL license, and uses some Oxygen icons under LGPL license.</source>
        <translation>AkariXB est un logiciel libre, sous licence GNU/GPL, et utilise les icônes Oxygen sous licence LGPL.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1150"/>
        <source>Offline</source>
        <translation>Hors ligne</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1202"/>
        <source>Account is not configured</source>
        <translation>Le compte n&apos;est pas configuré</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1287"/>
        <source>&amp;Hide Window</source>
        <translation>&amp;Cacher la fenêtre</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1294"/>
        <source>&amp;Show Window</source>
        <translation>&amp;Afficher la fenêtre</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1311"/>
        <source>Quit?</source>
        <translation>Quitter ?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1312"/>
        <source>Are you sure?</source>
        <translation>Êtes-vous sûr(e) ?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1314"/>
        <source>Yes, quit AkariXB</source>
        <translation>Oui, quittez AkariXB</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1314"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
</context>
<context>
    <name>MessageHandler</name>
    <message>
        <location filename="../src/messagehandler.cpp" line="87"/>
        <source>Headline from %1: %2</source>
        <comment>%1 is a user&apos;s address,%2 is the title of a message</comment>
        <translation>Ligne d&apos;entête de %1 : %2</translation>
    </message>
    <message>
        <location filename="../src/messagehandler.cpp" line="103"/>
        <source>Error %1 from %2: %3</source>
        <translation>Erreur %1 de %2 : %3</translation>
    </message>
</context>
<context>
    <name>RoomModule</name>
    <message>
        <location filename="../src/roommodule.cpp" line="81"/>
        <source>Add &amp;New Room</source>
        <translation>Ajouter un &amp;nouveau salon</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="95"/>
        <source>&amp;Remove Room</source>
        <translation>Supp&amp;rimer un salon</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="62"/>
        <location filename="../src/roommodule.cpp" line="112"/>
        <source>Room address</source>
        <translation>Adresse du salon</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="63"/>
        <location filename="../src/roommodule.cpp" line="115"/>
        <source>Nickname</source>
        <translation>Pseudo</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="120"/>
        <source>Password, if needed</source>
        <translation>Mot de passe, si nécessaire</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="65"/>
        <location filename="../src/roommodule.cpp" line="125"/>
        <source>Autojoin</source>
        <translation>Rejoindre automatiquement</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="141"/>
        <source>Join</source>
        <translation>Rejoindre</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="149"/>
        <source>Leave</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="131"/>
        <source>&amp;Edit</source>
        <translation>Éditio&amp;n</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="160"/>
        <source>Speak casually</source>
        <comment>Casually as in, from time to time</comment>
        <translation>Parler occasionnellement</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="182"/>
        <source>&amp;Update</source>
        <translation>&amp;Mise à jour</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="206"/>
        <source>Select a room from the list above</source>
        <translation>Sélectionner un salon à partir de la liste ci-dessus</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="269"/>
        <source>Room Details</source>
        <translation>Détails du salon</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/roommodule.cpp" line="374"/>
        <source>Loaded %Ln room(s).</source>
        <translation>
            <numerusform>Chargé %Ln salon.</numerusform>
            <numerusform>Chargé %Ln salons.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/roommodule.cpp" line="426"/>
        <source>Saved %Ln room(s).</source>
        <translation>
            <numerusform>Sauvegardé %Ln salon.</numerusform>
            <numerusform>Sauvegardé %Ln salons.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="64"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="66"/>
        <source>Allowed</source>
        <comment>Column header for features allowed</comment>
        <translation>Autorisé</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="157"/>
        <source>Allowed to:</source>
        <translation>A la permission de :</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="158"/>
        <source>Reply to commands</source>
        <translation>Répondre aux commandes</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="159"/>
        <source>Greet people</source>
        <translation>Saluer les gens</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="163"/>
        <source>Converse</source>
        <translation>Discuter</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="191"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/roommodule.cpp" line="372"/>
        <location filename="../src/roommodule.cpp" line="423"/>
        <source>%Ln room(s)</source>
        <translation>
            <numerusform>%Ln salon</numerusform>
            <numerusform>%Ln salons</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="531"/>
        <source>Joining room %1...</source>
        <translation>Rejoint le salon %1...</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="562"/>
        <source>Leaving room %1...</source>
        <translation>Quitte le salon %1...</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="610"/>
        <location filename="../src/roommodule.cpp" line="630"/>
        <location filename="../src/roommodule.cpp" line="647"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="611"/>
        <source>Room address is not valid.</source>
        <translation>L&apos;adresse du salon n&apos;est pas valide.</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="632"/>
        <source>There is already a room in the list with this address, in row %1.</source>
        <translation>Un salon existe déjà dans la liste avec cette adresse, à la ligne %1.</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="648"/>
        <source>Nickname is not valid.</source>
        <translation>Le pseudo n&apos;est pas valide.</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="702"/>
        <source>New Room</source>
        <translation>Nouveau salon</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="849"/>
        <source>Remove room?</source>
        <translation>Supprimer le salon ?</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="850"/>
        <source>Do you want to remove room %1 from your list?</source>
        <translation>Voulez-vous supprimer le salon %1 de votre liste ?</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="854"/>
        <source>Yes, remove it</source>
        <comment>It = A chat room</comment>
        <translation>Oui, le supprimer</translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="855"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
</context>
<context>
    <name>RoomSpeakModule</name>
    <message>
        <location filename="../src/roomspeakmodule.cpp" line="28"/>
        <source>Greetings</source>
        <translation>Salutations</translation>
    </message>
    <message>
        <location filename="../src/roomspeakmodule.cpp" line="29"/>
        <source>Casual speak</source>
        <translatorcomment>Previously &quot;Discussion décontractée&quot;, but this one, as discussed with David, seems better</translatorcomment>
        <translation type="unfinished">Discussion occasionnelle</translation>
    </message>
    <message>
        <location filename="../src/roomspeakmodule.cpp" line="30"/>
        <source>Reactions to words</source>
        <translation>Réactions aux mots</translation>
    </message>
    <message>
        <location filename="../src/roomspeakmodule.cpp" line="35"/>
        <source>Sentences used to greet people who enter a chatroom.</source>
        <translation>Phrase utilisée pour accueillir dans le salon de discussion.</translation>
    </message>
    <message>
        <location filename="../src/roomspeakmodule.cpp" line="38"/>
        <source>The %1 variable can be used to insert their names in the sentences.</source>
        <translation>La variable %1 peut être utilisée pour insérer leur pseudo dans une phrase.</translation>
    </message>
    <message>
        <location filename="../src/roomspeakmodule.cpp" line="52"/>
        <source>Sentences to be sent to chatrooms at random intervals.</source>
        <translation>Les phrases à envoyer aux salons de discussion à fréquence aléatoire.</translation>
    </message>
    <message>
        <location filename="../src/roomspeakmodule.cpp" line="64"/>
        <source>Regular expressions to be matched against messages coming from the chatrooms, and possible messages to send in response.</source>
        <translation>Expressions régulières à comparer aux messages provenant des salons de discussion et messages éventuels à envoyer en réponse.</translation>
    </message>
    <message>
        <location filename="../src/roomspeakmodule.cpp" line="69"/>
        <source>Example:</source>
        <translation>Exemple :</translation>
    </message>
    <message>
        <location filename="../src/roomspeakmodule.cpp" line="73"/>
        <source>Documentation for Perl regular expressions</source>
        <translation>Documentation pour les expressions régulières en Perl</translation>
    </message>
</context>
<context>
    <name>RoomSpeakTab</name>
    <message>
        <location filename="../src/roomspeaktab.cpp" line="55"/>
        <source>&amp;Save</source>
        <translation>&amp;Enregistrer</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/roomspeaktab.cpp" line="116"/>
        <source>Loaded %Ln sentence(s) of type %1.</source>
        <translation>
            <numerusform>Chargé %Ln phrase de type %1.</numerusform>
            <numerusform>Chargé %Ln phrases de type %1.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/roomspeaktab.cpp" line="174"/>
        <source>Saved %Ln sentence(s) of type %1.</source>
        <translation>
            <numerusform>Sauvegardé %Ln phrase de type %1.</numerusform>
            <numerusform>Sauvegardé  %Ln phrase de type %1.</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>StringListWidget</name>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="187"/>
        <source>Update the definition of the item</source>
        <translation>Mettre à jour la définition de l&apos;élément</translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="659"/>
        <source>Enter text</source>
        <translation>Entrer un texte</translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="624"/>
        <source>Item text is not valid</source>
        <translation>L&apos;élément texte est invalide</translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="142"/>
        <source>Add an item to the list (you can also press the Insert key)</source>
        <translation>Ajouter un élément à la liste (vous pouvez aussi appuyer sur la touche [Inser])</translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="660"/>
        <source>Enter a new line of text for the list</source>
        <translation>Entrer une nouvelle ligne de texte pour la liste</translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="514"/>
        <source>The item already exists</source>
        <translation>L&apos;élément existe déjà</translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="48"/>
        <source>String</source>
        <translation>Chaîne</translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="52"/>
        <source>Regular Expression</source>
        <translation>Expression régulière</translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="56"/>
        <source>Tags</source>
        <translation>Étiquettes</translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="103"/>
        <source>Search...</source>
        <translation>Rechercher...</translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="131"/>
        <source>Show All</source>
        <translation>Tout afficher</translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="152"/>
        <source>Edit the selected item (you can also double-click it or press F2/Enter)</source>
        <translation>Éditer l&apos;élément sélectionné (vous pouvez aussi double-cliquer dessus ou appuyer sur [F2]/[Entrée])</translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="162"/>
        <source>Remove the selected item (you can also press Shift+Delete)</source>
        <translation>Supprimer l&apos;élément sélectionné (vous pouvez aussi appuyer sur [Maj]+[Effacer])</translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="516"/>
        <source>Error: There is already an item in the list with the text %1, and this list does not allow duplicates.</source>
        <translation>Erreur : un élément dans la liste avec le texte %1 existe déjà, et cette liste n&apos;autorise pas les doublons.</translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="637"/>
        <source>Regular expression is not valid</source>
        <translation>L&apos;expression régulière est invalide</translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="639"/>
        <source>Syntax error at position %1:</source>
        <translation>Erreur de syntaxe à la position %1 :</translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="706"/>
        <source>Modify text</source>
        <translation>Modifier le texte</translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="707"/>
        <source>Update the text for this item</source>
        <translation>Mettre à jour le texte pour cet élément</translation>
    </message>
</context>
<context>
    <name>VariableParser</name>
    <message>
        <location filename="../src/variableparser.cpp" line="138"/>
        <source>Unbalanced braces: %1 opening and %2 closing</source>
        <translation>Parenthèses manquantes : %1 ouvrante(s) et %2 fermante(s)</translation>
    </message>
    <message>
        <location filename="../src/variableparser.cpp" line="168"/>
        <source>This substitution is unknown, or not valid in this context: %1</source>
        <translation>Cette substitution est inconnue ou invalide dans ce contexte : %1</translation>
    </message>
    <message>
        <location filename="../src/variableparser.cpp" line="186"/>
        <source>Syntax error:</source>
        <translation>Erreur de syntaxe :</translation>
    </message>
</context>
</TS>
