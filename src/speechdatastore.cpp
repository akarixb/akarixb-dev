/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "speechdatastore.h"


SpeechDataStore::SpeechDataStore(QObject *parent) : QObject(parent)
{
    qDebug() << "SpeechDataStore created";
}

SpeechDataStore::~SpeechDataStore()
{
    qDebug() << "SpeechDataStore destroyed";
}



void SpeechDataStore::setGreetingSentences(QVariantList greetingsList)
{
    m_greetingSentences = greetingsList;
}

QStringList SpeechDataStore::getAllGreetings()
{
    QStringList allGreetings;

    for (const QVariant greetingVariant : m_greetingSentences)
    {
        // Quite TMP...
        allGreetings.append(greetingVariant.toMap().keys().first());
    }

    return allGreetings;
}



void SpeechDataStore::setCasualSentences(QVariantList casualSentencesList)
{
    m_casualSentences = casualSentencesList;
}

QStringList SpeechDataStore::getAllCasual()
{
    QStringList allCasual;

    for (const QVariant casualVariant : m_casualSentences)
    {
        // Quite TMP, too...
        allCasual.append(casualVariant.toMap().keys().first());
    }

    return allCasual;
}



void SpeechDataStore::setReactionSentences(QVariantList reactionSentencesList)
{
    m_reactionSentences = reactionSentencesList;
}

QVariantList SpeechDataStore::getAllReactions()
{
    return m_reactionSentences;
}


void SpeechDataStore::setConversationSentences(QVariantList conversationSentencesList)
{
    m_conversationSentences = conversationSentencesList;
}

QVariantList SpeechDataStore::getAllConversationSentences()
{
    return m_conversationSentences;
}

