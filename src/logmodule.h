/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef LOGMODULE_H
#define LOGMODULE_H

#include <QTextBrowser>
#include <QTime>
#include <QApplication>
#include <QFontMetrics>
#include <QChar>

#include <QDebug>


class LogModule : public QTextBrowser
{
    Q_OBJECT

public:
    explicit LogModule(QWidget *parent = nullptr);
    ~LogModule();

signals:


public slots:
    void addToLog(QString message, int messageType);

private:
    QString m_crossSymbolString;

};

#endif // LOGMODULE_H
