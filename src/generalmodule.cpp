/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "generalmodule.h"


GeneralModule::GeneralModule(GlobalObject *globalObject,
                             QWidget *parent) : QWidget(parent)
{
    m_globalObject = globalObject;

    m_xmppClient = m_globalObject->getXmppClient();
    connect(m_xmppClient, &QXmppClient::connected,
            this, &GeneralModule::onConnected);
    connect(m_xmppClient, &QXmppClient::disconnected,
            this, &GeneralModule::onDisconnected);


    QFont infoFont;
    infoFont.setBold(true);
    infoFont.setPointSize(infoFont.pointSize() + 4);

    m_infoLabel = new QLabel(tr("Offline"), this);
    m_infoLabel->setFrameStyle(QFrame::StyledPanel | QFrame::Raised);
    m_infoLabel->setFont(infoFont);
    m_infoLabel->setAlignment(Qt::AlignCenter);


    m_serverVersionLabel = new QLabel(this);
    m_serverVersionLabel->setTextFormat(Qt::PlainText);
    m_serverVersionLabel->setAlignment(Qt::AlignCenter);


    infoFont.setPointSize(infoFont.pointSize() - 2);


    m_uptimeLabel = new QLabel(QStringLiteral("--"), this);
    m_uptimeLabel->setFont(infoFont);
    m_uptimeLabel->setWordWrap(true);
    m_uptimeLabel->setTextFormat(Qt::PlainText);
    m_uptimeLabel->setContentsMargins(4, 4, 4, 4);
    m_uptimeLabel->setSizePolicy(QSizePolicy::MinimumExpanding,
                                 QSizePolicy::Minimum);


    m_activityLabel = new QLabel(QStringLiteral("--"), this);
    m_activityLabel->setFont(infoFont);
    m_activityLabel->setWordWrap(true);
    m_activityLabel->setTextFormat(Qt::RichText);
    m_activityLabel->setContentsMargins(4, 4, 4, 4);
    m_activityLabel->setSizePolicy(QSizePolicy::MinimumExpanding,
                                   QSizePolicy::Minimum);
    connect(m_globalObject, &GlobalObject::activityChanged,
            m_activityLabel, &QLabel::setText);


    m_statusMessageLabel = new QLabel(QStringLiteral("--"), this);
    m_statusMessageLabel->setFont(infoFont);
    m_statusMessageLabel->setWordWrap(true);
    m_statusMessageLabel->setContentsMargins(4, 4, 4, 4);
    m_statusMessageLabel->setSizePolicy(QSizePolicy::MinimumExpanding,
                                        QSizePolicy::Minimum);
    connect(m_globalObject, &GlobalObject::statusChanged,
            this, &GeneralModule::onStatusChanged);


    m_roomsInfoLabel = new QLabel(QStringLiteral("--"), this);
    m_roomsInfoLabel->setFont(infoFont);
    m_roomsInfoLabel->setWordWrap(true);
    m_roomsInfoLabel->setContentsMargins(4, 4, 4, 4);
    m_roomsInfoLabel->setSizePolicy(QSizePolicy::MinimumExpanding,
                                    QSizePolicy::Minimum);
    connect(m_globalObject, &GlobalObject::fullyJoinedRoomsChanged,
            this, &GeneralModule::onFullyJoinedRoomsChanged);


    m_tmpPresenceBrowser = new QTextBrowser(this);


    m_connectButton = new QPushButton(QIcon::fromTheme("network-connect",
                                                       QIcon(":/images/button-online.png")),
                                      tr("Connect"),
                                      this);
    connect(m_connectButton, &QAbstractButton::clicked,
            this, &GeneralModule::connectionRequested);


    m_disconnectButton = new QPushButton(QIcon::fromTheme("network-disconnect",
                                                          QIcon(":/images/button-offline.png")),
                                         tr("Disconnect"),
                                         this);
    m_disconnectButton->hide();
    connect(m_disconnectButton, &QAbstractButton::clicked,
            this, &GeneralModule::disconnectionRequested);


    m_uptimeTimer = new QTimer(this);
    m_uptimeTimer->setInterval(60000);
    connect(m_uptimeTimer, &QTimer::timeout,
            this, &GeneralModule::onUptimeTimerTick);


    // Layout
    m_formLayout = new QFormLayout();
    m_formLayout->addRow(tr("Uptime:"),           m_uptimeLabel);
    m_formLayout->addRow(tr("Current Activity:"), m_activityLabel);
    m_formLayout->addRow(tr("Status Message:"),   m_statusMessageLabel);
    m_formLayout->addRow(tr("Rooms:"),            m_roomsInfoLabel);


    m_mainLayout = new QVBoxLayout();
    m_mainLayout->setContentsMargins(0, 0, 0, 0);
    m_mainLayout->addStretch(1);
    m_mainLayout->addWidget(m_infoLabel,          2);
    m_mainLayout->addWidget(m_serverVersionLabel, 1);
    m_mainLayout->addStretch(1);
    m_mainLayout->addLayout(m_formLayout,         2);
    m_mainLayout->addStretch(1);
    m_mainLayout->addWidget(m_tmpPresenceBrowser, 1);
    m_mainLayout->addWidget(m_connectButton,      0, Qt::AlignRight);
    m_mainLayout->addWidget(m_disconnectButton,   0, Qt::AlignRight);
    this->setLayout(m_mainLayout);

    qDebug() << "GeneralModule created";
}

GeneralModule::~GeneralModule()
{
    qDebug() << "GeneralModule destroyed";
}


void GeneralModule::setInfoMessage(QString message)
{
    m_infoLabel->setText(message);
}

void GeneralModule::setServerInfo(QString message)
{
    m_serverVersionLabel->setText(tr("Server version: %1").arg(message));
}


void GeneralModule::setConnecting(bool state)
{
    m_connectButton->setDisabled(state);
    m_uptimeLabel->setText(QStringLiteral("..."));
}


void GeneralModule::addPresence(QXmppPresence presence)
{
    // TMP FIXME
    QString availableType;
    if (presence.type() == QXmppPresence::Available)
    {
        switch (presence.availableStatusType())
        {
        case QXmppPresence::Away:
            availableType = "Away";
            break;
        case QXmppPresence::XA:
            availableType = "Extended away";
            break;
        case QXmppPresence::DND:
            availableType = "Do not disturb";
            break;
        case QXmppPresence::Chat:
            availableType = "Free for chat";
            break;

        default:
            availableType = "Online";
        }
    }
    else
    {
        availableType = "Offline";
    }

    if (!presence.statusText().isEmpty())
    {
        availableType.append(" (" + presence.statusText() + ")");
        // FIXME, statusText could include "%3" or something and mess this up
    }

    QString presenceType;
    switch (presence.type())
    {
    case QXmppPresence::Error:
        presenceType = "Error";
        break;
    case QXmppPresence::Available:
        presenceType = "Available";
        break;
    case QXmppPresence::Unavailable:
        presenceType = "Unavailable";
        break;

    default:
        presenceType = QString("%1, subscription-related").arg(presence.type());
    }

    const QString presenceString = QString("%1 is %2 [Priority: %3, Type: %4]")
                                   .arg(presence.from())
                                   .arg(availableType)
                                   .arg(presence.priority())
                                   .arg(presenceType);

    m_tmpPresenceBrowser->append(presenceString);
}


//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// SLOTS //////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


void GeneralModule::onConnected()
{
    m_connectButton->hide();
    m_connectButton->setEnabled(true);
    m_disconnectButton->show();

    m_uptimeLabel->setText(QStringLiteral("00:00"));
    m_upMinutes = 0;
    m_upHours = 0;
    m_upDays = 0;
    m_uptimeTimer->start();
}


void GeneralModule::onDisconnected()
{
    m_connectButton->setEnabled(true);
    m_connectButton->show();
    m_disconnectButton->hide();

    m_serverVersionLabel->clear();
    m_uptimeTimer->stop();
}


void GeneralModule::onUptimeTimerTick()
{
    ++m_upMinutes;
    if (m_upMinutes >= 60)
    {
        m_upMinutes = 0;
        ++m_upHours;
        if (m_upHours >= 24)
        {
            m_upHours = 0;
            ++m_upDays;
        }
    }

    QString uptime = QString("%1:%2").arg(m_upHours,   2, 10, QChar('0'))
                                     .arg(m_upMinutes, 2, 10, QChar('0'));
    if (m_upDays > 0)
    {
        uptime.prepend(QString("%1d, ").arg(m_upDays));
    }

    m_uptimeLabel->setText(uptime);
}


void GeneralModule::onStatusChanged(QXmppPresence presence)
{
    m_statusMessageLabel->setText(presence.statusText());
    // FIXME: show status type icon
}


void GeneralModule::onFullyJoinedRoomsChanged(int count)
{
    m_roomsInfoLabel->setText(tr("%1 joined, out of %2 configured",
                                 "Number of rooms joined and configured")
                              .arg(count)
                              .arg(m_globalObject->getConfiguredRoomsList()
                                                  .size()));
}

