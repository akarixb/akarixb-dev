/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "globalobject.h"


GlobalObject::GlobalObject(QObject *parent) : QObject(parent)
{
    m_userConnected = false;
    m_connectionStartTime = QDateTime();


    // Identity and activity-related
    m_availability = 99;
    m_minReactionTime = 1;
    m_maxReactionTime = 10;


    // Load settings
    QSettings settings;
    settings.beginGroup("Configuration");

    this->syncGeneralSettings(settings.value("adminJids").toStringList(),
                              settings.value("commandPrefix",
                                             "!").toString(),
                              settings.value("keepAliveInterval",
                                             60).toInt(),
                              settings.value("keepAliveTimeout",
                                             50).toInt());


    this->syncNotificationSettings(settings.value("notifyNewChats",
                                                  false).toBool(),
                                   settings.value("notifyDisconnection",
                                                  false).toBool());


    this->syncSystraySettings(settings.value("hideInTray",
                                             false).toBool(),
                              settings.value("useCustomTrayIcon",
                                             false).toBool(),
                              settings.value("customTrayIconFilename")
                                      .toString());



    qDebug() << "GlobalObject created";
}


GlobalObject::~GlobalObject()
{
    qDebug() << "GlobalObject destroyed";
}


void GlobalObject::syncGeneralSettings(QStringList adminJids,
                                       QString commandPrefix,
                                       int keepAliveInterval,
                                       int keepAliveTimeout)
{
    m_adminJids = adminJids;
    m_commandPrefix = commandPrefix;
    m_keepAliveInterval = keepAliveInterval;
    m_keepAliveTimeout = keepAliveTimeout;
}


QStringList GlobalObject::getAdminJids()
{
    return m_adminJids;
}


QString GlobalObject::getCommandPrefix()
{
    return m_commandPrefix;
}

int GlobalObject::getKeepAliveInterval()
{
    return m_keepAliveInterval;
}

int GlobalObject::getKeepAliveTimeout()
{
    return m_keepAliveTimeout;
}


void GlobalObject::syncNotificationSettings(bool forNewChats,
                                            bool forDisconnection)
{
    m_notifyNewChats = forNewChats;
    m_notifyDisconnection = forDisconnection;
}

bool GlobalObject::getNotifyNewChats()
{
    return m_notifyNewChats;
}

bool GlobalObject::getNotifyDisconnection()
{
    return m_notifyDisconnection;
}


void GlobalObject::syncSystraySettings(bool hideInTray, bool useCustomIcon,
                                       QString iconFilename)
{
    m_hideInSystemTray = hideInTray;
    m_useCustomTrayIcon = useCustomIcon;
    m_customTrayIconFilename = iconFilename;
}

bool GlobalObject::getHideInTray()
{
    return m_hideInSystemTray;
}

bool GlobalObject::getUseCustomTrayIcon()
{
    return m_useCustomTrayIcon;
}

QString GlobalObject::getCustomTrayIconFilename()
{
    return m_customTrayIconFilename;
}


/////


void GlobalObject::setXmppClient(QXmppClient *client)
{
    m_xmppClient = client;
}

QXmppClient *GlobalObject::getXmppClient()
{
    return m_xmppClient;
}


void GlobalObject::setMucManager(QXmppMucManager *mucManager)
{
    m_xmppMucManager = mucManager;
}

QXmppMucManager *GlobalObject::getMucManager()
{
    return m_xmppMucManager;
}


void GlobalObject::setVariableParser(VariableParser *varParser)
{
    m_variableParser = varParser;
}

VariableParser *GlobalObject::getVariableParser()
{
    return m_variableParser;
}


void GlobalObject::setSpeechDataStore(SpeechDataStore *speechDataStore)
{
    m_speechDataStore = speechDataStore;
}

SpeechDataStore *GlobalObject::getSpeechDataStore()
{
    return m_speechDataStore;
}



void GlobalObject::setDataDirectory(QString path)
{
    m_dataDirectory = path;
}

QString GlobalObject::getDataDirectory()
{
    return m_dataDirectory;
}



void GlobalObject::addToLog(QString message, bool inStatusBar,
                            int messageType)
{
    emit logMessage(message, messageType);

    if (inStatusBar)
    {
        emit showInStatusBar(message);
    }
}


void GlobalObject::showNotification(QString title, QString message)
{
    emit showNotificationPopup(title, message);
}


void GlobalObject::setConnected(bool state)
{
    m_userConnected = state;
    if (m_userConnected)
    {
        m_connectionStartTime = QDateTime::currentDateTimeUtc();
    }
    else
    {
        m_connectionStartTime = QDateTime(); // Invalid time
    }
}


bool GlobalObject::connectedToServer()
{
    return m_userConnected;
}

QDateTime GlobalObject::getConnectionStartTime()
{
    return m_connectionStartTime;
}



void GlobalObject::addJoinedRoom(QString roomJid)
{
    if (!m_joinedRooms.contains(roomJid))
    {
        m_joinedRooms.append(roomJid);
    }
}

void GlobalObject::removeJoinedRoom(QString roomJid)
{
    m_joinedRooms.removeAll(roomJid);
}

QStringList GlobalObject::getJoinedRooms()
{
    return m_joinedRooms;
}


void GlobalObject::addFullyJoinedRoom(QString roomJid)
{
    if (!m_fullyJoinedRooms.contains(roomJid))
    {
        m_fullyJoinedRooms.append(roomJid);
    }

    emit fullyJoinedRoomsChanged(m_fullyJoinedRooms.size());
}

void GlobalObject::removeFullyJoinedRoom(QString roomJid)
{
    m_fullyJoinedRooms.removeAll(roomJid);

    emit fullyJoinedRoomsChanged(m_fullyJoinedRooms.size());
}

QStringList GlobalObject::getFullyJoinedRooms()
{
    return m_fullyJoinedRooms;
}

bool GlobalObject::isRoomFullyJoined(QString roomJid)
{
    return m_fullyJoinedRooms.contains(roomJid);
}

/*
 * Keep a list of configured rooms, joined or not, for checks
 *
 */
void GlobalObject::setConfiguredRoomsList(QStringList roomList)
{
    m_configuredRooms = roomList;

    // Fully joined didn't change, but notify GeneralModule anyway,
    // to update number of configured rooms
    emit fullyJoinedRoomsChanged(m_fullyJoinedRooms.size());
}

QStringList GlobalObject::getConfiguredRoomsList()
{
    return m_configuredRooms;
}

bool GlobalObject::isRoomConfigured(QString roomJid)
{
    return m_configuredRooms.contains(roomJid);
}


QXmppMucRoom *GlobalObject::roomFromJid(QString roomJid)
{
    foreach (QXmppMucRoom *room, m_xmppMucManager->rooms())
    {
        if (room->jid() == roomJid)
        {
            return room;
        }
    }

    return nullptr;
}


void GlobalObject::setOwnNickname(QString newNick)
{
    m_ownNickname = newNick;
    m_variableParser->setOwnNickname(newNick);
}


QString GlobalObject::getOwnNickname()
{
    return m_ownNickname;
}


void GlobalObject::setAvailability(int availability)
{
    m_availability = availability;
}

int GlobalObject::getAvailability()
{
    return m_availability;
}


void GlobalObject::setReactionTime(int minReactionTime, int maxReactionTime)
{
    m_minReactionTime = minReactionTime;
    m_maxReactionTime = maxReactionTime;
}

int GlobalObject::getReactionTime()
{
    return Helpers::randomBetween(m_minReactionTime, m_maxReactionTime);
}


void GlobalObject::notifyActivityChange(QString name, QString timeRange)
{
    QString text = name;
    if (!timeRange.isEmpty())
    {
        text.append(QString::fromUtf8(" &nbsp; &nbsp; \342\217\260 ") // Alarm clock symbol
                    + timeRange);
    }

    // TMP: show availability -- FIXME
    text.append(QString(" &nbsp; &nbsp; [%1%]").arg(m_availability));

    emit activityChanged(text);
}

void GlobalObject::notifyStatusChange(QXmppPresence presence)
{
    emit statusChanged(presence);
}



void GlobalObject::sendMessageToJid(QString jid, QString message)
{
    if (!jid.isEmpty() && !message.isEmpty())
    {
        if (m_joinedRooms.contains(jid))
        {
            QXmppMucRoom *room = this->roomFromJid(jid);
            if (room != nullptr)
            {
                room->sendMessage(message);
            }
        }
        else
        {
            // Send it using this function so it gets logged
            this->sendPrivateMessage(jid, message);
        }
    }
}



void GlobalObject::sendPrivateMessage(QString jid, QString message)
{
    m_xmppClient->sendMessage(jid, message);

    // Let others know, to log it and so on
    emit privateMessageSent(jid, message);
}


void GlobalObject::sendMessageState(QString jid, QXmppMessage::State state)
{
    QXmppMessage stateStanza;
    stateStanza.setTo(jid);
    stateStanza.setState(state);
    m_xmppClient->sendPacket(stateStanza);
}


/*
 * Request empty tab to be added in ChatModule, and the module to be focused
 *
 */
void GlobalObject::startPrivateConversation(QString jid)
{
    emit conversationTabRequested(jid);
}

