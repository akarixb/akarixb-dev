## This one-liner is to be used by developers only.
## It's not necessary to use this when building the program.

# This will generate a small .ts file for English, with
# just a few singular/plural strings
lupdate ../AkariXB.pro -ts akarixb_en.ts -pluralonly

