/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef GLOBALOBJECT_H
#define GLOBALOBJECT_H

#include <QObject>
#include <QSettings>
#include <QStringList>
#include <QDateTime>

#include <QDebug>

#include <QXmppClient.h>
#include <QXmppMucManager.h>
#include <QXmppMessage.h>


#include "variableparser.h"
#include "helpers.h"
#include "speechdatastore.h"


class GlobalObject : public QObject
{
    Q_OBJECT

public:
    explicit GlobalObject(QObject *parent = nullptr);
    ~GlobalObject();


    // General settings
    void syncGeneralSettings(QStringList adminJids, QString commandPrefix,
                             int keepAliveInterval, int keepAliveTimeout);
    QStringList getAdminJids();
    QString getCommandPrefix();
    int getKeepAliveInterval();
    int getKeepAliveTimeout();

    // Notification settings
    void syncNotificationSettings(bool forNewChats,
                                  bool forDisconnection);
    bool getNotifyNewChats();
    bool getNotifyDisconnection();

    // Systray settings
    void syncSystraySettings(bool hideInTray, bool useCustomIcon,
                             QString iconFilename);
    bool getHideInTray();
    bool getUseCustomTrayIcon();
    QString getCustomTrayIconFilename();


    //////////


    void setXmppClient(QXmppClient *client);
    QXmppClient *getXmppClient();

    void setMucManager(QXmppMucManager *mucManager);
    QXmppMucManager *getMucManager();

    void setVariableParser(VariableParser *varParser);
    VariableParser *getVariableParser();

    void setSpeechDataStore(SpeechDataStore *speechDataStore);
    SpeechDataStore *getSpeechDataStore();

    void setDataDirectory(QString path);
    QString getDataDirectory();


    //////////


    void addToLog(QString message,
                  bool inStatusBar=true,
                  int messageType=LogNormal);
    void showNotification(QString title, QString message);

    void setConnected(bool state);
    bool connectedToServer();
    QDateTime getConnectionStartTime();

    void addJoinedRoom(QString roomJid);
    void removeJoinedRoom(QString roomJid);
    QStringList getJoinedRooms();
    void addFullyJoinedRoom(QString roomJid);
    void removeFullyJoinedRoom(QString roomJid);
    QStringList getFullyJoinedRooms();
    bool isRoomFullyJoined(QString roomJid);

    void setConfiguredRoomsList(QStringList roomList);
    QStringList getConfiguredRoomsList();
    bool isRoomConfigured(QString roomJid);


    QXmppMucRoom *roomFromJid(QString roomJid);


    // Activity-related
    void setOwnNickname(QString newNick);
    QString getOwnNickname();

    void setAvailability(int availability);
    int getAvailability();
    void setReactionTime(int minReactionTime, int maxReactionTime);
    int getReactionTime();


    void notifyActivityChange(QString name, QString timeRange);
    void notifyStatusChange(QXmppPresence presence);
    ///


    void sendMessageToJid(QString jid, QString message);
    void sendPrivateMessage(QString jid, QString message);
    void sendMessageState(QString jid, QXmppMessage::State state);
    void startPrivateConversation(QString jid);

    enum logMessageType
    {
        LogNormal,
        LogGood,
        LogBad
    };


signals:
    void logMessage(QString message, int messageType);
    void showInStatusBar(QString message);
    void showNotificationPopup(QString title, QString message);

    void activityChanged(QString activityInfo);
    void statusChanged(QXmppPresence presence);
    void fullyJoinedRoomsChanged(int count);

    void privateMessageSent(QString jid, QString message);
    void conversationTabRequested(QString jid);


public slots:


private:
    QXmppClient *m_xmppClient;
    QXmppMucManager *m_xmppMucManager;

    VariableParser *m_variableParser;
    SpeechDataStore *m_speechDataStore;


    QString m_dataDirectory;

    bool m_userConnected;
    QDateTime m_connectionStartTime;

    QStringList m_joinedRooms;
    QStringList m_fullyJoinedRooms;
    QStringList m_configuredRooms;

    // Identity and activity-related
    QString m_ownNickname;

    int m_availability;
    int m_minReactionTime;
    int m_maxReactionTime;


    // General options
    QStringList m_adminJids;
    QString m_commandPrefix;
    int m_keepAliveInterval;
    int m_keepAliveTimeout;


    // Notification options
    bool m_notifyNewChats;
    bool m_notifyDisconnection;


    // Systray options
    bool m_hideInSystemTray;
    bool m_useCustomTrayIcon;
    QString m_customTrayIconFilename;

};

#endif // GLOBALOBJECT_H
