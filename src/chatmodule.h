/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef CHATMODULE_H
#define CHATMODULE_H

#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QTabWidget>
#include <QTabBar>
#include <QAction>

#include <QDebug>

#include <QXmppClient.h>

#include "globalobject.h"
#include "chatwidget.h"


class ChatModule : public QWidget
{
    Q_OBJECT

public:
    explicit ChatModule(GlobalObject *globalObject, QWidget *parent = nullptr);
    ~ChatModule();

    void setTabTitle(int tabNumber, QString name, int unread);
    int tabForJid(QString jid, int messageDirection);

    enum MessageDirection
    {
        MessageIncoming,
        MessageOutgoing
    };


signals:
    void unreadCountChanged(int count);


public slots:
    void addChatTab(QXmppMessage message);
    void resetTabTitle(QString chatTitle, int count);
    void closeTab(int tabIndex);
    void closeCurrentTab();
    void addOutgoingMessage(QString toJid, QString message);
    void startConversation(QString toJid);


private:
    QVBoxLayout *m_mainLayout;
    QTabWidget *m_tabWidget;
    QLabel *m_noChatsLabel;

    QList<ChatWidget *> m_openedChats;

    QAction *m_closeTabAction;

    int m_unreadCount;

    QXmppClient *m_xmppClient;
    GlobalObject *m_globalObject;
};

#endif // CHATMODULE_H
