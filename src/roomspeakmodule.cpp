/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "roomspeakmodule.h"

RoomSpeakModule::RoomSpeakModule(GlobalObject *globalObject,
                                 QWidget *parent) : QTabWidget(parent)
{
    m_globalObject = globalObject;

    const QString titleGreetings = tr("Greetings");
    const QString titleCasualSpeak = tr("Casual speak");
    const QString titleReactions = tr("Reactions to words");


    m_greetingsTab = new RoomSpeakTab(RoomSpeakTab::SentenceGreeting,
                                      titleGreetings,
                                      tr("Sentences used to greet people "
                                         "who enter a chatroom.")
                                      + " "
                                      + tr("The %1 variable can be used "
                                           "to insert their names in the "
                                           "sentences.").arg("<b>%user%</b>"),
                                      QStringLiteral("/greetings.axb"),
                                      m_globalObject,
                                      this);
    m_greetingsTab->getStringListWidget()
                  ->setCheckSyntaxEnabled(m_globalObject->getVariableParser(),
                                          QStringList{"%user%"});



    m_casualSpeakTab = new RoomSpeakTab(RoomSpeakTab::SentenceCasual,
                                        titleCasualSpeak,
                                        tr("Sentences to be sent to "
                                           "chatrooms at random intervals."),
                                        QStringLiteral("/casualspeak.axb"),
                                        m_globalObject,
                                        this);
    m_casualSpeakTab->getStringListWidget()
                    ->setCheckSyntaxEnabled(m_globalObject->getVariableParser());



    m_reactionsTab = new RoomSpeakTab(RoomSpeakTab::SentenceReaction,
                                      titleReactions,
                                      tr("Regular expressions to be matched "
                                         "against messages coming from the "
                                         "chatrooms, and possible messages "
                                         "to send in response.")
                                      + "<br />"
                                      + tr("Example:")
                                      + QStringLiteral(" <b>\\b(he+llo+|hi+)\\b</b> "
                                                       "&nbsp; &bull; &nbsp; ")
                                      + "<a href=\"https://perldoc.perl.org/perlre.html\">"
                                      + tr("Documentation for Perl regular "
                                           "expressions")
                                      + " (perl.org)</a>",
                                      QStringLiteral("/reactions.axb"),
                                      m_globalObject,
                                      this);
    // Syntax checking enabled for sub-list, not for main list, which holds regexps
    m_reactionsTab->getStringListWidget()
                  ->setSublistSyntaxEnabled(m_globalObject->getVariableParser(),
                                            QStringList{"%user%",
                                                        "%cg1%","%cg2%","%cg3%",
                                                        "%cg4%","%cg5%","%cg6%",
                                                        "%cg7%","%cg8%","%cg9%"});



    this->addTab(m_greetingsTab,
                 QIcon::fromTheme("view-conversation-balloon",
                                  QIcon(":/images/section-conv.png")),
                 titleGreetings);

    this->addTab(m_casualSpeakTab,
                 QIcon::fromTheme("view-conversation-balloon",
                                  QIcon(":/images/section-conv.png")),
                 titleCasualSpeak);

    this->addTab(m_reactionsTab,
                 QIcon::fromTheme("view-conversation-balloon",
                                  QIcon(":/images/section-conv.png")),
                 titleReactions);


    qDebug() << "RoomSpeakModule created";
}


RoomSpeakModule::~RoomSpeakModule()
{
    qDebug() << "RoomSpeakModule destroyed";
}
