/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "messagehandler.h"


MessageHandler::MessageHandler(GlobalObject *globalObject,
                               QObject *parent) : QObject(parent)
{
    m_globalObject = globalObject;
    m_variableParser = m_globalObject->getVariableParser();

    m_xmppClient = m_globalObject->getXmppClient();
    m_mucManager = m_globalObject->getMucManager();

    m_conversationHandler = new ConversationHandler(m_globalObject, this);


    qDebug() << "MessageHandler created";
}


MessageHandler::~MessageHandler()
{
    qDebug() << "MessageHandler destroyed";
}



void MessageHandler::processMessage(QXmppMessage message)
{
    QStringList from = message.from().split("/");
    const QString bareFrom = from.first();
    from.removeFirst();

    if (message.type() == QXmppMessage::GroupChat)
    {
        // Find the matching room object
        QXmppMucRoom *room = m_globalObject->roomFromJid(bareFrom);
        if (room != nullptr)
        {
            const QString fromNick = from.join("/");
            if (fromNick == room->nickName()) // Ensure it's not somehow from us
            {
                return;
            }

            // FIXME: basic check for our nickname, to trigger chatroom public conversation
            // TODO: check for "nick," and "nick:" - Also user regexps like "hey %nick%"
            if (message.body().startsWith(room->nickName(), Qt::CaseInsensitive))
            {
                // Ignore conversation attempts if the room is configured to not allow it
                if (!room->property("allowConversing").toBool())
                {
                    qDebug() << "Room" << bareFrom << "doesn't allow conversing:"
                             << message.body();
                    return;
                }

                // FIXME: remove own nick from the beginning of the message
                m_conversationHandler->processMessage(message.from(),
                                                      message.body(),
                                                      room,
                                                      fromNick);
            }
        }
    }
    else if (message.type() == QXmppMessage::Normal
          || message.type() == QXmppMessage::Chat)
    {
        // FIXME: pass room info if this is a private chat from inside a room (WIP)
        if (m_globalObject->getJoinedRooms().contains(bareFrom))
        {
            m_conversationHandler->processMessage(message.from(),
                                                  message.body(),
                                                  m_globalObject->roomFromJid(bareFrom),
                                                  from.join("/"));
        }
        else
        {
            m_conversationHandler->processMessage(message.from(),
                                                  message.body());
        }
    }
    else if (message.type() == QXmppMessage::Headline)
    {
        const QString headlineMessage = tr("Headline from %1: %2",
                                           "%1 is a user's address,"
                                           "%2 is the title of a message")
                                        .arg("'<b>" + message.from().toHtmlEscaped()
                                             + "</b>'")
                                        .arg(message.subject().toHtmlEscaped())
                                        + "<br />"
                                        + message.body().toHtmlEscaped();
        // FIXME: log as important?
        m_globalObject->addToLog(headlineMessage);
        qDebug() << "Message type is Headline:";
        qDebug() << "Subject:" << message.subject();
        qDebug() << "Body:" << message.body();
    }
    else // QXmppMessage::Error
    {
        const QString errorMessage = tr("Error %1 from %2: %3")
                                     .arg(QString("<b>%1</b>")
                                          .arg(message.error().code()))
                                     .arg("'<b>" + message.from().toHtmlEscaped()
                                          + "</b>'")
                                     .arg(message.error().text());

        m_globalObject->addToLog(errorMessage,
                                 true, // Show in status bar too
                                 GlobalObject::LogBad);

        qDebug() << "Message type is Error:";
        qDebug() << "Subject:" << message.subject();
        qDebug() << "Body:" << message.body();
        qDebug() << "Error text:" << message.error().text();
        qDebug() << "Error code:" << message.error().code();
        qDebug() << "------";
    }
}

