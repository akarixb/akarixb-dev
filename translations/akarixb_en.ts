<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>ActivityHandler</name>
    <message numerus="yes">
        <source>%Ln hour(s)</source>
        <translation>
            <numerusform>1 hour</numerusform>
            <numerusform>%Ln hours</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%Ln minute(s)</source>
        <translation>
            <numerusform>1 minute</numerusform>
            <numerusform>%Ln minutes</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>ActivityModule</name>
    <message numerus="yes">
        <source>Saved %Ln activities.</source>
        <translation>
            <numerusform>Saved 1 activity.</numerusform>
            <numerusform>Saved %Ln activities.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>Loaded %Ln activities.</source>
        <translation>
            <numerusform>Loaded 1 activity.</numerusform>
            <numerusform>Loaded %Ln activities.</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>CommandModule</name>
    <message numerus="yes">
        <source>Loaded %Ln command(s).</source>
        <translation>
            <numerusform>Loaded 1 command.</numerusform>
            <numerusform>Loaded %Ln commands.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>Saved %Ln command(s).</source>
        <translation>
            <numerusform>Saved 1 command.</numerusform>
            <numerusform>Saved %Ln commands.</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>RoomModule</name>
    <message numerus="yes">
        <source>Loaded %Ln room(s).</source>
        <translation>
            <numerusform>Loaded 1 room.</numerusform>
            <numerusform>Loaded %Ln rooms.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>Saved %Ln room(s).</source>
        <translation>
            <numerusform>Saved 1 room.</numerusform>
            <numerusform>Saved %Ln rooms.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%Ln room(s)</source>
        <translation>
            <numerusform>1 room</numerusform>
            <numerusform>%Ln rooms</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>RoomSpeakTab</name>
    <message numerus="yes">
        <source>Loaded %Ln sentence(s) of type %1.</source>
        <translation>
            <numerusform>Loaded 1 sentence of type %1.</numerusform>
            <numerusform>Loaded %Ln sentences of type %1.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>Saved %Ln sentence(s) of type %1.</source>
        <translation>
            <numerusform>Saved 1 sentence of type %1.</numerusform>
            <numerusform>Saved %Ln sentences of type %1.</numerusform>
        </translation>
    </message>
</context>
</TS>
