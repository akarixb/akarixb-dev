/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "conversationhandler.h"


ConversationHandler::ConversationHandler(GlobalObject *globalObject,
                                         QObject *parent) : QObject(parent)
{
    m_globalObject = globalObject;
    m_variableParser = m_globalObject->getVariableParser();
    m_speechDataStore = m_globalObject->getSpeechDataStore();


    // Timer to send queued message states after a "reaction" delay
    m_scheduledStatesTimer = new QTimer(this);
    m_scheduledStatesTimer->setSingleShot(true);
    connect(m_scheduledStatesTimer, &QTimer::timeout,
            this, &ConversationHandler::sendScheduledState);

    // Timer to send queued messages after the "reaction+typing" delay
    m_scheduledMessagesTimer = new QTimer(this);
    m_scheduledMessagesTimer->setSingleShot(true);
    connect(m_scheduledMessagesTimer, &QTimer::timeout,
            this, &ConversationHandler::sendScheduledMessage);


    qDebug() << "ConversationHandler created";
}

ConversationHandler::~ConversationHandler()
{
    qDebug() << "ConversationHandler destroyed";
}


void ConversationHandler::processMessage(QString jidFrom, QString message,
                                         QXmppMucRoom *room, QString nickFrom)
{
    if (message.trimmed().isEmpty())
    {
        return;
    }

    if (!m_currentConversationJids.contains(jidFrom))
    {
        m_currentConversationJids.append(jidFrom);
    }

    if (true)  // tmp, FIXME; actual checks for availability, etc
    {
        QString scheduledMessage;
        const QVariantList allRegExps = m_speechDataStore->getAllConversationSentences();

        for (QVariant oneRegExp : allRegExps)
        {
            const QVariantMap reMap = oneRegExp.toMap();
            if (reMap.keys().isEmpty())
            {
                return;
            }

            const QString reString = reMap.keys().first();
            QRegularExpression re(reString, // Defaults to case insensitive for now
                                  QRegularExpression::CaseInsensitiveOption); // FIXME

            QRegularExpressionMatch reMatch = re.match(message);
            if (reMatch.hasMatch())
            {
                const QStringList replies = reMap.value(reString).toStringList();
                scheduledMessage = Helpers::randomString(replies);
                scheduledMessage = m_variableParser->getParsed(scheduledMessage);

                // Determine a decent user nick for the %user% in replies
                // For messages from rooms, this comes predefined
                if (nickFrom.isEmpty())
                {
                    // Not from a room and there's a username, unlike with server messages
                    if (room == nullptr && jidFrom.contains("@"))
                    {
                        nickFrom = jidFrom.split("@").first();
                    }
                    else
                    {
                        nickFrom = jidFrom; // This should ideally never happen
                    }
                }

                scheduledMessage.replace(QStringLiteral("%user%"), nickFrom);
                // FIXME, optionally prepend nickname in all conversation replies?
                // scheduledMessage.prepend(nickFrom + ", ");

                qDebug() << "~~~ CAPTURED GROUPS:" << reMatch.lastCapturedIndex();
                qDebug() << "* Implicit CG #0:" << reMatch.captured(0);
                // Replace each instance of %cg#% with the corresponding captured group
                for (int capCount = 1; capCount <= reMatch.lastCapturedIndex(); ++capCount)
                {
                    scheduledMessage.replace(QString("%cg%1%").arg(capCount), // FIXME
                                             reMatch.captured(capCount));
                    qDebug() << reMatch.captured(capCount);
                }


                // If nothing's pending and timer is stopped...
                if (m_scheduledMessagesList.isEmpty())
                {
                    const int reactionTime = m_globalObject->getReactionTime() * 1000;

                    // DO NOT send XMPP stanzas via sendPacket() from here, schedule it
                    m_scheduledStatesList.append(QPair(jidFrom, QXmppMessage::Composing));
                    m_scheduledStatesTimer->start(reactionTime);
                    // FIXME: Don't send 'composing' to rooms

                    // FIXME, reaction time + very basic "typing" delay);
                    m_scheduledMessagesTimer->start(reactionTime
                                                    + scheduledMessage.length() * 160);
                    qDebug() << "Scheduling first conversation message after"
                             << reactionTime << "msec (reaction) + "
                             << scheduledMessage.length() * 160 << "msec (typing)";
                }


                // Add replies to scheduled list
                const QString bareFrom = jidFrom.split("/").first();
                if (room == nullptr  /// Won't be NULL when msg is private FROM room, so...
                    || !m_globalObject->getJoinedRooms().contains(bareFrom))
                {
                    m_scheduledMessagesList.append(QPair(jidFrom, scheduledMessage));
                }
                else
                {
                    m_scheduledMessagesList.append(QPair(room->jid(), scheduledMessage));
                }

                return;
            }
        } // END Loop through all RegExps
    } // END Check if speaking allowed, availability...
}



//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// SLOTS //////////////////////////////////
//////////////////////////////////////////////////////////////////////////////



void ConversationHandler::sendScheduledState()
{
    if (m_scheduledStatesList.isEmpty())
    {
        return;
    }

    const QPair<QString,QXmppMessage::State> jidStatePair = m_scheduledStatesList.takeFirst();
    m_globalObject->sendMessageState(jidStatePair.first, jidStatePair.second);
}


void ConversationHandler::sendScheduledMessage()
{
    if (m_scheduledMessagesList.isEmpty())
    {
        return;
    }

    const QPair<QString,QString> jidMessagePair = m_scheduledMessagesList.takeFirst();
    m_globalObject->sendMessageToJid(jidMessagePair.first,
                                     jidMessagePair.second);

    if (!m_scheduledMessagesList.isEmpty())
    {
        const int reactionTime = m_globalObject->getReactionTime()
                                 * 1000; // To msec
        const int typingTime = m_scheduledMessagesList.first().second.length()
                               * 120; // 120ms per character FIXME?

        //FIXME: check for empty list before (re)starting timer?
        // Schedule to send message state via GlobalObject
        m_scheduledStatesList.append(QPair(jidMessagePair.first, QXmppMessage::Composing));
        m_scheduledStatesTimer->start(reactionTime);
        // Sending directly from here would crash on QXmppClient::sendPacket()

        // Schedule to send next message (now first), with a delay based on less reaction time
        m_scheduledMessagesTimer->start((reactionTime / 2) + typingTime); // plus message length
        qDebug() << "Scheduling to send next conversation message after"
                 << reactionTime/2 << "msec (reaction) + "
                 << typingTime << "msec (typing)";
    }
}
