/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef ROOMMODULE_H
#define ROOMMODULE_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTableWidget>
#include <QHeaderView>
#include <QGroupBox>
#include <QLineEdit>
#include <QCheckBox>
#include <QSpinBox>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QMessageBox>
#include <QLabel>
#include <QStackedWidget>
#include <QResizeEvent>
#include <QShowEvent>
#include <QFontMetrics>
#include <QChar>
#include <QAction>

#include <QDebug>

#include <QXmppMucManager.h>
#include <QXmppClient.h>

#include "globalobject.h"
#include "helpers.h"
#include "chatwidget.h"
#include "datafile.h"
#include "roomspeakhandler.h"


class RoomModule : public QWidget
{
    Q_OBJECT

public:
    explicit RoomModule(GlobalObject *globalObject, QWidget *parent = nullptr);
    ~RoomModule();

    void loadRoomInfo();
    void saveRoomInfo();

    void adjustRoomsTable();

    QTableWidgetItem *tableItemFromJid(QString roomJid);
    int stackIndexFromJid(QString roomJid);
    QString allowedFeaturesString(bool commands, bool greetings,
                                  bool casualSpeak, bool conversing);

    void joinRoom(QString roomJid, QString nickname, QString password);
    void leaveRoom(QString roomJid, QString reason);
    void autojoinRooms();

    bool newRoomIsValid(bool showErrors=false);
    void clearRoomDetails();

    void toggleJoinLeaveButtons(QTableWidgetItem *item);


signals:


public slots:
    void addRoom();
    void editRoom();
    void updateRoom();
    void removeRoom();
    void cancelEditMode();

    void selectFirstRow();
    void selectLastRow();

    void joinSelectedRoom();
    void leaveSelectedRoom();
    void changeNick();

    void showRoomDetails(int row);
    void toggleRoomDetails(bool state);
    void onRoomDoubleClicked(QTableWidgetItem *item);

    void onRoomJoined(QString jid);
    void onRoomLeft(QString jid);


protected:
    virtual void showEvent(QShowEvent *event);
    virtual void resizeEvent(QResizeEvent *event);


private:
    QVBoxLayout *m_mainLayout;
    QTableWidget *m_roomTableWidget;
    QString m_keySymbolString;

    QHBoxLayout *m_buttonsLayout;
    QPushButton *m_addRoomButton;
    QLabel *m_roomCountLabel;
    QPushButton *m_removeRoomButton;

    QGroupBox *m_detailsGroupbox;
    QVBoxLayout *m_detailsLayout;
    QHBoxLayout *m_detailsUpperLayout;
    QLineEdit *m_roomJidLineEdit;
    QLineEdit *m_nickLineEdit;
    QLineEdit *m_roomPasswordLineEdit;
    QCheckBox *m_autojoinCheckbox;
    QPushButton *m_editRoomButton;
    QPushButton *m_joinButton;
    QPushButton *m_leaveButton;
    QPushButton *m_updateRoomButton;
    QPushButton *m_cancelButton;
    QDialogButtonBox *m_updateCancelButtonBox;

    QHBoxLayout *m_detailsExtraLayout;
    QVBoxLayout *m_greetingsLayout;
    QVBoxLayout *m_casualSpeakLayout;
    QLabel *m_allowedToLabel;
    QCheckBox *m_allowCommandsCheckbox;
    QCheckBox *m_allowGreetingsCheckbox;
    QSpinBox *m_greetingsSpinbox;
    QCheckBox *m_allowCasualSpeakCheckbox; // Including activity-related
    QSpinBox *m_casualSpeakSpinbox;
    QCheckBox *m_allowConversingCheckbox;


    QStackedWidget *m_chatStackedWidget;
    QList<ChatWidget *> m_openedRooms;
    QList<RoomSpeakHandler *> m_roomSpeakHandlers;

    QLabel *m_firstPageLabel;


    QAction *m_goFirstAction;
    QAction *m_goLastAction;


    DataFile *m_dataFile;

    QXmppMucManager *m_mucManager;
    QXmppClient *m_xmppClient;
    GlobalObject *m_globalObject;
};

#endif // ROOMMODULE_H
