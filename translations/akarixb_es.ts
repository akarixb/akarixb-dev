<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>ActivityHandler</name>
    <message>
        <source>Starting activity %1, which will end at %2.</source>
        <translation>Comenzando actividad %1, que acabará a las %2.</translation>
    </message>
    <message>
        <source>Activity ended: %1.</source>
        <translation>Actividad finalizada: %1.</translation>
    </message>
    <message>
        <source>None</source>
        <comment>Regarding an activity</comment>
        <translation>Ninguna</translation>
    </message>
    <message numerus="yes">
        <source>%Ln hour(s)</source>
        <translation>
            <numerusform>1 hora</numerusform>
            <numerusform>%Ln horas</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%Ln minute(s)</source>
        <translation>
            <numerusform>1 minuto</numerusform>
            <numerusform>%Ln minutos</numerusform>
        </translation>
    </message>
    <message>
        <source>Not sending message to room %1 because I am not currently in it.</source>
        <translation>No puedo enviar el mensaje a la sala %1 porque no estoy actualmente en ella.</translation>
    </message>
</context>
<context>
    <name>ActivityModule</name>
    <message>
        <source>&amp;Add Activity</source>
        <translation>&amp;Añadir actividad</translation>
    </message>
    <message>
        <source>minutes</source>
        <translation>minutos</translation>
    </message>
    <message>
        <source>Online</source>
        <translation>Conectado</translation>
    </message>
    <message>
        <source>Free for Chat</source>
        <translatorcomment>&quot;charlar&quot;, como lo traducen en Psi</translatorcomment>
        <translation>Disponible para charlar</translation>
    </message>
    <message>
        <source>Away</source>
        <translation>Ausente</translation>
    </message>
    <message>
        <source>Extended Away</source>
        <translation>No disponible</translation>
    </message>
    <message>
        <source>Do not Disturb</source>
        <translation>No molestar</translation>
    </message>
    <message>
        <source>seconds</source>
        <translation>segundos</translation>
    </message>
    <message>
        <source>All rooms</source>
        <translation>Todas las salas</translation>
    </message>
    <message>
        <source>Several rooms</source>
        <translation>Varias salas</translation>
    </message>
    <message>
        <source>A few rooms</source>
        <translation>Unas pocas salas</translation>
    </message>
    <message>
        <source>Also to active private chats</source>
        <translation>También a conversaciones privadas activas</translation>
    </message>
    <message>
        <source>Sa&amp;ve Activity</source>
        <translation>&amp;Guardar actividad</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <source>Maximum Times Per Day</source>
        <translation>Máximo de veces por día</translation>
    </message>
    <message>
        <source>Probability</source>
        <translation>Probabilidad</translation>
    </message>
    <message>
        <source>Time Range</source>
        <translation>Rango de horas</translation>
    </message>
    <message>
        <source>Date Range</source>
        <translation>Rango de fechas</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation>Duración</translation>
    </message>
    <message>
        <source>Set Status Type</source>
        <translation>Establecer tipo de estado</translation>
    </message>
    <message>
        <source>Availability</source>
        <translation>Disponibilidad</translation>
    </message>
    <message>
        <source>Reaction Time</source>
        <translation>Tiempo de reacción</translation>
    </message>
    <message>
        <source>Descriptions</source>
        <translation>Descripciones</translation>
    </message>
    <message>
        <source>Messages Before</source>
        <translation>Mensajes antes</translation>
    </message>
    <message>
        <source>Send Messages To</source>
        <translation>Enviar mensajes a</translation>
    </message>
    <message>
        <source>&amp;Sync Activities</source>
        <translation>&amp;Sincronizar actividades</translation>
    </message>
    <message>
        <source>&amp;Remove Activity</source>
        <translation>&amp;Quitar actividad</translation>
    </message>
    <message>
        <source>Messages Afterwards</source>
        <translation>Mensajes después</translation>
    </message>
    <message>
        <source>New Activity</source>
        <translation>Nueva actividad</translation>
    </message>
    <message>
        <source>Activity exists</source>
        <translation>La actividad existe</translation>
    </message>
    <message>
        <source>Error: There is already an activity in the list with this name.</source>
        <translation>Error: Ya hay una actividad en la lista con este nombre.</translation>
    </message>
    <message>
        <source>List of room or user addresses, separated by space</source>
        <translation>Lista de direcciones de salas o de usuarios, separadas por espacios</translation>
    </message>
    <message>
        <source>Status Messages</source>
        <translation>Mensajes de estado</translation>
    </message>
    <message numerus="yes">
        <source>Saved %Ln activities.</source>
        <translation>
            <numerusform>Se ha guardado 1 actividad.</numerusform>
            <numerusform>Se han guardado %Ln actividades.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>Loaded %Ln activities.</source>
        <translation>
            <numerusform>Se ha cargado 1 actividad.</numerusform>
            <numerusform>Se han cargado %Ln actividades.</numerusform>
        </translation>
    </message>
    <message>
        <source>None of the rooms</source>
        <translation>Ninguna de las salas</translation>
    </message>
    <message>
        <source>Additional specific JIDs:</source>
        <translation>JIDs específicos adicionales:</translation>
    </message>
    <message>
        <source>Mon</source>
        <comment>Monday, shortened if possible</comment>
        <translation>Lun</translation>
    </message>
    <message>
        <source>Tue</source>
        <comment>Tuesday, shortened if possible</comment>
        <translation>Mar</translation>
    </message>
    <message>
        <source>Wed</source>
        <comment>Wednesday, shortened if possible</comment>
        <translation>Mie</translation>
    </message>
    <message>
        <source>Thu</source>
        <comment>Thursday, shortened if possible</comment>
        <translation>Jue</translation>
    </message>
    <message>
        <source>Fri</source>
        <comment>Friday, shortened if possible</comment>
        <translation>Vie</translation>
    </message>
    <message>
        <source>Sat</source>
        <comment>Saturday, shortened if possible</comment>
        <translation>Sab</translation>
    </message>
    <message>
        <source>Sun</source>
        <comment>Sunday, shortened if possible</comment>
        <translation>Dom</translation>
    </message>
    <message>
        <source>Days Of The Week</source>
        <translation>Días de la semana</translation>
    </message>
</context>
<context>
    <name>ChatModule</name>
    <message>
        <source>Chat started by %1.</source>
        <comment>%1 = A nickname</comment>
        <translation>Conversación iniciada por %1.</translation>
    </message>
    <message>
        <source>New chat</source>
        <translation>Nueva conversación</translation>
    </message>
    <message>
        <source>A new chat was started by %1.</source>
        <translation>%1 ha iniciado una nueva conversación.</translation>
    </message>
    <message>
        <source>There are no active chats</source>
        <translation>No hay conversaciones activas</translation>
    </message>
</context>
<context>
    <name>ChatWidget</name>
    <message>
        <source>Reason:</source>
        <translation>Motivo:</translation>
    </message>
    <message>
        <source>Kicked from the room!</source>
        <translation>¡Expulsado de la sala!</translation>
    </message>
    <message>
        <source>Joined room %1, as %2.</source>
        <translatorcomment>Jan: no me convence esta string, creo que cambiaré la original para la v0.8 - FIXME</translatorcomment>
        <translation>Se ha entrado a la sala %1, como %2.</translation>
    </message>
    <message>
        <source>Left room %1.</source>
        <translation>Se ha salido de la sala %1.</translation>
    </message>
    <message>
        <source>Room name is: %1</source>
        <translation>El nombre de la sala es: %1</translation>
    </message>
</context>
<context>
    <name>CommandHandler</name>
    <message>
        <source>Command from %1 in room %2: %3</source>
        <translation>Comando de %1 en la sala %2: %3</translation>
    </message>
    <message>
        <source>Command from %1: %2</source>
        <translation>Comando de %1: %2</translation>
    </message>
    <message>
        <source>The command failed.</source>
        <translation>Ha fallado el comando.</translation>
    </message>
    <message>
        <source>Error loading file %1 for command: %2</source>
        <translation>Error al cargar el archivo %1 para el comando: %2</translation>
    </message>
    <message>
        <source>Command gave no output text.</source>
        <translation>El comando no ha dado ningún texto de salida.</translation>
    </message>
    <message>
        <source>I don&apos;t have help about: %1</source>
        <translation>No tengo ayuda sobre: %1</translation>
    </message>
    <message>
        <source>Help for %1:</source>
        <translation>Ayuda de %1:</translation>
    </message>
    <message>
        <source>Available commands</source>
        <translation>Comandos disponibles</translation>
    </message>
    <message>
        <source>I don&apos;t have a match for %1</source>
        <translation>No tengo ninguna coincidencia para %1</translation>
    </message>
    <message>
        <source>This command is an alias to itself!</source>
        <translation>¡Este comando es un alias para sí mismo!</translation>
    </message>
    <message>
        <source>Use %1 to list the available commands.</source>
        <translation>Usa %1 para listar los comandos disponibles.</translation>
    </message>
    <message>
        <source>Use %1 [command] to get help about a specific command.</source>
        <translation>Usa %1 [comando] para recibir ayuda sobre un comando específico.</translation>
    </message>
    <message>
        <source>Started detached program successfully.</source>
        <translation>Se ha iniciado correctamente el programa, de forma independiente.</translation>
    </message>
    <message>
        <source>I need a parameter for the query:</source>
        <translation>Necesito un parámetro para la consulta:</translation>
    </message>
    <message>
        <source>Command %1 requested by %2 in room %3 without the right access level.</source>
        <translation>%2 ha solicitado el comando %1 en la sala %3, sin el nivel de acceso adecuado.</translation>
    </message>
    <message>
        <source>Command %1 requested by %2 without the right access level.</source>
        <translation>%2 ha solicitado el comando %1 sin el nivel de acceso adecuado.</translation>
    </message>
</context>
<context>
    <name>CommandModule</name>
    <message>
        <source>&amp;Add Command</source>
        <translation>&amp;Añadir comando</translation>
    </message>
    <message>
        <source>Static Message</source>
        <translation>Mensaje estático</translation>
    </message>
    <message>
        <source>Random Sentence</source>
        <translation>Frase aleatoria</translation>
    </message>
    <message>
        <source>Save &amp;Command</source>
        <translation>Guardar &amp;comando</translation>
    </message>
    <message>
        <source>Command</source>
        <translation>Comando</translation>
    </message>
    <message>
        <source>Help Text</source>
        <translation>Texto de ayuda</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <source>Access Level</source>
        <translation>Nivel de acceso</translation>
    </message>
    <message>
        <source>&amp;Sync Commands</source>
        <translation>&amp;Sincronizar comandos</translation>
    </message>
    <message>
        <source>&amp;Remove Command</source>
        <translation>&amp;Quitar comando</translation>
    </message>
    <message>
        <source>Anyone can use it</source>
        <comment>&apos;it&apos; refers to a command</comment>
        <translation>Cualquiera puede usarlo</translation>
    </message>
    <message>
        <source>Random Line from Text File</source>
        <translation>Línea aleatoria de archivo de texto</translation>
    </message>
    <message>
        <source>Run Program</source>
        <translation>Ejecutar un programa</translation>
    </message>
    <message>
        <source>Alias for another command</source>
        <translation>Alias para otro comando</translation>
    </message>
    <message>
        <source>Select File...</source>
        <translation>Selecciona archivo...</translation>
    </message>
    <message>
        <source>Static reply is empty.</source>
        <translation>La respuesta estática está vacía.</translation>
    </message>
    <message>
        <source>There are no sentences in the list.</source>
        <translation>No hay frases en la lista.</translation>
    </message>
    <message>
        <source>There is no file selected.</source>
        <translation>No hay ningún archivo seleccionado.</translation>
    </message>
    <message>
        <source>There is no program specified.</source>
        <translation>No se ha especificado ningún programa.</translation>
    </message>
    <message>
        <source>You need to enter the command this alias will execute.</source>
        <translation>Debes introducir el comando que este alias ejecutará.</translation>
    </message>
    <message>
        <source>Error validating fields</source>
        <translation>Error validando los campos</translation>
    </message>
    <message>
        <source>Select a text file</source>
        <translation>Selecciona un archivo de texto</translation>
    </message>
    <message>
        <source>Selected file does not exist.</source>
        <translation>El archivo selecionado no existe.</translation>
    </message>
    <message>
        <source>New Command</source>
        <translation>Nuevo comando</translation>
    </message>
    <message>
        <source>No file selected</source>
        <translation>Ningún archivo seleccionado</translation>
    </message>
    <message>
        <source>Command exists</source>
        <translation>El comando ya existe</translation>
    </message>
    <message>
        <source>Just enter a single sentence.</source>
        <translation>Introduce una única frase.</translation>
    </message>
    <message>
        <source>Add as many sentences as you want.
One of them will be selected at random as a reply.</source>
        <translation>Añade tantas frases como quieras.
Una de ellas será seleccionada al azar como respuesta.</translation>
    </message>
    <message>
        <source>A random line from the chosen file will be used as a reply.
File encoding should be UTF-8.</source>
        <translation>Se utilizará una línea aleatoria del archivo elegido como respuesta.
La codificación del archivo debe ser UTF-8.</translation>
    </message>
    <message>
        <source>Keyword-based Message</source>
        <translation>Mensaje basado en palabras clave</translation>
    </message>
    <message>
        <source>Add keywords to the list, then enter the corresponding definition for each keyword on the other side.</source>
        <translation>Añade palabras clave a la lista, luego introduce la definición correspondiente para cada palabra clave en el otro lado.</translation>
    </message>
    <message>
        <source>Enter the original command that should be run when this alias is used.</source>
        <translation>Introduce el comando original que se debe ejecutar cuando se use este alias.</translation>
    </message>
    <message>
        <source>Private Reply</source>
        <translation>Respuesta en privado</translation>
    </message>
    <message>
        <source>There are no keywords in the list.</source>
        <translation>No hay palabras clave en la lista.</translation>
    </message>
    <message>
        <source>A command can&apos;t be an alias of itself.</source>
        <translation>Un comando no puede ser un alias de sí mismo.</translation>
    </message>
    <message numerus="yes">
        <source>Loaded %Ln command(s).</source>
        <translation>
            <numerusform>Se ha cargado 1 comando.</numerusform>
            <numerusform>Se han cargado %Ln comandos.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>Saved %Ln command(s).</source>
        <translation>
            <numerusform>Se ha guardado 1 comando.</numerusform>
            <numerusform>Se han guardado %Ln comandos.</numerusform>
        </translation>
    </message>
    <message>
        <source>If the program you launch will take a long time to finish (like a GUI), you need to select the option to launch it independently, but then it will not be possible to use the program output as the reply for the command.</source>
        <translation>Si el programa que lanzas va a tardar mucho tiempo en terminar (como un GUI), debes seleccionar la opción de lanzarlo independientemente, pero entonces no será posible usar la salida del programa como respuesta para el comando.</translation>
    </message>
    <message>
        <source>Wait for the program to finish</source>
        <translation>Esperar a que termine el programa</translation>
    </message>
    <message>
        <source>Launch the program independently</source>
        <translation>Lanzar el programa de forma independiente</translation>
    </message>
    <message>
        <source>Error: There is already a command in the list with this name.</source>
        <translation>Error: Ya hay un comando en la lista con este nombre.</translation>
    </message>
    <message>
        <source>Enter a system command line to be run, with any needed parameters.
The reply will be the console output of the program, unless you specify a reply pattern, and don&apos;t include the %output% variable somewhere in it.</source>
        <comment>Don&apos;t translate %output%!</comment>
        <translation>Introduce una línea de comando del sistema para ejecutar, con los parámetros que pueda necesitar.
La respuesta será la salida en consola del programa, a menos que especifiques un patrón de respuesta, y no incluyas la variable %output% en algún lugar del mismo.</translation>
    </message>
    <message>
        <source>Reply Pattern</source>
        <translation>Patrón de respuesta</translation>
    </message>
    <message>
        <source>Set an optional reply pattern</source>
        <translation>Establece, opcionalmente, un patrón de respuesta</translation>
    </message>
    <message>
        <source>With a reply pattern you can make any possible reply to a command fit into a predefined structure, like having a specific symbol at the start or the end of the reply, or always start the reply with a fixed word.</source>
        <translation>Con un patrón de respuesta, puedes hacer que cualquier respuesta posible encaje en una estructura predefinida, como tener un símbolo específico al principio o al final de la respuesta, o empezar siempre la respuesta con una palabra fija.</translation>
    </message>
    <message>
        <source>You can insert variables like %user%, %params%, %output%, %line% and %smiley%.</source>
        <comment>Do not translate the words between % symbols</comment>
        <translation>Puedes insertar variables como %user%, %params%, %output%, %line% y %smiley%.</translation>
    </message>
    <message>
        <source>Usually, you will want to have the %output% variable somewhere in the pattern.</source>
        <comment>Do not translate %output%</comment>
        <translation>Normalmente, te interesará tener la variable %output% en algún lugar del patrón.</translation>
    </message>
    <message>
        <source>If you don&apos;t set a reply pattern, replies in chatrooms will use the default pattern, %user%: %output%.</source>
        <comment>Do not translate %user% or %output%</comment>
        <translation>Si no estableces un patrón de respuesta, las respuestas en salas de charla usarán el patrón predefinido, %user%: %output% (Usuario: Respuesta).</translation>
    </message>
    <message>
        <source>Example: [RESULT] &gt; %output%</source>
        <comment>Do not translate %output%</comment>
        <translation>Ejemplo: [RESULTADO] &gt; %output%</translation>
    </message>
    <message>
        <source>Explanation shown to users with the %1 command</source>
        <translation>Explicación mostrada a los usuarios con el comando %1</translation>
    </message>
    <message>
        <source>The command name can&apos;t contain spaces.</source>
        <translation>El nombre del comando no puede contener espacios.</translation>
    </message>
    <message>
        <source>The name, without the prefix or any spaces</source>
        <comment>Referring to the command&apos;s name/trigger</comment>
        <translation>El nombre, sin el prefijo ni espacios</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <source>Program Configuration</source>
        <translation>Configuración del programa</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Guardar</translation>
    </message>
    <message>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <source>Administrator JIDs</source>
        <translation>JIDs administradores</translation>
    </message>
    <message>
        <source>Command prefix</source>
        <translation>Prefijo de comandos</translation>
    </message>
    <message>
        <source>Notify new chats</source>
        <translation>Notificar nuevas conversaciones</translation>
    </message>
    <message>
        <source>Hide window on startup</source>
        <translation>Ocultar ventana al iniciar</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Notificaciones</translation>
    </message>
    <message>
        <source>System Tray</source>
        <translation>Bandeja del sistema</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <source>seconds</source>
        <comment>Suffix next to a number</comment>
        <translation>segundos</translation>
    </message>
    <message>
        <source>Connection timeout</source>
        <translation>Tiempo máximo de espera de la conexión</translation>
    </message>
    <message>
        <source>Notify disconnection</source>
        <translation>Notificar desconexión</translation>
    </message>
    <message>
        <source>Connection ping interval</source>
        <translation>Intervalo de ping de la conexión</translation>
    </message>
    <message>
        <source>Program configuration saved.</source>
        <translation>Se ha guardado la configuración del programa.</translation>
    </message>
    <message>
        <source>Select...</source>
        <translation>Seleccionar...</translation>
    </message>
    <message>
        <source>Use a custom icon</source>
        <translation>Usar un icono personalizado</translation>
    </message>
    <message>
        <source>Invalid icon</source>
        <translation>Icono no válido</translation>
    </message>
    <message>
        <source>The selected image is not valid.</source>
        <translation>La imagen seleccionada no es válida.</translation>
    </message>
    <message>
        <source>Icon file %1 does not exist</source>
        <translation>El archivo de icono %1 no existe</translation>
    </message>
    <message>
        <source>Select custom icon</source>
        <translation>Selecciona un icono personalizado</translation>
    </message>
    <message>
        <source>Image files</source>
        <translation>Archivos de imagen</translation>
    </message>
    <message>
        <source>All files</source>
        <translation>Todos los archivos</translation>
    </message>
</context>
<context>
    <name>ConnectionDialog</name>
    <message>
        <source>Connection Settings</source>
        <translation>Configuración de la conexión</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <source>XMPP Address (Jabber ID)</source>
        <translation>Dirección XMPP (ID Jabber)</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <source>Connect Automatically</source>
        <translation>Conectar automáticamente</translation>
    </message>
    <message>
        <source>Resource</source>
        <translation>Recurso</translation>
    </message>
    <message>
        <source>Priority</source>
        <translation>Prioridad</translation>
    </message>
    <message>
        <source>Like user@server</source>
        <translation>Como usuario@servidor</translation>
    </message>
    <message>
        <source>&amp;Connect</source>
        <translation>&amp;Conectar</translation>
    </message>
    <message>
        <source>Use carefully.</source>
        <translatorcomment>Meh...</translatorcomment>
        <translation>Usar con cuidado.</translation>
    </message>
    <message>
        <source>This allows connecting to servers with bad security certificates.</source>
        <translation>Esto permite conectar a servidores con certificados de seguridad no válidos.</translation>
    </message>
    <message>
        <source>Ignore SSL errors</source>
        <translation>Ignorar errores de SSL</translation>
    </message>
</context>
<context>
    <name>ContactsModule</name>
    <message>
        <source>&amp;Start chat</source>
        <translation>&amp;Iniciar conversación</translation>
    </message>
    <message>
        <source>&amp;Add Contact</source>
        <translation>&amp;Añadir contacto</translation>
    </message>
    <message>
        <source>Rename Contact</source>
        <translation>Renombrar contacto</translation>
    </message>
    <message>
        <source>&amp;Remove Contact</source>
        <translation>&amp;Eliminar contacto</translation>
    </message>
    <message>
        <source>Enter the new name for %1</source>
        <comment>%1 is contact&apos;s name + address</comment>
        <translation>Introduce el nuevo nombre para %1</translation>
    </message>
    <message>
        <source>Remove contact?</source>
        <translation>¿Eliminar contacto?</translation>
    </message>
    <message>
        <source>Do you want to remove %1 from your contact list?</source>
        <comment>%1 is contact&apos;s name + address</comment>
        <translation>¿Quieres eliminar a %1 de tu lista de contactos?</translation>
    </message>
    <message>
        <source>Yes, remove</source>
        <comment>Regarding a contact</comment>
        <translation>Si, eliminarlo</translation>
    </message>
    <message>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <source>Added %1 to the contact list.</source>
        <comment>%1 is an XMPP address</comment>
        <translation>Se ha añadido a %1 a la lista de contactos.</translation>
    </message>
    <message>
        <source>Removed %1 from the contact list.</source>
        <comment>%1 is an XMPP address</comment>
        <translation>Se ha eliminado a %1 de la lista de contactos.</translation>
    </message>
    <message>
        <source>Add a contact</source>
        <translation>Añadir un contacto</translation>
    </message>
    <message>
        <source>Accept Request</source>
        <translation>Aceptar solicitud</translation>
    </message>
    <message>
        <source>Refuse Request</source>
        <translation>Rechazar solicitud</translation>
    </message>
    <message>
        <source>Pending Subscription Requests</source>
        <translation>Solicitudes de suscripción pendientes</translation>
    </message>
    <message>
        <source>Accepted subscription request from %1.</source>
        <comment>%1 is an XMPP address</comment>
        <translation>Se ha aceptado la solicitud de suscripción de %1.</translation>
    </message>
    <message>
        <source>Refused subscription request from %1.</source>
        <comment>%1 is an XMPP address</comment>
        <translation>Se ha rechazado la solicitud de suscripción de %1.</translation>
    </message>
    <message>
        <source>Subscription status: %1</source>
        <translation>Estado de la suscripción: %1</translation>
    </message>
    <message>
        <source>Rename a contact</source>
        <translation>Renombrar un contacto</translation>
    </message>
    <message>
        <source>Enter the full XMPP address, with user@server format</source>
        <translation>Introduce la dirección XMPP completa, con formato usuario@servidor</translation>
    </message>
</context>
<context>
    <name>GeneralModule</name>
    <message>
        <source>Offline</source>
        <translation>Desconectado</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Conectar</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation>Desconectar</translation>
    </message>
    <message>
        <source>Current Activity:</source>
        <translation>Actividad actual:</translation>
    </message>
    <message>
        <source>Status Message:</source>
        <translation>Mensaje de estado:</translation>
    </message>
    <message>
        <source>Uptime:</source>
        <translatorcomment>Otras opciones serian: &quot;Tiempo de conexión:&quot; o &quot;Tiempo de actividad:&quot;</translatorcomment>
        <translation type="unfinished">Tiempo conectado:</translation>
    </message>
    <message>
        <source>Rooms:</source>
        <translation>Salas:</translation>
    </message>
    <message>
        <source>%1 joined, out of %2 configured</source>
        <comment>Number of rooms joined and configured</comment>
        <translation>Se ha entrado en %1, de %2 configuradas</translation>
    </message>
    <message>
        <source>Server version: %1</source>
        <translation>Versión del servidor: %1</translation>
    </message>
</context>
<context>
    <name>HelpWidget</name>
    <message>
        <source>General Help</source>
        <translation>Ayuda general</translation>
    </message>
    <message>
        <source>Getting started</source>
        <translation>Empezando</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Configuración</translation>
    </message>
    <message>
        <source>Commands</source>
        <translation>Comandos</translation>
    </message>
    <message>
        <source>Activities</source>
        <translation>Actividades</translation>
    </message>
    <message>
        <source>Substitution syntax</source>
        <translation>Sintaxis de sustituciones</translation>
    </message>
    <message>
        <source>Command line options</source>
        <translation>Opciones de línea de comandos</translation>
    </message>
    <message>
        <source>Contents</source>
        <translation>Índice</translation>
    </message>
    <message>
        <source>The first time you start AkariXB you will see the Connection Settings window, where you need to enter the XMPP address the bot will use, and its password.</source>
        <translation>La primera vez que inicies AkariXB, verás la ventana Configuración de la conexión, donde deberás introducir la dirección XMPP que usará el bot, y su contraseña.</translation>
    </message>
    <message>
        <source>Each type of command has an explanation of how it works, visible while you&apos;re adding the new command.</source>
        <translation>Cada tipo de comando tiene una explicación de como funciona, visible mientras añades el nuevo comando.</translation>
    </message>
    <message>
        <source>There are several special words that can be entered inside custom strings like replies to commands, conversation texts and status messages, which will be replaced by the appropriate variable text at the time the string is used. These are:</source>
        <translation>Hay varias palabras especiales que se pueden introducir dentro de las cadenas de texto personalizadas, como respuestas a comandos, textos de conversación y mensajes de estado, que serán reemplazadas por el texto variable apropiado en el momento en el que se use la cadena de texto. Estas son:</translation>
    </message>
    <message>
        <source>Replaced by the name of the user to whom this message is directed. Nickname in the case of chatrooms, and account name in the case of chats with contacts.</source>
        <translation>Reemplazada por el nombre del usuario a quien va dirigido este mensaje. El apodo en el caso de salas de charla, y el nombre de la cuenta en el caso de conversaciones con contactos.</translation>
    </message>
    <message>
        <source>Used in a custom reply pattern for a command, this will be replaced by the original reply to that command.</source>
        <translation>Usada en un patrón de respuesta personalizado para un comando, será reemplazada por la respuesta original al comando.</translation>
    </message>
    <message>
        <source>This is replaced by a newline character.</source>
        <translation>Esta se reemplazará por un carácter de salto de línea.</translation>
    </message>
    <message>
        <source>This is replaced by a random old-style smiley face.</source>
        <translation>Esta es reemplazada por un emoticono clásico al azar.</translation>
    </message>
    <message>
        <source>Example: &apos;Hello %user%&apos; could be converted to &apos;Hello Mary&apos; if a user called Mary does something that triggers that reply.</source>
        <translation>Ejemplo: &apos;Hola %user%&apos; se podría convertir en &apos;Hola María&apos; si una usuaria llamada María hace algo que active esa respuesta.</translation>
    </message>
    <message>
        <source>In addition, syntax like %1 can be used to insert one out of several choices at random, at any desired point in the string.</source>
        <translation>Además, se puede usar sintaxis como %1 para insertar una de entre varias posibilidades al azar, en cualquier punto que se desee de la cadena de texto.</translation>
    </message>
    <message>
        <source>You can use the --config parameter (or -c) to run the program with a different configuration. This way, you can run two instances of AkariXB at the same time, with different accounts, different rooms configured, using a different command prefix, etc.</source>
        <translation>Puedes usar el parámetro --config (o -c) para ejecutar el programa con una configuración diferente. De esta manera, puedes ejecutar dos instancias de AkariXB al mismo tiempo, con diferentes cuentas, diferentes salas configuradas, usando un prefijo de comandos diferente, etc.</translation>
    </message>
    <message>
        <source>Use the --debug parameter (or -d) to have extra information in your terminal window, about what the program is doing.</source>
        <translation>Usa el parámetro --debug (o -d) para tener información adicional en la ventana de la terminal, sobre lo que está haciendo el programa.</translation>
    </message>
    <message>
        <source>Use the --xmppdebug parameter (or -x) to have extensive XMPP traffic information (XML stanzas) shown in your terminal window.</source>
        <translation>Usa el parámetro --xmppdebug (o -x) para tener información exhaustiva del tráfico XMPP (stanzas XML) mostrada en la ventana de la terminal.</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
    <message>
        <source>These are replaced by a random, happy or sad, Unicode-based emoji face.</source>
        <translation>Estas son reemplazadas por una cara emoji (Unicode) feliz o triste, al azar.</translation>
    </message>
    <message>
        <source>Keep in mind that some users might not be able to see these symbols.</source>
        <translation>Recuerda que quizá algunos usuarios no puedan ver estos símbolos.</translation>
    </message>
    <message>
        <source>%1, %2 and %3 can&apos;t be used inside these choices.</source>
        <translation>%1, %2 y %3 no se pueden usar dentro de dichas posibilidades.</translation>
    </message>
    <message>
        <source>Afterwards, you can configure chatrooms to be automatically joined, manage the contact list, define commands for which the bot will provide answers, etc.</source>
        <translation>A continuación, puedes configurar salas de charla a las que entrar automáticamente, gestionar la lista de contactos, definir comandos para los que el bot dará respuestas, etc.</translation>
    </message>
    <message>
        <source>Activities can be defined to make the bot change its status, send messages, adapt its reaction time and so on, at certain times and dates.</source>
        <translation>Se pueden definir actividades para hacer que el bot cambie su estado, envíe mensajes, adapte su tiempo de reacción, etc, a ciertas horas y en ciertas fechas.</translation>
    </message>
    <message>
        <source>If you have defined messages to be sent before or after the activity, you can decrease the chances of them being sent by adding some strings with a single asterisk (*) in them to the list of messages.</source>
        <translation>Si has definido mensajes para enviar antes o después de la actividad, puedes reducir las posibilidades de que se envíen añadiendo algunas cadenas de texto con un único asterisco (*) en ellas, a la lista de mensajes.</translation>
    </message>
    <message>
        <source>In replies to commands, this is replaced with whatever follows the command name.</source>
        <translation>En respuestas a comandos, esta es reemplazada con cualquier cosa que siga al nombre del comando.</translation>
    </message>
    <message>
        <source>You can define several kinds of custom commands, like static message, random message from a list, keyword-based message, and program execution-based commands.</source>
        <translation>Puedes definir varios tipos de comandos personalizados, como mensaje estático, mensaje aleatorio de una lista, mensaje basado en palabras clave, y comandos basados en la ejecución de programas.</translation>
    </message>
    <message>
        <source>You can configure several things to your liking in the settings, like which symbol will act as prefix for commands, which XMPP addresses will be able to use special administration commands with the bot, whether you want popup notifications for certain events, etc.</source>
        <translation>Puedes ajustar varias cosas a tu gusto en la configuración, como qué símbolo servirá de prefijo para comandos, qué direcciones XMPP podrán usar comandos especiales de administración con el bot, si quieres notificaciones emergentes para ciertos eventos, etc.</translation>
    </message>
    <message>
        <source>If the access level for the command is not set for everyone, only users listed in the administrators list in the Program Configuration window will be able to use that command.</source>
        <translation>Si el nivel de acceso del comando no está establecido para todo el mundo, solo los usuarios presentes en la lista de administradores de la ventana Configuración del programa podrán usar ese comando.</translation>
    </message>
    <message>
        <source>The nickname of the bot, as specified in the Identity module.</source>
        <translation>El apodo del bot, como se especifica en el módulo Identidad.</translation>
    </message>
</context>
<context>
    <name>IdentityModule</name>
    <message>
        <source>&amp;Update</source>
        <translation>&amp;Actualizar</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Apodo</translation>
    </message>
    <message>
        <source>Loaded identity data for %1.</source>
        <comment>%1 is a nickname</comment>
        <translation>Se han cargado los datos de identidad para %1.</translation>
    </message>
    <message>
        <source>Saved identity data for %1.</source>
        <comment>%1 is a nickname</comment>
        <translation>Se han guardado los datos de identidad para %1.</translation>
    </message>
    <message>
        <source>Identity data has not been defined yet.</source>
        <translation>Los datos de identidad aún no se han definido.</translation>
    </message>
    <message>
        <source>Date of Birth</source>
        <translation>Fecha de nacimiento</translation>
    </message>
</context>
<context>
    <name>LogModule</name>
    <message>
        <source>AkariXB v%1 started.</source>
        <comment>%1 = Version number</comment>
        <translation>AkariXB v%1 iniciado.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Connect</source>
        <translation>Conectar</translation>
    </message>
    <message>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <source>Rooms</source>
        <translation>Salas</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Salir</translation>
    </message>
    <message>
        <source>About AkariXB</source>
        <translation>Acerca de AkariXB</translation>
    </message>
    <message>
        <source>Connecting...</source>
        <translation>Conectando...</translation>
    </message>
    <message>
        <source>AkariXB is a Jabber/XMPP bot.</source>
        <translation>AkariXB es un bot para Jabber/XMPP.</translation>
    </message>
    <message>
        <source>You can talk to it, make it join chatrooms, etc.</source>
        <translation>Puedes hablarle, hacer que entre en salas de charla, etc.</translation>
    </message>
    <message>
        <source>English translation by JanKusanagi.</source>
        <comment>TRANSLATORS: Change this with your language and name. If there was another translator before you, add your name after theirs ;)</comment>
        <translation>Traducción al castellano por JanKusanagi y Kheiron.</translation>
    </message>
    <message>
        <source>Thanks to all the testers, translators and packagers, who help make AkariXB better!</source>
        <translation>¡Gracias a todos los &apos;testers&apos;, traductores y empaquetadores, que ayudan a hacer AkariXB mejor!</translation>
    </message>
    <message>
        <source>&amp;Hide Window</source>
        <translation>&amp;Ocultar ventana</translation>
    </message>
    <message>
        <source>&amp;Show Window</source>
        <translation>&amp;Mostrar ventana</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>No conectado</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation>Desconectar</translation>
    </message>
    <message>
        <source>Log</source>
        <translation>Registro</translation>
    </message>
    <message>
        <source>Configure AkariXB</source>
        <translation>Configurar AkariXB</translation>
    </message>
    <message>
        <source>Visit Website</source>
        <translation>Visitar sitio web</translation>
    </message>
    <message>
        <source>Report a Bug</source>
        <translation>Informar de un error</translation>
    </message>
    <message>
        <source>%1 connected.</source>
        <translation>%1 conectado.</translation>
    </message>
    <message>
        <source>Disconnected.</source>
        <translation>Desconectado.</translation>
    </message>
    <message>
        <source>Network error: %1</source>
        <translation>Error de red: %1</translation>
    </message>
    <message>
        <source>Chats</source>
        <translation type="unfinished">Conversaciones</translation>
    </message>
    <message>
        <source>Commands</source>
        <translation>Comandos</translation>
    </message>
    <message>
        <source>Conversation</source>
        <translatorcomment>FIXME? la original quizá cambie a Speech o similar</translatorcomment>
        <translation type="unfinished">Conversación</translation>
    </message>
    <message>
        <source>Activities</source>
        <translation>Actividades</translation>
    </message>
    <message>
        <source>AkariXB is Free Software, licensed under the GNU GPL license, and uses some Oxygen icons under LGPL license.</source>
        <translation>AkariXB es software libre, licenciado bajo GNU GPL, y utiliza algunos iconos Oxygen con licencia LGPL.</translation>
    </message>
    <message>
        <source>Account is not configured</source>
        <translation>La cuenta no está configurada</translation>
    </message>
    <message>
        <source>Data directory created: %1</source>
        <translation>Se ha creado el directorio de datos: %1</translation>
    </message>
    <message>
        <source>Error creating data directory: %1</source>
        <translation>Error al crear el directorio de datos: %1</translation>
    </message>
    <message>
        <source>Data directory in use: %1</source>
        <translation>Directorio de datos en uso: %1</translation>
    </message>
    <message>
        <source>Identity</source>
        <translation>Identidad</translation>
    </message>
    <message>
        <source>Ready.</source>
        <translation>Preparado.</translation>
    </message>
    <message>
        <source>&amp;Session</source>
        <translation>&amp;Sesión</translation>
    </message>
    <message>
        <source>S&amp;ettings</source>
        <translation>&amp;Configuración</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>Ay&amp;uda</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation>Desconectado</translation>
    </message>
    <message>
        <source>Connection to the server was lost.</source>
        <translation>Se ha perdido la conexión con el servidor.</translation>
    </message>
    <message>
        <source>Socket Error</source>
        <translation>Error de socket</translation>
    </message>
    <message>
        <source>KeepAlive Error</source>
        <translation>Error de KeepAlive</translation>
    </message>
    <message>
        <source>XMPP Stream Error</source>
        <translation>Error de flujo XMPP</translation>
    </message>
    <message>
        <source>About XMPP</source>
        <translation>Acerca de XMPP</translation>
    </message>
    <message>
        <source>Quit?</source>
        <translation>¿Salir?</translation>
    </message>
    <message>
        <source>Are you sure?</source>
        <translation>¿Seguro?</translation>
    </message>
    <message>
        <source>Yes, quit AkariXB</source>
        <translation>Sí, salir de AkariXB</translation>
    </message>
    <message>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <source>Connection seems stuck. Trying again...</source>
        <translation>La conexión parece haberse atascado. Intentándolo de nuevo...</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation>Contactos</translation>
    </message>
    <message>
        <source>Stop connection attempts</source>
        <translation>Detener los intentos de conexión</translation>
    </message>
    <message>
        <source>General &amp;Help</source>
        <translation>&amp;Ayuda general</translation>
    </message>
    <message>
        <source>Aborted connection attempt.</source>
        <translation>Se ha interrumpido el intento de conexión.</translation>
    </message>
    <message>
        <source>Presence error %1 from %2: %3</source>
        <translation>Error de presencia %1 de %2: %3</translation>
    </message>
    <message>
        <source>%1 wants to subscribe to your presence.</source>
        <comment>%1 is another user&apos;s address</comment>
        <translation>%1 quiere suscribirse a tu presencia.</translation>
    </message>
    <message>
        <source>%1 unsubscribed from your presence.</source>
        <comment>%1 is another user&apos;s address</comment>
        <translation>%1 ha cancelado su suscripción a tu presencia.</translation>
    </message>
    <message>
        <source>%1 removed your authorization to see their presence.</source>
        <comment>%1 is another user&apos;s address</comment>
        <translatorcomment>Kheiron: ¿quitado o retirado?</translatorcomment>
        <translation>%1 ha quitado tu autorización para ver su presencia.</translation>
    </message>
    <message>
        <source>%1 accepted your subscription request.</source>
        <comment>%1 is another user&apos;s address</comment>
        <translation>%1 ha aceptado tu solicitud de suscripción.</translation>
    </message>
    <message>
        <source>Settings file in use: %1</source>
        <translation>Archivo de configuración en uso: %1</translation>
    </message>
    <message>
        <source>Room Speech</source>
        <translatorcomment>No me termina de gustar, pero de momento así se queda.</translatorcomment>
        <translation type="unfinished">Conversación en salas</translation>
    </message>
    <message>
        <source>Received version information from %1:</source>
        <translation>Se ha recibido versión de información de %1:</translation>
    </message>
    <message>
        <source>Warning! Ignoring errors in SSL connection to server.</source>
        <translation>¡Advertencia! Ignorando errores en la conexión SSL al servidor.</translation>
    </message>
</context>
<context>
    <name>MessageHandler</name>
    <message>
        <source>Error %1 from %2: %3</source>
        <translation>Error %1 de %2: %3</translation>
    </message>
    <message>
        <source>Headline from %1: %2</source>
        <comment>%1 is a user&apos;s address,%2 is the title of a message</comment>
        <translatorcomment>Psi usa &quot;titular&quot;; Gajim no parece tener una string para &quot;Headline&quot;</translatorcomment>
        <translation>Titular de %1: %2</translation>
    </message>
</context>
<context>
    <name>RoomModule</name>
    <message>
        <source>Add &amp;New Room</source>
        <translation>Añadir &amp;nueva sala</translation>
    </message>
    <message>
        <source>&amp;Remove Room</source>
        <translation>&amp;Quitar sala</translation>
    </message>
    <message>
        <source>Room address</source>
        <translation>Dirección de la sala</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Apodo</translation>
    </message>
    <message>
        <source>Autojoin</source>
        <translatorcomment>Meh...</translatorcomment>
        <translation>Auto-entrar</translation>
    </message>
    <message>
        <source>Join</source>
        <translation>Entrar</translation>
    </message>
    <message>
        <source>Leave</source>
        <translation>Salir</translation>
    </message>
    <message>
        <source>Select a room from the list above</source>
        <translation>Selecciona una sala de la lista de arriba</translation>
    </message>
    <message>
        <source>Room Details</source>
        <translation>Detalles de la sala</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <source>Room address is not valid.</source>
        <translation>La dirección de la sala no es válida.</translation>
    </message>
    <message>
        <source>Nickname is not valid.</source>
        <translation>El apodo no es válido.</translation>
    </message>
    <message>
        <source>New Room</source>
        <translation>Sala nueva</translation>
    </message>
    <message>
        <source>Leaving room %1...</source>
        <translation>Saliendo de la sala %1...</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation>&amp;Editar</translation>
    </message>
    <message>
        <source>&amp;Update</source>
        <translation>&amp;Actualizar</translation>
    </message>
    <message>
        <source>Joining room %1...</source>
        <translation>Entrando a la sala %1...</translation>
    </message>
    <message>
        <source>Password, if needed</source>
        <translation>Contraseña, si se necesita</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message numerus="yes">
        <source>Loaded %Ln room(s).</source>
        <translation>
            <numerusform>Se ha cargado una sala.</numerusform>
            <numerusform>Se han cargado %Ln salas.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>Saved %Ln room(s).</source>
        <translation>
            <numerusform>Se ha guardado una sala.</numerusform>
            <numerusform>Se han guardado %Ln salas.</numerusform>
        </translation>
    </message>
    <message>
        <source>Remove room?</source>
        <translation>¿Quitar sala?</translation>
    </message>
    <message>
        <source>Yes, remove it</source>
        <comment>It = A chat room</comment>
        <translation>Si, quitarla</translation>
    </message>
    <message>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <source>Do you want to remove room %1 from your list?</source>
        <translation>¿Quieres quitar la sala %1 de la lista?</translation>
    </message>
    <message>
        <source>Allowed</source>
        <comment>Column header for features allowed</comment>
        <translation>Permitido</translation>
    </message>
    <message>
        <source>Allowed to:</source>
        <translation>Se permite:</translation>
    </message>
    <message>
        <source>Reply to commands</source>
        <translation>Responder a comandos</translation>
    </message>
    <message>
        <source>Greet people</source>
        <translation>Saludar a gente</translation>
    </message>
    <message>
        <source>Converse</source>
        <translation>Conversar</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message numerus="yes">
        <source>%Ln room(s)</source>
        <translation>
            <numerusform>1 sala</numerusform>
            <numerusform>%Ln salas</numerusform>
        </translation>
    </message>
    <message>
        <source>There is already a room in the list with this address, in row %1.</source>
        <translation>Ya hay una sala en la lista con esta dirección, en la fila %1.</translation>
    </message>
    <message>
        <source>Speak casually</source>
        <comment>Casually as in, from time to time</comment>
        <translation>Hablar de forma casual</translation>
    </message>
</context>
<context>
    <name>RoomSpeakModule</name>
    <message>
        <source>Greetings</source>
        <translation>Saludos</translation>
    </message>
    <message>
        <source>Casual speak</source>
        <translation type="unfinished">Hablar casualmente</translation>
    </message>
    <message>
        <source>Reactions to words</source>
        <translation>Reacciones a palabras</translation>
    </message>
    <message>
        <source>Sentences used to greet people who enter a chatroom.</source>
        <translation>Frases utilizadas para saludar a las personas que entran a las salas de charla.</translation>
    </message>
    <message>
        <source>The %1 variable can be used to insert their names in the sentences.</source>
        <translation>La variable %1 se puede usar para insertar sus nombres en las frases.</translation>
    </message>
    <message>
        <source>Example:</source>
        <translation>Ejemplo:</translation>
    </message>
    <message>
        <source>Documentation for Perl regular expressions</source>
        <translation>Documentación de expresiones regulares de Perl</translation>
    </message>
    <message>
        <source>Sentences to be sent to chatrooms at random intervals.</source>
        <translation>Frases para enviar a las salas de charla a intervalos aleatorios.</translation>
    </message>
    <message>
        <source>Regular expressions to be matched against messages coming from the chatrooms, and possible messages to send in response.</source>
        <translatorcomment>Meh....</translatorcomment>
        <translation>Expresiones regulares con las que comparar los mensajes provenientes de las salas de charla, y posibles mensajes para enviar en respuesta.</translation>
    </message>
</context>
<context>
    <name>RoomSpeakTab</name>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Guardar</translation>
    </message>
    <message numerus="yes">
        <source>Loaded %Ln sentence(s) of type %1.</source>
        <translation>
            <numerusform>Se ha cargado una frase de tipo %1.</numerusform>
            <numerusform>Se han cargado %Ln frases de tipo %1.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>Saved %Ln sentence(s) of type %1.</source>
        <translation>
            <numerusform>Se ha guardado una frase de tipo %1.</numerusform>
            <numerusform>Se han guardado %Ln frases de tipo %1.</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>StringListWidget</name>
    <message>
        <source>Enter text</source>
        <translation>Introduce el texto</translation>
    </message>
    <message>
        <source>Enter a new line of text for the list</source>
        <translation>Introduce una nueva línea de texto para la lista</translation>
    </message>
    <message>
        <source>Update the text for this item</source>
        <translation>Actualiza el texto de este elemento</translation>
    </message>
    <message>
        <source>Update the definition of the item</source>
        <translation>Actualizar la definición de este elemento</translation>
    </message>
    <message>
        <source>The item already exists</source>
        <translation>El elemento ya existe</translation>
    </message>
    <message>
        <source>Error: There is already an item in the list with the text %1, and this list does not allow duplicates.</source>
        <translation>Error: Ya hay un elemento en la lista con el texto %1, y esta lista no permite duplicados.</translation>
    </message>
    <message>
        <source>Edit the selected item (you can also double-click it or press F2/Enter)</source>
        <translation>Editar el elemento seleccionado (también puedes hacer doble clic en él, o pulsar F2/Intro)</translation>
    </message>
    <message>
        <source>Item text is not valid</source>
        <translation>El texto del elemento no es válido</translation>
    </message>
    <message>
        <source>Modify text</source>
        <translation>Modifica el texto</translation>
    </message>
    <message>
        <source>Add an item to the list (you can also press the Insert key)</source>
        <translation>Añadir un elemento a la lista (también puedes pulsar la tecla Insert)</translation>
    </message>
    <message>
        <source>String</source>
        <translation>Cadena</translation>
    </message>
    <message>
        <source>Regular Expression</source>
        <translation>Expresión regular</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Etiquetas</translation>
    </message>
    <message>
        <source>Search...</source>
        <translation>Buscar...</translation>
    </message>
    <message>
        <source>Show All</source>
        <translation>Mostrar todo</translation>
    </message>
    <message>
        <source>Remove the selected item (you can also press Shift+Delete)</source>
        <translation>Quitar el elemento seleccionado (también puedes pulsar Mayúsculas+Supr)</translation>
    </message>
    <message>
        <source>Regular expression is not valid</source>
        <translation>L&apos;expressió regular no</translation>
    </message>
    <message>
        <source>Syntax error at position %1:</source>
        <translation>Error de sintaxis en la posición %1:</translation>
    </message>
</context>
<context>
    <name>VariableParser</name>
    <message>
        <source>Unbalanced braces: %1 opening and %2 closing</source>
        <translation>Llaves descompensadas: %1 que abren y %2 que cierran</translation>
    </message>
    <message>
        <source>This substitution is unknown, or not valid in this context: %1</source>
        <translation>Esta sustitución es desconocida, o no es válida en este contexto: %1</translation>
    </message>
    <message>
        <source>Syntax error:</source>
        <translation>Error de sintaxis:</translation>
    </message>
</context>
</TS>
