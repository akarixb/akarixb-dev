/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef CONNECTIONDIALOG_H
#define CONNECTIONDIALOG_H

#include <QWidget>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QLineEdit>
#include <QCheckBox>
#include <QSpinBox>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QAction>
#include <QShowEvent>

#include <QDebug>

#include "helpers.h"


class ConnectionDialog : public QWidget
{
    Q_OBJECT

public:
    explicit ConnectionDialog(QString jid, QString password,
                              bool autoconnect, QString resource,
                              int priority, bool ignoreSslErrors,
                              QWidget *parent = nullptr);
    ~ConnectionDialog();


signals:
    void connectionRequested(QString jid, QString password,
                             bool autoconnect, QString resource,
                             int priority, bool ignoreSslErrors);


public slots:
    void validateJid(QString jid);
    void startConnection();


protected:
    virtual void showEvent(QShowEvent *event);


private:
    QVBoxLayout *m_mainLayout;
    QFormLayout *m_topLayout;
    QDialogButtonBox *m_bottomButtonBox;

    QLineEdit *m_jidLineEdit;
    QLineEdit *m_passwordLineEdit;
    QCheckBox *m_autoconnectCheckbox;

    QLineEdit *m_resourceLineEdit;
    QSpinBox *m_prioritySpinbox;

    QCheckBox *m_ignoreSslCheckbox;

    QPushButton *m_connectButton;
    QPushButton *m_cancelButton;
    QAction *m_cancelAction;
};

#endif // CONNECTIONDIALOG_H
