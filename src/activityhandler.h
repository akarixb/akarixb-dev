/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef ACTIVITYHANDLER_H
#define ACTIVITYHANDLER_H

#include <QObject>
#include <QVariantList>
#include <QTimer>
#include <QDateTime>
#include <QStringList>

#include <QDebug>

#include <QXmppClient.h>
#include <QXmppMucManager.h>

#include "globalobject.h"
#include "variableparser.h"
#include "helpers.h"


class ActivityHandler : public QObject
{
    Q_OBJECT

public:
    explicit ActivityHandler(GlobalObject *globalObject,
                             QObject *parent = nullptr);
    ~ActivityHandler();

    void setActivityData(QVariantList dataList);

    QXmppPresence::AvailableStatusType presenceFromIndex(int presenceType);


signals:


public slots:
    void onMainTimerTick();
    void activityFinished();


private:
    QXmppClient *m_xmppClient;
    QXmppMucManager *m_mucManager;

    QVariantList m_activityDataList;

    QTimer *m_mainTimer;
    QTimer *m_currentActivityTimer;


    QVariantMap m_doneToday;
    QString m_activityInProgress;

    QStringList m_allRecipientJids;
    QStringList m_messagesAfter;

    QXmppPresence::AvailableStatusType m_oldStatusType;
    QString m_oldStatusMessage;


    GlobalObject *m_globalObject;
    VariableParser *m_variableParser;
};

#endif // ACTIVITYHANDLER_H
