/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "chatwidget.h"


ChatWidget::ChatWidget(GlobalObject *globalObject,
                       QWidget *parent) : QWidget(parent)
{
    m_globalObject = globalObject;
    m_xmppClient = m_globalObject->getXmppClient();

    m_isMuc = false;

    // Set it to 01/Jan/1970 for future comparison, so it appears before first message
    m_lastTimestamp = QDate(1970, 1, 1);

    m_unreadCount = 0;


    m_chatBrowser = new QTextBrowser(this);

    m_messageLineEdit = new QLineEdit(this);
    connect(m_messageLineEdit, &QLineEdit::returnPressed,
            this, &ChatWidget::sendMessage);

    // Give focus to line edit every time the text browser is focused
    //this->chatBrowser->setFocusProxy(this->messageLineEdit);
    // Disabled for now; it removes context menu in text browser -- FIXME


    m_mainLayout = new QVBoxLayout();
    m_mainLayout->setContentsMargins(0, 0, 0, 0);
    m_mainLayout->addWidget(m_chatBrowser);
    m_mainLayout->addWidget(m_messageLineEdit);
    this->setLayout(m_mainLayout);

    qDebug() << "ChatWidget created";
}


ChatWidget::~ChatWidget()
{
    qDebug() << "ChatWidget destroyed for:" << m_chatTitle;
}



void ChatWidget::setRoom(QXmppMucRoom *room)
{
    m_muc = room;
    m_isMuc = true;
    m_mucJid = room->jid();

    // This is not (currently) shown for MUCs, but keep it for internal use
    m_chatTitle = room->jid();

    connect(m_muc, &QXmppMucRoom::messageReceived,
            this, &ChatWidget::appendMessage);

    connect(m_muc, &QXmppMucRoom::joined,
            this, &ChatWidget::onJoined);
    connect(m_muc, &QXmppMucRoom::left,
            this, &ChatWidget::onLeft);

    connect(m_muc, &QXmppMucRoom::kicked,
            this, &ChatWidget::onKicked);

    connect(m_muc, &QXmppMucRoom::participantAdded,
            this, &ChatWidget::onParticipantJoined);
    connect(m_muc, &QXmppMucRoom::participantRemoved,
            this, &ChatWidget::onParticipantLeft);

    // Track the human-readable room name, possibly different from JID
    connect(m_muc, &QXmppMucRoom::nameChanged,
            this, &ChatWidget::onRoomNameChanged);
}


QString ChatWidget::getRoomJid()
{
    return m_mucJid;
}


void ChatWidget::setContact(QString jid)
{
    m_contactJid = jid;
    m_contactNick = jid.split("@").first();

    m_isMuc = false;
}

QString ChatWidget::getContactJid()
{
    return m_contactJid;
}

QString ChatWidget::getContactBareJid()
{
    return m_contactJid.split("/").first();
}

void ChatWidget::setContactNick(QString nickname)
{
    m_contactNick = nickname;
}


void ChatWidget::setChatTitle(QString title)
{
    m_chatTitle = title;
}

QString ChatWidget::getChatTitle()
{
    return m_chatTitle;
}


int ChatWidget::getUnreadCount()
{
    return m_unreadCount;
}


void ChatWidget::focusLineEdit()
{
    m_messageLineEdit->setFocus();
}



//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// SLOTS //////////////////////////////////
//////////////////////////////////////////////////////////////////////////////



void ChatWidget::appendMessage(QXmppMessage message)
{
    QString subject = Helpers::safeForHtml(message.subject().trimmed());
    if (!subject.isEmpty())
    {
        subject.append("<br />");
    }

    QString body = Helpers::safeForHtml(message.body());

    QString timestampLine;
    QString divStyle;
    QString nick;
    if (m_isMuc)
    {
        if (message.type() != QXmppMessage::GroupChat)
        {
            // A private message from a room, ignore it; handled by ChatModule
            return;
        }

        // Ignore completely empty messages (from ChatState changes, etc)
        // FIXME: In the future, maybe show these state changes somewhere else
        if (body.isEmpty())
        {
            return;
        }

        nick = Helpers::nickFromRoomJid(message.from());

        if (nick == m_muc->nickName())
        {
            // Own messages
            divStyle = " style=\"color:#00AAEE; font-weight:bold\""; // FIXME
        }
        else if (body.contains(m_muc->nickName(), Qt::CaseInsensitive))
        {
            // Mentioned in message
            divStyle = " style=\"color:#9910EE; font-weight:bold\""; // FIXME
        }
    }
    else
    {
        nick = m_contactNick;
    }


    // Add date line if day has changed
    if (m_lastTimestamp != QDate::currentDate())
    {
        timestampLine = QString("~~~~~~ %1 ~~~~~~<br />")
                        .arg(QLocale::system().toString(QDate::currentDate(),
                                                        QLocale::ShortFormat));
    }


    nick = Helpers::safeForHtml(nick);
    QString logMessage = QStringLiteral("<div") + divStyle
                       + QStringLiteral(">")
                       + timestampLine
                       + QStringLiteral("<b>[")
                       + QTime::currentTime().toString()
                       + QStringLiteral("]</b> ");

    if (message.stamp().isValid())
    {
        logMessage.append(QStringLiteral("{")
                          + QLocale::system().toString(message.stamp().toLocalTime(),
                                                       QLocale::ShortFormat)
                          + QStringLiteral("} "));
    }


    if (body.startsWith("/me "))
    {
        body.remove(0, 4);
        logMessage.append("<i>*" + nick + " "
                          + subject + body + "</i>");
    }
    else
    {
        logMessage.append("<i>&lt;" + nick + "&gt;</i> "
                          + subject + body);
    }

    /****
     **** FIXME: Handle chatstates
     ****
    QString state;
    switch (message.state())
    {
    case QXmppMessage::None:
        break;
    case QXmppMessage::Active:
        state="Active";
        break;
    case QXmppMessage::Inactive:
        state="Inactive";
        break;
    case QXmppMessage::Gone:
        state="Gone";
        break;
    case QXmppMessage::Composing:
        state="Composing";
        break;
    case QXmppMessage::Paused:
        state="Paused";
    }
    if (!state.isEmpty())
    {
        logMessage.append("  [state: " + state + "]");
    }

    ****
    ****/

    logMessage.append(QStringLiteral("</div>"));

    // Update for future comparison
    m_lastTimestamp = QDate::currentDate();

    m_chatBrowser->append(logMessage);
    if (!this->isVisible())
    {
        ++m_unreadCount;
    }
}




void ChatWidget::appendOutgoingMessage(QString messageText)
{
    QString body = Helpers::safeForHtml(messageText);

    const QString divStyle = " style=\"color:#00AAEE; font-weight:bold\"";
    QString nick;
    if (m_isMuc)
    {
        nick = m_muc->nickName();
    }
    else
    {
        nick = m_globalObject->getOwnNickname();
    }

    nick = Helpers::safeForHtml(nick);
    QString logMessage = "<div" + divStyle + ">"
                         "<b>[" + QTime::currentTime().toString() + "]</b> ";


    if (body.startsWith("/me "))
    {
        body.remove(0, 4);
        logMessage.append("<i>*" + nick + " " + body + "</i>");
    }
    else
    {
        logMessage.append("<i>&lt;" + nick + "&gt;</i> " + body);
    }

    logMessage.append("</div>");


    m_chatBrowser->append(logMessage);
}



void ChatWidget::sendMessage()
{
    const QString msg = m_messageLineEdit->text().trimmed();

    if (!msg.isEmpty())
    {
        if (m_isMuc)
        {
            m_muc->sendMessage(msg);
        }
        else
        {
            m_globalObject->sendPrivateMessage(m_contactJid, msg);
        }

        m_messageLineEdit->clear();
    }
}



void ChatWidget::onParticipantJoined(QString jid)
{
    const QString nick = Helpers::nickFromRoomJid(jid);

    // FIXME: make translatable
    m_chatBrowser->append("<div style=\"color:#DD0000\">"
                          "<b>[" + QTime::currentTime().toString()
                          + "]</b> <i>+++++ "
                          + Helpers::safeForHtml(nick)
                          + " joined.</i></div>");
}



void ChatWidget::onParticipantLeft(QString jid)
{
    const QString nick = Helpers::nickFromRoomJid(jid);

    // FIXME: make translatable
    m_chatBrowser->append("<div style=\"color:#DD0000\">"
                          "<b>[" + QTime::currentTime().toString()
                          + "]</b> <i>----- "
                          + Helpers::safeForHtml(nick)
                          + " left.</i></div>");
}



void ChatWidget::onJoined()
{
    QString mucName = m_mucJid;
    if (!m_muc->name().isEmpty())
    {
        mucName.append(" (" + m_muc->name() + ")");
    }

    m_kickedFromMucMessage.clear();

    const QString joinedMessage = tr("Joined room %1, as %2.") // FIXME
                                  .arg("<b>" + mucName.toHtmlEscaped() + "</b>")
                                  .arg("'<b>" + m_muc->nickName().toHtmlEscaped()
                                       + "</b>'");

    m_chatBrowser->append("<h3>~~~~~ " + joinedMessage + "</h3>");
    m_globalObject->addToLog(joinedMessage, true, GlobalObject::LogGood);

    // Inform RoomModule
    emit roomJoined(m_mucJid);
}


void ChatWidget::onLeft()
{
    qDebug() << "ChatWidget::onLeft()" << m_mucJid;
    QString mucName = m_mucJid;
    if (!m_muc->name().isEmpty())
    {
        mucName.append(" (" + m_muc->name() + ")");
    }

    // Confirm leave status to RoomModule
    emit roomLeft(m_mucJid);

    m_globalObject->removeFullyJoinedRoom(m_mucJid);

    if (m_kickedFromMucMessage.isEmpty())
    {
        m_globalObject->addToLog(tr("Left room %1.")
                                 .arg("<b>" + mucName + "</b>"));
    }
    else // Bot was kicked!
    {
        m_globalObject->addToLog("<b>" + mucName.toHtmlEscaped() + "</b>: "
                                 + m_kickedFromMucMessage,  // FIXME, make proper sentence
                                 true,
                                 GlobalObject::LogBad);
    }

    delete m_muc;
}


void ChatWidget::onKicked(QString jid, QString reason)
{
    if (!reason.isEmpty())
    {
        reason = tr("Reason:") + " "
                 + Helpers::safeForHtml(reason);
    }

    if (!jid.isEmpty())
    {
        reason.append(" (" + jid + ")");
    }

    m_kickedFromMucMessage = tr("Kicked from the room!") + " -- " + reason;

    m_chatBrowser->append("<hr />"
                          "<div style=\"color:#10BB10\">"  // TMP FIXME
                          "<b>[" + QTime::currentTime().toString()
                          + "] "
                          + m_kickedFromMucMessage
                          + "</b></div>"); // FIXME: currently this can barely be seen
}

/*
 * Track the human-readable room name, possibly different from its JID.
 *
 * It's usually received after the backlog messages, so it's a good indicator
 * that no more old messages, which must be ignored, will be received.
 *
 */
void ChatWidget::onRoomNameChanged(QString newName)
{
    m_chatBrowser->append("<h3>~~~~~ " + tr("Room name is: %1").arg(newName)
                          + "</h3>"
                            //"<hr />" // FIXME, this line keeps repeating sometimes
                            "<br />"); // ---------------------

    m_globalObject->addFullyJoinedRoom(m_mucJid);
}


//////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// PROTECTED ////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


void ChatWidget::showEvent(QShowEvent *event)
{
    const int unreadBefore = m_unreadCount;
    m_unreadCount = 0;
    emit messagesRead(m_chatTitle, unreadBefore);

    // Sometimes, on show, scrollbar sliders get set to slightly less than the maximum,
    // which makes them stay there after more messages appear; work around it
    const int scrollNow = m_chatBrowser->verticalScrollBar()->value();
    const int scrollMax = m_chatBrowser->verticalScrollBar()->maximum();
    if (scrollNow < scrollMax && scrollNow >= (scrollMax - 4))
    {
        m_chatBrowser->verticalScrollBar()->setValue(scrollMax);
    }

    event->accept();
}

