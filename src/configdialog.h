/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef CONFIGDIALOG_H
#define CONFIGDIALOG_H

#include <QWidget>
#include <QIcon>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QTabWidget>
#include <QCheckBox>
#include <QSpinBox>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QFileDialog>
#include <QSettings>
#include <QKeyEvent>

#include <QDebug>


#include "globalobject.h"
#include "stringlistwidget.h"


class ConfigDialog : public QWidget
{
    Q_OBJECT

public:
    explicit ConfigDialog(GlobalObject *globalObject,
                          QWidget *parent = nullptr);
    ~ConfigDialog();

    void setCustomIconButtonImage(const QString filename,
                                  bool reportErrors=true);


signals:
    void settingsSaved();


public slots:
    void onKeepAliveChanges(int newValue);
    void pickCustomIconFile();

    void saveSettings();


protected:
    virtual void keyPressEvent(QKeyEvent *event);


private:
    QVBoxLayout *m_mainLayout;
    QTabWidget *m_tabWidget;


    QWidget *m_generalOptionsWidget;
    QFormLayout *m_generalOptionsLayout;
    StringListWidget *m_adminJidListWidget;
    QLineEdit *m_commandPrefixLineEdit;
    QSpinBox *m_keepAliveIntervalSpinbox;
    QSpinBox *m_keepAliveTimeoutSpinbox;


    QWidget *m_notificationOptionsWidget;
    QFormLayout *m_notificationOptionsLayout;
    QCheckBox *m_notifyNewChatsCheckbox;
    QCheckBox *m_notifyDisconnectionCheckbox;


    QWidget *m_systrayOptionsWidget;
    QFormLayout *m_systrayOptionsLayout;
    QCheckBox *m_hideWindowCheckbox;
    QHBoxLayout *m_customIconLayout;
    QCheckBox *m_customIconCheckbox;
    QPushButton *m_customIconButton;
    QString m_customIconFilename;
    QString m_customIconLastDir;


    QDialogButtonBox *m_bottomButtonBox;
    QPushButton *m_saveButton;
    QPushButton *m_cancelButton;

    GlobalObject *m_globalObject;
};

#endif // CONFIGDIALOG_H
