/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef VARIABLEPARSER_H
#define VARIABLEPARSER_H

#include <QObject>
#include <QString>
#include <QStringList>
#include <QRegularExpression>
#include <QRandomGenerator>

#include <QDebug>


class VariableParser : public QObject
{
    Q_OBJECT

public:
    explicit VariableParser(QObject *parent = nullptr);
    ~VariableParser();

    void setOwnNickname(QString newNickname);


    QString getParsed(QString originalString);

    bool checkSyntax(QString stringToTest,
                     QStringList extraSubstitutions=QStringList());
    QString getLastCheckError();


private:
    QString m_ownNickname;

    QString m_lastCheckError;
};

#endif // VARIABLEPARSER_H
