/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef MESSAGEHANDLER_H
#define MESSAGEHANDLER_H

#include <QObject>

#include <QDebug>


#include <QXmppClient.h>
#include <QXmppMucManager.h>
#include <QXmppMessage.h>


#include "globalobject.h"
#include "variableparser.h"
#include "conversationhandler.h"


class MessageHandler : public QObject
{
    Q_OBJECT

public:
    explicit MessageHandler(GlobalObject *globalObject,
                            QObject *parent = nullptr);
    ~MessageHandler();

    void processMessage(QXmppMessage message);


signals:


public slots:


private:
    QXmppClient *m_xmppClient;
    QXmppMucManager *m_mucManager;

    ConversationHandler *m_conversationHandler;

    GlobalObject *m_globalObject;
    VariableParser *m_variableParser;
};

#endif // MESSAGEHANDLER_H
