/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "mainwindow.h"


MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    this->setWindowTitle(QStringLiteral("AkariXB"));
    this->setWindowIcon(QIcon(":/icon/64x64/akarixb.png"));

    // Ensure closing a dialog while the main window is hidden, won't end the program
    qApp->setQuitOnLastWindowClosed(false);

    m_trayIconAvailable = false;

    m_globalObject = new GlobalObject(this);
    connect(m_globalObject, &GlobalObject::showInStatusBar,
            this, &MainWindow::setStatusBarMessage);


#if 0 // Set to 1 to test the fallback icons
    QIcon::setThemeName("SHOW_ME_FALLBACK_ICONS_THX");
    // This one since Qt 5.12
 #if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
     QIcon::setFallbackThemeName("SRSLY_GIVE_ME_FALLBACK_ICONS");
 #endif
#endif

    setupXmppClient();

    // It's important to initialize these before creating the handlers, which use them
    m_variableParser = new VariableParser(this);
    m_globalObject->setVariableParser(m_variableParser);

    m_speechDataStore = new SpeechDataStore(this);
    m_globalObject->setSpeechDataStore(m_speechDataStore);


    m_messageHandler = new MessageHandler(m_globalObject, this);
    m_commandHandler = new CommandHandler(m_globalObject, this);
    m_activityHandler = new ActivityHandler(m_globalObject, this);


    // Page 10, runtime log, needed before others for early logging
    m_logModule = new LogModule(this);
    m_logModule->addToLog(QString("Qt v%1 (%2), QXMPP v%3, %4.")
                          .arg(qVersion(),
                               qApp->platformName(),
                               QXmppVersion(),
                               QSysInfo::prettyProductName()),
                          GlobalObject::LogNormal);
    m_logModule->addToLog(tr("Settings file in use: %1")
                          .arg(QSettings().fileName()),
                          GlobalObject::LogNormal);
    connect(m_globalObject, &GlobalObject::logMessage,
            m_logModule, &LogModule::addToLog);

    // Same thing, status bar widgets needed early
    createStatusBarWidgets();


    createDataDir();

    m_configDialog = new ConfigDialog(m_globalObject, this);
    connect(m_configDialog, &ConfigDialog::settingsSaved,
            this, &MainWindow::setTitleAndTrayInfo); // Kinda TMP...


    // Page 1, general
    m_generalModule = new GeneralModule(m_globalObject, this);

    // Page 2, MUCs
    m_roomModule = new RoomModule(m_globalObject, this);

    // Page 3, private chats
    m_chatModule = new ChatModule(m_globalObject, this);
    connect(m_chatModule, &ChatModule::unreadCountChanged,
            this, &MainWindow::setChatsTitle);

    // Page 4, contacts
    m_contactsModule = new ContactsModule(m_globalObject, this);

    // Page 5, commands
    m_commandModule = new CommandModule(m_commandHandler,
                                        m_globalObject,
                                        this);

    // Page 6, room speak: greetings, casual sentences, reaction sentences
    m_roomSpeakModule = new RoomSpeakModule(m_globalObject, this);

    // Page 7, conversations
    m_conversationModule = new ConversationModule(m_globalObject, this);

    // Page 8, activities
    m_activityModule = new ActivityModule(m_activityHandler,
                                          m_globalObject,
                                          this);

    // Page 9, identity
    m_identityModule = new IdentityModule(m_globalObject, this);



    // Categories panel
    m_categoriesList = new QListWidget(this);
    m_categoriesList->setViewMode(QListView::ListMode);
    m_categoriesList->setFlow(QListView::TopToBottom);
    m_categoriesList->setWrapping(false);
    m_categoriesList->setIconSize(QSize(48, 48));
    m_categoriesList->setWordWrap(true);
    m_categoriesList->setMovement(QListView::Static);
    m_categoriesList->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_categoriesList->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    m_categoriesList->addItem(tr("General"));
    m_categoriesList->item(0)->setIcon(QIcon::fromTheme("user-home",
                                                        QIcon(":/images/section-general.png")));
    m_categoriesList->addItem(tr("Rooms"));
    m_categoriesList->item(1)->setIcon(QIcon::fromTheme("system-users",
                                                        QIcon(":/images/section-rooms.png")));
    m_categoriesList->addItem(QString()); // Chats, title set later
    m_categoriesList->item(2)->setIcon(QIcon::fromTheme("system-users",
                                                        QIcon(":/images/section-rooms.png")));
    this->setChatsTitle(0);
    m_categoriesList->addItem(tr("Contacts"));
    m_categoriesList->item(3)->setIcon(QIcon::fromTheme("preferences-contact-list",
                                                        QIcon(":/images/section-rooms.png")));
    m_categoriesList->addItem(tr("Commands"));
    m_categoriesList->item(4)->setIcon(QIcon::fromTheme("system-run",
                                                        QIcon(":/images/section-commands.png")));
    m_categoriesList->addItem(tr("Room Speech"));
    m_categoriesList->item(5)->setIcon(QIcon::fromTheme("view-conversation-balloon",
                                                        QIcon(":/images/section-conv.png")));
    m_categoriesList->addItem(tr("Conversation"));
    m_categoriesList->item(6)->setIcon(QIcon::fromTheme("view-conversation-balloon",
                                                        QIcon(":/images/section-conv.png")));
    m_categoriesList->addItem(tr("Activities"));
    m_categoriesList->item(7)->setIcon(QIcon::fromTheme("view-categories",
                                                        QIcon(":/images/section-general.png")));
    m_categoriesList->addItem(tr("Identity"));
    m_categoriesList->item(8)->setIcon(QIcon::fromTheme("user-identity",
                                                        QIcon(":/images/no-avatar.png")));
    m_categoriesList->addItem(tr("Log"));
    m_categoriesList->item(9)->setIcon(QIcon::fromTheme("text-x-log",
                                                        QIcon(":/images/section-log.png")));

    // Pages on right side
    m_stackedWidget = new QStackedWidget(this);
    m_stackedWidget->addWidget(m_generalModule);
    m_stackedWidget->addWidget(m_roomModule);
    m_stackedWidget->addWidget(m_chatModule);
    m_stackedWidget->addWidget(m_contactsModule);
    m_stackedWidget->addWidget(m_commandModule);
    m_stackedWidget->addWidget(m_roomSpeakModule);
    m_stackedWidget->addWidget(m_conversationModule);
    m_stackedWidget->addWidget(m_activityModule);
    m_stackedWidget->addWidget(m_identityModule);
    m_stackedWidget->addWidget(m_logModule);
    connect(m_categoriesList, &QListWidget::currentRowChanged,
            m_stackedWidget, &QStackedWidget::setCurrentIndex);
    connect(m_categoriesList, &QListWidget::currentRowChanged,
            this, &MainWindow::setTitleAndTrayInfo);

    connect(m_globalObject, &GlobalObject::conversationTabRequested,
            this, &MainWindow::focusChatsModule);


    m_mainSplitter = new QSplitter(Qt::Horizontal, this);
    m_mainSplitter->setChildrenCollapsible(false);
    m_mainSplitter->setContentsMargins(0, 0, 0, 0);
    m_mainSplitter->setHandleWidth(m_mainSplitter->handleWidth() + 2);
    m_mainSplitter->addWidget(m_categoriesList);
    m_mainSplitter->addWidget(m_stackedWidget);
    this->setCentralWidget(m_mainSplitter);


    loadSettings();


    m_connectionDialog = new ConnectionDialog(m_userJid,
                                              m_userPassword,
                                              m_userAutoconnect,
                                              m_userResource,
                                              m_userPriority,
                                              m_ignoreSslErrors,
                                              this);
    connect(m_connectionDialog, &ConnectionDialog::connectionRequested,
            this, &MainWindow::connectToServer);
    connect(m_generalModule, &GeneralModule::connectionRequested,
            m_connectionDialog, &QWidget::show);
    connect(m_generalModule, &GeneralModule::disconnectionRequested,
            this, &MainWindow::disconnectFromServer);


    // Help widget; Syntax help, command parameters, etc.
    m_helpWidget = new HelpWidget(nullptr); // Created without a parent, so it
                                            // can be a truly independent window


    createMenus();
    createTrayIcon();


    // Selecting the first category will also end up calling setTitleAndTrayInfo()
    m_categoriesList->setCurrentRow(0);

    /*
     * If initial connection hangs for some reason, break it and retry after
     * a relatively short time, to avoid connection limbo.
     *
     * See https://github.com/qxmpp-project/qxmpp/issues/166
     */
    m_initialConnectionTimer = new QTimer(this);
    m_initialConnectionTimer->setSingleShot(true);
    m_initialConnectionTimer->setInterval(60000); // 60 sec, hardcoded, FIXME
    connect(m_initialConnectionTimer, &QTimer::timeout,
            this, &MainWindow::onConnectionTimerAbort);

    // Handle reconnections with a timer, with increasing delays up to 2 min or so
    m_reconnectionTimer = new QTimer(this);
    m_reconnectionTimer->setSingleShot(true);
    connect(m_reconnectionTimer, &QTimer::timeout,
            this, &MainWindow::autoConnectToServer);
    m_reconnectionDelaySecs = 0;


    this->setStatusBarMessage(tr("Ready."));


    if (m_userJid.isEmpty())
    {
        if (!m_globalObject->getHideInTray())
        {
            m_connectionDialog->show();
        }
    }
    else if (m_userAutoconnect)
    {
        this->autoConnectToServer();
    }

    qDebug() << "MainWindow created";
}


MainWindow::~MainWindow()
{
    this->saveSettings();

    qDebug() << "MainWindow destroyed";
}


void MainWindow::createMenus()
{
    // Session
    m_sessionConnectAction = new QAction(QIcon::fromTheme("network-connect",
                                                          QIcon(":/images/button-online.png")),
                                         tr("Connect"),
                                         this);
    m_sessionConnectAction->setShortcut(QKeySequence("Ctrl+Shift+C"));
    connect(m_sessionConnectAction, &QAction::triggered,
            m_connectionDialog, &QWidget::show);

    m_sessionDisconnectAction = new QAction(QIcon::fromTheme("network-disconnect",
                                                             QIcon(":/images/button-offline.png")),
                                            tr("Disconnect"),
                                            this);
    m_sessionDisconnectAction->setDisabled(true);
    m_sessionDisconnectAction->setShortcut(QKeySequence("Ctrl+Shift+D"));
    connect(m_sessionDisconnectAction, &QAction::triggered,
            this, &MainWindow::disconnectFromServer);

    m_sessionAbortAction = new QAction(QIcon::fromTheme("process-stop",
                                                        QIcon(":/images/button-cancel.png")),
                                       tr("Stop connection attempts"),
                                       this);
    m_sessionAbortAction->setDisabled(true);
    connect(m_sessionAbortAction, &QAction::triggered,
            this, &MainWindow::abortConnectionAttempts);

    m_sessionQuitAction = new QAction(QIcon::fromTheme("application-exit",
                                                       QIcon(":/images/button-close.png")),
                                      tr("Quit"),
                                      this);
    m_sessionQuitAction->setShortcut(QKeySequence("Ctrl+Q"));
    connect(m_sessionQuitAction, &QAction::triggered,
            this, &MainWindow::quitProgram);


    m_sessionMenu = new QMenu(tr("&Session"), this);
    m_sessionMenu->addAction(m_sessionConnectAction);
    m_sessionMenu->addAction(m_sessionDisconnectAction);
    m_sessionMenu->addAction(m_sessionAbortAction);
    m_sessionMenu->addSeparator();
    m_sessionMenu->addAction(m_sessionQuitAction);
    this->menuBar()->addMenu(m_sessionMenu);


    // Settings
    m_settingsConfigAction = new QAction(QIcon::fromTheme("configure",
                                                          QIcon(":/images/button-configure.png")),
                                         tr("Configure AkariXB"),
                                         this);
    m_settingsConfigAction->setShortcut(QKeySequence("Ctrl+Shift+S"));
    connect(m_settingsConfigAction, &QAction::triggered,
            m_configDialog, &QWidget::show);

    m_settingsMenu = new QMenu(tr("S&ettings"), this);
    m_settingsMenu->addAction(m_settingsConfigAction);
    this->menuBar()->addMenu(m_settingsMenu);

    this->menuBar()->addSeparator();


    // Help
    m_helpGeneralHelpAction = new QAction(QIcon::fromTheme("help-browser",
                                                           QIcon(":/icon/64x64/akarixb.png")),
                                          tr("General &Help"),
                                          this);
    m_helpGeneralHelpAction->setShortcut(Qt::Key_F1);
    connect(m_helpGeneralHelpAction, &QAction::triggered,
            m_helpWidget, &QWidget::show);

    m_helpWebsiteAction = new QAction(QIcon::fromTheme("internet-web-browser",
                                                       QIcon(":/images/button-online.png")),
                                      tr("Visit Website"),
                                      this);
    connect(m_helpWebsiteAction, &QAction::triggered,
            this, &MainWindow::visitWebsite);

    m_helpBugTrackerAction = new QAction(QIcon::fromTheme("tools-report-bug",
                                                          QIcon(":/images/button-online.png")),
                                         tr("Report a Bug"),
                                         this);
    connect(m_helpBugTrackerAction, &QAction::triggered,
            this, &MainWindow::visitBugTracker);

    m_helpAboutXmppAction = new QAction(QIcon::fromTheme("internet-web-browser",
                                                         QIcon(":/images/button-online.png")),
                                        tr("About XMPP"),
                                        this);
    connect(m_helpAboutXmppAction, &QAction::triggered,
            this, &MainWindow::visitXmppOrg);

    m_helpAboutAction = new QAction(QIcon(":/icon/64x64/akarixb.png"),
                                    tr("About AkariXB"),
                                    this);
    connect(m_helpAboutAction, &QAction::triggered,
            this, &MainWindow::aboutAkariXB);

    m_helpMenu = new QMenu(tr("&Help"), this);
    m_helpMenu->addAction(m_helpGeneralHelpAction);
    m_helpMenu->addSeparator();
    m_helpMenu->addAction(m_helpWebsiteAction);
    m_helpMenu->addAction(m_helpBugTrackerAction);
    m_helpMenu->addAction(m_helpAboutXmppAction);
    m_helpMenu->addSeparator();
    m_helpMenu->addAction(m_helpAboutAction);
    this->menuBar()->addMenu(m_helpMenu);


    // Context menu for the tray icon
    m_trayTitleAction = new QAction(QStringLiteral("AkariXB"), this);
    /* Separator actions with names don't work in some environments:
     *
     *  m_trayTitleAction->setSeparator(true);
     *
     * so just use a regular disabled action as 'title' of the menu instead
     */
    m_trayTitleAction->setDisabled(true);

    m_trayShowWindowAction = new QAction(QIcon(":/icon/64x64/akarixb.png"),
                                         QStringLiteral("*show-hide-window*"),
                                         this);
    connect(m_trayShowWindowAction, &QAction::triggered,
            this, &MainWindow::toggleMainWindow);


    m_systrayMenu = new QMenu(QStringLiteral("*akarixb-systray-menu*"), this);
    m_systrayMenu->setSeparatorsCollapsible(false);
    m_systrayMenu->addAction(m_trayTitleAction);
    m_systrayMenu->addSeparator();
    m_systrayMenu->addAction(m_trayShowWindowAction);
    m_systrayMenu->addAction(m_sessionConnectAction);
    m_systrayMenu->addAction(m_sessionDisconnectAction);
    m_systrayMenu->addSeparator();
    m_systrayMenu->addAction(m_settingsConfigAction);
    m_systrayMenu->addSeparator();
    m_systrayMenu->addAction(m_helpGeneralHelpAction);
    m_systrayMenu->addAction(m_helpAboutAction);
    m_systrayMenu->addSeparator();
    m_systrayMenu->addAction(m_sessionQuitAction);
}


void MainWindow::createTrayIcon()
{
    m_systrayIcon = new QSystemTrayIcon(QIcon(":/icon/64x64/akarixb.png"),
                                        this);

    if (m_systrayIcon->isSystemTrayAvailable())
    {
        m_trayIconAvailable = true;

        // Catch clicks on icon
        connect(m_systrayIcon, &QSystemTrayIcon::activated,
                this, &MainWindow::trayControl);

        // Show notifications requested via GlobalObject
        connect(m_globalObject, &GlobalObject::showNotificationPopup,
                this, &MainWindow::showNotification);

        // Set contextual menu for the icon
        m_systrayIcon->setContextMenu(m_systrayMenu);
        m_systrayIcon->setToolTip("AkariXB");
        m_systrayIcon->show();

        qDebug() << "Tray icon created";
    }
    else
    {
        m_trayIconAvailable = false;
        qDebug() << "System tray is NOT available";
    }
}



void MainWindow::createStatusBarWidgets()
{
    // Replace regular QStatusBar::showMessage with a permanent message label
    m_statusBarMessageLabel = new QLabel(this);

    m_statusBarIconLabel = new QLabel(this);
    this->setStatusIconOnline(QXmppPresence::Online, false);
    connect(m_globalObject, &GlobalObject::statusChanged,
            this, &MainWindow::onStatusChanged);

    this->statusBar()->addPermanentWidget(m_statusBarMessageLabel, 1);
    this->statusBar()->addPermanentWidget(m_statusBarIconLabel);
    this->statusBar()->setSizeGripEnabled(false);
}



void MainWindow::createDataDir()
{
    const QString dataDirectory = QStandardPaths::standardLocations(QStandardPaths::AppLocalDataLocation)
                                                 .first();

    m_globalObject->setDataDirectory(dataDirectory);
    qDebug() << "Data directory:" << dataDirectory;

    QDir dataDir;
    if (!dataDir.exists(dataDirectory))
    {
        qDebug() << "Creating data directory";
        if (dataDir.mkpath(dataDirectory))
        {
            m_globalObject->addToLog(tr("Data directory created: %1")
                                     .arg(dataDirectory));
            qDebug() << "Data directory created:" << dataDirectory;
        }
        else
        {
            m_globalObject->addToLog(tr("Error creating data directory: %1")
                                     .arg(dataDirectory),
                                     true,
                                     GlobalObject::LogBad);
            qDebug() << "Error creating data directory!";
        }
    }
    else
    {
        m_globalObject->addToLog(tr("Data directory in use: %1")
                                 .arg(dataDirectory));
    }

    if (!dataDir.exists(dataDirectory + QStringLiteral("/logs")))
    {
        qDebug() << "Creating logs directory";
        if (dataDir.mkpath(dataDirectory + QStringLiteral("/logs")))
        {
            qDebug() << "Logs directory created";
        }
        else
        {
            qDebug() << "Error creating logs directory!";
        }
    }
}



void MainWindow::setupXmppClient()
{
    m_xmppClient = new QXmppClient(this);
    m_versionManager = m_xmppClient->findExtension<QXmppVersionManager>();
    m_versionManager->setClientName(QStringLiteral("AkariXB"));
    m_versionManager->setClientOs("Akari-GNU-OS"); // tmp FIXME xD
    m_disconnectedManually = false;

    connect(m_xmppClient, &QXmppClient::connected,
            this, &MainWindow::onConnected);
    connect(m_xmppClient, &QXmppClient::disconnected,
            this, &MainWindow::onDisconnected);

    connect(m_xmppClient, &QXmppClient::messageReceived,
            this, &MainWindow::processMessage);
    connect(m_xmppClient, &QXmppClient::presenceReceived,
            this, &MainWindow::processPresence);
    connect(m_xmppClient, &QXmppClient::iqReceived,
            this, &MainWindow::processIq);

    connect(m_xmppClient, &QXmppClient::error,
            this, &MainWindow::onNetworkError);

    connect(m_versionManager, &QXmppVersionManager::versionReceived,
            this, &MainWindow::processVersionIq);

    m_mucManager = new QXmppMucManager();
    m_xmppClient->addExtension(m_mucManager);

    m_receiptManager = new QXmppMessageReceiptManager();
    m_xmppClient->addExtension(m_receiptManager);

    m_discoveryManager = new QXmppDiscoveryManager();
    m_discoveryManager->setClientName(QStringLiteral("AkariXB v")
                                      + qApp->applicationVersion());
    m_discoveryManager->setClientCapabilitiesNode("http://akarixb.nongnu.org");
    //m_discoveryManager->setClientCategory("client"); // Future tests
    //m_discoveryManager->setClientType("bot");
    m_xmppClient->addExtension(m_discoveryManager);


    m_globalObject->setXmppClient(m_xmppClient);
    m_globalObject->setMucManager(m_mucManager);
}


void MainWindow::enableXmppDebug()
{
    m_xmppClient->logger()->setLoggingType(QXmppLogger::StdoutLogging);
}




void MainWindow::loadSettings()
{
    QSettings settings;
    settings.beginGroup(QStringLiteral("Connection"));
    m_userJid = settings.value(QStringLiteral("jid")).toString();
    m_userPassword = QByteArray::fromBase64(settings.value(QStringLiteral("password"))
                                                    .toByteArray());
    m_userAutoconnect = settings.value(QStringLiteral("autoconnect"), false).toBool();
    m_userResource = settings.value(QStringLiteral("resource"), QStringLiteral("AkariXB"))
                             .toString();
    m_userPriority = settings.value(QStringLiteral("priority"), 10).toInt();
    m_ignoreSslErrors = settings.value(QStringLiteral("ignoresslerrors"), false).toBool();
    settings.endGroup();


    settings.beginGroup(QStringLiteral("MainWindow"));
    this->resize(settings.value(QStringLiteral("windowSize"),
                                QSize(800, 600)).toSize());

    if (settings.contains(QStringLiteral("windowPosition")))
    {
        this->move(settings.value(QStringLiteral("windowPosition")).toPoint());
    }

    if (settings.contains(QStringLiteral("mainSplitterState")))
    {
        m_mainSplitter->restoreState(settings.value(QStringLiteral("mainSplitterState"))
                                             .toByteArray());
        // Set childrenCollapsible to false AFTER loading state, to ensure
        m_mainSplitter->setChildrenCollapsible(false); // state does not mess with it
    }

    settings.endGroup();
}


void MainWindow::saveSettings()
{
    QSettings settings;
    settings.beginGroup("Connection");
    settings.setValue("jid",             m_userJid);
    settings.setValue("password",        m_userPassword.toLocal8Bit().toBase64());
    settings.setValue("autoconnect",     m_userAutoconnect);
    settings.setValue("resource",        m_userResource);
    settings.setValue("priority",        m_userPriority);
    settings.setValue("ignoresslerrors", m_ignoreSslErrors);
    settings.endGroup();


    settings.beginGroup("MainWindow");
    settings.setValue("windowSize",        this->size());
    settings.setValue("windowPosition",    this->pos());
    settings.setValue("mainSplitterState", m_mainSplitter->saveState());
    settings.endGroup();
    settings.sync();
}


void MainWindow::setStatusIconOnline(QXmppPresence::AvailableStatusType statusType,
                                     bool isOnline)
{
    QString iconFile;

    if (isOnline)
    {
        switch (statusType)
        {
        case QXmppPresence::Online:
            iconFile = QStringLiteral(":/images/button-online.png");
            break;

        case QXmppPresence::Away:
            iconFile = QStringLiteral(":/images/button-away.png");
            break;

        case QXmppPresence::XA:
            iconFile = QStringLiteral(":/images/button-xa.png");
            break;

        case QXmppPresence::DND:
            iconFile = QStringLiteral(":/images/button-dnd.png");
            break;

        default:
            iconFile = QStringLiteral(":/images/button-online.png");
            break;
        }
    }
    else
    {
        iconFile = QStringLiteral(":/images/button-offline.png");
    }

    m_statusBarIconLabel->setPixmap(QPixmap(iconFile).scaled(16, 16));
}


void MainWindow::increaseReconnectionTimeout()
{
    if (m_reconnectionDelaySecs < 120)
    {
        m_reconnectionDelaySecs += 10;
    }
}



//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// SLOTS //////////////////////////////////
//////////////////////////////////////////////////////////////////////////////



void MainWindow::connectToServer(QString jid, QString password,
                                 bool autoconnect, QString resource,
                                 int priority, bool ignoreSslErrors)
{
    m_userJid = jid;
    m_userPassword = password;
    m_userAutoconnect = autoconnect;
    m_userResource = resource;
    m_userPriority = priority;
    m_ignoreSslErrors = ignoreSslErrors;

    const QString message = tr("Connecting...");
    m_generalModule->setInfoMessage(message);
    m_globalObject->addToLog(message);

    m_generalModule->setConnecting(true);
    m_sessionConnectAction->setDisabled(true);
    m_sessionAbortAction->setEnabled(true);

    QXmppConfiguration clientConfig;
    clientConfig.setJid(m_userJid);
    clientConfig.setResource(m_userResource);
    clientConfig.setPassword(m_userPassword);
    clientConfig.setKeepAliveInterval(m_globalObject->getKeepAliveInterval());
    clientConfig.setKeepAliveTimeout(m_globalObject->getKeepAliveTimeout());
    clientConfig.setAutoReconnectionEnabled(false); // QXMPP autoreconnect kinda sucks
    clientConfig.setIgnoreSslErrors(m_ignoreSslErrors);
    if (m_ignoreSslErrors)
    {
        const QString sslWarning = tr("Warning! Ignoring errors in "
                                      "SSL connection to server.");
        m_generalModule->setInfoMessage(sslWarning);
        m_globalObject->addToLog(sslWarning, true, GlobalObject::LogBad);
    }

    m_disconnectedManually = false;
    m_initialConnectionTimer->start();

    qDebug() << QTime::currentTime().toString()
             << "Connecting to" << m_userJid;
    m_xmppClient->connectToServer(clientConfig);
}


void MainWindow::autoConnectToServer()
{
    qDebug() << "Autoconnecting to server...";
    this->connectToServer(m_userJid,
                          m_userPassword,
                          m_userAutoconnect,
                          m_userResource,
                          m_userPriority,
                          m_ignoreSslErrors);
}



void MainWindow::onConnected()
{
    const QString message = tr("%1 connected.").arg(m_userJid);
    m_generalModule->setInfoMessage(message); // FIXME: show "NO SSL" here?
    m_globalObject->addToLog(message, true, GlobalObject::LogGood);

    m_initialConnectionTimer->stop();

    m_reconnectionDelaySecs = 0; // Connection successful, so reset

    QStringList statusMessages;    // TMP hardcoded 'default' messages
    statusMessages << "H{i|ii|iii}!"
                   << "I'm h{e|ee|eee}re{!|!!}"
                   << "%ownnick% is here{!|!!|!!!}"
                   << "I'm around...";
    // FIXME: Restore the one previous to a disconnection, if that's the case
    // Also, if there's an activity in progress, use that one


    QXmppPresence presence;
    presence.setPriority(m_userPriority);
    presence.setAvailableStatusType(QXmppPresence::Online);
    presence.setStatusText(m_variableParser
                           ->getParsed(Helpers::randomString(statusMessages)));
    m_xmppClient->setClientPresence(presence);

    // Need to disable this one here again, for auto-reconnections
    m_sessionConnectAction->setDisabled(true);

    m_sessionDisconnectAction->setEnabled(true);
    m_sessionAbortAction->setDisabled(true);

    m_roomModule->autojoinRooms();

    m_globalObject->setConnected(true);
    this->setTitleAndTrayInfo();
    this->setStatusIconOnline(QXmppPresence::Online, true);

    // Request server version information
    const QStringList jidParts = m_userJid.split(QStringLiteral("@"));
    if (!jidParts.isEmpty())
    {
        m_userServer = jidParts.last();
        m_versionManager->requestVersion(m_userServer);
    }
}


void MainWindow::disconnectFromServer()
{
    m_initialConnectionTimer->stop();
    m_disconnectedManually = true;
    m_xmppClient->disconnectFromServer();

    m_sessionAbortAction->setDisabled(true);

    m_reconnectionDelaySecs = 0; // Reset
}


void MainWindow::abortConnectionAttempts()
{
    this->disconnectFromServer();

    const QString message = tr("Aborted connection attempt.");
    m_generalModule->setInfoMessage(message);
    m_globalObject->addToLog(message, true, GlobalObject::LogBad);

    // Enable 'Connect' action here regardless of when onDisconnected() runs,
    // since there's a race condition in some cases
    m_sessionConnectAction->setEnabled(true);
    // GeneralModule's Connect button, too
    m_generalModule->setConnecting(false);
}


void MainWindow::onDisconnected()
{
    const QString message = tr("Disconnected.");
    m_generalModule->setInfoMessage(message);
    m_globalObject->addToLog(message, true, GlobalObject::LogBad);
    qDebug() << "\n" << QDateTime::currentDateTime().toString() << "\n"
                "\n=========================================================="
                "\n======================-DISCONNECTED-======================"
                "\n=========================================================="
                "\n";

    m_sessionDisconnectAction->setDisabled(true);
    m_sessionConnectAction->setEnabled(true);

    m_globalObject->setConnected(false);
    this->setTitleAndTrayInfo();
    this->setStatusIconOnline(QXmppPresence::Online, false);

    if (!m_disconnectedManually)
    {
        // We only notify the disconnection if it wasn't manual
        if (m_globalObject->getNotifyDisconnection())
        {
            m_globalObject->showNotification(tr("Disconnected"),
                                             tr("Connection to the server "
                                                "was lost."));
        }

        // TODO: optional email notification for this event

        // Don't reconnect directly here to avoid crashes
        this->increaseReconnectionTimeout();
        m_reconnectionTimer->start(m_reconnectionDelaySecs * 1000);
        qDebug() << "Reconnecting again in" << m_reconnectionDelaySecs << "seconds";

        // FIXME: autoreconnection should be optional
    }
}

/*
 * Workaround for https://github.com/qxmpp-project/qxmpp/issues/166
 *
 */
void MainWindow::onConnectionTimerAbort()
{
    m_globalObject->addToLog(tr("Connection seems stuck. Trying again..."),
                             true,
                             GlobalObject::LogBad);
    qDebug() << "==========================================================\n"
                "==============-ABORTING CONNECTION BY TIMER-==============\n"
                "==========================================================\n";
    m_initialConnectionTimer->stop();
    m_xmppClient->disconnectFromServer(); // Set it to disconnected state, to
    m_disconnectedManually = false;       // avoid 'Trying to connect while
                                          // connection is in progress'

    // Don't reconnect directly here to avoid crashes
    this->increaseReconnectionTimeout();
    m_reconnectionTimer->start(m_reconnectionDelaySecs * 1000);
    qDebug() << "Attempting connection again in" << m_reconnectionDelaySecs
             << "seconds";
}


void MainWindow::onStatusChanged(QXmppPresence presence)
{
    this->setStatusIconOnline(presence.availableStatusType(),
                              m_globalObject->connectedToServer());
}


void MainWindow::processMessage(QXmppMessage message)
{
    // Send private messages to their tabs, first (order is important)
    if (message.type() == QXmppMessage::Chat
     || message.type() == QXmppMessage::Normal
     || message.type() == QXmppMessage::Headline) // TMP: This one could use its own place
    {
        // Ignore completely empty messages (from ChatState changes, etc)
        // FIXME: In the future, show these state changes somewhere else
        if (!message.body().isEmpty())
        {
            m_chatModule->addChatTab(message);
        }
    }


    /*
     * To avoid reacting to MUC backlog messages, check if the message is for
     * a room that hasn't been completely joined yet. If so, don't process
     * the message any further.
     *
     * MUC backlog messages have timestamps, message.stamp().isValid(), but
     * some users (especially mobile) might send messages with the delay tag
     * and a timestamp set, so that's not reliable.
     *
     */
    if (message.type() == QXmppMessage::GroupChat)
    {
        const QString roomJid = message.from().split("/").first();
        if (!m_globalObject->isRoomFullyJoined(roomJid))
        {
            // This message seems to be part of the backlog, so stop here
            return;
        }
    }


    // Afterwards, actually process the received message
    if (message.body().startsWith(m_globalObject->getCommandPrefix())
     && message.type() != QXmppMessage::Error)
    {
        m_commandHandler->processCommand(message);
    }
    else
    {
        m_messageHandler->processMessage(message);
    }
}


void MainWindow::processPresence(QXmppPresence presence)
{
    m_generalModule->addPresence(presence); // TMP...

    // Log presence errors, they might appear when a room can't be joined
    if (presence.type() == QXmppPresence::Error)
    {
        const QString errorMessage = tr("Presence error %1 from %2: %3")
                                     .arg(QString("<b>%1</b>")
                                          .arg(presence.error().code()))
                                     .arg("'<b>" + presence.from().toHtmlEscaped()
                                          + "</b>'")
                                     .arg(presence.error().text().toHtmlEscaped());

        m_globalObject->addToLog(errorMessage,
                                 true, // Show in status bar too
                                 GlobalObject::LogBad);
    }
    else
    {
        QString presenceMessage;
        int logCategory = -1;
        if (presence.type() == QXmppPresence::Subscribe)
        {
            presenceMessage = tr("%1 wants to subscribe to your presence.",
                                 "%1 is another user's address")
                              .arg("<b>" + presence.from().toHtmlEscaped()
                                   + "</b>");
            logCategory = GlobalObject::LogGood;
        }
        else if (presence.type() == QXmppPresence::Subscribed)
        {
            presenceMessage = tr("%1 accepted your subscription request.",
                                 "%1 is another user's address")
                              .arg("<b>" + presence.from().toHtmlEscaped()
                                   + "</b>");
            logCategory = GlobalObject::LogGood;
        }
        else if (presence.type() == QXmppPresence::Unsubscribe)
        {
            presenceMessage = tr("%1 unsubscribed from your presence.",
                                 "%1 is another user's address")
                              .arg("<b>" + presence.from().toHtmlEscaped()
                                   + "</b>");
            logCategory = GlobalObject::LogBad;
        }
        else if (presence.type() == QXmppPresence::Unsubscribed)
        {
            presenceMessage = tr("%1 removed your authorization to see "
                                 "their presence.",
                                 "%1 is another user's address")
                              .arg("<b>" + presence.from().toHtmlEscaped()
                                   + "</b>");
            logCategory = GlobalObject::LogBad;
        }

        if (logCategory != -1)
        {
            m_globalObject->addToLog(presenceMessage,
                                     true, // Also in status bar
                                     logCategory);
        }
    }
}


void MainWindow::processIq(QXmppIq iq)
{
    qDebug() << QTime::currentTime().toString() << "IQ" << iq.id()
             << iq.from() << ">" << iq.to() << "Type(EGSR):" << iq.type();

    for (const QXmppElement iqExt : iq.extensions())
    {
        qDebug() << iqExt.tagName();
    }
}


void MainWindow::processVersionIq(QXmppVersionIq versionIq)
{
    QString versionString = QString("%1 %2").arg(versionIq.name())
                                            .arg(versionIq.version());
    QString osString = versionIq.os();
    if (!osString.isEmpty())
    {
        versionString.append(QString(" (%1)").arg(osString));
    }

    const QString message = tr("Received version information from %1:")
                            .arg("<b>" + versionIq.from() + "</b>")
                            + " " + versionString;;

    m_globalObject->addToLog(message,
                             true,
                             GlobalObject::LogGood);

    // Info is about our server, display it in the General section
    if (versionIq.from() == m_userServer)
    {
        m_generalModule->setServerInfo(versionString);
    }
}



void MainWindow::onNetworkError(QXmppClient::Error error)
{
    QString errorString;
    switch (error)
    {
    case QXmppClient::SocketError:
        errorString = tr("Socket Error")
                      + " (" + m_xmppClient->socketErrorString() + ")";
        break;

    case QXmppClient::KeepAliveError:
        errorString = tr("KeepAlive Error");
        break;

    case QXmppClient::XmppStreamError:
        errorString = tr("XMPP Stream Error")
                      + " (" + m_xmppClient->socketErrorString() + ")";
        qDebug() << "::: XMPP Stream Error:" << m_xmppClient->xmppStreamError()
                 << m_xmppClient->socketErrorString();
        break;

    default:
        errorString = QStringLiteral("NoError");
    }


    const QString message = tr("Network error: %1")
                            .arg("<b>" + errorString + "</b>");
    m_globalObject->addToLog(message, true, GlobalObject::LogBad);

    qDebug() << QDateTime::currentDateTime().toString()
             << "Network error:" << errorString;
    //         << "\nStanza:" << this->xmppClient->xmppStreamError()
    //         << "\nSocket:" << this->xmppClient->socketErrorString();
}


void MainWindow::setChatsTitle(int count)
{
    QString title = tr("Chats");
    if (count > 0)
    {
        // FIXME: this has some issues if the translated title is long
        title.prepend(QStringLiteral("* "));
        title.append(QString("\n--  %1  --").arg(count));
        // TODO: maybe hint with color, like:
        //// categoriesList->item(2)->setBackgroundColor(Qt::red);
        //// categoriesList->item(2)->setTextColor(Qt::white);
    }
    m_categoriesList->item(2)->setText(title);

    // Title bar and tray icon tooltip might also need updating when this changes
    if (m_categoriesList->currentRow() == 2)
    {
        this->setTitleAndTrayInfo();
    }
}


void MainWindow::focusChatsModule()
{
    m_categoriesList->setCurrentRow(2);
}



void MainWindow::visitWebsite()
{
    qDebug() << "Opening website in browser";
    QDesktopServices::openUrl(QUrl("https://jancoding.wordpress.com/akarixb"));
}


void MainWindow::visitBugTracker()
{
    qDebug() << "Opening bugtracker in browser";
    QDesktopServices::openUrl(QUrl("https://gitlab.com/akarixb/akarixb-dev/issues"));
}


void MainWindow::visitXmppOrg()
{
    qDebug() << "Opening XMPP.org in browser";
    QDesktopServices::openUrl(QUrl("https://xmpp.org/about/technology-overview.html"));
}


/*
 * About... window
 *
 */
void MainWindow::aboutAkariXB()
{
    QMessageBox::about(this,
                       tr("About AkariXB"),
                       QString("<big><b>AkariXB v%1</b></big>")
                       .arg(qApp->applicationVersion())
                       + "<br />"
                         "Copyright 2015-2024  JanKusanagi<br />"
                         "<a href=\"https://jancoding.wordpress.com/akarixb\">"
                         "https://jancoding.wordpress.com/akarixb</a><br />"
                         "<br />"
                       + tr("AkariXB is a Jabber/XMPP bot.")
                       + "<br />"

                       + tr("You can talk to it, make it join chatrooms, etc.")

                       + "<hr>" // ---

                       + tr("English translation by JanKusanagi.",
                            "TRANSLATORS: Change this with your language "
                            "and name. If there was another translator before "
                            "you, add your name after theirs ;)")
                       + "<br />"
                         "<br />"

                       + tr("Thanks to all the testers, translators and "
                            "packagers, who help make AkariXB better!")

                       + "<hr>" // ---

                       + tr("AkariXB is Free Software, licensed under the "
                            "GNU GPL license, and uses some Oxygen icons "
                            "under LGPL license.")
                       + "<br><br>"

                         "<a href=\"http://www.gnu.org/licenses/gpl-2.0.html\">"
                         "GNU GPL v2</a> - "
                         "<a href=\"http://www.gnu.org/licenses/lgpl-2.1.html\">"
                         "GNU LGPL v2.1</a>"
                         "<br><br>"
                         "<a href=\"https://techbase.kde.org/Projects/Oxygen\">"
                         "techbase.kde.org/Projects/Oxygen</a>"
                         "<br>"
                         "<br>"

                       + QString("<b>Qt</b> v%1").arg(qVersion())
                       + QString(" &mdash; %1").arg(qApp->platformName())
                       + QString("<br>"
                                 "<b>QXMPP</b> v%1").arg(QXmppVersion()));
}


void MainWindow::setTitleAndTrayInfo()
{
    QString currentTabTitle = QStringLiteral("#");
    const QString offlineString = " (" + tr("Offline") + ")";

    if (m_categoriesList->currentRow() != -1)
    {
        currentTabTitle = m_categoriesList->currentItem()->text();
    }

    // Window title bar
    QString title = currentTabTitle;
    if (!m_userJid.isEmpty())
    {
        title.append(QString(" - %1/%2").arg(m_userJid, m_userResource));
    }

    if (!m_globalObject->connectedToServer())
    {
        title.append(offlineString);
    }

    title.append(" - AkariXB");
    this->setWindowTitle(title);


    // Tray icon tooltip
    if (m_trayIconAvailable)
    {
        // FIXME -- It's overkill to do all this if only the selected section changed
        QIcon newIcon;
        if (m_globalObject->getUseCustomTrayIcon())
        {
            newIcon = QIcon(m_globalObject->getCustomTrayIconFilename());
            if (newIcon.pixmap(QSize(64, 64)).isNull())
            {
                newIcon = QIcon(":/icon/64x64/akarixb.png");
            }
        }
        else
        {
            newIcon = QIcon(":/icon/64x64/akarixb.png");
        }

        if (m_globalObject->connectedToServer())
        {
            m_systrayIcon->setIcon(newIcon);
        }
        else
        {
            m_systrayIcon->setIcon(newIcon.pixmap(QSize(64, 64),
                                                  QIcon::Disabled));
        }

        const QString idToShow = m_userJid.isEmpty()
                                 ? tr("Account is not configured")
                                 : m_userJid + "/" + m_userResource;

        // Can't use HTML in the tray tooltip until Qt starts supporting
        // the Description field in SNI icons, which can have HTML
        title = "AkariXB - " + currentTabTitle + "\n"
                + idToShow
                + (m_globalObject->connectedToServer() ?
                   QString() : offlineString);

        m_systrayIcon->setToolTip(title);

        m_trayTitleAction->setText("AkariXB - " + m_userJid);
    }
}


void MainWindow::setStatusBarMessage(QString message)
{
    message = Helpers::newlinesRemoved(message);
    // FIXME: Need to make sure that user-defined parts of the message
    // get their HTML filtered out

    m_statusBarMessageLabel->setText("[" + QTime::currentTime().toString()
                                     + "] " + message);
}


void MainWindow::showNotification(QString title, QString message)
{
    if (m_trayIconAvailable)
    {
        title.append(QStringLiteral(" - AkariXB"));
        if (!m_userJid.isEmpty())
        {
            title.append(QString(" (%1)").arg(m_userJid));
        }

        // This themed icon overload requires Qt 5.9+, but QXMPP already requires it
        m_systrayIcon->showMessage(title,
                                   message,
                                   QIcon::fromTheme("akarixb",
                                                    QIcon::fromTheme("dialog-information")),
                                   10000); // 10 secs
    }
}


/*
 * Control interaction with the system tray icon
 *
 */
void MainWindow::trayControl(QSystemTrayIcon::ActivationReason reason)
{
    qDebug() << "Tray icon activation reason:" << reason;

    if (reason != QSystemTrayIcon::Context) // Simple "main button" click in icon
    {
        // Hide or show the main window
        if (this->isMinimized())
        {
            // Hide and show, because raise() wouldn't work
            this->hide();
            this->showNormal();
            this->activateWindow();
            qDebug() << "RAISING from minimized state";
        }
        else
        {
            this->toggleMainWindow();
        }
    }
}


void MainWindow::toggleMainWindow(bool onStartup)
{
    bool shouldHide = false;
    if (onStartup)
    {
        shouldHide = m_globalObject->getHideInTray();
    }

    if ((this->isHidden() && !shouldHide) || !m_trayIconAvailable)
    {
        m_trayShowWindowAction->setText(tr("&Hide Window"));
        this->show();
        this->activateWindow(); // Try to show it on top of others
        qDebug() << "SHOWING main window";
    }
    else
    {
        m_trayShowWindowAction->setText(tr("&Show Window"));
        this->hide();
        qDebug() << "HIDING main window";
    }
}



void MainWindow::quitProgram()
{
    if (this->isHidden())
    {
        this->toggleMainWindow();
    }

    // FIXME: make confirmation optional, with option of asking only if via tray
    int confirmation = QMessageBox::question(this,
                                             tr("Quit?") + " - AkariXB",
                                             tr("Are you sure?")
                                             + "<br />",
                                             tr("Yes, quit AkariXB"), tr("No"),
                                             QString(), 1, 1);
    if (confirmation == 1)
    {
        return;
    }


    qApp->setQuitOnLastWindowClosed(true);

    std::cout << "\nShutting down AkariXB (" + m_userJid.toStdString()
                 + ")...\n";
    std::cout.flush();

    m_xmppClient->disconnectFromServer();
    qApp->closeAllWindows();

    std::cout << "All windows closed, bye! o/\n";
    std::cout.flush();

    qApp->quit();
}


//////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// PROTECTED ////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


void MainWindow::resizeEvent(QResizeEvent *event)
{
    // FIXME, basic hardcoded adjustment to icons for smaller window sizes
    if (this->width() > 1024 && this->height() > 700)
    {
        m_categoriesList->setIconSize(QSize(48, 48));
    }
    else
    {
        m_categoriesList->setIconSize(QSize(22, 22));
    }

    event->accept();
}


void MainWindow::closeEvent(QCloseEvent *event)
{
    qDebug() << "Trying to close window; hiding instead";
    this->toggleMainWindow(); // FIXME: only if tray icon was available
                              // Also, handle environment shutdown
    event->ignore();
}
