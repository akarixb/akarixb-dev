/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "commandmodule.h"


CommandModule::CommandModule(CommandHandler *commandHandler,
                             GlobalObject *globalObject,
                             QWidget *parent) : QWidget(parent)
{
    m_globalObject = globalObject;
    m_commandHandler = commandHandler;

    m_variableParser = m_globalObject->getVariableParser();


    m_dataFile = new DataFile(m_globalObject->getDataDirectory()
                              + QStringLiteral("/commands.axb"),
                              this);

    // Left side
    m_syncCommandsButton = new QPushButton(QIcon::fromTheme("document-save",
                                                            QIcon(":/images/button-save.png")),
                                           tr("&Sync Commands"),
                                           this);
    connect(m_syncCommandsButton, &QAbstractButton::clicked,
            this, &CommandModule::syncCommands);



    m_commandListWidget = new QListWidget(this);
    m_commandListWidget->setDragDropMode(QAbstractItemView::InternalMove); // Movable items
    connect(m_commandListWidget, &QListWidget::currentRowChanged,
            this, &CommandModule::onCommandSelected);


    m_addCommandButton = new QPushButton(QIcon::fromTheme("list-add",
                                                          QIcon(":/images/list-add.png")),
                                         tr("&Add Command"),
                                         this);
    connect(m_addCommandButton, &QAbstractButton::clicked,
            this, &CommandModule::addCommand);

    m_removeCommandButton = new QPushButton(QIcon::fromTheme("list-remove",
                                                             QIcon(":/images/list-remove.png")),
                                            tr("&Remove Command"),
                                            this);
    connect(m_removeCommandButton, &QAbstractButton::clicked,
            this, &CommandModule::removeCommand);



    // Right side
    m_triggerLineEdit = new QLineEdit(this);
    m_triggerLineEdit->setPlaceholderText(tr("The name, without the prefix "
                                             "or any spaces",
                                             "Referring to the command's name/trigger"));
    connect(m_triggerLineEdit, &QLineEdit::textChanged,
            this, &CommandModule::onTriggerChanged);

    m_helpLineEdit = new QLineEdit(this);
    m_helpLineEdit->setPlaceholderText(tr("Explanation shown to users with "
                                          "the %1 command")
                                       .arg(QStringLiteral("!help"))); // FIXME, hardcoded

    m_accessLevelSpinbox = new QSpinBox(this);
    m_accessLevelSpinbox->setRange(0, 100);
    m_accessLevelSpinbox->setSpecialValueText(tr("Anyone can use it",
                                                 "'it' refers to a command"));

    m_typeCombobox = new QComboBox(this);
    m_typeCombobox->addItem(tr("Static Message"),
                            tr("Just enter a single sentence."));
    m_typeCombobox->addItem(tr("Random Sentence"),
                            tr("Add as many sentences as you want.\n"
                               "One of them will be selected at random as "
                               "a reply."));
    m_typeCombobox->addItem(tr("Random Line from Text File") + " (UTF-8)",
                            tr("A random line from the chosen file will be "
                               "used as a reply.\n"
                               "File encoding should be UTF-8."));
    m_typeCombobox->addItem(tr("Keyword-based Message"),
                            tr("Add keywords to the list, then enter the "
                               "corresponding definition for each keyword on "
                               "the other side."));
    m_typeCombobox->addItem(tr("Run Program"),
                            tr("Enter a system command line to be run, with "
                               "any needed parameters.\n"
                               "The reply will be the console output of the "
                               "program, unless you specify a reply pattern, "
                               "and don't include the %output% variable "
                               "somewhere in it.",
                               "Don't translate %output%!")
                            + "\n\n"
                            + tr("If the program you launch will take a long "
                                 "time to finish (like a GUI), you need to "
                                 "select the option to launch it independently, "
                                 "but then it will not be possible to use the "
                                 "program output as the reply for the command."));
    m_typeCombobox->addItem(tr("Alias for another command"),
                            tr("Enter the original command that should be run "
                               "when this alias is used."));

    QFont helpFont;
    helpFont.setItalic(true);

    m_typeExplanationLabel = new QLabel(this);
    m_typeExplanationLabel->setFont(helpFont);
    m_typeExplanationLabel->setWordWrap(true);
    m_typeExplanationLabel->setContentsMargins(16, 1, 16, 1);
    m_typeExplanationLabel->setSizePolicy(QSizePolicy::MinimumExpanding,
                                          QSizePolicy::Minimum);

    connect(m_typeCombobox, SIGNAL(currentIndexChanged(int)), // Kept old-style
            this, SLOT(onTypeChanged(int)));                  // due to overload


    m_staticLineEdit = new QLineEdit(this);

    m_randomListWidget = new StringListWidget(StringListWidget::StringsWithTags,
                                              this);
    m_randomListWidget->setCheckSyntaxEnabled(m_variableParser,
                                              QStringList{"%user%", "%params%"});

    m_filenameLabel = new QLabel(this);
    QFont filenameFont = m_filenameLabel->font();
    filenameFont.setBold(true);
    m_filenameLabel->setFont(filenameFont);
    m_filenameButton = new QPushButton(QIcon::fromTheme("document-open",
                                                        QIcon(":/images/button-edit.png")),
                                       tr("Select File..."),
                                       this);
    connect(m_filenameButton, &QAbstractButton::clicked,
            this, &CommandModule::findFile);
    m_filenameLayout = new QHBoxLayout();
    m_filenameLayout->addWidget(m_filenameLabel,  1);
    m_filenameLayout->addWidget(m_filenameButton, 0);


    m_keywordListWidget = new StringListWidget(StringListWidget::KeysWithValues,
                                               this);
    m_keywordListWidget->setLowercaseItems(true);
    m_keywordListWidget->setCheckSyntaxEnabled(m_variableParser,
                                               QStringList{"%user%", "%params%"});


    m_programLineEdit = new QLineEdit(this);
    m_runDetachedCombobox = new QComboBox(this);
    m_runDetachedCombobox->addItem(tr("Wait for the program to finish"));
    m_runDetachedCombobox->addItem(tr("Launch the program independently"));
    m_programLayout = new QHBoxLayout();
    m_programLayout->addWidget(m_programLineEdit);
    m_programLayout->addWidget(m_runDetachedCombobox);


    m_aliasLineEdit = new QLineEdit(this);



    m_outputLineEdit = new QLineEdit(this);
    m_outputLineEdit->setPlaceholderText(tr("Set an optional reply pattern"));

    m_outputPatternHelpLabel = new QLabel(
      tr("With a reply pattern you can make any possible reply to a command "
         "fit into a predefined structure, like having a specific symbol at "
         "the start or the end of the reply, or always start the reply with "
         "a fixed word.")
      + "<br />"
      + tr("You can insert variables like %user%, %params%, %output%, %line% "
           "and %smiley%.",
           "Do not translate the words between % symbols")
      + " "
      + tr("Usually, you will want to have the %output% variable somewhere in "
           "the pattern.",
           "Do not translate %output%")
      + "<br />"
      + tr("If you don't set a reply pattern, replies in chatrooms will "
           "use the default pattern, %user%: %output%.",
           "Do not translate %user% or %output%")
      + "<br /><br />"
      + tr("Example: [RESULT] > %output%",
           "Do not translate %output%"),
      this);
    m_outputPatternHelpLabel->setFont(helpFont);
    m_outputPatternHelpLabel->setWordWrap(true);
    m_outputPatternHelpLabel->setContentsMargins(16, 1, 16, 1);
    m_outputPatternHelpLabel->setSizePolicy(QSizePolicy::MinimumExpanding,
                                            QSizePolicy::Minimum);

    m_privateReplyCheckbox = new QCheckBox(this);



    m_saveCommandButton = new QPushButton(QIcon::fromTheme("document-save",
                                                           QIcon(":/images/button-save.png")),
                                          tr("Save &Command"),
                                          this);
    connect(m_saveCommandButton, &QAbstractButton::clicked,
            this, &CommandModule::saveCommand);


    // Layout
    m_leftLayout = new QVBoxLayout();
    m_leftLayout->addWidget(m_syncCommandsButton);
    m_leftLayout->addWidget(m_commandListWidget);
    m_leftLayout->addWidget(m_addCommandButton);
    m_leftLayout->addWidget(m_removeCommandButton);

    // These go in a sublayout to avoid spacing issues between them
    m_typeLayout = new QVBoxLayout();
    m_typeLayout->setContentsMargins(0, 0, 0, 0);
    m_typeLayout->setSpacing(0);
    m_typeLayout->addWidget(m_staticLineEdit);
    m_typeLayout->addWidget(m_randomListWidget);
    m_typeLayout->addLayout(m_filenameLayout);
    m_typeLayout->addWidget(m_keywordListWidget);
    m_typeLayout->addLayout(m_programLayout);
    m_typeLayout->addWidget(m_aliasLineEdit);


    m_formLayout = new QFormLayout();
    m_formLayout->addRow(tr("Command"),         m_triggerLineEdit);
    m_formLayout->addRow(tr("Help Text"),       m_helpLineEdit);
    m_formLayout->addRow(tr("Access Level"),    m_accessLevelSpinbox);

    m_formLayout->addRow(tr("Type"),            m_typeCombobox);
    m_formLayout->addWidget(m_typeExplanationLabel);
    m_formLayout->addRow(QString(),             m_typeLayout);

    m_formLayout->addRow(tr("Reply Pattern"),   m_outputLineEdit);
    m_formLayout->addWidget(m_outputPatternHelpLabel);
    m_formLayout->addRow(tr("Private Reply"),   m_privateReplyCheckbox);

    m_rightWidget = new QWidget(this);
    m_rightWidget->setLayout(m_formLayout);

    m_rightScrollArea = new QScrollArea(this);
    m_rightScrollArea->setContentsMargins(1, 1, 1, 1);
    m_rightScrollArea->setFrameStyle(QFrame::Box | QFrame::Raised);
    m_rightScrollArea->setWidgetResizable(true);
    m_rightScrollArea->setWidget(m_rightWidget);

    m_rightLayout = new QVBoxLayout();
    m_rightLayout->addWidget(m_rightScrollArea);
    m_rightLayout->addWidget(m_saveCommandButton, 0, Qt::AlignRight);

    m_mainLayout = new QHBoxLayout();
    m_mainLayout->setContentsMargins(0, 0, 0, 0);
    m_mainLayout->addLayout(m_leftLayout,  1);
    m_mainLayout->addLayout(m_rightLayout, 5);
    this->setLayout(m_mainLayout);


    this->onTypeChanged(0); // Select first type to show its extra widget
    m_removeCommandButton->setDisabled(true);
    m_rightWidget->setDisabled(true);
    m_saveCommandButton->setDisabled(true);


    this->loadCommands();


    qDebug() << "CommandModule created";
}


CommandModule::~CommandModule()
{
    qDebug() << "CommandModule destroyed";
}




/*
 * Load commands from disk
 *
 */
void CommandModule::loadCommands()
{
    const QVariantList commandDataList = m_dataFile->loadData();

    for (const QVariant listItem : commandDataList)
    {
        const QVariantMap map = listItem.toMap();
        QListWidgetItem *item = new QListWidgetItem();

        const int commandType = map.value("type").toInt();

        item->setText(map.value("trigger").toString());
        item->setData(Qt::UserRole,     map.value("trigger"));
        item->setData(Qt::UserRole + 1, map.value("helpText"));
        item->setData(Qt::UserRole + 2, map.value("accessLevel"));
        item->setData(Qt::UserRole + 3, commandType);

        if (commandType == 0)
        {
            item->setData(Qt::UserRole + 4,
                          map.value("staticReply"));
        }
        else if (commandType == 1)
        {
            item->setData(Qt::UserRole + 4,
                          map.value("randomSentence"));
        }
        else if (commandType == 2)
        {
            item->setData(Qt::UserRole + 4,
                          map.value("filename"));
        }
        else if (commandType == 3)
        {
            item->setData(Qt::UserRole + 4,
                          map.value("keywords"));
        }
        else if (commandType == 4)
        {
            item->setData(Qt::UserRole + 4,
                          map.value("programToRun"));
        }
        else if (commandType == 5)
        {
            item->setData(Qt::UserRole + 4,
                          map.value("aliasFor"));
        }

        item->setData(Qt::UserRole + 5, map.value("outputPattern"));
        item->setData(Qt::UserRole + 6, map.value("privateReply"));

        m_commandListWidget->addItem(item);
    }

    m_commandHandler->setCommandData(commandDataList);

    m_globalObject->addToLog(tr("Loaded %Ln command(s).",
                                "",
                                commandDataList.length()));
}



bool CommandModule::validateCommand(int commandType)
{
    QString errorMessage;

    const QString commandTrigger = m_triggerLineEdit->text().trimmed();
    const QString outputPattern = m_outputLineEdit->text().trimmed();

    if (commandTrigger.contains(QStringLiteral(" "))) // FIXME, regexp
    {
        errorMessage = tr("The command name can't contain spaces.");
    }
    else if (commandType == 0)
    {
        const QString staticReplyText = m_staticLineEdit->text().trimmed();
        if (staticReplyText.isEmpty())
        {
            errorMessage = tr("Static reply is empty.");
            m_staticLineEdit->setFocus();
        }
        else if (!m_variableParser->checkSyntax(staticReplyText,
                                                QStringList{"%user%",
                                                            "%params%"}))
        {
            errorMessage = m_variableParser->getLastCheckError();
            m_staticLineEdit->setFocus();
        }
    }
    else if (commandType == 1)
    {
        if (m_randomListWidget->getStringsFromList().length() == 0)
        {
            errorMessage = tr("There are no sentences in the list.");
            m_randomListWidget->setFocus();
        }
    }
    else if (commandType == 2)
    {
        if (m_textFilename.isEmpty())
        {
            errorMessage = tr("There is no file selected.");
            m_filenameButton->setFocus();
        }
    }
    else if (commandType == 3)
    {
        if (m_keywordListWidget->getStringsFromList().length() == 0)
        {
            errorMessage = tr("There are no keywords in the list.");
            m_keywordListWidget->setFocus();
        }

        // TODO FIXME: Check that each keyword has content (in StringListWidget)
    }
    else if (commandType == 4)
    {
        if (m_programLineEdit->text().trimmed().isEmpty())
        {
            errorMessage = tr("There is no program specified.");
            m_programLineEdit->setFocus();
        }
    }
    else if (commandType == 5)
    {
        if (m_aliasLineEdit->text().trimmed().isEmpty())
        {
            errorMessage = tr("You need to enter the command this alias "
                              "will execute.");
            m_aliasLineEdit->setFocus();
        }
        else if (m_aliasLineEdit->text().trimmed() == commandTrigger)
        {
            errorMessage = tr("A command can't be an alias of itself.");
            m_aliasLineEdit->setFocus();
        }
    }


    // Previous fields OK, and have some output pattern that needs checking too
    if (errorMessage.isEmpty() && !outputPattern.isEmpty())
    {   // FIXME: can't rely on empty errorMessage
        if (!m_variableParser->checkSyntax(outputPattern,
                                           QStringList{"%output%", "%user%",
                                                       "%params%"}))
        {
            errorMessage = m_variableParser->getLastCheckError();
            m_outputLineEdit->setFocus();
        }
    }


    if (!errorMessage.isEmpty()) // FIXME: can't rely on that
    {
        QMessageBox::warning(this,
                             tr("Error validating fields") +  " - AkariXB",
                             "\n"
                             + errorMessage
                             + "\t\t\t\t\t\n"); // Ensure some width
        return false;
    }

    return true;
}



//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// SLOTS //////////////////////////////////
//////////////////////////////////////////////////////////////////////////////




void CommandModule::onTriggerChanged(QString newTrigger)
{
    m_saveCommandButton->setDisabled(newTrigger.trimmed().isEmpty());
}


void CommandModule::findFile()
{
    QString filename = QFileDialog::getOpenFileName(this,
                                                    tr("Select a text file"),
                                                    QDir::homePath(),
                                                    "*");
    if (!QFile(filename).exists())
    {
        filename.clear();
        m_filenameLabel->setText(tr("Selected file does not exist."));
    }

    if (!filename.isEmpty())
    {
        m_textFilename = filename;
        m_filenameLabel->setText(filename);
    }
}



void CommandModule::addCommand()
{
    QListWidgetItem *item = new QListWidgetItem("-- "
                                                + tr("New Command")
                                                + " --");
    m_commandListWidget->addItem(item);

    const int lastRow = m_commandListWidget->count() - 1;
    m_commandListWidget->setCurrentRow(lastRow);
    m_commandListWidget->setCurrentItem(item);
    item->setSelected(true);

    m_saveCommandButton->setDisabled(true); // Until something's entered
    m_triggerLineEdit->setFocus();
}


void CommandModule::removeCommand()
{
    const int row = m_commandListWidget->currentRow();
    if (row != -1)
    {
        QListWidgetItem *item = m_commandListWidget->takeItem(row);
        delete item;
    }

    if (m_commandListWidget->count() == 0) // No commands left
    {
        m_removeCommandButton->setDisabled(true);
        m_rightWidget->setDisabled(true);
        m_saveCommandButton->setDisabled(true);
    }
}


void CommandModule::onCommandSelected(int row)
{
    if (row < 0) // Small protection
    {
        return;
    }

    QListWidgetItem *item = m_commandListWidget->item(row);

    m_triggerLineEdit->setText(item->data(Qt::UserRole).toString());
    m_helpLineEdit->setText(item->data(Qt::UserRole + 1).toString());
    m_accessLevelSpinbox->setValue(item->data(Qt::UserRole + 2).toInt());
    m_typeCombobox->setCurrentIndex(item->data(Qt::UserRole + 3).toInt());

    m_staticLineEdit->clear();
    m_randomListWidget->clearList();
    m_filenameLabel->setText("[" + tr("No file selected") + "]");
    m_textFilename.clear();
    m_keywordListWidget->clearList();
    m_programLineEdit->clear();
    m_runDetachedCombobox->setCurrentIndex(0);
    m_aliasLineEdit->clear();


    const int commandType = m_typeCombobox->currentIndex();
    if (commandType == 0)
    {
        m_staticLineEdit->setText(item->data(Qt::UserRole + 4).toString());
    }
    else if (commandType == 1)
    {
        const QStringList stringList = item->data(Qt::UserRole + 4)
                                            .toStringList();
        m_randomListWidget->addStringsToList(stringList);
    }
    else if (commandType == 2)
    {
        m_textFilename = item->data(Qt::UserRole + 4).toString();
        m_filenameLabel->setText(m_textFilename);
    }
    else if (commandType == 3)
    {
        const QVariantMap stringMap = item->data(Qt::UserRole + 4).toMap();
        m_keywordListWidget->addItemsToList(stringMap);
    }
    else if (commandType == 4)
    {
        const QVariantMap programMap = item->data(Qt::UserRole + 4).toMap();
        m_programLineEdit->setText(programMap.value("commandLine")
                                             .toString());
        m_runDetachedCombobox->setCurrentIndex(programMap.value("detached")
                                                         .toInt());
    }
    else if (commandType == 5)
    {
        m_aliasLineEdit->setText(item->data(Qt::UserRole + 4).toString());
    }


    m_outputLineEdit->setText(item->data(Qt::UserRole + 5).toString());
    m_privateReplyCheckbox->setChecked(item->data(Qt::UserRole + 6).toBool());


    m_removeCommandButton->setEnabled(true);
    m_rightWidget->setEnabled(true);
}


void CommandModule::onTypeChanged(int commandType)
{
    // Show the informational text for this command type
    m_typeExplanationLabel->setText(m_typeCombobox->itemData(commandType)
                                                   .toString());

    m_staticLineEdit->hide();
    m_randomListWidget->hide();
    m_filenameLabel->hide();
    m_filenameButton->hide();
    m_keywordListWidget->hide();
    m_programLineEdit->hide();
    m_runDetachedCombobox->hide();
    m_aliasLineEdit->hide();


    if (commandType == 0)
    {
        m_staticLineEdit->show();
    }
    else if (commandType == 1)
    {
        m_randomListWidget->show();
    }
    else if (commandType == 2)
    {
        m_filenameLabel->show();
        m_filenameButton->show();
    }
    else if (commandType == 3)
    {
        m_keywordListWidget->show();
    }
    else if (commandType == 4)
    {
        m_programLineEdit->show();
        m_runDetachedCombobox->show();
    }
    else if (commandType == 5)
    {
        m_aliasLineEdit->show();
    }
}


void CommandModule::saveCommand()
{
    const QString newTrigger = m_triggerLineEdit->text().trimmed();
    const int commandType = m_typeCombobox->currentIndex();

    // Check for duplicates
    for (int counter = 0; counter < m_commandListWidget->count(); ++counter)
    {
        QListWidgetItem *commandItem = m_commandListWidget->item(counter);
        if (commandItem->data(Qt::UserRole).toString() == newTrigger)
        {
            if (commandItem != m_commandListWidget->currentItem())
            {
                QMessageBox::warning(this,
                                     tr("Command exists"),
                                     tr("Error: There is already a command "
                                        "in the list with this name."));
                m_triggerLineEdit->setFocus();

                return;
            }
        }
    }


    // Check minimum required fields are filled
    if (!validateCommand(commandType))
    {
        return;
    }

    QListWidgetItem *item = m_commandListWidget->currentItem();

    m_triggerLineEdit->setText(newTrigger); // Show it as it will be stored
    item->setText(newTrigger);
    item->setData(Qt::UserRole,     newTrigger);
    item->setData(Qt::UserRole + 1, m_helpLineEdit->text().trimmed());
    item->setData(Qt::UserRole + 2, m_accessLevelSpinbox->value());
    item->setData(Qt::UserRole + 3, commandType);


    if (commandType == 0)
    {
        item->setData(Qt::UserRole + 4,
                      m_staticLineEdit->text().trimmed());
    }
    else if (commandType == 1)
    {
        item->setData(Qt::UserRole + 4,
                      m_randomListWidget->getStringsFromList());
    }
    else if (commandType == 2)
    {
        item->setData(Qt::UserRole + 4,
                      m_textFilename);
    }
    else if (commandType == 3)
    {
        item->setData(Qt::UserRole + 4,
                      m_keywordListWidget->getItemsFromList());
    }
    else if (commandType == 4)
    {
        QVariantMap programMap;
        programMap.insert("commandLine", m_programLineEdit->text().trimmed());
        programMap.insert("detached", m_runDetachedCombobox->currentIndex());

        item->setData(Qt::UserRole + 4, programMap);
    }
    else if (commandType == 5)
    {
        item->setData(Qt::UserRole + 4,
                      m_aliasLineEdit->text().trimmed());
    }

    item->setData(Qt::UserRole + 5, m_outputLineEdit->text().trimmed());
    item->setData(Qt::UserRole + 6, m_privateReplyCheckbox->isChecked());


    m_commandListWidget->setFocus();
}


/*
 * Synchronize commands with the CommandHandler,
 * and also save them to disk
 *
 */
void CommandModule::syncCommands()
{
    QVariantList commandDataList;

    for (int counter = 0; counter < m_commandListWidget->count(); ++counter)
    {
        QListWidgetItem *item = m_commandListWidget->item(counter);
        const int commandType = item->data(Qt::UserRole + 3).toInt();
        const QString commandTrigger = item->data(Qt::UserRole).toString()
                                                               .trimmed();

        QVariantMap commandMap;
        commandMap.insert("trigger",     commandTrigger);
        commandMap.insert("helpText",    item->data(Qt::UserRole + 1));
        commandMap.insert("accessLevel", item->data(Qt::UserRole + 2));
        commandMap.insert("type",        commandType);

        if (commandType == 0) // Static reply
        {
            commandMap.insert("staticReply", item->data(Qt::UserRole + 4));
        }
        else if (commandType == 1) // List of strings, to choose one at random
        {
            commandMap.insert("randomSentence", item->data(Qt::UserRole + 4));
        }
        else if (commandType == 2) // A text file, to choose a line at random
        {
            commandMap.insert("filename", item->data(Qt::UserRole + 4));
        }
        else if (commandType == 3) // A list of key:value pairs
        {
            commandMap.insert("keywords", item->data(Qt::UserRole + 4));
        }
        else if (commandType == 4) // A program to run
        {
            commandMap.insert("programToRun", item->data(Qt::UserRole + 4));
        }
        else if (commandType == 5) // Act as an alias for another command
        {
            commandMap.insert("aliasFor", item->data(Qt::UserRole + 4));
        }

        commandMap.insert("outputPattern", item->data(Qt::UserRole + 5));
        commandMap.insert("privateReply",  item->data(Qt::UserRole + 6));

        // Avoid the possibility of saving empty commands
        if (!commandTrigger.isEmpty())
        {
            commandDataList.append(commandMap);
        }
    }


    m_commandHandler->setCommandData(commandDataList);
    m_dataFile->saveData(commandDataList);

    m_globalObject->addToLog(tr("Saved %Ln command(s).",
                                "",
                                commandDataList.length()),
                             true,
                             GlobalObject::LogGood);
}

