/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "datafile.h"


DataFile::DataFile(QString filename, QObject *parent) : QObject(parent)
{
    m_file = new QFile(filename);
    if (!m_file->exists())
    {
        // Initialize empty file
        m_file->open(QIODevice::WriteOnly);
        m_file->write(QByteArray());
        m_file->close();
    }


    qDebug() << "DataFile created for " << filename;
}


DataFile::~DataFile()
{
    qDebug() << "DataFile destroyed for " << m_file->fileName();
}


/*
 * Read command data from disk in JSON format.
 *
 */
QVariantList DataFile::loadData()
{
    QVariantList commandDataList;
    bool parsedOk = false;

    m_file->open(QIODevice::ReadOnly);

    const QByteArray rawData = m_file->readAll();
    m_file->close();

    QJsonDocument jsonDocument;
    jsonDocument = QJsonDocument::fromJson(rawData);
    commandDataList = jsonDocument.toVariant().toList();
    parsedOk = !jsonDocument.isNull();

    if (!parsedOk)
    {
        // TMP FIXME
        commandDataList.clear();
    }

    return commandDataList;
}


/*
 * Save command data to disk in JSON format.
 *
 */
bool DataFile::saveData(QVariantList list)
{
    QByteArray commandData;

    QJsonDocument jsonDocument;
    jsonDocument = QJsonDocument::fromVariant(list);
    commandData = jsonDocument.toJson(QJsonDocument::Indented);


    m_file->open(QIODevice::WriteOnly);
    const int written = m_file->write(commandData);
    m_file->close();

    if (written < 1) // 0 bytes, or -1 for Error
    {
        return false;
    }

    return true;
}

