/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "roommodule.h"


RoomModule::RoomModule(GlobalObject *globalObject,
                       QWidget *parent) : QWidget(parent)
{
    m_globalObject = globalObject;

    m_xmppClient = m_globalObject->getXmppClient();
    m_mucManager = m_globalObject->getMucManager();


    m_dataFile = new DataFile(m_globalObject->getDataDirectory()
                              + QStringLiteral("/rooms.axb"),
                              this);

    // See if we can use the nice "key" symbol, or a more common fallback
    QFontMetrics fontMetrics(this->font());
    if (fontMetrics.inFont(QChar(0x1F511)))
    {
        // KEY
        m_keySymbolString = QString::fromUtf8("\360\237\224\221");
    }
    else
    {
        // BLACK DIAMOND SUIT as fallback (or BLACK LARGE CIRCLE)
        m_keySymbolString = QString::fromUtf8("\342\231\246");
    }

    // List of configured rooms
    m_roomTableWidget = new QTableWidget(this);
    m_roomTableWidget->setDragDropMode(QTableWidget::NoDragDrop);
    m_roomTableWidget->setEditTriggers(QTableWidget::NoEditTriggers);
    m_roomTableWidget->setTabKeyNavigation(false);
    m_roomTableWidget->setShowGrid(false);
    m_roomTableWidget->setSelectionMode(QTableWidget::SingleSelection);
    m_roomTableWidget->setSelectionBehavior(QTableWidget::SelectRows);
    m_roomTableWidget->setColumnCount(5);
    m_roomTableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    m_roomTableWidget->horizontalHeader()->setSectionsMovable(false);
    m_roomTableWidget->horizontalHeader()->setSectionsClickable(false);
    m_roomTableWidget->setHorizontalHeaderLabels(QStringList{tr("Room address"),
                                                             tr("Nickname"),
                                                             tr("Password"),
                                                             tr("Autojoin"),
                                                             tr("Allowed",
                                                                "Column header for "
                                                                "features allowed")});
    m_roomTableWidget->verticalHeader()->hide();
    m_roomTableWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);///////// FIXME TMP
    m_roomTableWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    connect(m_roomTableWidget, &QTableWidget::currentCellChanged,
            this, &RoomModule::showRoomDetails); // Only first param is used
    connect(m_roomTableWidget, &QTableWidget::itemDoubleClicked,
            this, &RoomModule::onRoomDoubleClicked);


    // Buttons to manage the rooms
    m_addRoomButton = new QPushButton(QIcon::fromTheme("list-add",
                                                       QIcon(":/images/list-add.png")),
                                      tr("Add &New Room"),
                                      this);
    m_addRoomButton->setFlat(true);
    connect(m_addRoomButton, &QAbstractButton::clicked,
            this, &RoomModule::addRoom);

    m_roomCountLabel = new QLabel(this);
    QFont roomCountFont;
    roomCountFont.setPointSize(m_roomCountLabel->font().pointSize() - 2);
    m_roomCountLabel->setFont(roomCountFont);
    m_roomCountLabel->setAlignment(Qt::AlignCenter);

    m_removeRoomButton = new QPushButton(QIcon::fromTheme("list-remove",
                                                          QIcon(":/images/list-remove.png")),
                                         tr("&Remove Room"),
                                         this);
    m_removeRoomButton->setFlat(true);
    m_removeRoomButton->setDisabled(true);
    connect(m_removeRoomButton, &QAbstractButton::clicked,
            this, &RoomModule::removeRoom);


    m_buttonsLayout = new QHBoxLayout();
    m_buttonsLayout->addWidget(m_addRoomButton);
    m_buttonsLayout->addWidget(m_roomCountLabel, 1);
    m_buttonsLayout->addWidget(m_removeRoomButton);


    //// Room details
    //// First row, main values
    m_roomJidLineEdit = new QLineEdit(this);
    m_roomJidLineEdit->setPlaceholderText(tr("Room address"));

    m_nickLineEdit = new QLineEdit(this);
    m_nickLineEdit->setPlaceholderText(tr("Nickname"));
    connect(m_roomJidLineEdit, SIGNAL(returnPressed()), // Leaving old-style
            m_nickLineEdit, SLOT(setFocus()));          // connect() due to overload

    m_roomPasswordLineEdit = new QLineEdit(this);
    m_roomPasswordLineEdit->setPlaceholderText(tr("Password, if needed"));
    m_roomPasswordLineEdit->setEchoMode(QLineEdit::Password);
    connect(m_nickLineEdit, SIGNAL(returnPressed()),   // Same as previous connect()
            m_roomPasswordLineEdit, SLOT(setFocus()));

    m_autojoinCheckbox = new QCheckBox(tr("Autojoin"), this);


    // Edit
    m_editRoomButton = new QPushButton(QIcon::fromTheme("document-edit",
                                                        QIcon(":/images/button-edit.png")),
                                       tr("&Edit"),
                                       this);
    m_editRoomButton->setDisabled(true);
    connect(m_editRoomButton, &QAbstractButton::clicked,
            this, &RoomModule::editRoom);


    // Join / Leave
    m_joinButton = new QPushButton(QIcon::fromTheme("list-add",
                                                    QIcon(":/images/list-add.png")),
                                   tr("Join"),
                                   this);
    m_joinButton->hide();
    connect(m_joinButton, &QAbstractButton::clicked,
            this, &RoomModule::joinSelectedRoom);

    m_leaveButton = new QPushButton(QIcon::fromTheme("list-remove",
                                                     QIcon(":/images/list-remove.png")),
                                    tr("Leave"),
                                    this);
    m_leaveButton->hide();
    connect(m_leaveButton, &QAbstractButton::clicked,
            this, &RoomModule::leaveSelectedRoom);


    //// Second row, how to behave in each room
    m_allowedToLabel = new QLabel(tr("Allowed to:"), this);
    m_allowCommandsCheckbox = new QCheckBox(tr("Reply to commands"), this);
    m_allowGreetingsCheckbox = new QCheckBox(tr("Greet people"), this);
    m_allowCasualSpeakCheckbox = new QCheckBox(tr("Speak casually",
                                                  "Casually as in, from time to time"),
                                               this);
    m_allowConversingCheckbox = new QCheckBox(tr("Converse"), this);

    m_greetingsSpinbox = new QSpinBox(this);
    m_greetingsSpinbox->setSuffix(QStringLiteral(" %"));
    m_greetingsSpinbox->setRange(1, 100);
    m_greetingsSpinbox->setDisabled(true);
    connect(m_allowGreetingsCheckbox, &QCheckBox::toggled,
            m_greetingsSpinbox, &QSpinBox::setEnabled);
    m_casualSpeakSpinbox = new QSpinBox(this);
    m_casualSpeakSpinbox->setSuffix(QStringLiteral(" %"));
    m_casualSpeakSpinbox->setRange(1, 100);
    m_casualSpeakSpinbox->setDisabled(true);
    connect(m_allowCasualSpeakCheckbox, &QCheckBox::toggled,
            m_casualSpeakSpinbox, &QSpinBox::setEnabled);


    // Additional row, Update/Cancel buttons
    m_updateRoomButton = new QPushButton(QIcon::fromTheme("view-refresh",
                                                          QIcon(":/images/button-refresh.png")),
                                         tr("&Update"),
                                         this);
    connect(m_updateRoomButton, &QAbstractButton::clicked,
            this, &RoomModule::updateRoom);
    connect(m_roomPasswordLineEdit, SIGNAL(returnPressed()), // Using old-style
            m_updateRoomButton, SLOT(setFocus()));           // connect() due to overload

    m_cancelButton = new QPushButton(QIcon::fromTheme("dialog-cancel",
                                                      QIcon(":/images/button-cancel.png")),
                                     tr("Cancel"),
                                     this);
    connect(m_cancelButton, &QPushButton::clicked,
            this, &RoomModule::cancelEditMode);

    // Together in environment-aware layout
    m_updateCancelButtonBox = new QDialogButtonBox(this);
    m_updateCancelButtonBox->addButton(m_updateRoomButton,
                                       QDialogButtonBox::AcceptRole);
    m_updateCancelButtonBox->addButton(m_cancelButton,
                                       QDialogButtonBox::RejectRole);


    // Stacked widget to hold ChatWidgets
    m_firstPageLabel = new QLabel("<big>"
                                  + tr("Select a room from the list above")
                                  + "</big>",
                                  this);
    m_firstPageLabel->setAlignment(Qt::AlignCenter);

    m_chatStackedWidget = new QStackedWidget(this);
    m_chatStackedWidget->addWidget(m_firstPageLabel);


    // Keyboard shortcuts to go to first/last items with Home/End keys, like basic lists
    m_goFirstAction = new QAction(this);
    m_goFirstAction->setShortcut(Qt::Key_Home);
    m_goFirstAction->setShortcutContext(Qt::WidgetShortcut);
    connect(m_goFirstAction, &QAction::triggered,
            this, &RoomModule::selectFirstRow);
    m_roomTableWidget->addAction(m_goFirstAction);

    m_goLastAction = new QAction(this);
    m_goLastAction->setShortcut(Qt::Key_End);
    m_goLastAction->setShortcutContext(Qt::WidgetShortcut);
    connect(m_goLastAction, &QAction::triggered,
            this, &RoomModule::selectLastRow);
    m_roomTableWidget->addAction(m_goLastAction);


    // Layout
    m_detailsUpperLayout = new QHBoxLayout();
    m_detailsUpperLayout->setSpacing(2); // Avoid smaller spacing from parent layout
    m_detailsUpperLayout->addWidget(m_roomJidLineEdit,      7);
    m_detailsUpperLayout->addWidget(m_nickLineEdit,         4);
    m_detailsUpperLayout->addWidget(m_roomPasswordLineEdit, 3);
    m_detailsUpperLayout->addWidget(m_autojoinCheckbox);
    m_detailsUpperLayout->addStretch();
    m_detailsUpperLayout->addSpacing(8);
    m_detailsUpperLayout->addWidget(m_editRoomButton);
    m_detailsUpperLayout->addWidget(m_joinButton);
    m_detailsUpperLayout->addWidget(m_leaveButton);

    // Central columns of the extra details layout
    m_greetingsLayout = new QVBoxLayout();
    m_greetingsLayout->setAlignment(Qt::AlignTop);
    m_greetingsLayout->addWidget(m_allowGreetingsCheckbox);
    m_greetingsLayout->addWidget(m_greetingsSpinbox, 0, Qt::AlignCenter);

    m_casualSpeakLayout = new QVBoxLayout();
    m_casualSpeakLayout->setAlignment(Qt::AlignTop);
    m_casualSpeakLayout->addWidget(m_allowCasualSpeakCheckbox);
    m_casualSpeakLayout->addWidget(m_casualSpeakSpinbox, 0, Qt::AlignCenter);

    m_detailsExtraLayout = new QHBoxLayout();
    m_detailsExtraLayout->setAlignment(Qt::AlignCenter);
    m_detailsExtraLayout->addWidget(m_allowedToLabel,          0, Qt::AlignTop);
    m_detailsExtraLayout->addSpacing(16);
    m_detailsExtraLayout->addWidget(m_allowCommandsCheckbox,   0, Qt::AlignTop);
    m_detailsExtraLayout->addLayout(m_greetingsLayout);
    m_detailsExtraLayout->addLayout(m_casualSpeakLayout);
    m_detailsExtraLayout->addWidget(m_allowConversingCheckbox, 0, Qt::AlignTop);

    m_detailsLayout = new QVBoxLayout();
    m_detailsLayout->setSpacing(1);
    m_detailsLayout->addLayout(m_detailsUpperLayout);
    m_detailsLayout->addLayout(m_detailsExtraLayout);
    m_detailsLayout->addWidget(m_updateCancelButtonBox);
    m_detailsLayout->addWidget(m_chatStackedWidget, 1);

    m_detailsGroupbox = new QGroupBox(tr("Room Details"), this);
    m_detailsGroupbox->setLayout(m_detailsLayout);


    m_mainLayout = new QVBoxLayout();
    m_mainLayout->setContentsMargins(0, 0, 0, 0);
    m_mainLayout->setSpacing(1); // Small spacing to help with small resolutions, but
                                 // not 0, which looks horrible with some Qt styles
    m_mainLayout->addWidget(m_roomTableWidget, 1);
    m_mainLayout->addLayout(m_buttonsLayout);
    m_mainLayout->addWidget(m_detailsGroupbox, 3);
    this->setLayout(m_mainLayout);


    this->loadRoomInfo();
    this->toggleRoomDetails(false);

    qDebug() << "RoomModule created";
}


RoomModule::~RoomModule()
{
    qDebug() << "RoomModule destroyed";
}



void RoomModule::loadRoomInfo()
{
    const QVariantList roomDataList = m_dataFile->loadData();
    QStringList configuredRoomsList;

    foreach (const QVariant listItem, roomDataList)
    {
        const QVariantMap map = listItem.toMap();

        const QString roomJid = map.value("jid").toString();
        const QString roomNickname = map.value("nickname").toString();
        const QString roomPassword = QByteArray::fromBase64(map.value("password")
                                                               .toByteArray());
        const bool roomAutojoin = map.value("autojoin").toBool();

        const bool roomAllowCommands = map.value("allowCommands", true)
                                          .toBool();
        const bool roomAllowGreetings = map.value("allowGreetings", false)
                                           .toBool();
        const bool roomAllowCasualSpeak = map.value("allowCasualSpeak", false)
                                             .toBool();
        const bool roomAllowConversing = map.value("allowConversing", false)
                                            .toBool();

        const int newRow = m_roomTableWidget->rowCount();
        m_roomTableWidget->insertRow(newRow);
        QTableWidgetItem *tableItem = new QTableWidgetItem(roomJid);
        tableItem->setIcon(QIcon::fromTheme(QStringLiteral("user-offline"),
                                            QIcon(":/images/button-offline.png")));
        tableItem->setData(Qt::UserRole,     roomJid);
        tableItem->setData(Qt::UserRole + 1, roomNickname);
        tableItem->setData(Qt::UserRole + 2, roomPassword);
        tableItem->setData(Qt::UserRole + 3, roomAutojoin);
        tableItem->setData(Qt::UserRole + 4, false); // Joined status, initially false
        tableItem->setData(Qt::UserRole + 5, roomAllowCommands);
        tableItem->setData(Qt::UserRole + 6, roomAllowGreetings);
        tableItem->setData(Qt::UserRole + 7, roomAllowCasualSpeak);
        tableItem->setData(Qt::UserRole + 8, roomAllowConversing);
        tableItem->setData(Qt::UserRole + 9,  map.value("greetingProb",
                                                        50).toInt());
        tableItem->setData(Qt::UserRole + 10, map.value("casualSpeakProb",
                                                        50).toInt());
        m_roomTableWidget->setItem(newRow, 0, tableItem);

        m_roomTableWidget->setItem(newRow, 1, new QTableWidgetItem());
        m_roomTableWidget->item(newRow, 1)->setText(roomNickname);

        m_roomTableWidget->setItem(newRow, 2, new QTableWidgetItem());
        m_roomTableWidget->item(newRow, 2)->setTextAlignment(Qt::AlignCenter);
        if (!roomPassword.isEmpty())
        {
            m_roomTableWidget->item(newRow, 2) // Key symbol
                             ->setText(m_keySymbolString);
        }

        m_roomTableWidget->setItem(newRow, 3, new QTableWidgetItem());
        m_roomTableWidget->item(newRow, 3)->setTextAlignment(Qt::AlignCenter);
        if (roomAutojoin)
        {
            m_roomTableWidget->item(newRow, 3) // Check mark symbol
                             ->setText(QString::fromUtf8("\342\234\224"));
        }

        m_roomTableWidget->setItem(newRow, 4, new QTableWidgetItem());
        m_roomTableWidget->item(newRow, 4)->setTextAlignment(Qt::AlignCenter);
        const QString allowedFeatures = this->allowedFeaturesString(roomAllowCommands,
                                                                    roomAllowGreetings,
                                                                    roomAllowCasualSpeak,
                                                                    roomAllowConversing);
        m_roomTableWidget->item(newRow, 4)->setText(allowedFeatures);

        configuredRoomsList.append(roomJid);
    }

    // Keep track of the configured rooms, for some checks
    m_globalObject->setConfiguredRoomsList(configuredRoomsList);

    m_roomCountLabel->setText(tr("%Ln room(s)", "", roomDataList.length()));

    m_globalObject->addToLog(tr("Loaded %Ln room(s).",
                                "",
                                roomDataList.length()));
}


void RoomModule::saveRoomInfo()
{
    QVariantList roomDataList;
    QStringList configuredRoomsList;

    for (int counter = 0; counter < m_roomTableWidget->rowCount(); ++counter)
    {
        QTableWidgetItem *item = m_roomTableWidget->item(counter, 0);

        const QString roomJid = item->data(Qt::UserRole).toString().trimmed();
        const QString roomNick = item->data(Qt::UserRole + 1).toString().trimmed();

        // Don't save data for invalid rooms
        if (Helpers::isValidJid(roomJid) && !roomNick.isEmpty())
        {
            QVariantMap roomMap;
            roomMap.insert("jid",        roomJid);
            roomMap.insert("nickname",   roomNick);
            roomMap.insert("password",   item->data(Qt::UserRole + 2).toByteArray()
                                                                     .toBase64());
            roomMap.insert("autojoin",   item->data(Qt::UserRole + 3).toBool());

            // Qt::UserRole + 4 is used for a transient value

            roomMap.insert("allowCommands",    item->data(Qt::UserRole + 5).toBool());
            roomMap.insert("allowGreetings",   item->data(Qt::UserRole + 6).toBool());
            roomMap.insert("allowCasualSpeak", item->data(Qt::UserRole + 7).toBool());
            roomMap.insert("allowConversing",  item->data(Qt::UserRole + 8).toBool());

            roomMap.insert("greetingProb",     item->data(Qt::UserRole + 9).toInt());
            roomMap.insert("casualSpeakProb",  item->data(Qt::UserRole + 10).toInt());


            roomDataList.append(roomMap);

            configuredRoomsList.append(roomJid);
        }
    }

    m_dataFile->saveData(roomDataList);

    m_globalObject->setConfiguredRoomsList(configuredRoomsList);

    m_roomCountLabel->setText(tr("%Ln room(s)", "",
                                 roomDataList.length()));

    m_globalObject->addToLog(tr("Saved %Ln room(s).", "",
                                roomDataList.length()),
                             true,
                             GlobalObject::LogGood);
}


void RoomModule::adjustRoomsTable()
{
    const int oneTenthWidth = m_roomTableWidget->viewport()->width() / 10;

    m_roomTableWidget->setColumnWidth(0, oneTenthWidth * 5);
    m_roomTableWidget->setColumnWidth(1, oneTenthWidth * 2);
    m_roomTableWidget->setColumnWidth(2, oneTenthWidth);
    m_roomTableWidget->setColumnWidth(3, oneTenthWidth);
    m_roomTableWidget->setColumnWidth(4, oneTenthWidth);
}


/*
 * Find out which item from the table matches a given JID
 *
 */
QTableWidgetItem *RoomModule::tableItemFromJid(QString roomJid)
{
    for (int count = 0; count < m_roomTableWidget->rowCount(); ++count)
    {
        QTableWidgetItem *item = m_roomTableWidget->item(count, 0);
        if (item->data(Qt::UserRole).toString() == roomJid)
        {
            return item;
        }
    }

    return nullptr;
}



/*
 * Get the page number in the QStackedWidget that matches
 * the ChatWidget with the given room JID
 *
 */
int RoomModule::stackIndexFromJid(QString roomJid)
{
    qDebug() << "RoomModule::stackIndexFromJid()" << roomJid;

    int page = 1; // Starts with 1, since 0 is an empty page with no chat
    foreach (ChatWidget *chat, m_openedRooms)
    {
        if (chat->getRoomJid() == roomJid)
        {
            return page;
        }

        ++page;
    }

    return 0;
}

/*
 * Build string like **-* to indicate allowed features in a column
 *
 */
QString RoomModule::allowedFeaturesString(bool commands, bool greetings,
                                          bool casualSpeak, bool conversing)
{
    QString featuresString;
    featuresString.append(commands    ? QString::fromUtf8("\342\200\242 ")   // BULLET
                                      : QStringLiteral("_ "));
    featuresString.append(greetings   ? QString::fromUtf8("\342\200\242 ")
                                      : QStringLiteral("_ "));
    featuresString.append(casualSpeak ? QString::fromUtf8("\342\200\242 ")
                                      : QStringLiteral("_ "));
    featuresString.append(conversing  ? QString::fromUtf8("\342\200\242")
                                      : QStringLiteral("_"));

    return featuresString;
}



void RoomModule::joinRoom(QString roomJid, QString nickname, QString password)
{
    QXmppMucRoom *room = m_mucManager->addRoom(roomJid);
    room->setNickName(nickname);
    if (!password.isEmpty())
    {
        room->setPassword(password);
    }

    QTableWidgetItem *item = this->tableItemFromJid(roomJid);
    room->setProperty("allowCommands",    item->data(Qt::UserRole + 5).toBool());
    room->setProperty("allowGreetings",   item->data(Qt::UserRole + 6).toBool());
    room->setProperty("allowCasualSpeak", item->data(Qt::UserRole + 7).toBool());
    room->setProperty("allowConversing",  item->data(Qt::UserRole + 8).toBool());

    room->setProperty("greetingProb",     item->data(Qt::UserRole + 9).toInt());
    room->setProperty("casualSpeakProb",  item->data(Qt::UserRole + 10).toInt());

    room->join();


    const QString message = tr("Joining room %1...")
                            .arg("<b>" + room->jid().toHtmlEscaped() + "</b>");
    m_globalObject->addToLog(message);


    ChatWidget *newChatWidget = new ChatWidget(m_globalObject, this);
    newChatWidget->setRoom(room);
    connect(newChatWidget, &ChatWidget::roomJoined,
            this, &RoomModule::onRoomJoined);
    connect(newChatWidget, &ChatWidget::roomLeft,
            this, &RoomModule::onRoomLeft);


    m_chatStackedWidget->addWidget(newChatWidget);
    m_openedRooms.append(newChatWidget);


    RoomSpeakHandler *newRoomSpeakHandler = new RoomSpeakHandler(m_globalObject,
                                                                 room,
                                                                 this);
    m_roomSpeakHandlers.append(newRoomSpeakHandler);

    m_globalObject->addJoinedRoom(roomJid);
}


void RoomModule::leaveRoom(QString roomJid, QString reason)
{
    QXmppMucRoom *room = m_globalObject->roomFromJid(roomJid);
    if (room != nullptr)
    {
        const QString message = tr("Leaving room %1...")
                                .arg("<b>" + room->jid().toHtmlEscaped() + "</b>");
        m_globalObject->addToLog(message);

        room->leave(reason);
    }
    else
    {
        qDebug() << "Trying to leave: " << roomJid;
        qDebug() << "Room NOT FOUND via globalObject!";
    }

    qDebug() << "RoomModule::leaveRoom() ended" << roomJid;
}



/*
 * Join rooms configured for autojoin
 *
 */
void RoomModule::autojoinRooms()
{
    for (int count = 0; count < m_roomTableWidget->rowCount(); ++count)
    {
        QTableWidgetItem *item = m_roomTableWidget->item(count, 0);

        const QString roomJid = item->data(Qt::UserRole).toString();
        const QString roomNick = item->data(Qt::UserRole + 1).toString();
        const QString roomPass = item->data(Qt::UserRole + 2).toString();

        if (item->data(Qt::UserRole + 3).toBool()               // autojoin=true
         || m_globalObject->getJoinedRooms().contains(roomJid)) // or previously joined
        {
            this->joinRoom(roomJid, roomNick, roomPass);
        }
    }
}


bool RoomModule::newRoomIsValid(bool showErrors)
{
    const QString roomJid = m_roomJidLineEdit->text().trimmed();
    if (!Helpers::isValidJid(roomJid))
    {
        if (showErrors)
        {
            QMessageBox::warning(this,
                                 tr("Error") + QStringLiteral(" - AkariXB"),
                                 tr("Room address is not valid."));
        }

        m_roomJidLineEdit->setFocus();

        return false;
    }

    // Check for more than one room with same JID; that can't be allowed
    for (int row = 0; row < m_roomTableWidget->rowCount(); ++row)
    {
        if (m_roomTableWidget->item(row, 0)
                             ->data(Qt::UserRole).toString() == roomJid)
        {
            if (row != m_roomTableWidget->currentRow())
            {
                if (showErrors)
                {
                    QMessageBox::warning(this,
                                         tr("Error")
                                         + QStringLiteral(" - AkariXB"),
                                         tr("There is already a room in the "
                                            "list with this address, in "
                                            "row %1.").arg(row + 1));
                }

                return false;
            }
        }
    }

    if (m_nickLineEdit->text().trimmed().isEmpty())
    {
        if (showErrors)
        {
            QMessageBox::warning(this,
                                 tr("Error") + QStringLiteral(" - AkariXB"),
                                 tr("Nickname is not valid."));
        }

        m_nickLineEdit->setFocus();

        return false;
    }

    return true;
}


void RoomModule::clearRoomDetails()
{
    m_roomJidLineEdit->clear();
    m_nickLineEdit->clear();
    m_roomPasswordLineEdit->clear();
    m_autojoinCheckbox->setChecked(false);

    m_allowCommandsCheckbox->setChecked(false);
    m_allowGreetingsCheckbox->setChecked(false);
    m_allowCasualSpeakCheckbox->setChecked(false);
    m_allowConversingCheckbox->setChecked(false);
}


void RoomModule::toggleJoinLeaveButtons(QTableWidgetItem *item)
{
    // UserRole+4 contains true if joined
    if (item->data(Qt::UserRole + 4).toBool())
    {
        m_joinButton->hide();
        m_leaveButton->show();
    }
    else
    {
        m_joinButton->show();
        m_leaveButton->hide();
    }
}



//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// SLOTS //////////////////////////////////
//////////////////////////////////////////////////////////////////////////////



void RoomModule::addRoom()
{
    const int newRow = m_roomTableWidget->rowCount();
    m_roomTableWidget->insertRow(newRow);
    QTableWidgetItem *tableItem = new QTableWidgetItem("--- "
                                                       + tr("New Room")
                                                       + " ---");
    m_roomTableWidget->setItem(newRow, 0, tableItem);
    m_roomTableWidget->setItem(newRow, 1, new QTableWidgetItem());
    m_roomTableWidget->setItem(newRow, 2, new QTableWidgetItem());
    m_roomTableWidget->item(newRow, 2)->setTextAlignment(Qt::AlignCenter);
    m_roomTableWidget->setItem(newRow, 3, new QTableWidgetItem());
    m_roomTableWidget->item(newRow, 3)->setTextAlignment(Qt::AlignCenter);
    m_roomTableWidget->setItem(newRow, 4, new QTableWidgetItem());
    m_roomTableWidget->item(newRow, 4)->setTextAlignment(Qt::AlignCenter);

    // Select this new row
    m_roomTableWidget->setCurrentItem(tableItem);
    m_roomTableWidget->scrollToItem(tableItem);

    this->clearRoomDetails();
    m_allowCommandsCheckbox->setChecked(true); // Default new rooms to allow commands

    this->editRoom();
}


/*
 * Enable fields for room editing, and Update button
 *
 */
void RoomModule::editRoom()
{
    this->toggleRoomDetails(true);
}


void RoomModule::updateRoom()
{
    if (m_roomTableWidget->currentRow() < 0)
    {
        return;
    }

    const QString roomJid = m_roomJidLineEdit->text().trimmed();
    const QString roomNickname = m_nickLineEdit->text().trimmed();
    const QString roomPassword = m_roomPasswordLineEdit->text();
    const bool roomAutojoin = m_autojoinCheckbox->isChecked();
    const bool roomAllowCommands = m_allowCommandsCheckbox->isChecked();
    const bool roomAllowGreetings = m_allowGreetingsCheckbox->isChecked();
    const bool roomAllowCasualSpeak = m_allowCasualSpeakCheckbox->isChecked();
    const bool roomAllowConversing = m_allowConversingCheckbox->isChecked();
    const int roomGreetingProb = m_greetingsSpinbox->value();
    const int roomCasualSpeakProb = m_casualSpeakSpinbox->value();

    if (newRoomIsValid(true)) // Showing errors = true
    {
        const int newRow = m_roomTableWidget->currentRow();
        QTableWidgetItem *item = m_roomTableWidget->item(newRow, 0);

        // Icon depends on current joined status
        if (item->data(Qt::UserRole + 4).toBool())
        {
            item->setIcon(QIcon::fromTheme("user-online",
                                           QIcon(":/images/button-online.png")));

            // For online rooms, check if nick needs changing
            if (item->data(Qt::UserRole + 1).toString() != roomNickname)
            {
                this->changeNick();
            }

            QXmppMucRoom *room = m_globalObject->roomFromJid(roomJid);
            if (room != nullptr)
            {
                // Update 'allow' properties
                room->setProperty("allowCommands",    roomAllowCommands);
                room->setProperty("allowGreetings",   roomAllowGreetings);
                room->setProperty("allowCasualSpeak", roomAllowCasualSpeak);
                room->setProperty("allowConversing",  roomAllowConversing);

                room->setProperty("greetingProb",     roomGreetingProb);
                room->setProperty("casualSpeakProb",  roomCasualSpeakProb);
            }
        }
        else
        {
            item->setIcon(QIcon::fromTheme("user-offline",
                                           QIcon(":/images/button-offline.png")));
        }

        item->setText(roomJid);
        item->setData(Qt::UserRole,     roomJid);
        item->setData(Qt::UserRole + 1, roomNickname);
        item->setData(Qt::UserRole + 2, roomPassword);
        item->setData(Qt::UserRole + 3, roomAutojoin);
        // Qt::UserRole + 4 is used for a transient value
        item->setData(Qt::UserRole + 5, roomAllowCommands);
        item->setData(Qt::UserRole + 6, roomAllowGreetings);
        item->setData(Qt::UserRole + 7, roomAllowCasualSpeak);
        item->setData(Qt::UserRole + 8, roomAllowConversing);
        item->setData(Qt::UserRole + 9,  roomGreetingProb);
        item->setData(Qt::UserRole + 10, roomCasualSpeakProb);

        m_roomTableWidget->item(newRow, 1)->setText(roomNickname);

        if (roomPassword.isEmpty())
        {
            m_roomTableWidget->item(newRow, 2)->setText(QString());
        }
        else
        {
            m_roomTableWidget->item(newRow, 2)
                             ->setText(m_keySymbolString); // Key
        }

        if (roomAutojoin)
        {
            m_roomTableWidget->item(newRow, 3)
                             ->setText(QString::fromUtf8("\342\234\224")); // Check mark
        }
        else
        {
            m_roomTableWidget->item(newRow, 3)->setText(QString());
        }

        const QString allowedFeatures = this->allowedFeaturesString(roomAllowCommands,
                                                                    roomAllowGreetings,
                                                                    roomAllowCasualSpeak,
                                                                    roomAllowConversing);
        m_roomTableWidget->item(newRow, 4)->setText(allowedFeatures);


        this->toggleRoomDetails(false);

        this->saveRoomInfo();
    }
}


void RoomModule::removeRoom()
{
    const int currentRow = m_roomTableWidget->currentRow();
    if (currentRow >= 0) // Ensure something's selected
    {
        // Don't takeItem(), otherwise leaveSelectedRoom() will act on another one
        QTableWidgetItem *selectedItem = m_roomTableWidget->item(currentRow, 0);
        const QString roomJid = selectedItem->data(Qt::UserRole).toString();

        if (!roomJid.isEmpty()) // Don't ask for confimation if still empty
        {
            const int confirm = QMessageBox::question(this,
                                 tr("Remove room?") + " - AkariXB",
                                 tr("Do you want to remove room %1 from "
                                    "your list?")
                                 .arg("<b>'" + roomJid + "'</b>")
                                 + "<br />",
                                 tr("Yes, remove it", "It = A chat room"),
                                 tr("No"),
                                 QString(), 1, 1);
            if (confirm == 1) // Not sure, so bail
            {
                return;
            }
        }

        if (m_globalObject->isRoomFullyJoined(roomJid))
        {
            qDebug() << "Leaving room" << roomJid << "before removing it";
            this->leaveSelectedRoom();
        }

        m_roomTableWidget->removeRow(currentRow); // This deletes the 5 items
        this->toggleRoomDetails(false);

        if (m_roomTableWidget->rowCount() == 0)
        {
            m_removeRoomButton->setDisabled(true);

            // Remove any trace of the last removed room, and disable any
            // button that allows editing or joining nonexistent rooms
            this->clearRoomDetails();
            m_editRoomButton->setDisabled(true);
            m_joinButton->setDisabled(true);
            m_leaveButton->setDisabled(true);
        }

        this->saveRoomInfo();
    }
}


void RoomModule::cancelEditMode()
{
    // This takes care of restoring fields to preexisting values, and disabling widgets
    this->showRoomDetails(m_roomTableWidget->currentRow());
}


void RoomModule::selectFirstRow()
{
    m_roomTableWidget->selectRow(0);
}

void RoomModule::selectLastRow()
{
    m_roomTableWidget->selectRow(m_roomTableWidget->rowCount() - 1);
}



void RoomModule::joinSelectedRoom()
{
    this->joinRoom(m_roomJidLineEdit->text().trimmed(),
                   m_nickLineEdit->text().trimmed(),
                   m_roomPasswordLineEdit->text());

    // Room's ChatWidget will be shown later, via onRoomJoined()

    m_joinButton->setDisabled(true);
}


void RoomModule::leaveSelectedRoom()
{
    const QString roomJid = m_roomJidLineEdit->text();
    this->leaveRoom(roomJid, "Bye!"); // TMP reason FIXME

    // Remove it from joined rooms, since we're not leaving due to errors
    m_globalObject->removeJoinedRoom(roomJid);
    m_globalObject->removeFullyJoinedRoom(roomJid);

    this->showRoomDetails(m_roomTableWidget->currentRow());
}


void RoomModule::changeNick()
{
    QXmppMucRoom *room = m_globalObject->roomFromJid(m_roomJidLineEdit->text());
    if (room != nullptr)
    {
        room->setNickName(m_nickLineEdit->text().trimmed());
    }
}



void RoomModule::showRoomDetails(int row)
{
    if (row < 0) // Protect against use after removing last room
    {
        return;
    }

    QTableWidgetItem *item = m_roomTableWidget->item(row, 0);

    //qDebug() << "showRoomDetails()" << item->data(Qt::UserRole).toString();
    m_roomJidLineEdit->setText(item->data(Qt::UserRole).toString());
    m_nickLineEdit->setText(item->data(Qt::UserRole + 1).toString());
    m_roomPasswordLineEdit->setText(item->data(Qt::UserRole + 2).toString());
    m_autojoinCheckbox->setChecked(item->data(Qt::UserRole + 3).toBool());

    // Qt::UserRole + 4 is a transient value

    m_allowCommandsCheckbox->setChecked(item->data(Qt::UserRole + 5).toBool());
    m_allowGreetingsCheckbox->setChecked(item->data(Qt::UserRole + 6).toBool());
    m_allowCasualSpeakCheckbox->setChecked(item->data(Qt::UserRole + 7).toBool());
    m_allowConversingCheckbox->setChecked(item->data(Qt::UserRole + 8).toBool());

    m_greetingsSpinbox->setValue(item->data(Qt::UserRole + 9).toInt());
    m_casualSpeakSpinbox->setValue(item->data(Qt::UserRole + 10).toInt());

    this->toggleRoomDetails(false);

    m_editRoomButton->setEnabled(true);
    m_removeRoomButton->setEnabled(true);


    this->toggleJoinLeaveButtons(item);


    const int stackPos = this->stackIndexFromJid(m_roomJidLineEdit->text());
    m_chatStackedWidget->setCurrentIndex(stackPos);
}


void RoomModule::toggleRoomDetails(bool state)
{
    // Room JID field will only be enabled if the room is not currently joined
    bool roomJoined = false;
    if (m_roomTableWidget->currentRow() >= 0) // Ensure something's selected
    {
        roomJoined = m_roomTableWidget->item(m_roomTableWidget->currentRow(), 0)
                                      ->data(Qt::UserRole + 4).toBool();
    }

    m_roomJidLineEdit->setEnabled(state && !roomJoined);
    m_nickLineEdit->setEnabled(state);
    m_roomPasswordLineEdit->setEnabled(state);
    m_autojoinCheckbox->setEnabled(state);

    if (m_roomTableWidget->currentRow() != -1) // Only if a room is selected
    {
        m_editRoomButton->setDisabled(state); // Reversed state!
    }


    // Ensure join button isn't available for invalid rooms, or when offline
    m_joinButton->setEnabled(m_globalObject->connectedToServer()
                             && this->newRoomIsValid());

    m_leaveButton->setEnabled(m_globalObject->connectedToServer());

    // Second row
    m_allowedToLabel->setVisible(state);
    m_allowCommandsCheckbox->setVisible(state);
    m_allowGreetingsCheckbox->setVisible(state);
    m_allowCasualSpeakCheckbox->setVisible(state);
    m_allowConversingCheckbox->setVisible(state);

    // Third row
    m_greetingsSpinbox->setVisible(state);
    m_casualSpeakSpinbox->setVisible(state);

    // Fourth row
    m_updateCancelButtonBox->setVisible(state);


    if (state)
    {
        if (roomJoined)  // Jump directly to the nickname field
        {
            m_nickLineEdit->setFocus();
        }
        else             // Room JID is editable, so jump to that one first
        {
            m_roomJidLineEdit->setFocus();
        }
    }
    else
    {
        m_roomTableWidget->setFocus();
    }
}

/*
 * Set details editing mode when doubleclicking a table row
 *
 */
void RoomModule::onRoomDoubleClicked(QTableWidgetItem *item)
{
    const QString currentRowJid = m_roomTableWidget->item(item->row(), 0)
                                                   ->data(Qt::UserRole)
                                                    .toString();

    // If the doubleclicked room has effectively been set as current already...
    if (currentRowJid == m_roomJidLineEdit->text())
    {
        this->toggleRoomDetails(true);
    }
}



void RoomModule::onRoomJoined(QString jid)
{
    QTableWidgetItem *item = this->tableItemFromJid(jid);
    if (item != nullptr)
    {
        item->setIcon(QIcon::fromTheme("user-online",
                                       QIcon(":/images/button-online.png")));

        item->setData(Qt::UserRole + 4, true); // Mark as joined

        if (item->isSelected())
        {
            this->toggleJoinLeaveButtons(item);
            m_joinButton->setEnabled(true);

            // Ensure room is visible, even after reconnections
            this->showRoomDetails(item->row());
        }

        // TODO: ability to query participants at any time, shown in UI
        qDebug() << "Joined" << jid << ";\nCurrent participants:"
                 << m_globalObject->roomFromJid(jid)->participants();
    }
}


void RoomModule::onRoomLeft(QString jid)
{
    qDebug() << "RoomModule::onRoomLeft()" << jid;

    // Remove the corresponding ChatWidget
    for (int count = 0; count < m_openedRooms.size(); ++count)
    {
        if (m_openedRooms.at(count)->getRoomJid() == jid)
        {
            m_openedRooms.at(count)->deleteLater();
            m_openedRooms.removeAt(count);
            break;
        }
    }

    // Remove the corresponding RoomSpeakHandler
    for (int count = 0; count < m_roomSpeakHandlers.size(); ++count)
    {
        if (m_roomSpeakHandlers.at(count)->getRoomJid() == jid)
        {
            m_roomSpeakHandlers.at(count)->deleteLater();
            m_roomSpeakHandlers.removeAt(count);
            break;
        }
    }

    QTableWidgetItem *item = this->tableItemFromJid(jid);
    if (item != nullptr) // In case of leaving due to room removal
    {
        item->setData(Qt::UserRole + 4, false); // Set as not-joined
        item->setIcon(QIcon::fromTheme("user-offline",
                                       QIcon(":/images/button-offline.png")));

        // Avoid showing this now-null room, if another one is currently selected
        if (item->isSelected())
        {
            this->showRoomDetails(item->row());
        }
    }
}



//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// PROTECTED ///////////////////////////////
//////////////////////////////////////////////////////////////////////////////



void RoomModule::showEvent(QShowEvent *event)
{
    this->adjustRoomsTable();

    event->accept();
}

void RoomModule::resizeEvent(QResizeEvent *event)
{
    this->adjustRoomsTable();

    event->accept();
}
