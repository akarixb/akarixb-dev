/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "helpers.h"


Helpers::Helpers(QObject *parent) : QObject(parent)
{
    // Unnecessary
}


Helpers::~Helpers()
{
    // Unnecessary
}


bool Helpers::isValidJid(QString jid)
{
    // Matching something@something, and only one @ sign
    QRegularExpression re("\\S+@\\S+");
    QRegularExpressionMatch reMatch = re.match(jid);

    if (reMatch.hasMatch() && jid.count("@") == 1)
    {
        return true;
    }

    return false;
}


QString Helpers::nickFromRoomJid(QString jid)
{
    QStringList jidParts = jid.split("/");
    jidParts.removeFirst();

    return jidParts.join("/");
}


QString Helpers::safeForHtml(QString text)
{
    text = text.toHtmlEscaped();
    text.replace(QStringLiteral("\n"), QStringLiteral("<br />"));

    return text;
}

/*
 * Return the string with both kinds of newlines removed:
 * \n, and variations of <br />
 *
 */
QString Helpers::newlinesRemoved(QString text)
{
    text.replace(QStringLiteral("\n"),
                 QStringLiteral(" "));

    text.replace(QRegularExpression("<br.*?>"), // Non-greedy!
                 QStringLiteral(" "));

    return text;
}


/*
 * Random string from a string list
 */
QString Helpers::randomString(QStringList stringList)
{
    QString chosenString;

    if (!stringList.isEmpty())
    {
        const int choice = QRandomGenerator::global()->bounded(stringList.length());
        if (choice >= 0 && choice < stringList.length()) // For safety
        {
            chosenString = stringList.at(choice);
        }
    }

    return chosenString;
}

/*
 * Create a reduced list of strings from a full one
 *
 */
QStringList Helpers::someStrings(QStringList stringList, int percent)
{
    if (stringList.isEmpty())
    {
        return QStringList();
    }

    QStringList chosenStrings;

    int minimumCount = (stringList.size() * percent) / 100;
    minimumCount = qMax(minimumCount, 1);

    int stringCount = 0;
    while (stringCount < minimumCount)
    {
        const QString string = randomString(stringList);
        chosenStrings.append(string);
        stringList.removeAll(string);

        ++stringCount;
    }

    return chosenStrings;
}


/*
 * Random number between 2 numbers
 *
 */
int Helpers::randomBetween(int lowNum, int highNum)
{
    if (lowNum > highNum)
    {
        qSwap(lowNum, highNum);
    }

    int randomNumber = QRandomGenerator::global()->bounded(lowNum, highNum + 1);

    return randomNumber;
}


/*
 * Random probability, depending on how high the chance percentage is.
 *
 */
bool Helpers::probability(int chancePercent)
{
    const int randomNumber = QRandomGenerator::global()->bounded(100); // Random between 0 and 100

    if (randomNumber < chancePercent)
    {
        return true;
    }

    return false;
}


/*
 * Generate a readable string from an amount of seconds, like 5d HH:MM:SS
 *
 */
QString Helpers::durationString(qint64 seconds)
{
    qint64 minutes = 0;
    qint64 hours = 0;
    qint64 days = 0;

    if (seconds >= 60)
    {
        minutes = seconds / 60;
        seconds -= minutes * 60;
        if (minutes >= 60)
        {
            hours = minutes / 60;
            minutes -= hours * 60;
            if (hours >= 24)
            {
                days = hours / 24;
                hours -= days * 24;
            }
        }
    }

    return QString("%1d %2:%3:%4").arg(days)
                                  .arg(hours,   2, 10, QChar('0'))
                                  .arg(minutes, 2, 10, QChar('0'))
                                  .arg(seconds, 2, 10, QChar('0'));
}


