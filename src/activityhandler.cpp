/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "activityhandler.h"


ActivityHandler::ActivityHandler(GlobalObject *globalObject,
                                 QObject *parent) : QObject(parent)
{
    m_globalObject = globalObject;

    m_xmppClient = m_globalObject->getXmppClient();
    m_mucManager = m_globalObject->getMucManager();
    m_variableParser = m_globalObject->getVariableParser();


    // Start main timer, ticking once a minute, to check if an activity needs to be started
    m_mainTimer = new QTimer(this);
    m_mainTimer->setInterval(60000); // 60 seconds
    connect(m_mainTimer, &QTimer::timeout,
            this, &ActivityHandler::onMainTimerTick);
    m_mainTimer->start();


    // Activity-specific timer; signals the end of the activity
    m_currentActivityTimer = new QTimer(this);
    m_currentActivityTimer->setSingleShot(true);
    connect(m_currentActivityTimer, &QTimer::timeout,
            this, &ActivityHandler::activityFinished);

    qDebug() << "ActivityHandler created";
}

ActivityHandler::~ActivityHandler()
{
    qDebug() << "ActivityHandler destroyed";
}



void ActivityHandler::setActivityData(QVariantList dataList)
{
    m_activityDataList = dataList;
}


QXmppPresence::AvailableStatusType ActivityHandler::presenceFromIndex(int presenceType)
{
    QXmppPresence::AvailableStatusType statusType;

    switch (presenceType)
    {
    case 1:
        statusType = QXmppPresence::Chat;
        break;

    case 2:
        statusType = QXmppPresence::Away;
        break;

    case 3:
        statusType = QXmppPresence::XA;
        break;

    case 4:
        statusType = QXmppPresence::DND;
        break;

    default:
        statusType = QXmppPresence::Online;
    }

    return statusType;
}


//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// SLOTS //////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


void ActivityHandler::onMainTimerTick()
{
    const QTime currentTime = QTime::currentTime();
    // timeNow set to current time with seconds and milliseconds reset to 0
    const QTime timeNow = QTime(currentTime.hour(), currentTime.minute(), 0, 0);
    const QDate dateNow = QDate::currentDate();

    if (timeNow.hour() == 0 && timeNow.minute() == 0) // FIXME: ensure it's not skipped
    {
        qDebug() << "--ActivityHandler-- Midnight! Resetting DoneToday values";
        m_doneToday.clear();
        // FIXME: activities that range from late hours to early hours
        //        need to be excluded from this
    }

    if (!m_activityInProgress.isEmpty())
    {
        return;
    }


    foreach (const QVariant mapVariant, m_activityDataList)
    {
        bool shouldDoIt = true; // Until proven guilty

        const QVariantMap map = mapVariant.toMap();
        const QString activityName = map.value("name").toString();

        // Check how many times this has been already done in the current day
        const int timesDoneToday = m_doneToday.value(activityName).toInt();
        if (timesDoneToday >= map.value("timesPerDay").toInt())
        {
            shouldDoIt = false;
        }

        // Check time range
        if (shouldDoIt)
        {
            const QTime minTime = map.value("minTime").toTime();
            const QTime maxTime = map.value("maxTime").toTime();

            if (minTime <= maxTime)    // Cases such as 10:00 to 21:00,
            {                          // including rare cases like 15:30-15:30
                if (minTime > timeNow
                 || maxTime < timeNow)
                {
                    shouldDoIt = false;
                }
            }
            else                       // Cases such as 22:00 to 03:00
            {
                if (timeNow < minTime
                 && timeNow > maxTime)
                {
                    shouldDoIt = false;
                }
            }
        }


        // Check date range
        if (shouldDoIt)
        {
            // Set minimum and maximum dates, which have no year, to current year
            QDate minDate = map.value("minDate").toDate();
            minDate.setDate(dateNow.year(), minDate.month(), minDate.day());
            QDate maxDate = map.value("maxDate").toDate();
            maxDate.setDate(dateNow.year(), maxDate.month(), maxDate.day());

            if (minDate <= maxDate)    // Cases such as April to September,
            {                          // including cases like 15/Mar-15/Mar
                if (minDate > dateNow
                 || maxDate < dateNow)
                {
                    shouldDoIt = false;
                }
            }
            else                       // Cases such as October to March
            {
                if (dateNow < minDate
                 && dateNow > maxDate)
                {
                    shouldDoIt = false;
                }
            }
        }

        // Check day of the week
        if (shouldDoIt)
        {
            const int today = QDate::currentDate().dayOfWeek(); // 1~7
            const int weekdays = map.value("daysOfWeek").toInt();

            int dayBitChecker = 1; // 1, 2, 4, 8...
            for (int dayCount = 1; dayCount < 8; ++dayCount)
            {
                if (dayCount == today && !(weekdays & dayBitChecker))
                {
                    // Evaluating today, and the weekdays int doesn't have the matching bit
                    shouldDoIt = false;
                }

                dayBitChecker *= 2; // Monday & 1, Sunday & 64
            }
        }


        // Check probability percentage
        if (shouldDoIt)
        {
            shouldDoIt = Helpers::probability(map.value("probability").toInt());
        }


        ///////////////////////////////////////////////////////////////////////
        // It passed all the trials, and survived! Start the activity
        if (shouldDoIt)
        {
            m_activityInProgress = activityName;
            m_doneToday.insert(activityName, timesDoneToday + 1);

            const int duration = Helpers::randomBetween(map.value("minDuration").toInt(),
                                                        map.value("maxDuration").toInt());

            QTime durationTime;
            durationTime.setHMS(0, 0, 0); // Qt 5.x requires this initialization
            durationTime = durationTime.addSecs(duration * 60);

            QString durationString;
            if (durationTime.hour() > 0)
            {
                durationString.append(tr("%Ln hour(s)", "",
                                         durationTime.hour()));
            }

            if (durationTime.minute() > 0)
            {
                if (!durationString.isEmpty())
                {
                    durationString.append(", ");
                }

                durationString.append(tr("%Ln minute(s)", "",
                                         durationTime.minute()));
            }

            // Add at least one extra second, up to 9, to the duration, randomly
            const int extraMilliseconds = Helpers::randomBetween(1000, 9000);

            m_currentActivityTimer->start((duration * 60000)   // Mins to msecs
                                          + extraMilliseconds);


            const QTime endTime = timeNow.addSecs(duration * 60);

            m_globalObject->addToLog(tr("Starting activity %1, which "
                                        "will end at %2.")
                                     .arg("'<b>" + m_activityInProgress + "</b>'")
                                     .arg(QLocale::system().toString(endTime,
                                                                     QLocale::ShortFormat))
                                     + " [" + durationString + "]");


            const QString timeRange = QLocale::system().toString(timeNow,
                                                                 QLocale::ShortFormat)
                                      + QStringLiteral(" ~ ")
                                      + QLocale::system().toString(endTime,
                                                                   QLocale::ShortFormat);

            m_globalObject->setAvailability(map.value("availability").toInt());
            m_globalObject->setReactionTime(map.value("minReaction").toInt(),
                                            map.value("maxReaction").toInt());

            m_globalObject->notifyActivityChange(m_activityInProgress, timeRange);


            // Status type and message
            QXmppPresence presence = m_xmppClient->clientPresence();
            m_oldStatusType = presence.availableStatusType();
            m_oldStatusMessage = presence.statusText();

            presence.setAvailableStatusType(this->presenceFromIndex(map.value("statusType")
                                                                       .toInt()));

            QString statusMsg = Helpers::randomString(map.value("statusMessages")
                                                         .toStringList());
            statusMsg = m_variableParser->getParsed(statusMsg);
            presence.setStatusText(statusMsg);


            m_messagesAfter = map.value("msgAfter").toStringList();


            if (m_globalObject->connectedToServer())
            {
                // Send the new presence
                m_xmppClient->setClientPresence(presence);


                // Send messages to configured recipients about the activity
                m_allRecipientJids.clear();


                // Get a list of all fully-joined rooms that may be recipients...
                QStringList allAllowedRooms = m_globalObject->getFullyJoinedRooms();

                // ...and remove the ones that don't allow 'casual speak'
                const QList<QXmppMucRoom *> allMucObjects = m_mucManager->rooms();
                foreach (const QXmppMucRoom *muc, allMucObjects)
                {
                    if (muc->property("allowCasualSpeak").toBool() == false)
                    {
                        allAllowedRooms.removeAll(muc->jid());
                    }
                }


                const int msgToRooms = map.value("msgToRooms").toInt();
                if (msgToRooms == 0)       // All rooms
                {
                    m_allRecipientJids = allAllowedRooms;
                }
                else if (msgToRooms == 1)  // Several rooms
                {
                    m_allRecipientJids = Helpers::someStrings(allAllowedRooms,
                                                              50);
                }
                else if (msgToRooms == 2)  // A few rooms
                {
                    m_allRecipientJids = Helpers::someStrings(allAllowedRooms,
                                                              20);
                }


                // TODO: add JIDs of -active- private chats


                // Additional JIDs can be added regardless of the other options
                QStringList allSpecificJids = map.value("specificJids")
                                                 .toString()
                                                 .split(QStringLiteral(" "));
                /*
                 * Filter the specific JIDs list, not adding any that match a
                 * configured room, but are not in the fully-joined-rooms list
                 *
                 */
                foreach (const QString specificJid, allSpecificJids)
                {
                    if (m_globalObject->isRoomConfigured(specificJid))
                    {
                        // Apparently it's a room, let's see if we've really joined it
                        if (m_globalObject->isRoomFullyJoined(specificJid))
                        {
                            // Fully joined, so add it to recipients
                            m_allRecipientJids.append(specificJid);
                        }
                        else
                        {
                            m_globalObject
                            ->addToLog(tr("Not sending message to room %1 "
                                          "because I am not currently in it.") // FIXME?
                                       .arg("<b>" + specificJid.toHtmlEscaped()
                                            + "</b>"),
                                       true,
                                       GlobalObject::LogBad);
                        }
                    }
                    else
                    {
                        // Apparently not a room, so just add it
                        m_allRecipientJids.append(specificJid);
                    }
                }

                // Specific JIDs or active chats could introduce duplicates
                m_allRecipientJids.removeDuplicates(); // FIXME: a bit overkill

                // Proceed to send the before-activity message
                const QStringList messagesBefore = map.value("msgBefore")
                                                      .toStringList();
                QString msgBefore;
                foreach (const QString recipient, m_allRecipientJids)
                {
                    msgBefore = Helpers::randomString(messagesBefore);
                    msgBefore = m_variableParser->getParsed(msgBefore);

                    if (msgBefore != QStringLiteral("*")) // TMP FIXME?
                    {
                        m_globalObject->sendMessageToJid(recipient, msgBefore);
                    }
                }
            }

            m_globalObject->notifyStatusChange(presence);


            break;
        }
    }
}


void ActivityHandler::activityFinished()
{
    m_globalObject->addToLog(tr("Activity ended: %1.")
                             .arg("<b>" + m_activityInProgress + "</b>"));

    m_activityInProgress.clear();

    m_globalObject->setAvailability(99);    // FIXME TMP HARDCODED
    m_globalObject->setReactionTime(1, 10); // FIXME TMP HARDCODED
    m_globalObject->notifyActivityChange("[ " + tr("None",
                                                   "Regarding an activity")
                                         + " ]",
                                         QString()); // No duration

    QXmppPresence presence = m_xmppClient->clientPresence();
    presence.setAvailableStatusType(m_oldStatusType);
    presence.setStatusText(m_oldStatusMessage);

    if (m_globalObject->connectedToServer())
    {
        m_xmppClient->setClientPresence(presence);

        // Send messages to configured recipients, about the end of the activity
        QString msgAfter;
        foreach (const QString recipient, m_allRecipientJids)
        {
            msgAfter = Helpers::randomString(m_messagesAfter);
            msgAfter = m_variableParser->getParsed(msgAfter);

            if (msgAfter != QStringLiteral("*"))  // TMP FIXME?
            {
                m_globalObject->sendMessageToJid(recipient, msgAfter);
            }
        }
    }

    //presence.setCapabilityNode("http://akarixb.nongnu.org"); // FIXME, future tests
    m_globalObject->notifyStatusChange(presence);

    // Cleanup
    m_allRecipientJids.clear();
    m_messagesAfter.clear();
}

