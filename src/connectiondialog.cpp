/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "connectiondialog.h"


ConnectionDialog::ConnectionDialog(QString jid, QString password,
                                   bool autoconnect, QString resource,
                                   int priority, bool ignoreSslErrors,
                                   QWidget *parent) : QWidget(parent)
{
    this->setWindowTitle(tr("Connection Settings") + " - AkariXB");
    this->setWindowIcon(QIcon::fromTheme("network-connect",
                                         QIcon(":/images/button-online.png")));
    this->setWindowFlags(Qt::Dialog);
    this->setWindowModality(Qt::ApplicationModal); // Works better in some environments
    this->setMinimumSize(560, 240);


    const QString jidHelp = tr("Like user@server");
    m_jidLineEdit = new QLineEdit(jid, this);
    m_jidLineEdit->setPlaceholderText(jidHelp);
    m_jidLineEdit->setToolTip(jidHelp);
    connect(m_jidLineEdit, &QLineEdit::textChanged,
            this, &ConnectionDialog::validateJid);


    m_passwordLineEdit = new QLineEdit(password, this);
    m_passwordLineEdit->setEchoMode(QLineEdit::Password);
    connect(m_jidLineEdit, SIGNAL(returnPressed()),// Keeping old-style connect
            m_passwordLineEdit, SLOT(setFocus())); // due to slot overload (TMP)


    m_autoconnectCheckbox = new QCheckBox(this);
    m_autoconnectCheckbox->setChecked(autoconnect);


    m_resourceLineEdit = new QLineEdit(resource, this);


    m_prioritySpinbox = new QSpinBox(this);
    m_prioritySpinbox->setRange(-1, 100);
    m_prioritySpinbox->setValue(priority);

    m_ignoreSslCheckbox = new QCheckBox(tr("Use carefully.") + "\n"
                                        + tr("This allows connecting to "
                                             "servers with bad "
                                             "security certificates."),
                                        this);
    m_ignoreSslCheckbox->setChecked(ignoreSslErrors);


    m_connectButton = new QPushButton(QIcon::fromTheme("network-connect",
                                                       QIcon(":/images/button-online.png")),
                                      tr("&Connect"),
                                      this);
    connect(m_connectButton, &QAbstractButton::clicked,
            this, &ConnectionDialog::startConnection);
    connect(m_passwordLineEdit, &QLineEdit::returnPressed,
            m_connectButton, &QAbstractButton::click);


    m_cancelButton = new QPushButton(QIcon::fromTheme("dialog-cancel",
                                                      QIcon(":/images/button-cancel.png")),
                                     tr("Cancel"),
                                     this);
    connect(m_cancelButton, &QAbstractButton::clicked,
            this, &QWidget::hide);


    m_cancelAction = new QAction(this);
    m_cancelAction->setShortcut(QKeySequence(Qt::Key_Escape));
    connect(m_cancelAction, &QAction::triggered,
            this, &QWidget::hide);
    this->addAction(m_cancelAction);


    m_topLayout = new QFormLayout();
    m_topLayout->addRow(tr("XMPP Address (Jabber ID)"), m_jidLineEdit);
    m_topLayout->addRow(tr("Password"),                 m_passwordLineEdit);
    m_topLayout->addRow(tr("Connect Automatically"),    m_autoconnectCheckbox);
    m_topLayout->addRow(tr("Resource"),                 m_resourceLineEdit);
    m_topLayout->addRow(tr("Priority"),                 m_prioritySpinbox);
    m_topLayout->addRow(tr("Ignore SSL errors"),        m_ignoreSslCheckbox);

    m_bottomButtonBox = new QDialogButtonBox(this);
    m_bottomButtonBox->addButton(m_connectButton, QDialogButtonBox::AcceptRole);
    m_bottomButtonBox->addButton(m_cancelButton, QDialogButtonBox::RejectRole);

    m_mainLayout = new QVBoxLayout();
    m_mainLayout->addLayout(m_topLayout);
    m_mainLayout->addSpacing(16);
    m_mainLayout->addStretch(0);
    m_mainLayout->addWidget(m_bottomButtonBox);
    this->setLayout(m_mainLayout);


    this->validateJid(jid);

    qDebug() << "ConnectionDialog created";
}


ConnectionDialog::~ConnectionDialog()
{
    qDebug() << "ConnectionDialog destroyed";
}



//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// SLOTS //////////////////////////////////
//////////////////////////////////////////////////////////////////////////////




void ConnectionDialog::validateJid(QString jid)
{
    m_connectButton->setEnabled(Helpers::isValidJid(jid));
}



void ConnectionDialog::startConnection()
{
    if (m_resourceLineEdit->text().trimmed().isEmpty())
    {
        m_resourceLineEdit->setText("AkariXB"); // Use default
    }

    emit connectionRequested(m_jidLineEdit->text().trimmed(),
                             m_passwordLineEdit->text(),
                             m_autoconnectCheckbox->isChecked(),
                             m_resourceLineEdit->text().trimmed(),
                             m_prioritySpinbox->value(),
                             m_ignoreSslCheckbox->isChecked());

    this->hide();
}


//////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// PROTECTED ////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


/*
 * If any of the two main fields is still empty, focus it
 *
 */
void ConnectionDialog::showEvent(QShowEvent *event)
{
    if (m_jidLineEdit->text().trimmed().isEmpty())
    {
        m_jidLineEdit->setFocus();
    }
    else if (m_passwordLineEdit->text().trimmed().isEmpty())
    {
        m_passwordLineEdit->setFocus();
    }

    event->accept();
}
