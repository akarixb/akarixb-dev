
v1.0  (UNRELEASED)

 - Added basic conversation handling.   (WIP)
 - Small improvements in chat widgets.
 - Improved usability with smaller screens/resolutions.
 - Improved network reconnection and fixed some crashes.   (WIP)



v0.9  (January 25, 2022)

 - Activities can be restricted to specific days of the week.
 - The categories side panel can now be resized.
 - Fixed issue with chat widget scrollbars sometimes getting stuck.
 - Added option to ignore SSL errors while connecting to XMPP server.
 - Updated usage of deprecated QXMPP methods.
 - Further code refactoring.



v0.8  (June 22, 2019)

 - Ability to greet users in chatrooms, talk to the rooms at random intervals,
   and react to room messages that match custom regular expressions.
 - Room configuration can specify whether the bot can talk in each room,
   and for what purposes. Old configured rooms will have most of these
   checkboxes disabled initially, allowing only commands, which is also the
   default for new rooms.
 - String syntax will be checked for valid %substitutions% and {a|b|c} choices,
   where needed.
 - The list of chatrooms now uses a proper table with more information, and
   room configuration is clearer.
 - Many improvements in string list editors, including search and better
   keyboard support.
 - System tray icon can be customized.
 - The General section shows more useful information.
 - Users will be prevented from defining commands with spaces in the name,
   and duplicated chatrooms, which is not supported.



v0.7  (February 9, 2019)

 - When joining a chatroom (MUC), backlog messages will not be processed.
 - Added a menu option to abort ongoing connections.
 - Added basic documentation in a help window.
 - Activities will take the date into account.
 - Late-to-early activity time and date ranges are now supported.
 - Nicer, categorized logging, including subscription-related and
   presence errors, and using rich text formatting.
 - Basic contact list (roster) management.
 - Added %happyemoji% and %sademoji% string substitutions.
 - Avoid sending messages to rooms the bot has not joined.
 - Duplicate items will not be allowed in keyword-based lists.
 - Other minor fixes.



v0.6  (December 31, 2018)

 - Parameters passed to a command can be inserted in the reply, via %params%,
   and the nickname of the calling user can be inserted via %user%.
 - In chatrooms, commands that have a custom reply pattern won't show the
   nickname of the user who triggered the command, unless specified.
 - Ability to report uptime.
 - When connection is lost, reconnection will always be attempted.
 - Connection keepalive timeout will be forced to be lower than keepalive
   interval, to ensure proper timeouts.
 - Basic handling of headline (broadcast) messages.
 - Option to show detailed XML debug info in the console.
 - Being kicked from a chatroom is now logged properly.
 - Several improvements in list editors.
 - Fixed bug in aliased commands that take parameters.
 - Changed OK/Cancel buttons in some dialogs to follow the environment's style.
 - Added Appdata (Appstream) file.
 - Code refactoring.



v0.5  (January 11, 2018)

 - Honor "Send Messages To" setting in Activities.
 - Log outgoing private messages in corresponding chat module tab.
 - Ability to detach processes for commands of "Run program" type.
   ** Previously defined commands of this type will need to be reconfigured.
 - Keyword-based commands will list possible queries when no parameter is given.
 - Option to hide window in system tray on startup.
 - Configurable XMPP keepalive interval.
 - Status bar icon now matches current status type.
 - Fixed XMPP keepalive timeout setting.
 - Qt 4 is no longer supported.



v0.4  (June 27, 2016)

- Added Keyword (dictionary-like) command type.
- Added Alias command type.
- Basic variable replacement in replies.
- Basic activity handling.
- Option to reply to commands always in private.
- Support for password-protected MUCs.
- Basic identity module.
- Configurable popup notifications.
- Configurable XMPP keepalive timeout.
- Completely empty messages (from ChatStates, etc) will be ignored.
- Other minor fixes and improvements.



v0.3  (January 21, 2016)

- A few custom command types can be added, and stored:
    Static reply, random sentence, random line from file, run program.
- Configurable command prefix.
- Configurable list of administrator JIDs.
- Basic presence logging.
- Small fixes everywhere.



v0.2  (December 17, 2015)

- Basic command parsing.
- Basic activity definition widget.
- Basic private chat handling.
- Room autojoining and more room handling fixes.
- Log network/protocol errors and other events.



v0.1  (December 9, 2015)

- Basic connectivity.
- Ability to join MUC rooms and send messages to them.

