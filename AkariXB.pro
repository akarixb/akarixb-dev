##  Akari XMPP Bot
##  Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
##
##  This program is free software; you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation; either version 2 of the License, or
##  (at your option) any later version.
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##  You should have received a copy of the GNU General Public License
##  along with this program; if not, write to the
##  Free Software Foundation, Inc.,
##  51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
##
## -------------------------------------------------
## Project created by QtCreator
## -------------------------------------------------

message("Generating Makefile for AkariXB... $$escape_expand(\\n)\
Using $$_FILE_$$escape_expand(\\n)")

QT       += core gui network widgets
isEqual(QT_MAJOR_VERSION, 5) {
    message("Building for Qt 5")
    message("If you want to build for Qt 6, you might need to use qmake-qt6 $$escape_expand(\\n\\n)")
}

isEqual(QT_MAJOR_VERSION, 6) {
    message("Building for Qt 6")
    message("If you want to build for Qt 5, you might need to use qmake-qt5 $$escape_expand(\\n\\n)")
}

lessThan(QT_MAJOR_VERSION, 5) {
    error(">>>> You're trying to build with Qt 4 or older.")
    error(">>>> This version of AkariXB requires Qt 5 or Qt 6.")
    error(">>>> You might need to use qmake-qt5 or qmake-qt6 instead.")
}

message("Building with Qt v$$QT_VERSION $$escape_expand(\\n)")




TARGET = akarixb
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x050900
##DEFINES += QT_NO_CAST_FROM_ASCII
##DEFINES += QT_RESTRICTED_CAST_FROM_ASCII

CONFIG += qxmpp

## Required to fix std::optional build issues with QXMPP 1.4.x, in some cases
CONFIG += c++17  ## Requires Qt 5.12+

## TMP/FIXME: workaround, until QXMPP upstream provides a qxmpp.prf feature file
## See https://github.com/qxmpp-project/qxmpp/issues/100
## and https://github.com/qxmpp-project/qxmpp/issues/193
!load(qxmpp) {
    warning("QXMPP feature file not available; enabling workaround:")

    freebsd { ## FreeBSD
      warning("- FreeBSD, includes at /usr/local/include/qxmpp")
      INCLUDEPATH += /usr/local/include/qxmpp
    }
    else {    ## GNU/Linux, in general
      warning("- Assuming includes at:")

      exists("/usr/include/qxmpp") {
        warning("   /usr/include/qxmpp")
        INCLUDEPATH += /usr/include/qxmpp
        warning("- Linking with -lqxmpp")
        LIBS += -lqxmpp
      }

      isEqual(QT_MAJOR_VERSION, 5) {
          exists("/usr/include/QXmppQt5") {
            warning("   /usr/include/QXmppQt5")
            INCLUDEPATH += /usr/include/QXmppQt5
            warning("- Linking with -lQXmppQt5")
            LIBS += -lQXmppQt5
          }
      }

      isEqual(QT_MAJOR_VERSION, 6) {
          exists("/usr/include/QXmppQt6") {
            warning("   /usr/include/QXmppQt6")
            INCLUDEPATH += /usr/include/QXmppQt6
            warning("- Linking with -lQXmppQt6")
            LIBS += -lQXmppQt6
          }
      }
    }
}



SOURCES += src/main.cpp\
    src/mainwindow.cpp \
    src/connectiondialog.cpp \
    src/messagehandler.cpp \
    src/configdialog.cpp \
    src/globalobject.cpp \
    src/commandhandler.cpp \
    src/chatwidget.cpp \
    src/helpers.cpp \
    src/chatmodule.cpp \
    src/commandmodule.cpp \
    src/logmodule.cpp \
    src/roommodule.cpp \
    src/activitymodule.cpp \
    src/conversationmodule.cpp \
    src/conversationhandler.cpp \
    src/stringlistwidget.cpp \
    src/datafile.cpp \
    src/generalmodule.cpp \
    src/variableparser.cpp \
    src/identitymodule.cpp \
    src/activityhandler.cpp \
    src/helpwidget.cpp \
    src/contactsmodule.cpp \
    src/roomspeakhandler.cpp \
    src/roomspeakmodule.cpp \
    src/roomspeaktab.cpp \
    src/speechdatastore.cpp

HEADERS  += src/mainwindow.h \
    src/connectiondialog.h \
    src/messagehandler.h \
    src/configdialog.h \
    src/globalobject.h \
    src/commandhandler.h \
    src/chatwidget.h \
    src/helpers.h \
    src/chatmodule.h \
    src/commandmodule.h \
    src/logmodule.h \
    src/roommodule.h \
    src/activitymodule.h \
    src/conversationmodule.h \
    src/conversationhandler.h \
    src/stringlistwidget.h \
    src/datafile.h \
    src/generalmodule.h \
    src/variableparser.h \
    src/identitymodule.h \
    src/activityhandler.h \
    src/helpwidget.h \
    src/contactsmodule.h \
    src/roomspeakhandler.h \
    src/roomspeakmodule.h \
    src/roomspeaktab.h \
    src/speechdatastore.h

DISTFILES += \
    README \
    BUGS \
    CHANGELOG \
    INSTALL \
    TODO \
    org.nongnu.akarixb.desktop \
    TRANSLATING \
    manual/akarixb.1 \
    translations/translation-status


TRANSLATIONS += translations/akarixb_en.ts \
    translations/akarixb_es.ts \
    translations/akarixb_ca.ts \
    translations/akarixb_gl.ts \
    translations/akarixb_eu.ts \
    translations/akarixb_fr.ts \
    translations/akarixb_it.ts \
    translations/akarixb_de.ts \
    translations/akarixb_pt.ts \
    translations/akarixb_ru.ts \
    translations/akarixb_EMPTY.ts


## Auto-generate .qm files
QMAKE_EXTRA_COMPILERS += lrelease
lrelease.input         = TRANSLATIONS
lrelease.output        = ${QMAKE_FILE_BASE}.qm
lrelease.commands      = $$[QT_INSTALL_BINS]/lrelease ${QMAKE_FILE_IN} \
                         -qm ${QMAKE_FILE_PATH}/${QMAKE_FILE_OUT}
lrelease.CONFIG       += no_link target_predep ignore_no_exist

## Ensure .qm files are generated before
PRE_TARGETDEPS += compiler_lrelease_make_all


RESOURCES += akarixb.qrc


## This is here so the makefile has a 'make install' target
target.path = /usr/bin/

## Additions by David Geiger, from Mageia, for easier packaging
desktop_file.files = org.nongnu.akarixb.desktop
desktop_file.path = /usr/share/applications/

man_file.files = manual/akarixb.1
man_file.path = /usr/share/man/man1/

appdata_file.files = appdata/org.nongnu.akarixb.appdata.xml
appdata_file.path = /usr/share/metainfo/

icon32_png.files = icon/32x32/akarixb.png
icon32_png.path = /usr/share/icons/hicolor/32x32/apps/

icon64_png.files = icon/64x64/akarixb.png
icon64_png.path = /usr/share/icons/hicolor/64x64/apps/

INSTALLS += target \
            desktop_file \
            man_file \
            appdata_file \
            icon32_png \
            icon64_png


message("$$escape_expand(\\n\\n\\n)\
Makefile done!$$escape_expand(\\n\\n)\
If you're building the binary, you can run 'make' now. $$escape_expand(\\n) \
$$escape_expand(\\n)")

message("Please ignore possible RCC errors about missing .qm files. \
$$escape_expand(\\n)\
They will be built later, by make.\
$$escape_expand(\\n)")

