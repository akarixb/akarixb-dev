/*
 *   This file is part of AkariXB
 *   Copyright 2015-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef GENERALMODULE_H
#define GENERALMODULE_H

#include <QWidget>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QLabel>
#include <QPushButton>
#include <QTextBrowser>
#include <QTimer>

#include <QDebug>


#include <QXmppClient.h>
#include <QXmppPresence.h>

#include "globalobject.h"
#include "helpers.h"

class GeneralModule : public QWidget
{
    Q_OBJECT

public:
    explicit GeneralModule(GlobalObject *globalObject,
                           QWidget *parent = nullptr);
    ~GeneralModule();

    void setInfoMessage(QString message);
    void setServerInfo(QString message);
    void setConnecting(bool state);
    void addPresence(QXmppPresence presence);


signals:
    void connectionRequested();
    void disconnectionRequested();


public slots:
    void onConnected();
    void onDisconnected();

    void onUptimeTimerTick();

    void onStatusChanged(QXmppPresence presence);
    void onFullyJoinedRoomsChanged(int count);


private:
    QVBoxLayout *m_mainLayout;
    QFormLayout *m_formLayout;

    QLabel *m_infoLabel;
    QLabel *m_serverVersionLabel;

    QLabel *m_uptimeLabel;
    QLabel *m_activityLabel;
    QLabel *m_statusMessageLabel;
    QLabel *m_roomsInfoLabel;

    QTextBrowser *m_tmpPresenceBrowser;

    QPushButton *m_connectButton;
    QPushButton *m_disconnectButton;

    QTimer *m_uptimeTimer;
    int m_upMinutes;
    int m_upHours;
    int m_upDays;

    QXmppClient *m_xmppClient;
    GlobalObject *m_globalObject;
};

#endif // GENERALMODULE_H
