<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>ActivityHandler</name>
    <message numerus="yes">
        <location filename="../src/activityhandler.cpp" line="230"/>
        <source>%Ln hour(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/activityhandler.cpp" line="241"/>
        <source>%Ln minute(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/activityhandler.cpp" line="254"/>
        <source>Starting activity %1, which will end at %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activityhandler.cpp" line="358"/>
        <source>Not sending message to room %1 because I am not currently in it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activityhandler.cpp" line="403"/>
        <source>Activity ended: %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activityhandler.cpp" line="410"/>
        <source>None</source>
        <comment>Regarding an activity</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ActivityModule</name>
    <message>
        <location filename="../src/activitymodule.cpp" line="42"/>
        <source>&amp;Sync Activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="56"/>
        <source>&amp;Add Activity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="64"/>
        <source>&amp;Remove Activity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="98"/>
        <source>Mon</source>
        <comment>Monday, shortened if possible</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="100"/>
        <source>Tue</source>
        <comment>Tuesday, shortened if possible</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="102"/>
        <source>Wed</source>
        <comment>Wednesday, shortened if possible</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="104"/>
        <source>Thu</source>
        <comment>Thursday, shortened if possible</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="106"/>
        <source>Fri</source>
        <comment>Friday, shortened if possible</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="108"/>
        <source>Sat</source>
        <comment>Saturday, shortened if possible</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="110"/>
        <source>Sun</source>
        <comment>Sunday, shortened if possible</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="117"/>
        <source>minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="120"/>
        <source>Online</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="121"/>
        <source>Free for Chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="122"/>
        <source>Away</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="123"/>
        <source>Extended Away</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="124"/>
        <source>Do not Disturb</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="141"/>
        <source>seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="160"/>
        <source>All rooms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="161"/>
        <source>Several rooms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="162"/>
        <source>A few rooms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="177"/>
        <source>List of room or user addresses, separated by space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="247"/>
        <source>Days Of The Week</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="166"/>
        <source>Also to active private chats</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="163"/>
        <source>None of the rooms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="172"/>
        <source>Additional specific JIDs:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="184"/>
        <source>Sa&amp;ve Activity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="242"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="243"/>
        <source>Maximum Times Per Day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="244"/>
        <source>Probability</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="245"/>
        <source>Time Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="246"/>
        <source>Date Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="248"/>
        <source>Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="249"/>
        <source>Set Status Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="250"/>
        <source>Status Messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="251"/>
        <source>Availability</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="252"/>
        <source>Reaction Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="253"/>
        <source>Descriptions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="254"/>
        <source>Messages Before</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="255"/>
        <source>Messages Afterwards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="256"/>
        <source>Send Messages To</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../src/activitymodule.cpp" line="334"/>
        <source>Loaded %Ln activities.</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="363"/>
        <source>New Activity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="514"/>
        <source>Activity exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/activitymodule.cpp" line="515"/>
        <source>Error: There is already an activity in the list with this name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../src/activitymodule.cpp" line="622"/>
        <source>Saved %Ln activities.</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>ChatModule</name>
    <message>
        <location filename="../src/chatmodule.cpp" line="46"/>
        <source>There are no active chats</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/chatmodule.cpp" line="165"/>
        <source>Chat started by %1.</source>
        <comment>%1 = A nickname</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/chatmodule.cpp" line="171"/>
        <source>New chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/chatmodule.cpp" line="172"/>
        <source>A new chat was started by %1.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChatWidget</name>
    <message>
        <location filename="../src/chatwidget.cpp" line="414"/>
        <source>Reason:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/chatwidget.cpp" line="423"/>
        <source>Kicked from the room!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/chatwidget.cpp" line="442"/>
        <source>Room name is: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/chatwidget.cpp" line="366"/>
        <source>Joined room %1, as %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/chatwidget.cpp" line="395"/>
        <source>Left room %1.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CommandHandler</name>
    <message>
        <location filename="../src/commandhandler.cpp" line="103"/>
        <source>Command %1 requested by %2 in room %3 without the right access level.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="116"/>
        <source>Command from %1 in room %2: %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="159"/>
        <source>Command %1 requested by %2 without the right access level.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="169"/>
        <source>Command from %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="197"/>
        <source>The command failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="209"/>
        <source>Use %1 to list the available commands.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="213"/>
        <source>Use %1 [command] to get help about a specific command.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="239"/>
        <source>I don&apos;t have help about: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="243"/>
        <source>Help for %1:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="252"/>
        <source>Available commands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="333"/>
        <source>Error loading file %1 for command: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="349"/>
        <source>I need a parameter for the query:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="373"/>
        <source>I don&apos;t have a match for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="399"/>
        <source>Started detached program successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="437"/>
        <source>Command gave no output text.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandhandler.cpp" line="477"/>
        <source>This command is an alias to itself!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CommandModule</name>
    <message>
        <location filename="../src/commandmodule.cpp" line="41"/>
        <source>&amp;Sync Commands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="56"/>
        <source>&amp;Add Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="63"/>
        <source>&amp;Remove Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="72"/>
        <source>The name, without the prefix or any spaces</source>
        <comment>Referring to the command&apos;s name/trigger</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="79"/>
        <source>Explanation shown to users with the %1 command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="85"/>
        <source>Anyone can use it</source>
        <comment>&apos;it&apos; refers to a command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="89"/>
        <source>Static Message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="90"/>
        <source>Just enter a single sentence.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="91"/>
        <source>Random Sentence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="92"/>
        <source>Add as many sentences as you want.
One of them will be selected at random as a reply.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="95"/>
        <source>Random Line from Text File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="96"/>
        <source>A random line from the chosen file will be used as a reply.
File encoding should be UTF-8.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="99"/>
        <source>Keyword-based Message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="100"/>
        <source>Add keywords to the list, then enter the corresponding definition for each keyword on the other side.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="103"/>
        <source>Run Program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="112"/>
        <source>If the program you launch will take a long time to finish (like a GUI), you need to select the option to launch it independently, but then it will not be possible to use the program output as the reply for the command.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="117"/>
        <source>Alias for another command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="118"/>
        <source>Enter the original command that should be run when this alias is used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="148"/>
        <source>Select File...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="166"/>
        <source>Wait for the program to finish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="167"/>
        <source>Launch the program independently</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="181"/>
        <source>With a reply pattern you can make any possible reply to a command fit into a predefined structure, like having a specific symbol at the start or the end of the reply, or always start the reply with a fixed word.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="186"/>
        <source>You can insert variables like %user%, %params%, %output%, %line% and %smiley%.</source>
        <comment>Do not translate the words between % symbols</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="190"/>
        <source>Usually, you will want to have the %output% variable somewhere in the pattern.</source>
        <comment>Do not translate %output%</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="194"/>
        <source>If you don&apos;t set a reply pattern, replies in chatrooms will use the default pattern, %user%: %output%.</source>
        <comment>Do not translate %user% or %output%</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="198"/>
        <source>Example: [RESULT] &gt; %output%</source>
        <comment>Do not translate %output%</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="213"/>
        <source>Save &amp;Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="239"/>
        <source>Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="240"/>
        <source>Help Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="241"/>
        <source>Access Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="243"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="104"/>
        <source>Enter a system command line to be run, with any needed parameters.
The reply will be the console output of the program, unless you specify a reply pattern, and don&apos;t include the %output% variable somewhere in it.</source>
        <comment>Don&apos;t translate %output%!</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="178"/>
        <source>Set an optional reply pattern</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="247"/>
        <source>Reply Pattern</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="249"/>
        <source>Private Reply</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../src/commandmodule.cpp" line="352"/>
        <source>Loaded %Ln command(s).</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="368"/>
        <source>The command name can&apos;t contain spaces.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="375"/>
        <source>Static reply is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="390"/>
        <source>There are no sentences in the list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="398"/>
        <source>There is no file selected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="406"/>
        <source>There are no keywords in the list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="416"/>
        <source>There is no program specified.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="424"/>
        <source>You need to enter the command this alias will execute.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="430"/>
        <source>A command can&apos;t be an alias of itself.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="452"/>
        <source>Error validating fields</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="480"/>
        <source>Select a text file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="486"/>
        <source>Selected file does not exist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="501"/>
        <source>New Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="549"/>
        <source>No file selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="660"/>
        <source>Command exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/commandmodule.cpp" line="661"/>
        <source>Error: There is already a command in the list with this name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../src/commandmodule.cpp" line="790"/>
        <source>Saved %Ln command(s).</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../src/configdialog.cpp" line="27"/>
        <source>Program Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="51"/>
        <location filename="../src/configdialog.cpp" line="57"/>
        <source>seconds</source>
        <comment>Suffix next to a number</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="69"/>
        <source>Administrator JIDs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="71"/>
        <source>Command prefix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="75"/>
        <source>Connection timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="73"/>
        <source>Connection ping interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="91"/>
        <source>Notify new chats</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="93"/>
        <source>Notify disconnection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="107"/>
        <source>Select...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="127"/>
        <source>Hide window on startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="129"/>
        <source>Use a custom icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="142"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="147"/>
        <source>Notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="152"/>
        <source>System Tray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="157"/>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="165"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="213"/>
        <source>Invalid icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="214"/>
        <source>The selected image is not valid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="219"/>
        <source>Icon file %1 does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="249"/>
        <source>Select custom icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="251"/>
        <source>Image files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="254"/>
        <source>All files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="307"/>
        <source>Program configuration saved.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConnectionDialog</name>
    <message>
        <location filename="../src/connectiondialog.cpp" line="29"/>
        <source>Connection Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/connectiondialog.cpp" line="37"/>
        <source>Like user@server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/connectiondialog.cpp" line="62"/>
        <source>Use carefully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/connectiondialog.cpp" line="63"/>
        <source>This allows connecting to servers with bad security certificates.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/connectiondialog.cpp" line="72"/>
        <source>&amp;Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/connectiondialog.cpp" line="82"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/connectiondialog.cpp" line="96"/>
        <source>XMPP Address (Jabber ID)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/connectiondialog.cpp" line="97"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/connectiondialog.cpp" line="98"/>
        <source>Connect Automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/connectiondialog.cpp" line="99"/>
        <source>Resource</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/connectiondialog.cpp" line="100"/>
        <source>Priority</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/connectiondialog.cpp" line="101"/>
        <source>Ignore SSL errors</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ContactsModule</name>
    <message>
        <location filename="../src/contactsmodule.cpp" line="57"/>
        <source>&amp;Start chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="66"/>
        <source>&amp;Add Contact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="74"/>
        <source>Rename Contact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="82"/>
        <source>&amp;Remove Contact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="108"/>
        <source>Accept Request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="115"/>
        <source>Refuse Request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="125"/>
        <source>Pending Subscription Requests</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="198"/>
        <source>Subscription status: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="327"/>
        <source>Accepted subscription request from %1.</source>
        <comment>%1 is an XMPP address</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="352"/>
        <source>Refused subscription request from %1.</source>
        <comment>%1 is an XMPP address</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="394"/>
        <source>Add a contact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="395"/>
        <source>Enter the full XMPP address, with user@server format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="425"/>
        <source>Rename a contact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="426"/>
        <source>Enter the new name for %1</source>
        <comment>%1 is contact&apos;s name + address</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="458"/>
        <source>Remove contact?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="459"/>
        <source>Do you want to remove %1 from your contact list?</source>
        <comment>%1 is contact&apos;s name + address</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="464"/>
        <source>Yes, remove</source>
        <comment>Regarding a contact</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="465"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="486"/>
        <source>Added %1 to the contact list.</source>
        <comment>%1 is an XMPP address</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactsmodule.cpp" line="519"/>
        <source>Removed %1 from the contact list.</source>
        <comment>%1 is an XMPP address</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GeneralModule</name>
    <message>
        <location filename="../src/generalmodule.cpp" line="40"/>
        <source>Offline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/generalmodule.cpp" line="99"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/generalmodule.cpp" line="107"/>
        <source>Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/generalmodule.cpp" line="122"/>
        <source>Uptime:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/generalmodule.cpp" line="123"/>
        <source>Current Activity:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/generalmodule.cpp" line="124"/>
        <source>Status Message:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/generalmodule.cpp" line="125"/>
        <source>Rooms:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/generalmodule.cpp" line="157"/>
        <source>Server version: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/generalmodule.cpp" line="295"/>
        <source>%1 joined, out of %2 configured</source>
        <comment>Number of rooms joined and configured</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HelpWidget</name>
    <message>
        <location filename="../src/helpwidget.cpp" line="26"/>
        <source>General Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="40"/>
        <source>Getting started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="41"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="42"/>
        <source>Commands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="43"/>
        <source>Activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="44"/>
        <source>Substitution syntax</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="45"/>
        <source>Command line options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="47"/>
        <source>Contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="70"/>
        <source>The first time you start AkariXB you will see the Connection Settings window, where you need to enter the XMPP address the bot will use, and its password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="74"/>
        <source>Afterwards, you can configure chatrooms to be automatically joined, manage the contact list, define commands for which the bot will provide answers, etc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="88"/>
        <source>You can configure several things to your liking in the settings, like which symbol will act as prefix for commands, which XMPP addresses will be able to use special administration commands with the bot, whether you want popup notifications for certain events, etc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="104"/>
        <source>You can define several kinds of custom commands, like static message, random message from a list, keyword-based message, and program execution-based commands.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="109"/>
        <source>Each type of command has an explanation of how it works, visible while you&apos;re adding the new command.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="112"/>
        <source>If the access level for the command is not set for everyone, only users listed in the administrators list in the Program Configuration window will be able to use that command.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="126"/>
        <source>Activities can be defined to make the bot change its status, send messages, adapt its reaction time and so on, at certain times and dates.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="130"/>
        <source>If you have defined messages to be sent before or after the activity, you can decrease the chances of them being sent by adding some strings with a single asterisk (*) in them to the list of messages.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="144"/>
        <source>There are several special words that can be entered inside custom strings like replies to commands, conversation texts and status messages, which will be replaced by the appropriate variable text at the time the string is used. These are:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="151"/>
        <source>Replaced by the name of the user to whom this message is directed. Nickname in the case of chatrooms, and account name in the case of chats with contacts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="156"/>
        <source>The nickname of the bot, as specified in the Identity module.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="159"/>
        <source>In replies to commands, this is replaced with whatever follows the command name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="162"/>
        <source>Used in a custom reply pattern for a command, this will be replaced by the original reply to that command.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="166"/>
        <source>This is replaced by a newline character.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="168"/>
        <source>This is replaced by a random old-style smiley face.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="171"/>
        <source>These are replaced by a random, happy or sad, Unicode-based emoji face.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="174"/>
        <source>Keep in mind that some users might not be able to see these symbols.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="179"/>
        <source>Example: &apos;Hello %user%&apos; could be converted to &apos;Hello Mary&apos; if a user called Mary does something that triggers that reply.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="183"/>
        <source>In addition, syntax like %1 can be used to insert one out of several choices at random, at any desired point in the string.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="188"/>
        <source>%1, %2 and %3 can&apos;t be used inside these choices.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="202"/>
        <source>You can use the --config parameter (or -c) to run the program with a different configuration. This way, you can run two instances of AkariXB at the same time, with different accounts, different rooms configured, using a different command prefix, etc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="208"/>
        <source>Use the --debug parameter (or -d) to have extra information in your terminal window, about what the program is doing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="212"/>
        <source>Use the --xmppdebug parameter (or -x) to have extensive XMPP traffic information (XML stanzas) shown in your terminal window.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="228"/>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IdentityModule</name>
    <message>
        <location filename="../src/identitymodule.cpp" line="47"/>
        <source>&amp;Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/identitymodule.cpp" line="54"/>
        <source>Nickname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/identitymodule.cpp" line="55"/>
        <source>Date of Birth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/identitymodule.cpp" line="109"/>
        <source>Loaded identity data for %1.</source>
        <comment>%1 is a nickname</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/identitymodule.cpp" line="115"/>
        <source>Identity data has not been defined yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/identitymodule.cpp" line="149"/>
        <source>Saved identity data for %1.</source>
        <comment>%1 is a nickname</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LogModule</name>
    <message>
        <location filename="../src/logmodule.cpp" line="41"/>
        <source>AkariXB v%1 started.</source>
        <comment>%1 = Version number</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="70"/>
        <source>Settings file in use: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="132"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="135"/>
        <source>Rooms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="142"/>
        <source>Contacts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="145"/>
        <source>Commands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="148"/>
        <source>Room Speech</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="151"/>
        <source>Conversation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="154"/>
        <source>Activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="157"/>
        <source>Identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="160"/>
        <source>Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="237"/>
        <source>Ready.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="269"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="277"/>
        <source>Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="286"/>
        <source>Stop connection attempts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="294"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="301"/>
        <source>&amp;Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="313"/>
        <source>Configure AkariXB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="319"/>
        <source>S&amp;ettings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="329"/>
        <source>General &amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="337"/>
        <source>Visit Website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="344"/>
        <source>Report a Bug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="351"/>
        <source>About XMPP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="357"/>
        <location filename="../src/mainwindow.cpp" line="1098"/>
        <source>About AkariXB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="362"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="471"/>
        <source>Data directory created: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="477"/>
        <source>Error creating data directory: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="486"/>
        <source>Data directory in use: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="677"/>
        <source>Connecting...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="695"/>
        <source>Warning! Ignoring errors in SSL connection to server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="722"/>
        <source>%1 connected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="780"/>
        <source>Aborted connection attempt.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="794"/>
        <source>Disconnected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="815"/>
        <source>Disconnected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="816"/>
        <source>Connection to the server was lost.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="832"/>
        <source>Connection seems stuck. Trying again...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="909"/>
        <source>Presence error %1 from %2: %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="926"/>
        <source>%1 wants to subscribe to your presence.</source>
        <comment>%1 is another user&apos;s address</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="934"/>
        <source>%1 accepted your subscription request.</source>
        <comment>%1 is another user&apos;s address</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="942"/>
        <source>%1 unsubscribed from your presence.</source>
        <comment>%1 is another user&apos;s address</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="950"/>
        <source>%1 removed your authorization to see their presence.</source>
        <comment>%1 is another user&apos;s address</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="990"/>
        <source>Received version information from %1:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1013"/>
        <source>Socket Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1018"/>
        <source>KeepAlive Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1022"/>
        <source>XMPP Stream Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1031"/>
        <source>Network error: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1043"/>
        <source>Chats</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1106"/>
        <source>AkariXB is a Jabber/XMPP bot.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1109"/>
        <source>You can talk to it, make it join chatrooms, etc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1113"/>
        <source>English translation by JanKusanagi.</source>
        <comment>TRANSLATORS: Change this with your language and name. If there was another translator before you, add your name after theirs ;)</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1120"/>
        <source>Thanks to all the testers, translators and packagers, who help make AkariXB better!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1125"/>
        <source>AkariXB is Free Software, licensed under the GNU GPL license, and uses some Oxygen icons under LGPL license.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1150"/>
        <source>Offline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1202"/>
        <source>Account is not configured</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1287"/>
        <source>&amp;Hide Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1294"/>
        <source>&amp;Show Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1311"/>
        <source>Quit?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1312"/>
        <source>Are you sure?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1314"/>
        <source>Yes, quit AkariXB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1314"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessageHandler</name>
    <message>
        <location filename="../src/messagehandler.cpp" line="87"/>
        <source>Headline from %1: %2</source>
        <comment>%1 is a user&apos;s address,%2 is the title of a message</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/messagehandler.cpp" line="103"/>
        <source>Error %1 from %2: %3</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RoomModule</name>
    <message>
        <location filename="../src/roommodule.cpp" line="81"/>
        <source>Add &amp;New Room</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="95"/>
        <source>&amp;Remove Room</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="62"/>
        <location filename="../src/roommodule.cpp" line="112"/>
        <source>Room address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="63"/>
        <location filename="../src/roommodule.cpp" line="115"/>
        <source>Nickname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="120"/>
        <source>Password, if needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="65"/>
        <location filename="../src/roommodule.cpp" line="125"/>
        <source>Autojoin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="141"/>
        <source>Join</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="149"/>
        <source>Leave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="131"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="160"/>
        <source>Speak casually</source>
        <comment>Casually as in, from time to time</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="182"/>
        <source>&amp;Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="206"/>
        <source>Select a room from the list above</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="269"/>
        <source>Room Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../src/roommodule.cpp" line="374"/>
        <source>Loaded %Ln room(s).</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/roommodule.cpp" line="426"/>
        <source>Saved %Ln room(s).</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="64"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="66"/>
        <source>Allowed</source>
        <comment>Column header for features allowed</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="157"/>
        <source>Allowed to:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="158"/>
        <source>Reply to commands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="159"/>
        <source>Greet people</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="163"/>
        <source>Converse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="191"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../src/roommodule.cpp" line="372"/>
        <location filename="../src/roommodule.cpp" line="423"/>
        <source>%Ln room(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="531"/>
        <source>Joining room %1...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="562"/>
        <source>Leaving room %1...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="610"/>
        <location filename="../src/roommodule.cpp" line="630"/>
        <location filename="../src/roommodule.cpp" line="647"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="611"/>
        <source>Room address is not valid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="632"/>
        <source>There is already a room in the list with this address, in row %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="648"/>
        <source>Nickname is not valid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="702"/>
        <source>New Room</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="849"/>
        <source>Remove room?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="850"/>
        <source>Do you want to remove room %1 from your list?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="854"/>
        <source>Yes, remove it</source>
        <comment>It = A chat room</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roommodule.cpp" line="855"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RoomSpeakModule</name>
    <message>
        <location filename="../src/roomspeakmodule.cpp" line="28"/>
        <source>Greetings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roomspeakmodule.cpp" line="29"/>
        <source>Casual speak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roomspeakmodule.cpp" line="30"/>
        <source>Reactions to words</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roomspeakmodule.cpp" line="35"/>
        <source>Sentences used to greet people who enter a chatroom.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roomspeakmodule.cpp" line="38"/>
        <source>The %1 variable can be used to insert their names in the sentences.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roomspeakmodule.cpp" line="52"/>
        <source>Sentences to be sent to chatrooms at random intervals.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roomspeakmodule.cpp" line="64"/>
        <source>Regular expressions to be matched against messages coming from the chatrooms, and possible messages to send in response.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roomspeakmodule.cpp" line="69"/>
        <source>Example:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/roomspeakmodule.cpp" line="73"/>
        <source>Documentation for Perl regular expressions</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RoomSpeakTab</name>
    <message>
        <location filename="../src/roomspeaktab.cpp" line="55"/>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../src/roomspeaktab.cpp" line="116"/>
        <source>Loaded %Ln sentence(s) of type %1.</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/roomspeaktab.cpp" line="174"/>
        <source>Saved %Ln sentence(s) of type %1.</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>StringListWidget</name>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="187"/>
        <source>Update the definition of the item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="659"/>
        <source>Enter text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="624"/>
        <source>Item text is not valid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="142"/>
        <source>Add an item to the list (you can also press the Insert key)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="660"/>
        <source>Enter a new line of text for the list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="514"/>
        <source>The item already exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="48"/>
        <source>String</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="52"/>
        <source>Regular Expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="56"/>
        <source>Tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="103"/>
        <source>Search...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="131"/>
        <source>Show All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="152"/>
        <source>Edit the selected item (you can also double-click it or press F2/Enter)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="162"/>
        <source>Remove the selected item (you can also press Shift+Delete)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="516"/>
        <source>Error: There is already an item in the list with the text %1, and this list does not allow duplicates.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="637"/>
        <source>Regular expression is not valid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="639"/>
        <source>Syntax error at position %1:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="706"/>
        <source>Modify text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/stringlistwidget.cpp" line="707"/>
        <source>Update the text for this item</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VariableParser</name>
    <message>
        <location filename="../src/variableparser.cpp" line="138"/>
        <source>Unbalanced braces: %1 opening and %2 closing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/variableparser.cpp" line="168"/>
        <source>This substitution is unknown, or not valid in this context: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/variableparser.cpp" line="186"/>
        <source>Syntax error:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
